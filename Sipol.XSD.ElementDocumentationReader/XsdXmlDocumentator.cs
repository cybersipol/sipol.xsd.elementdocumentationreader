﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Sipol.XML.Interfaces;
using Sipol.XSD.ElementDocumentationReader.Interfaces;

namespace Sipol.XSD.ElementDocumentationReader
{
    public class XsdXmlDocumentator : IXmlDocumentFactory
    {
        public IXSDElementDocumentationComposite XsdElementRoot { get; private set; } = null;

        public XsdXmlDocumentator(IXSDElementDocumentationComposite xsdElementRoot)
        {
            XsdElementRoot = xsdElementRoot ?? throw new ArgumentNullException(nameof(xsdElementRoot));
        }

        public XmlDocument CreateXmlDocument()
        {
            string xml = XsdElementRoot.ToXmlString();

            XmlDocument doc = new XmlDocument();

            if (!String.IsNullOrWhiteSpace(xml))
                doc.LoadXml(xml);

            return doc;
        }
    }
}
