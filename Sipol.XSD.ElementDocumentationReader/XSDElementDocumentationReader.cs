﻿using Sipol.XSD.ElementDocumentationReader.Interfaces;
using System;
using System.IO;
using System.Xml.Schema;

namespace Sipol.XSD.ElementDocumentationReader
{
    public class XSDElementDocumentationReader : IXSDElementDocumentationReader
    {
        public XSDElementDocumentationReader(Stream xsdStream, IXSDElementDocumentationCompositeFactory elementFactory)
        {
            XsdStream = xsdStream ?? throw new ArgumentNullException(nameof(xsdStream));
            ElementFactory = elementFactory ?? throw new ArgumentNullException(nameof(elementFactory));
        }

        public Stream XsdStream { get; protected set; } = null;
        public XmlSchema XsdSchema { get; protected set; } = null;

        public IXSDElementDocumentationComposite RootElement { get; protected set; } = null;
        public IXSDElementDocumentationCompositeFactory ElementFactory { get; protected set; } = null;

        public void ReadXSD()
        {
            ReadSchema();

            foreach (XmlSchemaElement element in XsdSchema.Elements.Values)
            {
                CreateRootElement(element);
                break;
            }
        }

        private void ReadSchema()
        {
            XsdStream.Seek(0, SeekOrigin.Begin);

            XmlSchemaSet schemaSet = new XmlSchemaSet();
            schemaSet.ValidationEventHandler += new ValidationEventHandler(ValidationCallback);
                        
            XsdSchema = XmlSchema.Read(XsdStream, new ValidationEventHandler(ValidationCallback));

            schemaSet.Add(XsdSchema);
            schemaSet.Compile();
        }


        private void CreateRootElement(XmlSchemaElement element)
        {
            RootElement = null;
            if (element == null) return;

            RootElement = ElementFactory.CreateElementDocumentationComposite(element, null);
            RootElement.ReadContent();
        }

        private void ValidationCallback(object sender, ValidationEventArgs e)
        {
            if (e.Severity == XmlSeverityType.Error)
                throw e.Exception;
            
        }


    }
}
