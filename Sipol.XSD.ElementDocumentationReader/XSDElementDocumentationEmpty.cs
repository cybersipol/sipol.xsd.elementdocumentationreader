﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Schema;
using Sipol.XSD.ElementDocumentationReader.Interfaces;

namespace Sipol.XSD.ElementDocumentationReader
{
    public class XSDElementDocumentationEmpty : IXSDElementDocumentationEmpty
    {
        private static IXSDElementDocumentationEmpty s_istance;

        public static IXSDElementDocumentationEmpty Instance => GetInstance();

        private XSDElementDocumentationEmpty()
        {
        }

        private XmlSchemaElement xmlSchemaElement = new XmlSchemaElement();

        public string Name => string.Empty;

        public string XPath => string.Empty;

        public int Level => -1;

        public XmlSchemaElement XsdElement => xmlSchemaElement;

        public IXSDElementDocumentationComposite Parent => null;

        public IList<IXSDElementDocumentationComposite> ChildElements => new List<IXSDElementDocumentationComposite>();

        public IXSDElementDocumentationCompositeFactory FromFactory { get => null; set { } }

        public string Documentation => string.Empty;

        public IXSDElementDocumentationComposite Current => this;

        object IEnumerator.Current => this;

        public string XsdType => string.Empty;

        public void AddChild(IXSDElementDocumentationComposite child)
        {

        }

        public void Dispose()
        {

        }

        public bool MoveNext()
        {
            return false;
        }

        public void ReadContent()
        {

        }

        public void Reset()
        {

        }

        private static IXSDElementDocumentationEmpty GetInstance()
        {
            if (s_istance == null) s_istance = new XSDElementDocumentationEmpty();

            return s_istance;
        }

        public IEnumerator<IXSDElementDocumentationComposite> GetEnumerator()
        {
            return this;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this;
        }

        public string ToXmlString()
        {
            return string.Empty;
        }
    }
}
