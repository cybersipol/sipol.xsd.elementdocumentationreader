﻿using Sipol.XSD.ElementDocumentationReader.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Sipol.XML;
using Sipol.XML.Interfaces;

namespace Sipol.XSD.ElementDocumentationReader
{
    public class XsdDocElementToXmlElementConnectorFactory : IXsdDocElementToXmlElementConnectorFactory
    {
        private IXSDElementDocumentationReader XsdDocReader = null;

        private bool IsXsdXPathListCreated = false;
        private SortedDictionary<string, IXSDElementDocumentationComposite> xsdXPathList = new SortedDictionary<string, IXSDElementDocumentationComposite>();

        public XsdDocElementToXmlElementConnectorFactory(IXSDElementDocumentationReader xsdDocReader)
        {
            XsdDocReader = xsdDocReader ?? throw new ArgumentNullException(nameof(xsdDocReader));
        }

        public IXsdDocElementToXmlElementConnector Create(XmlElement element)
        {
            if (element == null) throw new ArgumentNullException(nameof(element));

            CreateXsdList();

            XPathXmlNode xPathXmlNode = new XPathXmlNodeShort(element);
            string xpath = xPathXmlNode.XPath;

            if (!xsdXPathList.ContainsKey(xpath)) return XsdDocElementToXmlElementEmptyConnector.Instance;

            return new XsdDocElementToXmlElementConnector(xsdXPathList[xpath], element);
        }

        #region Help funcs
        private void CreateXsdList()
        {
            if (IsXsdXPathListCreated) return;
            XsdDocReader.ReadXSD();

            IXSDElementDocumentationComposite root = XsdDocReader.RootElement;
            root.Reset();
            do
            {
                IXSDElementDocumentationComposite current = root.Current;

                if (!String.IsNullOrEmpty(current.Documentation))
                {
                    string xsdXpath = current.XPath;
                    xsdXPathList.Add(xsdXpath, current);
                }
            }
            while (root.MoveNext());

            IsXsdXPathListCreated = true;
        }

        #endregion
    }
}