﻿using Sipol.XSD.ElementDocumentationReader.Interfaces;
using System;
using System.Collections.Generic;

namespace Sipol.XSD.ElementDocumentationReader
{
    public class XPathXsdDocElementMapper : IXPathXsdDocElementMapper
    {
        private bool isMapCreated = false;
        private IXSDElementDocumentationReader XsdDocReader = null;
        private SortedDictionary<string, IXSDElementDocumentationComposite> xsdXPathList = new SortedDictionary<string, IXSDElementDocumentationComposite>();

        public int Count => xsdXPathList.Count;

        public XPathXsdDocElementMapper(IXSDElementDocumentationReader xsdDocReader)
        {
            XsdDocReader = xsdDocReader ?? throw new ArgumentNullException(nameof(xsdDocReader));
        }

        public void CreateMap()
        {
            if (isMapCreated) return;   
            XsdDocReader.ReadXSD();

            IXSDElementDocumentationComposite root = XsdDocReader.RootElement;
            root.Reset();
            do
            {
                IXSDElementDocumentationComposite current = root.Current;

                if (!String.IsNullOrEmpty(current.Documentation))
                {
                    string xsdXpath = current.XPath;
                    xsdXPathList.Add(xsdXpath, current);
                }

               
            }
            while (root.MoveNext());

            isMapCreated = true;
        }

        public IXSDElementDocumentationComposite Get(string XPath)
        {
            if (!isMapCreated) throw new Exception("Map not created");
            return (xsdXPathList.ContainsKey(XPath) ? xsdXPathList[XPath] : XSDElementDocumentationEmpty.Instance);
        }

    }
}
