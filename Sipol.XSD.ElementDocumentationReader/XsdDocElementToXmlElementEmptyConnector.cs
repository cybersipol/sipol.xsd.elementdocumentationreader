﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Sipol.XSD.ElementDocumentationReader.Interfaces;

namespace Sipol.XSD.ElementDocumentationReader
{
    public class XsdDocElementToXmlElementEmptyConnector : IXsdDocElementToXmlElementConnector
    {
        private static IXsdDocElementToXmlElementConnector s_instance = null;
        public static IXsdDocElementToXmlElementConnector Instance { get => GetInstance(); }

        public XmlElement xmlElement { get => null; }
        public IXSDElementDocumentationComposite xsdDocElement { get => null; }

        private XsdDocElementToXmlElementEmptyConnector() { }

        private static IXsdDocElementToXmlElementConnector GetInstance()
        {
            if (s_instance == null) s_instance = new XsdDocElementToXmlElementEmptyConnector();

            return s_instance;
        }


    }
}