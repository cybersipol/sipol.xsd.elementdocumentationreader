﻿using Sipol.XSD.ElementDocumentationReader.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Schema;

namespace Sipol.XSD.ElementDocumentationReader
{
    public class XSDElementDocumentationComposite : IXSDElementDocumentationComposite
    {
        #region static vars
        public static int MAX_LEVEL = 10;

        public static readonly string DOC_ATTR_NAME = "xsdDoc";
        public static readonly string XPATH_ATTR_NAME = "xsdXPath";
        public static readonly string TYPE_ATTR_NAME = "xsdType";

        #endregion

        #region private vars
        private IList<IXSDElementDocumentationComposite> _childElements = null;
        private int _currentIndex = -1;

        #endregion

        #region public vars

        public string Name => XsdElement?.Name;
        public string XPath => getXPath();

        private XmlSchemaElement xsdElement = null;

        public XmlSchemaElement XsdElement => xsdElement;

        private void SetXsdElement(XmlSchemaElement value)
        {
            xsdElement = value;
        }

        public IXSDElementDocumentationComposite Parent { get; private set; } = null;
        public IList<IXSDElementDocumentationComposite> ChildElements
        {
            get => ((List<IXSDElementDocumentationComposite>)_childElements).AsReadOnly();
            private set => _childElements = value;
        }

        public int Level => getLevel();

        public string Documentation { get; private set; }
        public string XsdType { get; private set; }


        public IXSDElementDocumentationCompositeFactory FromFactory { get; set; } = null;

        public IXSDElementDocumentationComposite Current => GetCurrent();
        object IEnumerator.Current => GetCurrent();

        public IEnumerator<IXSDElementDocumentationComposite> Iterator { get; set; } = null;

        public static IXSDElementDocumentationComposite Empty => XSDElementDocumentationEmpty.Instance;

        #endregion

        public XSDElementDocumentationComposite(XmlSchemaElement xmlSchemaElement, IXSDElementDocumentationComposite parent = null)
        {
            SetXsdElement(xmlSchemaElement ?? throw new ArgumentNullException(nameof(xmlSchemaElement)));
            Parent = parent;
            _childElements = new List<IXSDElementDocumentationComposite>();
            Documentation = String.Empty;
            XsdType = String.Empty;
            Iterator = this;

            AddThisToParent();
        }

        #region Func implemenetation of interface

        public void AddChild(IXSDElementDocumentationComposite child)
        {
            if (child == null) throw new ArgumentNullException(nameof(child));
            if (child == this) return;
            if (_childElements.IndexOf(child) >= 0) return;
            
            _childElements.Add(child);
        }


        public void ReadContent()
        {
            if (Level > MAX_LEVEL) return;

            ReadDocumentation();

            ReadXsdType();

            ReadComplexContentElement();
        }


        public bool MoveNext()
        {

            if (_currentIndex >= 0 && _currentIndex < _childElements.Count)
            {
                IXSDElementDocumentationComposite child = _childElements[_currentIndex];
                if (!child.MoveNext()) _currentIndex++;
            }

            if (_currentIndex < 0) _currentIndex = 0;
            return (_currentIndex < _childElements.Count);
        }

        public void Reset()
        {
            foreach (IXSDElementDocumentationComposite child in _childElements)
                child.Reset();

            _currentIndex = -1;
        }

        public void Dispose()
        {

        }

        public IXSDElementDocumentationComposite GetCurrent()
        {
            if (_currentIndex < 0) return this;
            return (_currentIndex >= 0 && _currentIndex < _childElements.Count) ? _childElements[_currentIndex].Current : null;
        }


        public IEnumerator<IXSDElementDocumentationComposite> GetEnumerator()
        {
            return Iterator ?? this;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Iterator ?? this;
        }


        public string ToXmlString()
        {
            StringBuilder sb = new StringBuilder("");
            sb.Append("<" + Name );
            sb.Append(" " + TYPE_ATTR_NAME + "=\"" + (XsdType ?? String.Empty).Replace("\"", "'") + "\"");
            sb.Append(" "+ XPATH_ATTR_NAME + "=\"" + XPath.Replace("\"", "'") + "\"");
            sb.Append(" "+ DOC_ATTR_NAME + "=\"" + Documentation.Replace("\"", "'") + "\"");

            if (ChildElements != null && ChildElements.Count > 0)
            {
                sb.Append(">");

                foreach (IXSDElementDocumentationComposite child in ChildElements)
                    sb.Append(child.ToXmlString());

                sb.Append("</" + Name + ">");
            }
            else sb.Append("/>");

            return sb.ToString();
        }



        #endregion

        #region Help functions
        private void AddThisToParent()
        {
            if (Parent == null) return;
            if (Parent.ChildElements.IndexOf(this) >= 0) return;
            Parent.AddChild(this);
        }

        private void ReadComplexContentElement()
        {
            if (XsdElement.ElementSchemaType is XmlSchemaComplexType)
            {
                // Get the complex type of the Customer element.
                XmlSchemaComplexType complexType = XsdElement.ElementSchemaType as XmlSchemaComplexType;

                if (complexType.ContentTypeParticle is XmlSchemaGroupBase)
                {
                    XmlSchemaGroupBase sequence = complexType.ContentTypeParticle as XmlSchemaGroupBase;
                    ReadGroupContent(sequence);
                }

            }
        }


        private void ReadSubElement(XmlSchemaObject childElement)
        {
            if (childElement is XmlSchemaElement)
                CreateChildElement((XmlSchemaElement)childElement);

            if (childElement is XmlSchemaGroupBase)
                ReadGroupContent((XmlSchemaGroupBase)childElement);

        }

        private void CreateChildElement(XmlSchemaElement element)
        {
            IXSDElementDocumentationComposite childrenDocElement = (FromFactory == null ? new XSDElementDocumentationComposite(element, this) : FromFactory.CreateElementDocumentationComposite(element, this));
            childrenDocElement.ReadContent();
        }

        private void ReadGroupContent(XmlSchemaGroupBase groupElement)
        {
            foreach (XmlSchemaObject xmlSchemaObject in groupElement.Items)
            {
                ReadSubElement(xmlSchemaObject);
            }

        }

        private int getLevel()
        {
            IXSDElementDocumentationComposite currentParent = this;
            int level = 0;
            while (currentParent.Parent != null)
            {
                currentParent = currentParent.Parent;
                level++;
            }
            return level;
        }

        private string getXPath()
        {
            IXSDElementDocumentationComposite current = this;
            List<string> tags = new List<string>();
            while (current != null)
            {
                tags.Add(current.Name);
                current = current.Parent;
            }
            tags.Reverse();
            StringBuilder xpathSb = new StringBuilder("");
            foreach (string tag in tags)
            {
                xpathSb.Append("/" + (tag ?? String.Empty));
                if (String.IsNullOrEmpty(tag)) throw new NullReferenceException(String.Format("Element XSD in /{0} is null or empty", xpathSb.ToString().TrimStart('/')));
            }

            return xpathSb.ToString();
        }

        private void ReadDocumentation()
        {
            Documentation = String.Empty;
            if (XsdElement.Annotation != null && XsdElement.Annotation.Items.Count > 0)
            {
                StringBuilder sb = new StringBuilder(String.Empty);

                foreach (XmlSchemaObject childElement in XsdElement.Annotation.Items)
                {
                    if (childElement is XmlSchemaDocumentation)
                    {
                        XmlSchemaDocumentation doc = (XmlSchemaDocumentation)childElement;

                        if (doc.Markup != null && doc.Markup.Length > 0)
                        {
                            foreach (XmlNode docNode in doc.Markup)
                            {
                                sb.AppendLine(docNode.InnerText.Trim());
                            }
                        }
                    }
                }
                Documentation = sb.ToString().Trim().Trim(Environment.NewLine.ToCharArray());
            }

        }

        private void ReadXsdType()
        {
            XsdType = String.Empty;
            string typeElement = (XsdElement?.SchemaTypeName?.Name ?? String.Empty).Trim();
            if (!String.IsNullOrWhiteSpace(typeElement))
            {
                XsdType = typeElement;
                return;
            }

            if (XsdElement.ElementSchemaType is XmlSchemaComplexType)
            {
                XsdType = "complex";
                return;
            }

            if (XsdElement.ElementSchemaType is XmlSchemaSimpleType)
            {
                XsdType = "simple";
                return;
            }
        }



        #endregion
    }
}
