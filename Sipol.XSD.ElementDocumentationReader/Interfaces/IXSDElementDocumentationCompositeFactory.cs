﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Schema;
namespace Sipol.XSD.ElementDocumentationReader.Interfaces
{
    public interface IXSDElementDocumentationCompositeFactory
    {
        IXSDElementDocumentationComposite CreateElementDocumentationComposite(XmlSchemaElement xmlSchemaElement, IXSDElementDocumentationComposite parent);
    }
}