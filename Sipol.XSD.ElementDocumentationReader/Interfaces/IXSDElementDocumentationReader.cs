﻿namespace Sipol.XSD.ElementDocumentationReader.Interfaces
{
    public interface IXSDElementDocumentationReader
    {
        IXSDElementDocumentationCompositeFactory ElementFactory { get; }
        IXSDElementDocumentationComposite RootElement { get; }

        void ReadXSD();
    }
}
