﻿using System.Xml;
using Sipol.XSD.ElementDocumentationReader.Interfaces;

namespace Sipol.XSD.ElementDocumentationReader.Interfaces
{
    public interface IXsdDocElementToXmlElementConnectorFactory
    {
        IXsdDocElementToXmlElementConnector Create(XmlElement element);
    }
}