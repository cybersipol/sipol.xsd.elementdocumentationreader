﻿using Sipol.XML.Interfaces;

namespace Sipol.XSD.ElementDocumentationReader.Interfaces
{
    public interface IXPathXsdDocElementMapper: IXPathMapper<IXSDElementDocumentationComposite>
    {
    }
}