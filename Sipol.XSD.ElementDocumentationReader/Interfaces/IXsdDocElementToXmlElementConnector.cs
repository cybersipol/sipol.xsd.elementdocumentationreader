﻿using System.Xml;
using Sipol.XSD.ElementDocumentationReader.Interfaces;

namespace Sipol.XSD.ElementDocumentationReader.Interfaces
{
    public interface IXsdDocElementToXmlElementConnector
    {
        XmlElement xmlElement { get; }
        IXSDElementDocumentationComposite xsdDocElement { get; }
    }
}