﻿using Sipol.XML.Interfaces;
using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;

namespace Sipol.XSD.ElementDocumentationReader.Interfaces
{
    public interface IXSDElementDocumentationComposite: IEnumerator<IXSDElementDocumentationComposite>, IEnumerable<IXSDElementDocumentationComposite>, IToXmlString
    {
        string Name { get; }
        string XPath { get; }
        int Level { get; }

        XmlSchemaElement XsdElement { get; }
        IXSDElementDocumentationComposite Parent { get; }
        IList<IXSDElementDocumentationComposite> ChildElements { get; }

        IXSDElementDocumentationCompositeFactory FromFactory { get; set; }

        string Documentation { get; }
        string XsdType { get; }

        void AddChild(IXSDElementDocumentationComposite child);
        void ReadContent();


        
    }

}
