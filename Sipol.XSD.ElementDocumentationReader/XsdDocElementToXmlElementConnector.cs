﻿using Sipol.XSD.ElementDocumentationReader.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Sipol.XSD.ElementDocumentationReader
{
    public class XsdDocElementToXmlElementConnector : IXsdDocElementToXmlElementConnector
    {
        public XsdDocElementToXmlElementConnector(IXSDElementDocumentationComposite xsdDocElement, XmlElement xmlElement)
        {
            this.xmlElement = xmlElement ?? throw new ArgumentNullException(nameof(xmlElement));
            this.xsdDocElement = xsdDocElement;
        }

        public IXSDElementDocumentationComposite xsdDocElement { get; set; }
        public XmlElement xmlElement { get; set; }
    }
}
