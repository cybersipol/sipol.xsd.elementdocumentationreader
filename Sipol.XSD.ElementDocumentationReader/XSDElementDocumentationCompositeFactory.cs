﻿using Sipol.XSD.ElementDocumentationReader.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Schema;

namespace Sipol.XSD.ElementDocumentationReader
{
    public class XSDElementDocumentationCompositeFactory : IXSDElementDocumentationCompositeFactory
    {
        public IXSDElementDocumentationComposite CreateElementDocumentationComposite(XmlSchemaElement xmlSchemaElement, IXSDElementDocumentationComposite parent = null)
        {
            XSDElementDocumentationComposite elem = new XSDElementDocumentationComposite(xmlSchemaElement, parent);
            elem.FromFactory = this;
            return elem;
        }
    }
}