﻿using NUnit.Framework;
using Sipol.XSD.ElementDocumentationReader;
using Sipol.XSD.ElementDocumentationReader.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;

namespace Sipol.XSD.ElementDocumentationReader.Tests
{
    [TestFixture()]
    public class XPathXsdDocElementMapper_Tests
    {
        private XPathXsdDocElementMapper sut = null;

        private IXSDElementDocumentationReader reader = null;
        private Stream stream = null;
        private IXSDElementDocumentationCompositeFactory xsdDocElemFactory = null;

        [Test()]
        public void ctor()
        {
            object result = CreateSUT();

            Assert.That(result, Is.Not.Null & Is.InstanceOf<XPathXsdDocElementMapper>());
        }

        [Test()]
        public void ctor_NullReader_ThrowException()
        {
            object result = null;
            Assert.That(() => { result = CreateSUT(null); }, Throws.InstanceOf<ArgumentNullException>());
        }

        [Test()]
        public void CreateMap_ReadXSD_IsReaderReadXsdCall()
        {
            IXSDElementDocumentationReader rdr = Substitute.For<IXSDElementDocumentationReader>();
            sut = CreateSUT(rdr);

            sut.CreateMap();

            rdr.Received().ReadXSD();
        }

        [Test()]
        public void CreateMap_byDefault_CountGreaterThanZero()
        {
            sut = CreateSUT();

            sut.CreateMap();

            Assert.That(sut.Count, Is.GreaterThan(0));
        }

        [Test()]
        public void Get_byDefault_XsdDocElementWithDoc()
        {
            sut = CreateSUT();
            sut.CreateMap();

            IXSDElementDocumentationComposite result = sut.Get("/shiporder/item/contenerS");

            Assert.That(result, 
                            Is.Not.Null 
                            & Is.InstanceOf<IXSDElementDocumentationComposite>() 
                            & Has.Property("Documentation").Not.Null 
                            & Has.Property("Documentation").Not.Empty
                       );

        }

        [Test()]
        public void Get_NoCreateMap_ThrowExcpetion()
        {
            sut = CreateSUT();

            Assert.That(() => {
                IXSDElementDocumentationComposite result = sut.Get("/shiporder/item/contenerS");
                },
                Throws.InstanceOf<Exception>().With.Message.EqualTo("Map not created")
            );

        }

        [Test()]
        public void Get_NonExistsXpath_EmptyXsdDocElement()
        {
            sut = CreateSUT();
            sut.CreateMap();

            IXSDElementDocumentationComposite xsdDocElem = sut.Get("/abra/kana/bra");

            Assert.That(xsdDocElem, Is.Not.Null & Is.InstanceOf<IXSDElementDocumentationEmpty>());
        }

        #region Help funcs

        private XPathXsdDocElementMapper CreateSUT(IXSDElementDocumentationReader reader)
        {
            sut = new XPathXsdDocElementMapper(reader);
            return sut;
        }

        private XPathXsdDocElementMapper CreateSUT()
        {
            stream = XSDElementDocumentationReader_Tests.String2MemoryStream(XSDElementDocumentationReader_Tests.getXSDSource_SimpleXSD());
            xsdDocElemFactory = new XSDElementDocumentationCompositeFactory();
            reader = new XSDElementDocumentationReader(stream, xsdDocElemFactory);

            return CreateSUT(reader);
        }

        #endregion

    }
}