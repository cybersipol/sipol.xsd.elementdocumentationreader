﻿using NSubstitute;
using NUnit.Framework;
using Sipol.XSD.ElementDocumentationReader.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Sipol.XSD.ElementDocumentationReader.Tests
{
    [TestFixture()]
    public class XSDElementDocumentationReader_Tests
    {
        private XSDElementDocumentationReader sut = null;
        private MemoryStream stream = null;
        private IXSDElementDocumentationCompositeFactory factory = null;

        public static string xsdSimpleFirstElementName = "testElement";

        [SetUp]
        public void Init()
        {
            XSDElementDocumentationComposite.MAX_LEVEL = 3;
        }

        #region ctor tests
        [Test()]
        public void ctor()
        {
            CreateTestStream();
            CreateDefaultFactory();
            sut = CreateSUT();
            Assert.IsNotNull(sut);
            Assert.IsInstanceOf<XSDElementDocumentationReader>(sut);
        }

        [Test()]
        public void ctor_StreamNull_ThrowException()
        {
            CreateDefaultFactory();
            sut = null;
            Assert.Throws<ArgumentNullException>(() =>
            {
                sut = CreateSUT((Stream)null);
            }
            );

            Assert.IsNull(sut);
        }

        [Test()] 
        public void ctor_ElementFactoryNull_ThrowExcpetion()
        {            
            sut = null;
            CreateTestStream();
            Assert.Throws<ArgumentNullException>(() =>
            {
                sut = CreateSUT(stream, null);
            }
            );

            Assert.IsNull(sut);

        }

        #endregion

        #region ReadXSD Tests

        #region Validate XSD
        [Test()]
        public void ReadXSD_ValidateWrongSchemaXML_ThrowException()
        {
            CreateTestStreamFromString(getXSDSource_WrongXmlInXsdSchema());
            CreateDefaultFactory();
            sut = CreateSUT();

            Assert.Throws<System.Xml.XmlException>(() =>
            {
                sut.ReadXSD();
            }
            );
        }

        [Test()]
        public void ReadXSD_ValidateWrongSchemaXSD_ThrowException()
        {
            CreateTestStreamFromString(getXSDSource_WrongXsdSchema());
            CreateDefaultFactory();
            sut = CreateSUT();

            Assert.Throws<System.Xml.Schema.XmlSchemaException>(() => 
            { 
                sut.ReadXSD();
            }
            );

        }

        #endregion

        [Test()]
        public void ReadXSD_CheckCreateRootElement_RootElementAsComposite()
        {
            CreateTestStream();
            CreateDefaultFactory();
            sut = CreateSUT();

            sut.ReadXSD();

            Assert.That(sut.RootElement, Is.Not.Null & Is.InstanceOf<IXSDElementDocumentationComposite>());

            Assert.That(sut.RootElement.Name, Is.Not.Null & Is.Not.Empty, "Name is Null or Empty");
            Assert.That(sut.RootElement.XPath, Is.Not.Null & Is.Not.Empty, "XPath is NULL or Empty");

        }

        [Test()]
        public void ReadXSD_byDefault_RootElementHasCompositeStructure()
        {
            CreateTestStreamFromString(getXSDSource_SimpleXSD());
            CreateDefaultFactory();
            sut = CreateSUT();

            sut.ReadXSD();

            Assert.That(sut.RootElement, Is.Not.Null & Is.InstanceOf<IXSDElementDocumentationComposite>());

            Assert.That(sut.RootElement.ChildElements, Is.Not.Null & Is.InstanceOf<IList<IXSDElementDocumentationComposite>>());
            Assert.That(sut.RootElement.ChildElements.Count,  Is.GreaterThan(0) );

            Assert.That(sut.RootElement.ChildElements[0], Is.Not.Null & Is.InstanceOf<IXSDElementDocumentationComposite>());
            Assert.That(sut.RootElement.ChildElements[0].ChildElements, Is.Not.Null & Is.InstanceOf<IList<IXSDElementDocumentationComposite>>());
            Assert.That(sut.RootElement.ChildElements[0].ChildElements.Count, Is.GreaterThan(0) );

            Assert.That(sut.RootElement.ChildElements[0].Parent, Is.Not.Null & Is.InstanceOf<IXSDElementDocumentationComposite>() & Is.SameAs(sut.RootElement) );
        }

        [Test()]
        public void ReadXSD_byDefault_CompositeStructureHasNoRepeat()
        {
            CreateTestStreamFromString(getXSDSource_SimpleXSD());
            CreateDefaultFactory();
            sut = CreateSUT();

            sut.ReadXSD();

            Assert.That(sut.RootElement, Is.Not.Null & Is.InstanceOf<IXSDElementDocumentationComposite>());

            Assert.That(sut.RootElement.ChildElements, Is.Not.Null & Is.InstanceOf<IList<IXSDElementDocumentationComposite>>());
            Assert.That(sut.RootElement.ChildElements.Count, Is.GreaterThan(0));

            IXSDElementDocumentationComposite prevItem = null;
            foreach (IXSDElementDocumentationComposite item in sut.RootElement.ChildElements)
            {
                Assert.That(item, Is.Not.SameAs(prevItem));
                prevItem = item;
            }
        }

        #endregion

        #region ElementFactory Tests

        [Test()]
        public void ElementFactory_CreateWithReceivedBuildFactoryMethod_RecetiveBulidMetodOfFactory()
        {
            CreateTestStream();
            CreateMockFactory();
            CreateSUT();

            sut.ReadXSD();

            factory.ReceivedWithAnyArgs().CreateElementDocumentationComposite(null, null);
        }


        [Test()]
        public void ElementFactory_ValidateWrongSchemaXML_DidNotBuildElementWithFactory()
        {
            CreateTestStreamFromString(getXSDSource_WrongXmlInXsdSchema());
            CreateMockFactory();
            sut = CreateSUT();

            try
            {
                sut.ReadXSD();
            }
            catch (Exception ex)
            {

            }

            factory.DidNotReceiveWithAnyArgs().CreateElementDocumentationComposite(null, null);
        }

        [Test()]
        public void ElementFactory_ValidateWrongSchemaXSD_DidNotBuildElementWithFactory()
        {
            CreateTestStreamFromString(getXSDSource_WrongXsdSchema());
            CreateMockFactory();
            sut = CreateSUT();

            try
            {
                sut.ReadXSD();
            }
            catch (Exception ex)
            {
                
                
            }

            factory.DidNotReceiveWithAnyArgs().CreateElementDocumentationComposite(null, null);
        }


        #endregion

        #region Static funcs

        internal static byte[] Encoding(String str, Encoding enc)
        {
            return enc.GetBytes(str);
        }

        internal static byte[] Encoding(String str)
        {
            return Encoding(str, System.Text.Encoding.UTF8);
        }

        public static string getXSDSource_JednostkaMalaWZlotych()
        {
            return Sipol.XSD.ElementDocumentationReader.Unit.Tests.Automation_Tests.Tests_ReadXSDDocumentation.getXSDSource_JednostkaMalaWZlotych();
        }

        public static string getXSDSource_WrongXmlInXsdSchema()
        {
            return @"<xsd:schema xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:etd=""http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2016/01/25/eD/DefinicjeTypy/"" xmlns:dtsf=""http://www.mf.gov.pl/schematy/SF/DefinicjeTypySprawozdaniaFinansowe/2018/07/09/DefinicjeTypySprawozdaniaFinansowe/"" xmlns:jma=""http://www.mf.gov.pl/schematy/SF/DefinicjeTypySprawozdaniaFinansowe/2018/07/09/JednostkaMalaStruktury"" xmlns:jin=""http://www.mf.gov.pl/schematy/SF/DefinicjeTypySprawozdaniaFinansowe/2018/07/09/JednostkaInnaStruktury"" xmlns:tns=""http://www.mf.gov.pl/schematy/SF/DefinicjeTypySprawozdaniaFinansowe/2018/07/09/JednostkaMalaWZlotych"" targetNamespace=""http://www.mf.gov.pl/schematy/SF/DefinicjeTypySprawozdaniaFinansowe/2018/07/09/JednostkaMalaWZlotych"" elementFormDefault=""qualified"" attributeFormDefault=""unqualified"" version=""1.0"" xml:lang=""pl"">
	<xsd:import namespace=""http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2016/01/25/eD/DefinicjeTypy/"" schemaLocation=""http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2016/01/25/eD/DefinicjeTypy/StrukturyDanych_v4-0E.xsd""/>
	<xsd:import namespace=""http://www.mf.gov.pl/schematy/SF/DefinicjeTypySprawozdaniaFinansowe/2018/07/09/DefinicjeTypySprawozdaniaFinansowe/"" schemaLocation=""https://www.mf.gov.pl/documents/764034/6464789/StrukturyDanychSprFin_v1-0.xsd""/>
	<xsd:import namespace=""http://www.mf.gov.pl/schematy/SF/DefinicjeTypySprawozdaniaFinansowe/2018/07/09/JednostkaMalaStruktury"" schemaLocation=""https://www.mf.gov.pl/documents/764034/6464789/JednostkaMalaStrukturyDanychSprFin_v1-0.xsd""/>
	<xsd:import namespace=""http://www.mf.gov.pl/schematy/SF/DefinicjeTypySprawozdaniaFinansowe/2018/07/09/JednostkaInnaStruktury"" schemaLocation=""https://www.mf.gov.pl/documents/764034/6464789/JednostkaInnaStrukturyDanychSprFin_v1-0.xsd""/>
	<xsd:element name=""JednostkaMala"">
		<xsd:annotation>
			<xsd:documentation>";
        }

        public static string getXSDSource_WrongXsdSchema()
        {
            return @"<xsd:schema xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:etd=""http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2016/01/25/eD/DefinicjeTypy/"" xmlns:dtsf=""http://www.mf.gov.pl/schematy/SF/DefinicjeTypySprawozdaniaFinansowe/2018/07/09/DefinicjeTypySprawozdaniaFinansowe/"" xmlns:jma=""http://www.mf.gov.pl/schematy/SF/DefinicjeTypySprawozdaniaFinansowe/2018/07/09/JednostkaMalaStruktury"" xmlns:jin=""http://www.mf.gov.pl/schematy/SF/DefinicjeTypySprawozdaniaFinansowe/2018/07/09/JednostkaInnaStruktury"" xmlns:tns=""http://www.mf.gov.pl/schematy/SF/DefinicjeTypySprawozdaniaFinansowe/2018/07/09/JednostkaMalaWZlotych"" targetNamespace=""http://www.mf.gov.pl/schematy/SF/DefinicjeTypySprawozdaniaFinansowe/2018/07/09/JednostkaMalaWZlotych"" elementFormDefault=""qualified"" attributeFormDefault=""unqualified"" version=""1.0"" xml:lang=""pl"">
	<xsd:import namespace=""http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2016/01/25/eD/DefinicjeTypy/"" schemaLocation=""http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2016/01/25/eD/DefinicjeTypy/StrukturyDanych_v4-0E.xsd""/>
	<xsd:import namespace=""http://www.mf.gov.pl/schematy/SF/DefinicjeTypySprawozdaniaFinansowe/2018/07/09/DefinicjeTypySprawozdaniaFinansowe/"" schemaLocation=""https://www.mf.gov.pl/documents/764034/6464789/StrukturyDanychSprFin_v1-0.xsd""/>
	<xsd:import namespace=""http://www.mf.gov.pl/schematy/SF/DefinicjeTypySprawozdaniaFinansowe/2018/07/09/JednostkaMalaStruktury"" schemaLocation=""https://www.mf.gov.pl/documents/764034/6464789/JednostkaMalaStrukturyDanychSprFin_v1-0.xsd""/>
	<xsd:import namespace=""http://www.mf.gov.pl/schematy/SF/DefinicjeTypySprawozdaniaFinansowe/2018/07/09/JednostkaInnaStruktury"" schemaLocation=""https://www.mf.gov.pl/documents/764034/6464789/JednostkaInnaStrukturyDanychSprFin_v1-0.xsd""/>
	<xsd:element name=""JednostkaMala"">
        <xsd:dupa />    
    </xsd:element>
</xsd:schema>";
        }


        public static string getXSDSource_SimpleXSD()
        {
            return @"<?xml version=""1.0"" encoding=""UTF-8"" ?>
<xs:schema xmlns:xs=""http://www.w3.org/2001/XMLSchema"">

<xs:element name=""shiporder"">
		<xs:annotation>
			<xs:documentation>Root documentation</xs:documentation>
		</xs:annotation>
  <xs:complexType>
    <xs:sequence>
      <xs:element name=""firstElement"">
		<xs:annotation>
			<xs:documentation>firstElement documentation</xs:documentation>
		</xs:annotation>
        <xs:complexType>
          <xs:sequence>
            <xs:element name=""name"" type=""xs:string""/>
            <xs:element name=""address"" type=""xs:string""/>
            <xs:element name=""city"" type=""xs:string""/>
            <xs:element name=""country"" type=""xs:string""/>
          </xs:sequence>
        </xs:complexType>
      </xs:element>
      <xs:element name=""orderperson"" type=""xs:string""/>
      <xs:element name=""item"" maxOccurs=""unbounded"">
		<xs:annotation>
			<xs:documentation>item documentation</xs:documentation>
		</xs:annotation>
        <xs:complexType>
          <xs:sequence>
            <xs:element name=""title"" type=""xs:string""/>
            <xs:element name=""note"" type=""xs:string"" minOccurs=""0""/>
            <xs:element name=""quantity"" type=""xs:positiveInteger""/>
            <xs:element name=""price"" type=""xs:decimal""/>
            <xs:choice>
                <xs:element name=""contenerS"" type=""xs:decimal"">
		            <xs:annotation>
			            <xs:documentation>contenerS documentation</xs:documentation>
		            </xs:annotation>
                </xs:element>
                <xs:element name=""contenerM"" type=""xs:decimal""/>
                <xs:element name=""contenerL"" type=""xs:decimal""/>
                <xs:element name=""contenerXL"" type=""xs:decimal""/>
            </xs:choice>
          </xs:sequence>
        </xs:complexType>
      </xs:element>
    </xs:sequence>
    <xs:attribute name=""orderid"" type=""xs:string"" use=""required""/>
  </xs:complexType>
</xs:element>

</xs:schema>".Replace("firstElement", xsdSimpleFirstElementName);
        }

        #endregion

        #region Help funcs

        private XSDElementDocumentationReader CreateSUT()
        {
            return CreateSUT(stream, factory);
        }

        private XSDElementDocumentationReader CreateSUT(Stream _stream)
        {
            sut = new XSDElementDocumentationReader(_stream, factory);
            return sut;
        }

        private XSDElementDocumentationReader CreateSUT(Stream _stream, IXSDElementDocumentationCompositeFactory elementFactory)
        {
            sut = new XSDElementDocumentationReader(_stream, elementFactory);
            return sut;
        }

        private Stream CreateTestStreamFromString(String txt, Encoding enc)
        {
            stream = String2MemoryStream(txt, enc);
            return stream;
        }

        private Stream CreateTestStreamFromString(String txt) => CreateTestStreamFromString(txt, System.Text.Encoding.UTF8);

        private Stream CreateTestStream() => CreateTestStreamFromString(getXSDSource_SimpleXSD());

        private IXSDElementDocumentationCompositeFactory CreateMockFactory()
        {
            factory = Substitute.For<IXSDElementDocumentationCompositeFactory>();
            return factory;
        }

        private IXSDElementDocumentationCompositeFactory CreateDefaultFactory()
        {
            factory = new XSDElementDocumentationCompositeFactory();
            return factory;
        }

        internal static MemoryStream String2MemoryStream(String txt, Encoding enc)
        {
            return new MemoryStream(Encoding(txt, enc));
        }

        internal static MemoryStream String2MemoryStream(String txt)
        {
            return String2MemoryStream(txt, System.Text.Encoding.UTF8);
        }

        #endregion

    }
}