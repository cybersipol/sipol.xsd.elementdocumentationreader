﻿using NSubstitute;
using NUnit.Framework;
using Sipol.XSD.ElementDocumentationReader.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Schema;


namespace Sipol.XSD.ElementDocumentationReader.Tests
{
    [TestFixture()]
    public class XSDElementDocumentationComposite_Tests
    {
        private XSDElementDocumentationComposite sut = null;
        private XSDElementDocumentationComposite parent = null;
        private XmlSchemaElement parentElement = null;
        private XmlSchemaElement element = null;

        private IXSDElementDocumentationCompositeFactory factory = null;

        XSDElementDocumentationReader reader = null;

        [SetUp]
        public void Init()
        {
            XSDElementDocumentationComposite.MAX_LEVEL = 3;
        }

        #region Tests

        #region ctor Tests

        [Test()]
        public void ctor()
        {
            element = new XmlSchemaElement();
            element.Name = "test";

            object result = new XSDElementDocumentationComposite(element, null);
            Assert.That(result, Is.Not.Null & Is.InstanceOf<XSDElementDocumentationComposite>());

        }


        [Test()]
        public void ctor_forNotRootElement_ParentHasElement()
        {
            element = new XmlSchemaElement();
            element.Name = "test";

            parent = CreateDefaultParent();

            object result = new XSDElementDocumentationComposite(element, parent);
            Assert.That(result, Is.Not.Null & Is.InstanceOf<XSDElementDocumentationComposite>());

            XSDElementDocumentationComposite resultXsdElem = (XSDElementDocumentationComposite)result;
            Assert.That(resultXsdElem.Parent, Is.Not.Null & Is.InstanceOf<XSDElementDocumentationComposite>() & Has.Property("Name").EqualTo("root"));
            Assert.That(resultXsdElem.Parent.ChildElements, Is.Not.Null & Has.Property("Count").GreaterThan(0));
            Assert.That(resultXsdElem.Parent.ChildElements[0], Is.Not.Null & Is.SameAs(resultXsdElem));
        }

        #endregion

        #region XPath Test

        [Test()]
        public void XPath_RootElement_XPathIsOk()
        {
            element = new XmlSchemaElement();
            element.Name = "root";

            parent = CreateSUT(element, null);

            Assert.That(sut.XPath, Is.EqualTo("/root"));
        }

        [Test()]
        public void XPath_RootXsdElementNoName_XPathThrowException()
        {
            element = new XmlSchemaElement();

            XSDElementDocumentationComposite result = new XSDElementDocumentationComposite(element, null);
            Assert.That(result.Name, Is.Null);

            Assert.That(() => result.XPath, Throws.InstanceOf<NullReferenceException>().With.Message.EqualTo("Element XSD in / is null or empty"));
        }

        [Test()]
        public void XPath_ChildXsdElementNoName_XPathThrowException()
        {
            element = new XmlSchemaElement();

            XmlSchemaElement rootElement = new XmlSchemaElement();
            rootElement.Name = "root";

            XSDElementDocumentationComposite xsdRootElement = new XSDElementDocumentationComposite(rootElement, null);

            XSDElementDocumentationComposite result = new XSDElementDocumentationComposite(element, xsdRootElement);
            Assert.That(result.Name, Is.Null);

            Assert.That(() => result.XPath, Throws.InstanceOf<NullReferenceException>().With.Message.EqualTo("Element XSD in /root/ is null or empty"));
        }

        [Test()]
        public void XPath_OneElementInRoot_XPathIsOk()
        {
            element = new XmlSchemaElement();
            element.Name = "root";

            parent = CreateDefaultParent(element);

            XmlSchemaElement childElement = new XmlSchemaElement();
            childElement.Name = "child";

            sut = CreateSUT(childElement, parent);

            Assert.That(parent.XPath, Is.EqualTo("/root"));
            Assert.That(sut.XPath, Is.EqualTo("/root/child"));
        }

        [Test()]
        public void XPath_TwoElementInRoot_XPathIsOk()
        {
            element = new XmlSchemaElement();
            element.Name = "root";

            parent = CreateDefaultParent(element);

            XmlSchemaElement child1Element = new XmlSchemaElement();
            child1Element.Name = "child1";

            XmlSchemaElement child2Element = new XmlSchemaElement();
            child2Element.Name = "child2";

            XSDElementDocumentationComposite elem1 = CreateSUT(child1Element, parent);
            XSDElementDocumentationComposite elem2 = CreateSUT(child2Element, parent);

            Assert.That(parent.XPath, Is.EqualTo("/root"));
            Assert.That(elem1.XPath, Is.EqualTo("/root/child1"));
            Assert.That(elem2.XPath, Is.EqualTo("/root/child2"));
        }

        [Test()]
        public void XPath_PathOfTreeElementInRoot_XPathIsOk()
        {
            element = new XmlSchemaElement();
            element.Name = "root";

            parent = CreateDefaultParent(element);

            XmlSchemaElement child1Element = new XmlSchemaElement();
            child1Element.Name = "child1";

            XmlSchemaElement child2Element = new XmlSchemaElement();
            child2Element.Name = "child2";

            XmlSchemaElement child3Element = new XmlSchemaElement();
            child3Element.Name = "child3";

            XSDElementDocumentationComposite elem1 = CreateSUT(child1Element, parent);
            XSDElementDocumentationComposite elem2 = CreateSUT(child2Element, elem1);
            XSDElementDocumentationComposite elem3 = CreateSUT(child3Element, elem2);

            Assert.That(parent.XPath, Is.EqualTo("/root"));
            Assert.That(elem1.XPath, Is.EqualTo("/root/child1"));
            Assert.That(elem2.XPath, Is.EqualTo("/root/child1/child2"));
            Assert.That(elem3.XPath, Is.EqualTo("/root/child1/child2/child3"));
        }

        #endregion

        #region Level Tests
        [Test()]
        public void Level_RootElement_Level0()
        {
            element = new XmlSchemaElement();
            element.Name = "root";

            parent = CreateSUT(element, null);

            Assert.That(sut.Level, Is.EqualTo(0));
        }

        [Test()]
        public void Level_OneElementInRoot_Level1()
        {
            element = new XmlSchemaElement();
            element.Name = "root";

            parent = CreateDefaultParent(element);

            XmlSchemaElement childElement = new XmlSchemaElement();
            childElement.Name = "child";

            sut = CreateSUT(childElement, parent);

            Assert.That(sut.Level, Is.EqualTo(1));
        }

        [Test()]
        public void Level_TwoElementInRoot_Level1OnEachChild()
        {
            element = new XmlSchemaElement();
            element.Name = "root";

            parent = CreateDefaultParent(element);

            XmlSchemaElement child1Element = new XmlSchemaElement();
            child1Element.Name = "child1";

            XmlSchemaElement child2Element = new XmlSchemaElement();
            child2Element.Name = "child2";

            XSDElementDocumentationComposite elem1 = CreateSUT(child1Element, parent);
            XSDElementDocumentationComposite elem2 = CreateSUT(child2Element, parent);

            Assert.That(elem1.Level, Is.EqualTo(1));
            Assert.That(elem2.Level, Is.EqualTo(1));
        }

        [Test()]
        public void Level_PathOfTreeElementInRoot_ProprerLevelOnEachChild()
        {
            element = new XmlSchemaElement();
            element.Name = "root";

            parent = CreateDefaultParent(element);

            XmlSchemaElement child1Element = new XmlSchemaElement();
            child1Element.Name = "child1";

            XmlSchemaElement child2Element = new XmlSchemaElement();
            child2Element.Name = "child2";

            XmlSchemaElement child3Element = new XmlSchemaElement();
            child3Element.Name = "child3";

            XSDElementDocumentationComposite elem1 = CreateSUT(child1Element, parent);
            XSDElementDocumentationComposite elem2 = CreateSUT(child2Element, elem1);
            XSDElementDocumentationComposite elem3 = CreateSUT(child3Element, elem2);

            Assert.That(parent.Level, Is.EqualTo(0));
            Assert.That(elem1.Level, Is.EqualTo(1));
            Assert.That(elem2.Level, Is.EqualTo(2));
            Assert.That(elem3.Level, Is.EqualTo(3));
        }


        #endregion

        #region Documentation Tests

        [Test()]
        public void Documentation_NotExistsDocInElement_Empty()
        {
            element = new XmlSchemaElement();
            element.Name = "root";

            sut = CreateSUT(element, null);

            Assert.That(sut.Documentation, Is.Empty);
        }

        [Test()]
        public void Documentation_RootElementWithDoc_NotEmptyDocWithRootDoc()
        {
            reader = CreateTestReader();
            reader.ReadXSD();

            sut = (XSDElementDocumentationComposite)reader.RootElement;

            Assert.That(sut.Documentation, Is.Not.Null & Is.Not.Empty & Is.EqualTo("Root documentation"));
        }

        [Test()]
        public void Documentation_ChildElementWithDoc_NotEmptyDocWithChildDoc()
        {
            reader = CreateTestReader();
            reader.ReadXSD();

            sut = (XSDElementDocumentationComposite)reader.RootElement.ChildElements[0];
            string expectedDoc = sut.Name + " documentation";

            Assert.That(sut.Documentation, Is.Not.Null & Is.Not.Empty & Is.EqualTo(expectedDoc));
        }

        [Test()]
        public void Documentation_ChildElementWithoutDoc_EmptyDoc()
        {
            reader = CreateTestReader();
            reader.ReadXSD();

            sut = (XSDElementDocumentationComposite)reader.RootElement.ChildElements[1];

            Assert.That(sut.Documentation, Is.Empty);
        }

        [Test()]
        public void Documentation_ChildElementNamedItemWithDoc_DocItemDoc()
        {
            reader = CreateTestReader();
            reader.ReadXSD();

            sut = (XSDElementDocumentationComposite)reader.RootElement.ChildElements[2];
            string expectedDoc = sut.Name + " documentation";

            Assert.That(sut.Name, Is.EqualTo("item"));
            Assert.That(sut.Documentation, Is.Not.Null & Is.Not.Empty & Is.EqualTo(expectedDoc));
        }

        [Test()]
        public void Documentation_ChildElementChoiceWithDoc_DocNotEmptyWithSpecialText()
        {
            reader = CreateTestReader();
            reader.ReadXSD();

            sut = (XSDElementDocumentationComposite)reader.RootElement.ChildElements[2].ChildElements[4];
            string expectedDoc = sut.Name + " documentation";

            Assert.That(sut.Name, Is.EqualTo("contenerS"));
            Assert.That(sut.Documentation, Is.Not.Null & Is.Not.Empty & Is.EqualTo(expectedDoc));
        }


        #endregion

        #region XsdType Tests


        [Test()]
        public void XsdType__GetContenerSElement__ExpectDecimal()
        {
            // Arrange
            reader = CreateTestReader();
            reader.ReadXSD();
            string expectedType = "decimal";

            // Act
            sut = (XSDElementDocumentationComposite)reader.RootElement.ChildElements[2].ChildElements[4];

            // Assert
            Assert.That(sut.Name, Is.EqualTo("contenerS"));
            Assert.That(sut.XsdType, Is.Not.Null & Is.Not.Empty & Is.EqualTo(expectedType));
        }

        [Test()]
        public void XsdType__GetItemElement__ExpectComplex()
        {
            // Arrange
            reader = CreateTestReader();
            reader.ReadXSD();
            string expectedType = "complex";

            // Act
            sut = (XSDElementDocumentationComposite)reader.RootElement.ChildElements[2];

            // Assert
            Assert.That(sut.Name, Is.EqualTo("item"));
            Assert.That(sut.XsdType, Is.Not.Null & Is.Not.Empty & Is.EqualTo(expectedType));
        }

        #endregion


        #region ReadContent Tests

        [Test()]
        public void ReadContent_ReadComplexContentWithChoice_ChildElementExists()
        {
            CreateDefaultFactory();
            sut = CreateReaderRootElementForTests();
            sut.ReadContent();

            int contenerMRootIndex = 2;
            int contenerMIndex = 5;


            IXSDElementDocumentationComposite elementContenerM = sut.ChildElements[contenerMRootIndex].ChildElements[contenerMIndex];

            Assert.That(elementContenerM, Is.Not.Null & Is.InstanceOf<XSDElementDocumentationComposite>() & Has.Property("Name").EqualTo("contenerM"));
        }

        [Test()]
        public void ReadContent_ReadComplexContentWithSequence_ChildElementExists()
        {
            CreateDefaultFactory();
            sut = CreateReaderRootElementForTests();
            sut.ReadContent();

            int priceRootIndex = 2;
            int priceIndex = 3;

            IXSDElementDocumentationComposite elementContenerM = sut.ChildElements[priceRootIndex].ChildElements[priceIndex];

            Assert.That(elementContenerM, Is.Not.Null & Is.InstanceOf<XSDElementDocumentationComposite>() & Has.Property("Name").EqualTo("price"));
        }

        #endregion

        #region ElementFactory Tests

        [Test()]
        public void ElementFactory_CreateChildElementsWithBuildFactoryMethod_ReceivedBuildFactoryMethod()
        {
            CreateMockFactory();
            sut = CreateReaderRootElementForTests();

            sut.ReadContent();

            factory.ReceivedWithAnyArgs().CreateElementDocumentationComposite(null, null);
        }

        [Test()]
        public void ElementFactory_ChildElementsGetFactoryAfterParent_ChildElementsHasSameFactory()
        {

            sut = CreateReaderRootElementForTests();
            factory = reader.ElementFactory;

            sut.ReadContent();

            foreach (IXSDElementDocumentationComposite child1 in sut.ChildElements)
            {
                Assert.That(child1.FromFactory, Is.SameAs(factory));
                foreach (IXSDElementDocumentationComposite child2 in child1.ChildElements)
                    Assert.That(child2.FromFactory, Is.SameAs(factory));
            }


        }

        #endregion

        #region Enumerator Tests

        [TestCase("/shiporder")]
        [TestCase("/shiporder/orderperson")]
        [TestCase("/shiporder/item")]
        [TestCase("/shiporder/item/contenerS")]
        public void Enumerator_Iterate_ExistsXPath(string expectedXPath)
        {
            CreateDefaultFactory();
            sut = CreateReaderRootElementForTests();
            sut.ReadContent();

            List<string> listXPaths = GetListXPathAllIterateElements(sut);

            Assert.That(listXPaths.Contains(expectedXPath), Is.True);
        }

        [Test()]
        public void Enumerator_IterateIfOlnyRootElementExists_ExistsOlnyRootXPath()
        {
            XmlSchemaElement xsdRoot = new XmlSchemaElement();
            xsdRoot.Name = "root";
            sut = new XSDElementDocumentationComposite(xsdRoot, null);
            sut.ReadContent();

            List<string> listXPaths = GetListXPathAllIterateElements(sut);

            Assert.That(listXPaths.Count, Is.EqualTo(1));
            Assert.That(listXPaths[0], Is.EqualTo("/root"));
        }




        #endregion

        #region AddChild Tests

        [Test()]
        public void AddChild_AddSelf_NoSelfInChildElments()
        {
            CreateDefaultFactory();
            sut = CreateReaderRootElementForTests();

            sut.AddChild(sut);

            Assert.That(sut.ChildElements.IndexOf(sut),
                        Is.LessThan(0)
                        );

        }

        #endregion


        #region ToXmlString Tests

        [Test()]
        public void ToXmlString_OlnyRootElement_ResultHasNameOfRootElement()
        {
            element = new XmlSchemaElement();
            element.Name = "root";

            sut = CreateSUT(element, null);

            string result = sut.ToXmlString();
            Assert.That(result,
                           Is.Not.Null &
                           Is.Not.Empty &
                           (
                                (
                                Contains.Substring("<root") &
                                Contains.Substring("</root>")
                                )
                                |
                                Does.Match("<root([^\\>]*)/>")
                           )
                           );
        }

        [Test()]
        public void ToXmlString_WithDocumentation_ResultHasElementsWithXsdDoc()
        {
            reader = CreateTestReader();
            reader.ReadXSD();

            XSDElementDocumentationComposite root = (XSDElementDocumentationComposite)reader.RootElement;

            XSDElementDocumentationComposite innerElem = (XSDElementDocumentationComposite)reader.RootElement.ChildElements[2].ChildElements[4];

            string result = root.ToXmlString();

            Assert.That(result,
                           Is.Not.Null &
                           Is.Not.Empty &
                           Does.Match("<" + root.Name + "([^\\>]+)"+ XSDElementDocumentationComposite.DOC_ATTR_NAME + "=\"" + root.Documentation.Replace("\"", "'") + "\"(.*)>") &
                           Does.Match("<" + innerElem.Name + "([^\\>]+)"+ XSDElementDocumentationComposite.DOC_ATTR_NAME + "=\"" + innerElem.Documentation.Replace("\"", "'") + "\"(.*)>")
                           );


        }


        [Test()]
        public void ToXmlString_WithType_ResultHasElementsWithXsdDoc()
        {
            reader = CreateTestReader();
            reader.ReadXSD();

            XSDElementDocumentationComposite root = (XSDElementDocumentationComposite)reader.RootElement;

            XSDElementDocumentationComposite innerElem = (XSDElementDocumentationComposite)reader.RootElement.ChildElements[2].ChildElements[4];


            string type = innerElem.XsdType;

            string result = root.ToXmlString();

            Assert.That(type, 
                            Is.Not.Null & 
                            Is.Not.Empty
                            );

            Assert.That(result,
                           Is.Not.Null &
                           Is.Not.Empty &
                           Does.Match("<" + root.Name + "([^\\>]+)" + XSDElementDocumentationComposite.DOC_ATTR_NAME + "=\"" + root.Documentation.Replace("\"", "'") + "\"(.*)>") &
                           Does.Match("<" + innerElem.Name + "([^\\>]+)" + XSDElementDocumentationComposite.TYPE_ATTR_NAME + "=\"" + type.Replace("\"", "'") + "\"(.*)>")
                           );


        }

        [Test()]
        public void ToXmlString_WithXPath_ResultHasElementsWithXsdDoc()
        {
            reader = CreateTestReader();
            reader.ReadXSD();

            XSDElementDocumentationComposite root = (XSDElementDocumentationComposite)reader.RootElement;

            XSDElementDocumentationComposite innerElem = (XSDElementDocumentationComposite)reader.RootElement.ChildElements[2].ChildElements[4];

            string result = root.ToXmlString();

            Assert.That(result,
                             Is.Not.Null
                           & Is.Not.Empty
                           & Does.Match("<" + root.Name + "([^\\>]+)"+ XSDElementDocumentationComposite.XPATH_ATTR_NAME+ "=\"" + root.XPath.Replace("\"", "'") + "\"(.*)>")
                           & Does.Match("<" + innerElem.Name + "([^\\>]+)"+ XSDElementDocumentationComposite.XPATH_ATTR_NAME+ "=\"" + innerElem.XPath.Replace("\"", "'") + "\"(.*)>")
                           );


        }


        [Test()]
        public void ToXmlString_byDefault_ResultHasElementsWithNames()
        {
            reader = CreateTestReader();
            reader.ReadXSD();

            XSDElementDocumentationComposite root = (XSDElementDocumentationComposite) reader.RootElement;

            XSDElementDocumentationComposite innerElem = (XSDElementDocumentationComposite) reader.RootElement.ChildElements[2].ChildElements[4];

            string result = root.ToXmlString();

            Assert.That(result,
                           Is.Not.Null &
                           Is.Not.Empty &
                           Contains.Substring("<"+root.Name) &
                           Contains.Substring("</" + root.Name+">") &
                           (
                                (
                                    Contains.Substring("<"+innerElem.Name) &
                                    Contains.Substring("</" + innerElem.Name + ">")
                                )
                                |
                                
                                Does.Match("<" + innerElem.Name + "([^\\>]*)/>")
                           )
                           );


        }


        #endregion



        #endregion Tests

        #region Help functions

        private XSDElementDocumentationComposite CreateSUT(XmlSchemaElement element, XSDElementDocumentationComposite parent = null)
        {
            sut = new XSDElementDocumentationComposite(element, parent);
            return sut;
        }

        private XSDElementDocumentationComposite CreateSUTWithDefaultParent(XmlSchemaElement element)
        {
            parentElement = CreateDefaultParentXsdElement();
            parent = CreateDefaultParent(parentElement);

            sut = CreateSUT(element, parent);
            return sut;
        }

        private XSDElementDocumentationComposite CreateDefaultParent(XmlSchemaElement element)
        {
            parent = new XSDElementDocumentationComposite(element, null);
            return parent;
        }

        private XSDElementDocumentationComposite CreateDefaultParent()
        {
            parentElement = CreateDefaultParentXsdElement();
            return CreateDefaultParent(parentElement);
        }

        private XmlSchemaElement CreateDefaultParentXsdElement()
        {
            parentElement = new XmlSchemaElement();
            parentElement.Name = "root";

            return parentElement;
        }

        private XSDElementDocumentationReader CreateTestReader()
        {
            Stream stream = XSDElementDocumentationReader_Tests.String2MemoryStream(XSDElementDocumentationReader_Tests.getXSDSource_SimpleXSD());
            if (factory == null)
                CreateDefaultFactory();
            XSDElementDocumentationReader reader = new XSDElementDocumentationReader(stream, factory);
            return reader;
        }


        private void ValidationCallback(object sender, ValidationEventArgs e)
        {
            if (e.Severity == XmlSeverityType.Error)
                throw e.Exception;
        }

        private XSDElementDocumentationComposite CreateReaderRootElementForTests()
        {
            sut = null;
            reader = CreateTestReader();

            XmlSchemaSet schemaSet = new XmlSchemaSet();
            schemaSet.ValidationEventHandler += new ValidationEventHandler(ValidationCallback);
            XmlSchema XsdSchema = XmlSchema.Read(reader.XsdStream, new ValidationEventHandler(ValidationCallback));
            schemaSet.Add(XsdSchema);
            schemaSet.Compile();


            foreach (XmlSchemaElement element in XsdSchema.Elements.Values)
            {
                sut = new XSDElementDocumentationComposite(element, null);
                sut.FromFactory = reader.ElementFactory;
                return sut;
            }

            return null;
        }


        private IXSDElementDocumentationCompositeFactory CreateMockFactory()
        {
            factory = Substitute.For<IXSDElementDocumentationCompositeFactory>();
            return factory;
        }

        private IXSDElementDocumentationCompositeFactory CreateDefaultFactory()
        {
            factory = new XSDElementDocumentationCompositeFactory();
            return factory;
        }

        private List<string> GetListXPathAllIterateElements(IXSDElementDocumentationComposite root)
        {
            List<string> listXPaths = new List<string>();

            root.Reset();
            do
            {
                IXSDElementDocumentationComposite element = root.Current;
                if (element != null) listXPaths.Add(element.XPath);
            }
            while (root.MoveNext());
            return listXPaths;
        }


        #endregion



    }
}