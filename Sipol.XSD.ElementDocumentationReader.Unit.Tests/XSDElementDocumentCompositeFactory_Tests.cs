﻿using NUnit.Framework;
using NSubstitute;
using System.Xml.Schema;
using Sipol.XSD.ElementDocumentationReader.Interfaces;
using Sipol.XSD.ElementDocumentationReader;

namespace Sipol.XSD.ElementDocumentationReader.Tests
{
    [TestFixture()]
    public class XSDElementDocumentCompositeFactory_Tests
    {
        private XSDElementDocumentationCompositeFactory sut = null;

        [Test()]
        public void CreateElementDocumentationComposite_byDefault_XSDElementDocumentationCompositeObject()
        {
            CreateSUT();

            XmlSchemaElement xsdParentElementMock = new XmlSchemaElement();
            xsdParentElementMock.Name= "root" ;

            IXSDElementDocumentationComposite parent = new XSDElementDocumentationComposite(xsdParentElementMock);

            XmlSchemaElement XsdElementMock = new XmlSchemaElement();
            XsdElementMock.Name = "item";

            IXSDElementDocumentationComposite result = sut.CreateElementDocumentationComposite(XsdElementMock, parent);

            Assert.That(result, Is.Not.Null & Is.InstanceOf<XSDElementDocumentationComposite>());
            Assert.That(result.XsdElement, Is.SameAs(XsdElementMock));
            Assert.That(result.Parent, Is.SameAs(parent));
            Assert.That(result.Parent.XsdElement, Is.SameAs(xsdParentElementMock));

            Assert.That(result.Name, Is.EqualTo("item"));
            Assert.That(result.Parent.Name, Is.EqualTo("root"));

        }

        [Test()]
        public void CreateElementDocumentationComposite_CheckParentAddElement_ParentElementHasResultElement()
        {
            CreateSUT();

            XmlSchemaElement xsdParentElementMock = new XmlSchemaElement();
            xsdParentElementMock.Name = "root";

            IXSDElementDocumentationComposite parent = new XSDElementDocumentationComposite(xsdParentElementMock);

            XmlSchemaElement XsdElementMock = new XmlSchemaElement();
            XsdElementMock.Name = "item";

            IXSDElementDocumentationComposite result = sut.CreateElementDocumentationComposite(XsdElementMock, parent);

            Assert.That(result, Is.Not.Null & Is.InstanceOf<XSDElementDocumentationComposite>());
            Assert.That(result.XsdElement, Is.SameAs(XsdElementMock));
            Assert.That(result.Parent, Is.Not.Null & Is.SameAs(parent));
            Assert.That(result.Parent.XsdElement, Is.SameAs(xsdParentElementMock));

            Assert.That(result.Name, Is.EqualTo("item"));
            Assert.That(result.Parent.Name, Is.EqualTo("root"));

            Assert.That(result.Parent.ChildElements, Is.Not.Null & Has.Property("Count").GreaterThan(0));
            Assert.That(result.Parent.ChildElements[0], Is.Not.Null & Is.SameAs(result) & Has.Property("Name").EqualTo("item"));
        }

        [Test()]
        public void CreateElementDocumentationComposite_ParentIsNull_CompositeHasOlnyRootElement()
        {
            CreateSUT();

            XmlSchemaElement xsdParentElementMock = new XmlSchemaElement();
            xsdParentElementMock.Name = "root";

            IXSDElementDocumentationComposite parent = new XSDElementDocumentationComposite(xsdParentElementMock);

            XmlSchemaElement XsdElementMock = new XmlSchemaElement();
            XsdElementMock.Name = "item";

            IXSDElementDocumentationComposite result = sut.CreateElementDocumentationComposite(xsdParentElementMock, null);

            Assert.That(result, Is.Not.Null & Is.InstanceOf<XSDElementDocumentationComposite>());
            Assert.That(result.XsdElement, Is.SameAs(xsdParentElementMock));
            Assert.That(result.Parent, Is.Null);

            Assert.That(result.Name, Is.EqualTo("root"));

            Assert.That(result.ChildElements, Is.Not.Null & Has.Property("Count").EqualTo(0) & Has.Count.EqualTo(0) & Has.No.Contain(XsdElementMock));
        }

        #region Help funcs

        private XSDElementDocumentationCompositeFactory CreateSUT()
        {
            sut = new XSDElementDocumentationCompositeFactory();
            return sut;
        }

        #endregion
    }
}