﻿using NUnit.Framework;
using Sipol.XSD.ElementDocumentationReader.Interfaces;
using Sipol.XSD.ElementDocumentationReader.Tests;
using System.IO;


namespace Sipol.XSD.ElementDocumentationReader.Unit.Tests.Automation_Tests
{
    internal class Tests_ReadXSDDocumentation : Sipol.Test.Automation.AutomationTests
    {
        private string xsdSource = null;
        private Stream stream = null;
        private XSDElementDocumentationReader reader = null;

        public override void RunTests()
        {
            RunTest(
                LoadXml,
                MapXsdSchema
                );
        }

        public void MapXsdSchema()
        {
            CreateDefaultReader();

            XPathXsdDocElementMapper mapper = new XPathXsdDocElementMapper(reader);
            mapper.CreateMap();

            Assert.That(    mapper.Count, 
                            
                            Is.GreaterThan(10)
                       );

            object result = mapper.Get("/JednostkaMala/BilansJednostkaMala");

            
            Assert.That(    result,
                        
                            Is.Not.Null
                            & Is.InstanceOf<IXSDElementDocumentationComposite>()
                            & Is.Not.InstanceOf<IXSDElementDocumentationEmpty>()
                            & Has.Property("Documentation").Not.Null
                            & Has.Property("Documentation").Not.Empty
                        );
            
        }

        public void LoadXml()
        {

            /*
            var doc = new XmlDocument();
            doc.LoadXml(getXmlSource_JednostkaMalaWZlotych());

            int c = 0;

            doc.IterateThroughAllNodes(
                delegate (XmlNode node)
                {
                    if (node == null) return;
                    if (!(node is XmlElement)) return;

                    String xpath = XmlDocumentExtensions.FindXPath(node,true,false);

                    Console.WriteLine("{0}\r\n{1}",node.LocalName, xpath ?? "(null)");
                    c++;
                    if ((c % 100) == 0) Console.ReadKey();
                }
                );

            */
        }


        #region static funcs
        public static string getXSDSource_JednostkaMalaWZlotych()
        {
            return @"<?xml version=""1.0"" encoding=""UTF-8""?>
<xsd:schema xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:etd=""http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2016/01/25/eD/DefinicjeTypy/"" xmlns:dtsf=""http://www.mf.gov.pl/schematy/SF/DefinicjeTypySprawozdaniaFinansowe/2018/07/09/DefinicjeTypySprawozdaniaFinansowe/"" xmlns:jma=""http://www.mf.gov.pl/schematy/SF/DefinicjeTypySprawozdaniaFinansowe/2018/07/09/JednostkaMalaStruktury"" xmlns:jin=""http://www.mf.gov.pl/schematy/SF/DefinicjeTypySprawozdaniaFinansowe/2018/07/09/JednostkaInnaStruktury"" xmlns:tns=""http://www.mf.gov.pl/schematy/SF/DefinicjeTypySprawozdaniaFinansowe/2018/07/09/JednostkaMalaWZlotych"" targetNamespace=""http://www.mf.gov.pl/schematy/SF/DefinicjeTypySprawozdaniaFinansowe/2018/07/09/JednostkaMalaWZlotych"" elementFormDefault=""qualified"" attributeFormDefault=""unqualified"" version=""1.0"" xml:lang=""pl"">
	<xsd:import namespace=""http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2016/01/25/eD/DefinicjeTypy/"" schemaLocation=""http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2016/01/25/eD/DefinicjeTypy/StrukturyDanych_v4-0E.xsd""/>
	<xsd:import namespace=""http://www.mf.gov.pl/schematy/SF/DefinicjeTypySprawozdaniaFinansowe/2018/07/09/DefinicjeTypySprawozdaniaFinansowe/"" schemaLocation=""https://www.mf.gov.pl/documents/764034/6464789/StrukturyDanychSprFin_v1-0.xsd""/>
	<xsd:import namespace=""http://www.mf.gov.pl/schematy/SF/DefinicjeTypySprawozdaniaFinansowe/2018/07/09/JednostkaMalaStruktury"" schemaLocation=""https://www.mf.gov.pl/documents/764034/6464789/JednostkaMalaStrukturyDanychSprFin_v1-0.xsd""/>
	<xsd:import namespace=""http://www.mf.gov.pl/schematy/SF/DefinicjeTypySprawozdaniaFinansowe/2018/07/09/JednostkaInnaStruktury"" schemaLocation=""https://www.mf.gov.pl/documents/764034/6464789/JednostkaInnaStrukturyDanychSprFin_v1-0.xsd""/>
	<xsd:element name=""JednostkaMala"">
		<xsd:annotation>
			<xsd:documentation>ZAKRES INFORMACJI WYKAZYWANYCH W SPRAWOZDANIU FINANSOWYM, O KTÓRYM MOWA W ART. 45 USTAWY, DLA JEDNOSTEK MAŁYCH KORZYSTAJĄCYCH Z UPROSZCZEŃ ODNOSZĄCYCH SIĘ DO SPRAWOZDANIA FINANSOWEGO</xsd:documentation>
		</xsd:annotation>
		<xsd:complexType>
			<xsd:sequence>
				<xsd:element name=""Naglowek"" type=""jma:TNaglowekSprawozdaniaFinansowegoJednostkaMalaWZlotych"">
					<xsd:annotation>
						<xsd:documentation>Nagłówek sprawozdania finansowego</xsd:documentation>
					</xsd:annotation>
				</xsd:element>
				<xsd:choice>
					<xsd:element name=""WprowadzenieDoSprawozdaniaFinansowegoJednostkaMala"">
						<xsd:annotation>
							<xsd:documentation>Wprowadzenie do sprawozdania finansowego zgodnie z Załącznikiem Nr 5 do ustawy o rachunkowości</xsd:documentation>
						</xsd:annotation>
						<xsd:complexType>
							<xsd:sequence>
								<xsd:element name=""P_1"">
									<xsd:annotation>
										<xsd:documentation>Dane identyfikujące jednostkę</xsd:documentation>
									</xsd:annotation>
									<xsd:complexType>
										<xsd:sequence>
											<xsd:element name=""P_1A"" type=""dtsf:TNazwaSiedziba"">
												<xsd:annotation>
													<xsd:documentation>Firma, siedziba albo miejsce zamieszkania</xsd:documentation>
												</xsd:annotation>
											</xsd:element>
											<xsd:element name=""P_1B"" type=""dtsf:TAdresZOpcZagranicznym"">
												<xsd:annotation>
													<xsd:documentation>Adres</xsd:documentation>
												</xsd:annotation>
											</xsd:element>
											<xsd:element name=""P_1C"" type=""dtsf:TIdentyfikatorPodmiotu"">
												<xsd:annotation>
													<xsd:documentation>Numer we własciwym rejestrze sądowym albo ewidencji </xsd:documentation>
												</xsd:annotation>
											</xsd:element>
										</xsd:sequence>
									</xsd:complexType>
								</xsd:element>
								<xsd:element name=""P_2"" type=""dtsf:TZakresDatOpcjonalnych"" minOccurs=""0"">
									<xsd:annotation>
										<xsd:documentation>Czas trwania działalności jednostki, jeżeli jest ograniczony</xsd:documentation>
									</xsd:annotation>
								</xsd:element>
								<xsd:element name=""P_3"" type=""dtsf:TZakresDatSF"">
									<xsd:annotation>
										<xsd:documentation>Wskazanie okresu objętego sprawozdaniem finansowym</xsd:documentation>
									</xsd:annotation>
								</xsd:element>
								<xsd:element name=""P_4"" type=""etd:TTekstowy"">
									<xsd:annotation>
										<xsd:documentation>Wskazanie zastosowanych uproszczeń przewidzianych dla jednostek małych</xsd:documentation>
									</xsd:annotation>
								</xsd:element>
								<xsd:element name=""P_5"">
									<xsd:annotation>
										<xsd:documentation>Założenie kontynuacji działalnności</xsd:documentation>
									</xsd:annotation>
									<xsd:complexType>
										<xsd:sequence>
											<xsd:element name=""P_5A"" type=""xsd:boolean"">
												<xsd:annotation>
													<xsd:documentation>Wskazanie, czy sprawozdanie finansowe zostało sporządzone przy założeniu kontynuowania działalności gospodarczej		przez jednostkę w dającej się przewidzieć przyszłości: true - sprawozdanie sporządzone przy założeniu kontynuowania działalności, false - sprawozdanie zostało sporzadzone przy zalożeniu, że działalność nie będzie kontynuowana</xsd:documentation>
												</xsd:annotation>
											</xsd:element>
											<xsd:element name=""P_5B"" type=""xsd:boolean"">
												<xsd:annotation>
													<xsd:documentation>Wskazanie, czy nie istnieją okoliczności wskazujące na zagrożenie kontynuowania przez nią działalności</xsd:documentation>
												</xsd:annotation>
											</xsd:element>
											<xsd:element name=""P_5C"" type=""etd:TTekstowy"" minOccurs=""0"">
												<xsd:annotation>
													<xsd:documentation>Opis okoliczności wskazujących na zagrożenie kontynuowania działalności</xsd:documentation>
												</xsd:annotation>
											</xsd:element>
										</xsd:sequence>
									</xsd:complexType>
								</xsd:element>
								<xsd:element name=""P_6"">
									<xsd:annotation>
										<xsd:documentation>Zasady (polityka) rachunkowości</xsd:documentation>
									</xsd:annotation>
									<xsd:complexType>
										<xsd:sequence>
											<xsd:element name=""P_6A"" type=""etd:TTekstowy"" maxOccurs=""unbounded"">
												<xsd:annotation>
													<xsd:documentation>Omówienie przyjętych zasad (polityki) rachunkowości, w zakresie w jakim ustawa pozostawia jednostce prawo wyboru, w tym:</xsd:documentation>
												</xsd:annotation>
											</xsd:element>
											<xsd:element name=""P_6B"" type=""etd:TTekstowy"" maxOccurs=""unbounded"">
												<xsd:annotation>
													<xsd:documentation>metod wyceny aktywów i pasywów (także amortyzacji)</xsd:documentation>
												</xsd:annotation>
											</xsd:element>
											<xsd:element name=""P_6C"" type=""etd:TTekstowy"" maxOccurs=""unbounded"">
												<xsd:annotation>
													<xsd:documentation>ustalenia wyniku finansowego oraz sposobu sporządzenia sprawozdania finansowego</xsd:documentation>
												</xsd:annotation>
											</xsd:element>
										</xsd:sequence>
									</xsd:complexType>
								</xsd:element>
								<xsd:element name=""P_7"" type=""dtsf:TPozycjaUzytkownika"" minOccurs=""0"" maxOccurs=""unbounded"">
									<xsd:annotation>
										<xsd:documentation>Informacja uszczegóławiająca, wynikająca z potrzeb lub specyfiki jednostki</xsd:documentation>
									</xsd:annotation>
								</xsd:element>
							</xsd:sequence>
						</xsd:complexType>
					</xsd:element>
					<xsd:element name=""WprowadzenieDoSprawozdaniaFinansowegoJednostkaInna"">
						<xsd:annotation>
							<xsd:documentation>Wprowadzenie do sprawozdania finansowego zgodnie z Załącznikiem Nr 1 do ustawy o rachunkowości zawierająca informacje w zakresie nie mniejszym niż w Załączniku Nr 5 do ustawy o rachunkowości</xsd:documentation>
						</xsd:annotation>
						<xsd:complexType>
							<xsd:sequence>
								<xsd:element name=""P_1"">
									<xsd:annotation>
										<xsd:documentation>Dane identyfikujące jednostkę</xsd:documentation>
									</xsd:annotation>
									<xsd:complexType>
										<xsd:sequence>
											<xsd:element name=""P_1A"" type=""dtsf:TNazwaSiedziba"">
												<xsd:annotation>
													<xsd:documentation>Firma, siedziba albo miejsce zamieszkania</xsd:documentation>
												</xsd:annotation>
											</xsd:element>
											<xsd:element name=""P_1B"" type=""dtsf:TAdresZOpcZagranicznym"">
												<xsd:annotation>
													<xsd:documentation>Adres</xsd:documentation>
												</xsd:annotation>
											</xsd:element>
											<xsd:element name=""P_1C"" type=""dtsf:PKDPodstawowaDzialalnosc"">
												<xsd:annotation>
													<xsd:documentation>Podstawowy przedmiot działalności jednostki</xsd:documentation>
												</xsd:annotation>
											</xsd:element>
											<xsd:element name=""P_1D"" type=""dtsf:TIdentyfikatorPodmiotu"">
												<xsd:annotation>
													<xsd:documentation>Numer we właściwym rejestrze sądowym albo ewidencji </xsd:documentation>
												</xsd:annotation>
											</xsd:element>
										</xsd:sequence>
									</xsd:complexType>
								</xsd:element>
								<xsd:element name=""P_2"" type=""dtsf:TZakresDatOpcjonalnych"" minOccurs=""0"">
									<xsd:annotation>
										<xsd:documentation>Czas trwania działalności jednostki, jeżeli jest ograniczony</xsd:documentation>
									</xsd:annotation>
								</xsd:element>
								<xsd:element name=""P_3"" type=""dtsf:TZakresDatSF"">
									<xsd:annotation>
										<xsd:documentation>Wskazanie okresu objętego sprawozdaniem finansowym</xsd:documentation>
									</xsd:annotation>
								</xsd:element>
								<xsd:element name=""P_4"" type=""xsd:boolean"">
									<xsd:annotation>
										<xsd:documentation>Wskazanie, że sprawozdanie finansowe zawiera dane łączne, jeżeli w skład jednostki wchodzą wewnętrzne jednostki organizacyjne sporządzające samodzielne sprawozdania finansowe: true - sprawozdanie finansowe zawiera dane łącznie; false - sprawozdanie nie zawiera danych łącznych </xsd:documentation>
									</xsd:annotation>
								</xsd:element>
								<xsd:element name=""P_5"">
									<xsd:annotation>
										<xsd:documentation>Założenie kontynuacji działalnności</xsd:documentation>
									</xsd:annotation>
									<xsd:complexType>
										<xsd:sequence>
											<xsd:element name=""P_5A"" type=""xsd:boolean"">
												<xsd:annotation>
													<xsd:documentation>Wskazanie, czy sprawozdanie finansowe zostało sporządzone przy założeniu kontynuowania działalności gospodarczej		przez jednostkę w dającej się przewidzieć przyszłości</xsd:documentation>
												</xsd:annotation>
											</xsd:element>
											<xsd:element name=""P_5B"" type=""xsd:boolean"">
												<xsd:annotation>
													<xsd:documentation>Wskazanie, czy nie istnieją okoliczności wskazujące na zagrożenie kontynuowania przez nią działalności</xsd:documentation>
												</xsd:annotation>
											</xsd:element>
											<xsd:element name=""P_5C"" type=""etd:TTekstowy"" minOccurs=""0"">
												<xsd:annotation>
													<xsd:documentation>Opis okoliczności wskazujących na zagrożenie kontynuowania działalności</xsd:documentation>
												</xsd:annotation>
											</xsd:element>
										</xsd:sequence>
									</xsd:complexType>
								</xsd:element>
								<xsd:element name=""P_6"" minOccurs=""0"">
									<xsd:annotation>
										<xsd:documentation>Informacja czy sprawozdanie finansowe jest sporządzone po połączeniu spółek</xsd:documentation>
									</xsd:annotation>
									<xsd:complexType>
										<xsd:sequence>
											<xsd:element name=""P_6A"" type=""xsd:boolean"">
												<xsd:annotation>
													<xsd:documentation>W przypadku sprawozdania finansowego sporządzonego za okres, w ciągu którego nastąpiło połączenie, wskazanie,
że jest to sprawozdanie finansowe sporządzone po połączeniu spółek: true - sprawozdanie sporządzone po połączeniu spółek; false - sprawozdanie sporządzone przed połączeniem </xsd:documentation>
												</xsd:annotation>
											</xsd:element>
											<xsd:element name=""P_6B"" type=""etd:TTekstowy"">
												<xsd:annotation>
													<xsd:documentation>Wskazanie zastosowanej metody rozliczenia połączenia (nabycia, łączenia udziałów)</xsd:documentation>
												</xsd:annotation>
											</xsd:element>
										</xsd:sequence>
									</xsd:complexType>
								</xsd:element>
								<xsd:element name=""P_7"">
									<xsd:annotation>
										<xsd:documentation>Zasady (polityka) rachunkowości</xsd:documentation>
									</xsd:annotation>
									<xsd:complexType>
										<xsd:sequence>
											<xsd:element name=""P_7A"" type=""etd:TTekstowy"" maxOccurs=""unbounded"">
												<xsd:annotation>
													<xsd:documentation>Omówienie przyjętych zasad (polityki) rachunkowości, w zakresie w jakim ustawa pozostawia jednostce prawo wyboru, w tym:</xsd:documentation>
												</xsd:annotation>
											</xsd:element>
											<xsd:element name=""P_7B"" type=""etd:TTekstowy"" maxOccurs=""unbounded"">
												<xsd:annotation>
													<xsd:documentation>metod wyceny aktywów i pasywów (także amortyzacji),</xsd:documentation>
												</xsd:annotation>
											</xsd:element>
											<xsd:element name=""P_7C"" type=""etd:TTekstowy"" maxOccurs=""unbounded"">
												<xsd:annotation>
													<xsd:documentation>ustalenia wyniku finansowego</xsd:documentation>
												</xsd:annotation>
											</xsd:element>
											<xsd:element name=""P_7D"" type=""etd:TTekstowy"" maxOccurs=""unbounded"">
												<xsd:annotation>
													<xsd:documentation>ustalenia sposobu sporządzenia sprawozdania finansowego</xsd:documentation>
												</xsd:annotation>
											</xsd:element>
										</xsd:sequence>
									</xsd:complexType>
								</xsd:element>
								<xsd:element name=""P_8"" type=""dtsf:TPozycjaUzytkownika"" minOccurs=""0"" maxOccurs=""unbounded"">
									<xsd:annotation>
										<xsd:documentation>Informacja uszczegóławiająca, wynikająca z potrzeb lub specyfiki jednostki</xsd:documentation>
									</xsd:annotation>
								</xsd:element>
								<xsd:element name=""P_9"" type=""etd:TTekstowy"">
									<xsd:annotation>
										<xsd:documentation>Wskazanie zastosowanych uproszczeń przewidzianych dla jednostek małych</xsd:documentation>
									</xsd:annotation>
								</xsd:element>
							</xsd:sequence>
						</xsd:complexType>
					</xsd:element>
				</xsd:choice>
				<xsd:choice>
					<xsd:element name=""BilansJednostkaMala"" type=""jma:BilansJednostkaMala"">
						<xsd:annotation>
							<xsd:documentation>Bilans zgodnie z Załącznikiem Nr 5 do ustawy o rachunkowości</xsd:documentation>
						</xsd:annotation>
					</xsd:element>
					<xsd:element name=""BilansJednostkaInna"" type=""jin:BilansJednostkaInna"">
						<xsd:annotation>
							<xsd:documentation>Bilans zgodnie z Załącznikiem Nr 1 do ustawy o rachunkowości</xsd:documentation>
						</xsd:annotation>
					</xsd:element>
				</xsd:choice>
				<xsd:choice>
					<xsd:element name=""RZiSJednostkaMala"" type=""jma:RZiSJednostkaMala"">
						<xsd:annotation>
							<xsd:documentation>Rachunek zysków i strat zgodnie z Załącznikiem Nr 5 do ustawy o rachunkowości</xsd:documentation>
						</xsd:annotation>
					</xsd:element>
					<xsd:element name=""RZiSJednostkaInna"" type=""jin:RZiSJednostkaInna"">
						<xsd:annotation>
							<xsd:documentation>Rachunek zysków i strat zgodnie z Załącznikiem Nr 1 do ustawy o rachunkowości</xsd:documentation>
						</xsd:annotation>
					</xsd:element>
				</xsd:choice>
				<xsd:element name=""ZestZmianWKapitaleJednostkaInna"" type=""jin:ZestZmianWKapitaleJednostkaInna"" minOccurs=""0"">
					<xsd:annotation>
						<xsd:documentation>Zestawienie zmian w kapitale (funduszu) własnym</xsd:documentation>
					</xsd:annotation>
				</xsd:element>
				<xsd:element name=""RachPrzeplywowJednostkaInna"" type=""jin:RachPrzeplywowJednostkaInna"" minOccurs=""0"">
					<xsd:annotation>
						<xsd:documentation>Rachunek przepływów pieniężnych</xsd:documentation>
					</xsd:annotation>
				</xsd:element>
				<xsd:choice>
					<xsd:element name=""DodatkoweInformacjeIObjasnieniaJednostkaMala"">
						<xsd:annotation>
							<xsd:documentation>Dodatkowe informacje i objaśnienia zgodnie z Załącznikiem Nr 5 do ustawy o rachunkowości</xsd:documentation>
						</xsd:annotation>
						<xsd:complexType>
							<xsd:sequence>
								<xsd:element name=""DodatkoweInformacjeIObjasnienia"" type=""dtsf:TInformacjaDodatkowa"" maxOccurs=""unbounded"">
									<xsd:annotation>
										<xsd:documentation>Dodatkowe informacje i objaśnienia</xsd:documentation>
									</xsd:annotation>
								</xsd:element>
								<xsd:element name=""InformacjaDodatkowaDotyczacaPodatkuDochodowego"" type=""dtsf:TInformacjaDodatkowaDotyczacaPodatkuDochodowego"" minOccurs=""0"">
									<xsd:annotation>
										<xsd:documentation>Rozliczenie różnicy pomiędzy podstawą opodatkowania podatkiem dochodowym a wynikiem finansowym (zyskiem, stratą) brutto</xsd:documentation>
									</xsd:annotation>
								</xsd:element>
							</xsd:sequence>
						</xsd:complexType>
					</xsd:element>
					<xsd:element name=""DodatkoweInformacjeIObjasnieniaJednostkaInna"">
						<xsd:annotation>
							<xsd:documentation>Dodatkowe informacje i objaśnienia zgodnie z Załącznikiem Nr 1 do ustawy o rachunkowości zawierająca informacje w zakresie nie mniejszym niż w Załączniku Nr 5 do ustawy o rachunkowości</xsd:documentation>
						</xsd:annotation>
						<xsd:complexType>
							<xsd:sequence>
								<xsd:element name=""DodatkoweInformacjeIObjasnienia"" type=""dtsf:TInformacjaDodatkowa"" maxOccurs=""unbounded"">
									<xsd:annotation>
										<xsd:documentation>Dodatkowe informacje i objaśnienia</xsd:documentation>
									</xsd:annotation>
								</xsd:element>
								<xsd:element name=""InformacjaDodatkowaDotyczacaPodatkuDochodowego"" type=""dtsf:TInformacjaDodatkowaDotyczacaPodatkuDochodowegoTys"">
									<xsd:annotation>
										<xsd:documentation>Rozliczenie różnicy pomiędzy podstawą opodatkowania podatkiem dochodowym a wynikiem finansowym (zyskiem, stratą) brutto</xsd:documentation>
									</xsd:annotation>
								</xsd:element>
							</xsd:sequence>
						</xsd:complexType>
					</xsd:element>
				</xsd:choice>
			</xsd:sequence>
		</xsd:complexType>
	</xsd:element>
</xsd:schema>";


        }

        public static string getXmlSource_JednostkaMalaWZlotych()
        {
            return @"<JednostkaMala xmlns=""http://www.mf.gov.pl/schematy/SF/DefinicjeTypySprawozdaniaFinansowe/2018/07/09/JednostkaMalaWZlotych"" xmlns:ns5=""http://www.mf.gov.pl/schematy/SF/DefinicjeTypySprawozdaniaFinansowe/2018/07/09/DefinicjeTypySprawozdaniaFinansowe/"" xmlns:ns6=""http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2016/01/25/eD/DefinicjeTypy/"" xmlns:ns7=""http://www.mf.gov.pl/schematy/SF/DefinicjeTypySprawozdaniaFinansowe/2018/07/09/JednostkaMalaStruktury"" xmlns:p4=""http://www.w3.org/2001/XMLSchema-instance"" p4:schemaLocation=""http://www.mf.gov.pl/schematy/SF/DefinicjeTypySprawozdaniaFinansowe/2018/07/09/JednostkaMalaWZlotych http://www.mf.gov.pl/documents/764034/6464789/JednostkaMalaWZlotych(1)_v1-0.xsd"">
  <Naglowek>
    <ns5:OkresOd>2018-01-01</ns5:OkresOd>
    <ns5:OkresDo>2018-12-31</ns5:OkresDo>
    <ns5:DataSporzadzenia>2019-01-08</ns5:DataSporzadzenia>
    <ns7:KodSprawozdania kodSystemowy=""SFJMAZ (1)"" wersjaSchemy=""1-0E"">SprFinJednostkaMalaWZlotych</ns7:KodSprawozdania>
    <ns7:WariantSprawozdania>1</ns7:WariantSprawozdania>
  </Naglowek>
  <WprowadzenieDoSprawozdaniaFinansowegoJednostkaMala>
    <P_1>
      <P_1A>
        <ns5:NazwaFirmy>PRO INSIGHT SPÓŁKA Z OGRANICZONĄ ODPOWIEDZIALNOŚCIĄ</ns5:NazwaFirmy>
        <ns5:Siedziba>
          <ns5:Wojewodztwo>MAZOWIECKIE</ns5:Wojewodztwo>
          <ns5:Powiat>WARSZAWA</ns5:Powiat>
          <ns5:Gmina>WARSZAWA</ns5:Gmina>
          <ns5:Miejscowosc>WARSZAWA</ns5:Miejscowosc>
        </ns5:Siedziba>
      </P_1A>
      <P_1B>
        <ns5:Adres>
          <ns6:KodKraju>PL</ns6:KodKraju>
          <ns6:Wojewodztwo>MAZOWIECKIE</ns6:Wojewodztwo>
          <ns6:Powiat>WARSZAWA</ns6:Powiat>
          <ns6:Gmina>WARSZAWA</ns6:Gmina>
          <ns6:Ulica>ALEJA ""SOLIDARNOŚCI""</ns6:Ulica>
          <ns6:NrDomu>117</ns6:NrDomu>
          <ns6:NrLokalu>219A</ns6:NrLokalu>
          <ns6:Miejscowosc>WARSZAWA</ns6:Miejscowosc>
          <ns6:KodPocztowy>00-140</ns6:KodPocztowy>
          <ns6:Poczta>WARSZAWA</ns6:Poczta>
        </ns5:Adres>
      </P_1B>
      <P_1C>
        <ns5:KRS>0000584219</ns5:KRS>
      </P_1C>
    </P_1>
    <P_3>
      <ns5:DataOd>2018-01-01</ns5:DataOd>
      <ns5:DataDo>2018-12-31</ns5:DataDo>
    </P_3>
    <P_4>Zastosowano uproszczony bilans i rachunek wyników zgodnie z załącznikiem nr 5 UoR.</P_4>
    <P_5>
      <P_5A>true</P_5A>
      <P_5B>true</P_5B>
    </P_5>
    <P_6>
      <P_6A>ZAPASY
Towary na dzień bilansowy wycenia się w cenach nabycia przy uwzględnieniu ich ostrożnej wyceny. Na
dzień bilansowy ceny towarów porównuje się z cenami sprzedaży netto. Ewentualne odpisy aktualizujące
wartość towarów wynikające z wyceny w cenach sprzedaży netto zaliczane odpowiednio do pozostałych
kosztów operacyjnych.
Rozchód towarów odbywa się według zasady cen przeciętnych, tj. ustalonych w wysokości średniej
ważonej cen zakupu.

ROZRACHUNKI - NALEŻNOŚCI
Na dzień bilansowy należności wycenia się w kwocie wymaganej zapłaty, z zachowaniem ostrożności.
Wartość należności aktualizuje się uwzględniając stopień prawdopodobieństwa ich zapłaty poprzez
dokonanie odpisu aktualizującego, w odniesieniu do należności:
- od dłużników postawionych w stan likwidacji lub upadłości- do wysokości należności nieobjętej
zabezpieczeniem,
- od dłużników w przypadku oddalenia wniosku o ogłoszenie upadłości - w wysokości 100% należności,
- kwestionowanych lub z których zapłatą dłużnik zalega a spłata należności nie jest prawdopodobna- do
wysokości należności nieobjętej zabezpieczeniem,
- stanowiących równowartość kwot podwyższających należności- do wysokości tych kwot
- przeterminowanych lub nieprzeterminowanych o znacznym stopniu prawdopodobieństwa nieściągalności,
w wysokości:
- według okresu przeterminowania:
na należności przeterminowane z przedziału powyżej 360 dni - odpis 100%
- według indywidualnej oceny:
na należności dochodzone na drodze sądowej - odpis 100%.
Odpisy aktualizujące wartość należności zalicza się do pozostałych kosztów operacyjnych i kosztów
finansowych, w zależności od rodzaju należności, której odpis dotyczył.

ROZRACHUNKI - ZOBOWIĄZANIA
Za zobowiązania uznaje się wynikający z przeszłych zdarzeń obowiązek wykonania świadczeń o
wiarygodnie określonej wartości, które spowodują wykorzystanie już posiadanych lub przyszłych aktywów
Spółki.
Na dzień powstania, zobowiązania wycenia się według wartości nominalnej. Na dzień bilansowy,
zobowiązania wycenia się w kwocie wymaganej zapłaty.

RÓŻNICE KURSOWE
Wyrażone w walutach obcych operacje gospodarcze ujmuje się w księgach rachunkowych na dzień ich
przeprowadzenia odpowiednio po kursie:
- kupna lub sprzedaży walut stosowanych przez bank, z którego usług korzysta Spółka- w przypadku
operacji sprzedaży lub kupna walut oraz operacji zapłaty należności lub zobowiązań,
- średnim ustalonym dla danej waluty przez Narodowy Bank Polski na ten dzień.
Na dzień bilansowy wycenia się wyrażone w walutach obcych:
- składniki aktywów - po kursie ustalonym dla danej waluty przez Narodowy Bank Polski na ten dzień,
- składniki pasywów - po kursie ustalonym dla danej waluty przez Narodowy Bank Polski na ten dzień.
Różnice kursowe, dotyczące innych niż inwestycje długoterminowe pozostałych aktywów i pasywów
wyrażonych w walutach obcych, powstałe na dzień wyceny oraz przy zapłacie należności i zobowiązań w
walutach obcych, zalicza się odpowiednio do przychodów lub kosztów finansowych. W uzasadnionych
przypadkach różnice kursowe zalicza się do ceny nabycia lub kosztu wytworzenia środków trwałych,
środków trwałych w budowie lub wartości niematerialnych i prawnych.

REZERWY
Rezerwy są to zobowiązania, których termin wymagalności lub kwota nie są pewne. Tworzy się je na
pewne lub prawdopodobne przyszłe zobowiązania w ciężar pozostałych kosztów operacyjnych, kosztów
finansowych, strat nadzwyczajnych, zależnie od okoliczności z którymi przyszłe zobowiązania się wiążą.


</P_6A>
      <P_6B>WARTOŚCI NIEMATERIALNE I PRAWNE
W pozycji tej ujmuje się nabyte przez jednostkę, zaliczane do aktywów trwałych, prawa majątkowe
nadające się do gospodarczego wykorzystania, o przewidywanym okresie ekonomicznej użyteczności
dłuższym niż rok, przeznaczone do używania na potrzeby jednostki.
Wartości niematerialne i prawne wycenia się według cen nabycia, pomniejszonych o odpisy amortyzacyjne
lub umorzeniowe oraz o odpisy z tytułu trwałej utraty wartości
Stawki amortyzacyjne ustala się z uwzględnieniem okresu ekonomicznej użyteczności wartości
niematerialnych i prawnych. Odzwierciedlają faktyczny okres ich użytkowania. Wartości niematerialne
amortyzuje się metoda liniową przy zastosowaniu następujących stawek:
- oprogramowanie - 50%,
- prawa autorskie - 50%,
- know-how - 20%,
- koszty zakończonych prac rozwojowych - 33%,
- wartość firmy - 20%,
Wartości niematerialne i prawne o wartości jednostkowej do 10 000 zł amortyzowane są jednorazowo.
Rozpoczęcie amortyzacji następuje nie wcześniej niż po przyjęciu wartości niematerialnych i prawnych do
używania. 

ŚRODKI TRWAŁE
W pozycji tej ujmuje się rzeczowe aktywa trwałe i zrównane z nimi, o przewidywanym okresie
ekonomicznej użyteczności dłuższym niż rok, kompletne, zdatne do użytku i przeznaczone na potrzeby
jednostki.
Środki trwałe zakupione wycenia się według cen nabycia lub kosztu wytworzenia. Wartość początkowa
środków trwałych podlega podwyższeniu o wartość nakładów poniesionych na ich ulepszenie (przebudowę,
rozbudowę, modernizację, rekonstrukcję ).
Wartość początkową środków trwałych pomniejsza się o odpisy amortyzacyjne. Stawki amortyzacyjne
ustala się z uwzględnieniem okresu użyteczności środków trwałych i odzwierciedlają faktyczne zużycie
środków trwałych.
W poszczególnych grupach stosuje się następujące stawki i metody amortyzacji:
- budynki i budowle 2,5% - am. liniowa,
- grupa IV 10%-30% - am. liniowa,
- grupa VII 20% - am. liniowa,
- grupa VIII 20% - am. liniowa.
Środki trwałe o wartości jednostkowej do 10 000 zł amortyzuje się jednorazowo. Rozpoczęcie amortyzacji
następuje nie wcześniej niż po przyjęciu środka trwałego do używania.
Środki trwałe używane na podstawie umów najmu, dzierżawy, leasingu lub innej o podobnym charakterze
nie są zaliczane, biorąc pod uwagę zasadę istotności, do majątku jednostki. 
W przypadku wystąpienia przyczyn powodujących trwałą utratę wartości środków trwałych stosowane
odpisy aktualizujące pomniejszają wartość bilansową środków trwałych. Odpisy aktualizujące
spowodowane trwałą utratą wartości obciążają pozostałe koszty operacyjne.

ŚRODKI TRWAŁE W BUDOWIE
W pozycji tej ujmuje się zaliczane do aktywów trwałych środki trwałe w okresie ich budowy, montażu lub
ulepszenia już istniejącego środka trwałego.
Cena nabycia i koszt wytworzenia środków trwałych w budowie zawiera się ogół ich kosztów poniesionych
przez jednostkę za okres budowy, montażu przystosowania i ulepszenia, do dnia bilansowego lub przyjęcia
do używania, w tym również:
- niepodlegający odliczeniu podatek od towarów i usług oraz podatek akcyzowy,
- koszt obsługi zobowiązań zaciągniętych w celu ich finansowania i związane z nimi różnice kursowe,
pomniejszony o przychody z tego tytułu.
Wartość środków trwałych w budowie pomniejsza się o odpisy aktualizujące w wypadku wystąpienia
okoliczności wskazujących na trwałą utratę ich wartości.

ŚRODKI PIENIĘŻNE
Wycenia się według wartości nominalnej.

ZOBOWIĄZANIA WARUNKOWE - POZABILANSOWE
Za zobowiązania warunkowe Spółka uznaje potencjalny przyszły ob</P_6B>
      <P_6B>owiązek wykonania świadczeń, których
powstanie jest uzależnione od zaistnienia określonych zdarzeń.

ROZLICZENIE MIĘDZYOKRESOWE
Rozliczenia międzyokresowe czynne obejmują rozliczenia:
- długoterminowe, które dotyczą przyszłych okresów sprawozdawczych i trwają dłużej niż 12 miesięcy od
dnia bilansowego,
- krótkoterminowe, które dotyczą przyszłych okresów sprawozdawczych i trwają nie dłużej niż 12 miesięcy
od dnia bilansowego.
Odpisy czynnych i biernych rozliczeń międzyokresowych kosztów następują stosownie do upływu czasu lub
wielkości świadczeń.

PODATEK ODROCZONY
Spółka ustala aktywa i rezerwę z tytułu odroczonego podatku dochodowego w związku z występowaniem
przejściowych różnic między wykazywaną w księgach rachunkowych wartością aktywów i pasywów a ich
wartością podatkową oraz stratą możliwą do odliczenia w przyszłości.
Przy ustalaniu wysokości aktywów i rezerwy z tytułu odroczonego podatku dochodowego uwzględnia się
stawki podatku dochodowego obowiązujące w roku powstania obowiązku podatkowego.

KAPITAŁY WŁASNE
Na dzień bilansowy kapitał zakładowy wykazuje się w wysokości określonej w umowie spółki i wpisanej w
KRS.
Kapitał zapasowy Spółki tworzony jest z podziału zysku. Przeznaczenie kapitału zapasowego określa
umowa Spółki.
Zysk lub strata z lat ubiegłych odzwierciedla nierozliczony wynik z lat poprzednich pozostający do decyzji
Zgromadzenia Wspólników, a także skutki ewentualnych korekt zmian zasad rachunkowości i błędów
podstawowych dotyczących lat poprzednich, a ujawnionych w bieżącym roku obrotowym.



</P_6B>
      <P_6C>PRZYCHODY I ZYSKI
Za przychody i zyski Spółka uznaje uprawdopodobnione powstanie w okresie sprawozdawczym korzyści
ekonomicznych, o wiarygodnie określonej wartości, w formie zwiększenia wartości aktywów, albo
zmniejszenia wartości zobowiązań, które doprowadzą do wzrostu kapitału własnego lub zmniejszenia jego
niedoboru w inny sposób niż wniesienie wkładów przez udziałowców lub właścicieli.

KOSZTY I STRATY
Przez koszty i straty jednostka rozumie uprawdopodobnione zmniejszenia w okresie sprawozdawczym
korzyści ekonomicznych, o wiarygodnie określonej wartości, w formie zmniejszenia wartości aktywów, albo
zwiększenia wartości zobowiązań i rezerw, które doprowadzą do zmniejszenia kapitału własnego lub
zwiększenia jego niedoboru w inny sposób niż wycofanie środków przez udziałowców lub właścicieli.

Na wynik finansowy netto składają się:
- wynik działalności operacyjnej, w tym z tytułu pozostałych przychodów i kosztów operacyjnych ( pośrednio
związanych z działalnością operacyjną jednostki ),
wynik operacji finansowych,
- wynik operacji nadzwyczajnych ( powstałych na skutek zdarzeń trudnych do przewidzenia, poza
działalnością operacyjną jednostki i niezwiązane z ogólnym ryzykiem jej prowadzenia ),
- obowiązkowe obciążenia wyniku finansowego z tytułu podatku dochodowego, którego podatnikiem jest
jednostka i płatności z nim zrównane, na podstawie odrębnych przepisów.</P_6C>
    </P_6>
  </WprowadzenieDoSprawozdaniaFinansowegoJednostkaMala>
  <BilansJednostkaMala>
    <ns7:Aktywa>
      <ns5:KwotaA>4303.94</ns5:KwotaA>
      <ns5:KwotaB>4403.94</ns5:KwotaB>
      <ns7:Aktywa_A>
        <ns5:KwotaA>0</ns5:KwotaA>
        <ns5:KwotaB>0</ns5:KwotaB>
        <ns7:Aktywa_A_I>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
        </ns7:Aktywa_A_I>
        <ns7:Aktywa_A_II>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
          <ns7:Aktywa_A_II_1>
            <ns5:KwotaA>0</ns5:KwotaA>
            <ns5:KwotaB>0</ns5:KwotaB>
          </ns7:Aktywa_A_II_1>
          <ns7:Aktywa_A_II_2>
            <ns5:KwotaA>0</ns5:KwotaA>
            <ns5:KwotaB>0</ns5:KwotaB>
          </ns7:Aktywa_A_II_2>
        </ns7:Aktywa_A_II>
        <ns7:Aktywa_A_III>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
        </ns7:Aktywa_A_III>
        <ns7:Aktywa_A_IV>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
          <ns7:Aktywa_A_IV_1>
            <ns5:KwotaA>0</ns5:KwotaA>
            <ns5:KwotaB>0</ns5:KwotaB>
          </ns7:Aktywa_A_IV_1>
          <ns7:Aktywa_A_IV_2>
            <ns5:KwotaA>0</ns5:KwotaA>
            <ns5:KwotaB>0</ns5:KwotaB>
          </ns7:Aktywa_A_IV_2>
        </ns7:Aktywa_A_IV>
        <ns7:Aktywa_A_V>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
        </ns7:Aktywa_A_V>
      </ns7:Aktywa_A>
      <ns7:Aktywa_B>
        <ns5:KwotaA>4303.94</ns5:KwotaA>
        <ns5:KwotaB>4403.94</ns5:KwotaB>
        <ns7:Aktywa_B_I>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
        </ns7:Aktywa_B_I>
        <ns7:Aktywa_B_II>
          <ns5:KwotaA>73</ns5:KwotaA>
          <ns5:KwotaB>50</ns5:KwotaB>
          <ns7:Aktywa_B_II_A>
            <ns5:KwotaA>0</ns5:KwotaA>
            <ns5:KwotaB>0</ns5:KwotaB>
            <ns7:Aktywa_B_II_A_1>
              <ns5:KwotaA>0</ns5:KwotaA>
              <ns5:KwotaB>0</ns5:KwotaB>
            </ns7:Aktywa_B_II_A_1>
            <ns7:Aktywa_B_II_A_2>
              <ns5:KwotaA>0</ns5:KwotaA>
              <ns5:KwotaB>0</ns5:KwotaB>
            </ns7:Aktywa_B_II_A_2>
          </ns7:Aktywa_B_II_A>
          <ns7:PozycjaUszczegolawiajaca_2>
            <ns5:NazwaPozycji>b) z tytułu podatków, dotacji, ceł, ubezpieczeń społecznych i zdrowotnych oraz innych świadczeń</ns5:NazwaPozycji>
            <ns5:KwotyPozycji>
              <ns5:KwotaA>73</ns5:KwotaA>
              <ns5:KwotaB>50</ns5:KwotaB>
            </ns5:KwotyPozycji>
          </ns7:PozycjaUszczegolawiajaca_2>
        </ns7:Aktywa_B_II>
        <ns7:Aktywa_B_III>
          <ns5:KwotaA>4230.94</ns5:KwotaA>
          <ns5:KwotaB>4353.94</ns5:KwotaB>
          <ns7:Aktywa_B_III_A>
            <ns5:KwotaA>4230.94</ns5:KwotaA>
            <ns5:KwotaB>4353.94</ns5:KwotaB>
            <ns7:Aktywa_B_III_A_1>
              <ns5:KwotaA>4230.94</ns5:KwotaA>
              <ns5:KwotaB>4353.94</ns5:KwotaB>
            </ns7:Aktywa_B_III_A_1>
          </ns7:Aktywa_B_III_A>
        </ns7:Aktywa_B_III>
        <ns7:Aktywa_B_IV>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
        </ns7:Aktywa_B_IV>
      </ns7:Aktywa_B>
      <ns7:Aktywa_C>
        <ns5:KwotaA>0</ns5:KwotaA>
        <ns5:KwotaB>0</ns5:KwotaB>
      </ns7:Aktywa_C>
      <ns7:Aktywa_D>
        <ns5:KwotaA>0</ns5:KwotaA>
        <ns5:KwotaB>0</ns5:KwotaB>
      </ns7:Aktywa_D>
    </ns7:Aktywa>
    <ns7:Pasywa>
      <ns5:KwotaA>4303.94</ns5:KwotaA>
      <ns5:KwotaB>4403.94</ns5:KwotaB>
      <ns7:Pasywa_A>
        <ns5:KwotaA>4303.94</ns5:KwotaA>
        <ns5:KwotaB>4403.94</ns5:KwotaB>
        <ns7:Pasywa_A_I>
          <ns5:KwotaA>5000</ns5:KwotaA>
          <ns5:KwotaB>5000</ns5:KwotaB>
        </ns7:Pasywa_A_I>
        <ns7:Pasywa_A_II>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
          <ns7:Pasywa_A_II_1>
            <ns5:KwotaA>0</ns5:KwotaA>
            <ns5:KwotaB>0</ns5:KwotaB>
          </ns7:Pasywa_A_II_1>
        </ns7:Pasywa_A_II>
        <ns7:Pasywa_A_III>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
          <ns7:Pasywa_A_III_1>
            <ns5:KwotaA>0</ns5:KwotaA>
            <ns5:KwotaB>0</ns5:KwotaB>
          </ns7:Pasywa_A_III_1>
        </ns7:Pasywa_A_III>
        <ns7:Pasywa_A_IV>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
        </ns7:Pasywa_A_IV>
        <ns7:Pasywa_A_V>
          <ns5:KwotaA>-596.06</ns5:KwotaA>
          <ns5:KwotaB>-496.06</ns5:KwotaB>
        </ns7:Pasywa_A_V>
        <ns7:Pasywa_A_VI>
          <ns5:KwotaA>-100</ns5:KwotaA>
          <ns5:KwotaB>-100</ns5:KwotaB>
        </ns7:Pasywa_A_VI>
        <ns7:Pasywa_A_VII>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
        </ns7:Pasywa_A_VII>
      </ns7:Pasywa_A>
      <ns7:Pasywa_B>
        <ns5:KwotaA>0</ns5:KwotaA>
        <ns5:KwotaB>0</ns5:KwotaB>
        <ns7:Pasywa_B_I>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
          <ns7:Pasywa_B_I_1>
            <ns5:KwotaA>0</ns5:KwotaA>
            <ns5:KwotaB>0</ns5:KwotaB>
          </ns7:Pasywa_B_I_1>
        </ns7:Pasywa_B_I>
        <ns7:Pasywa_B_II>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
          <ns7:Pasywa_B_II_1>
            <ns5:KwotaA>0</ns5:KwotaA>
            <ns5:KwotaB>0</ns5:KwotaB>
          </ns7:Pasywa_B_II_1>
        </ns7:Pasywa_B_II>
        <ns7:Pasywa_B_III>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
          <ns7:Pasywa_B_III_A>
            <ns5:KwotaA>0</ns5:KwotaA>
            <ns5:KwotaB>0</ns5:KwotaB>
          </ns7:Pasywa_B_III_A>
          <ns7:Pasywa_B_III_B>
            <ns5:KwotaA>0</ns5:KwotaA>
            <ns5:KwotaB>0</ns5:KwotaB>
            <ns7:Pasywa_B_III_B_1>
              <ns5:KwotaA>0</ns5:KwotaA>
              <ns5:KwotaB>0</ns5:KwotaB>
            </ns7:Pasywa_B_III_B_1>
            <ns7:Pasywa_B_III_B_2>
              <ns5:KwotaA>0</ns5:KwotaA>
              <ns5:KwotaB>0</ns5:KwotaB>
            </ns7:Pasywa_B_III_B_2>
          </ns7:Pasywa_B_III_B>
          <ns7:Pasywa_B_III_C>
            <ns5:KwotaA>0</ns5:KwotaA>
            <ns5:KwotaB>0</ns5:KwotaB>
          </ns7:Pasywa_B_III_C>
        </ns7:Pasywa_B_III>
        <ns7:Pasywa_B_IV>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
        </ns7:Pasywa_B_IV>
      </ns7:Pasywa_B>
    </ns7:Pasywa>
  </BilansJednostkaMala>
  <RZiSJednostkaMala>
    <ns7:RZiSPor>
      <ns7:A>
        <ns5:KwotaA>0</ns5:KwotaA>
        <ns5:KwotaB>0</ns5:KwotaB>
        <ns7:A_I>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
        </ns7:A_I>
        <ns7:A_II>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
        </ns7:A_II>
        <ns7:A_III>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
        </ns7:A_III>
      </ns7:A>
      <ns7:B>
        <ns5:KwotaA>100</ns5:KwotaA>
        <ns5:KwotaB>100</ns5:KwotaB>
        <ns7:B_I>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
        </ns7:B_I>
        <ns7:B_II>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
        </ns7:B_II>
        <ns7:B_III>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
        </ns7:B_III>
        <ns7:B_IV>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
        </ns7:B_IV>
        <ns7:B_V>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
          <ns7:B_V_1>
            <ns5:KwotaA>0</ns5:KwotaA>
            <ns5:KwotaB>0</ns5:KwotaB>
          </ns7:B_V_1>
        </ns7:B_V>
        <ns7:B_VI>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
          <ns7:B_VI_1>
            <ns5:KwotaA>0</ns5:KwotaA>
            <ns5:KwotaB>0</ns5:KwotaB>
          </ns7:B_VI_1>
        </ns7:B_VI>
      </ns7:B>
      <ns7:C>
        <ns5:KwotaA>-100</ns5:KwotaA>
        <ns5:KwotaB>-100</ns5:KwotaB>
      </ns7:C>
      <ns7:D>
        <ns5:KwotaA>0</ns5:KwotaA>
        <ns5:KwotaB>0</ns5:KwotaB>
        <ns7:D_1>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
        </ns7:D_1>
      </ns7:D>
      <ns7:E>
        <ns5:KwotaA>0</ns5:KwotaA>
        <ns5:KwotaB>0</ns5:KwotaB>
        <ns7:E_1>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
        </ns7:E_1>
      </ns7:E>
      <ns7:F>
        <ns5:KwotaA>0</ns5:KwotaA>
        <ns5:KwotaB>0</ns5:KwotaB>
        <ns7:F_I>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
          <ns7:F_I_1>
            <ns5:KwotaA>0</ns5:KwotaA>
            <ns5:KwotaB>0</ns5:KwotaB>
          </ns7:F_I_1>
        </ns7:F_I>
        <ns7:F_II>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
          <ns7:F_II_1>
            <ns5:KwotaA>0</ns5:KwotaA>
            <ns5:KwotaB>0</ns5:KwotaB>
          </ns7:F_II_1>
        </ns7:F_II>
        <ns7:F_III>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
          <ns7:F_III_1>
            <ns5:KwotaA>0</ns5:KwotaA>
            <ns5:KwotaB>0</ns5:KwotaB>
          </ns7:F_III_1>
        </ns7:F_III>
        <ns7:F_IV>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
        </ns7:F_IV>
      </ns7:F>
      <ns7:G>
        <ns5:KwotaA>0</ns5:KwotaA>
        <ns5:KwotaB>0</ns5:KwotaB>
        <ns7:G_I>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
          <ns7:G_I_1>
            <ns5:KwotaA>0</ns5:KwotaA>
            <ns5:KwotaB>0</ns5:KwotaB>
          </ns7:G_I_1>
        </ns7:G_I>
        <ns7:G_II>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
          <ns7:G_II_1>
            <ns5:KwotaA>0</ns5:KwotaA>
            <ns5:KwotaB>0</ns5:KwotaB>
          </ns7:G_II_1>
        </ns7:G_II>
        <ns7:G_III>
          <ns5:KwotaA>0</ns5:KwotaA>
          <ns5:KwotaB>0</ns5:KwotaB>
        </ns7:G_III>
      </ns7:G>
      <ns7:H>
        <ns5:KwotaA>-100</ns5:KwotaA>
        <ns5:KwotaB>-100</ns5:KwotaB>
      </ns7:H>
      <ns7:I>
        <ns5:KwotaA>0</ns5:KwotaA>
        <ns5:KwotaB>0</ns5:KwotaB>
      </ns7:I>
      <ns7:J>
        <ns5:KwotaA>-100</ns5:KwotaA>
        <ns5:KwotaB>-100</ns5:KwotaB>
      </ns7:J>
    </ns7:RZiSPor>
  </RZiSJednostkaMala>
  <DodatkoweInformacjeIObjasnieniaJednostkaMala>
    <DodatkoweInformacjeIObjasnienia>
      <ns5:Opis>NOTA 1. Dane o strukturze wlasnosci kapitalu podstawowego</ns5:Opis>
      <ns5:Plik>
        <ns5:Nazwa>NOTA1.pdf</ns5:Nazwa>
        <ns5:Zawartosc>JVBERi0xLjUKJb/3ov4KMiAwIG9iago8PCAvTGluZWFyaXplZCAxIC9MIDIzODY5IC9IIFsgNjg3IDEyNyBdIC9PIDYgL0UgMjM1OTQgL04gMSAvVCAyMzU5MyA+PgplbmRvYmoKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKMyAwIG9iago8PCAvVHlwZSAvWFJlZiAvTGVuZ3RoIDUxIC9GaWx0ZXIgL0ZsYXRlRGVjb2RlIC9EZWNvZGVQYXJtcyA8PCAvQ29sdW1ucyA0IC9QcmVkaWN0b3IgMTIgPj4gL1cgWyAxIDIgMSBdIC9JbmRleCBbIDIgMTUgXSAvSW5mbyAxMSAwIFIgL1Jvb3QgNCAwIFIgL1NpemUgMTcgL1ByZXYgMjM1OTQgICAgICAgICAgICAgICAgIC9JRCBbPGQyYWUzZDljN2FjMjVhYTg0NjVmNDdkOTAxMGRiNmI0PjxkMmFlM2Q5YzdhYzI1YWE4NDY1ZjQ3ZDkwMTBkYjZiND5dID4+CnN0cmVhbQp4nGNiZOBnYGJgOAkkmJaCWEZAgrEexFoPJJirgURgDEj2KgMT4/JGkAQDIzYCABT1BjAKZW5kc3RyZWFtCmVuZG9iagogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCjQgMCBvYmoKPDwgL1BhZ2VzIDEzIDAgUiAvVHlwZSAvQ2F0YWxvZyA+PgplbmRvYmoKNSAwIG9iago8PCAvRmlsdGVyIC9GbGF0ZURlY29kZSAvUyAzNiAvTGVuZ3RoIDUwID4+CnN0cmVhbQp4nGNgYGBlYGBazwAEEX8Y4ADKZgZiFoQoSC0YMzCsZ+BnYGA3as0QSrBzYAAAz80GRAplbmRzdHJlYW0KZW5kb2JqCjYgMCBvYmoKPDwgL0NvbnRlbnRzIDcgMCBSIC9NZWRpYUJveCBbIDAgMCA1OTYgODQzIF0gL1BhcmVudCAxMyAwIFIgL1Jlc291cmNlcyA8PCAvRXh0R1N0YXRlIDw8IC9HMCAxNCAwIFIgPj4gL0ZvbnQgPDwgL0YyIDEyIDAgUiA+PiA+PiAvU3RydWN0UGFyZW50cyAwIC9UeXBlIC9QYWdlID4+CmVuZG9iago3IDAgb2JqCjw8IC9GaWx0ZXIgL0ZsYXRlRGVjb2RlIC9MZW5ndGggNTY0ID4+CnN0cmVhbQp4nJ1W24rbQAx991fMc6Gzuo5moCxsN84+twT6AW13odCFbv8fOrHTeKZYxG4MwZaic6QjjRUMUK/3WL+ycPj6c/g1nC1aUjVQLGqo4e378OVdeK2+aAqJitEU1z/VYAzn6/NTmG/eXoa7JwgvvydMKxIQic9wz/9tqYlNJiBbfgRnzvmmcn48DXdHCigxaamfHE7PAy6VQjjVVOn8hPX+W/gAgHIfTj+GHFGr0XK4OnByjKeNsCItLOUpukQzTSB0heXiOPwInRwIUWtnrHbrrwd48kgEw2JqC1byQkh2s7iJufScPBJ2HI/HmR2jVOUBFqwRlv5AIV3SGhsslrxaohrber4Fl6wOF4qilOpc3w6gNltMC/VD44Cs4sibS7qdLqMb8tjkVXV0+t6GdDVysvVa+pQ9fgRviDYJ1pGQuRl7s+Kqzxf1OWZmUtqglzfafVpabuvoc3j9pWPz5iHe0HdXRsSDdxi6iaCsNxPuuuudHwLCBQrh4sgZEurqPPwTIeu1I++zdwPXqq55PUBs3a6zgBaZMjZvGZlV4kiaMMnSC3EqGNEJsOIwP3jtdou76L11NaVi0UrKuV1QF/B9642vSZOzNd2d0JfZ7hfxNrBqM1aijcM8EictxNFpb9rQdikbVq6iV4Unlc5dRIoE2QzybXaXpNWw25JydMaxg2oPSPaQDg0Sy7oizTsfPaC9Z7y1d0Aes3eWx33/6JhTrbWd+p1nhgUiowfxqV5/AE1+Z35lbmRzdHJlYW0KZW5kb2JqCjggMCBvYmoKPDwgL0ZpbHRlciAvRmxhdGVEZWNvZGUgL0xlbmd0aDEgNDA1MjQgL0xlbmd0aCAyMDc0MCA+PgpzdHJlYW0KeJzsvQl8FMXWN3yqunq6Z++ZZDJLlplJMpONJWQhBCIZlgCC7GswkbDJLoQggqKEK/siiIrgxiIqi8iQBAyoV0RcwAVcwAUEVHC9CF4Rr4bMfKd6JhG493nufZ/3fb/f9/t+TOffp9auqlOnzjlVPQNAAEAP1SBA9ugpI6eNq+u9EsD2KUDMA6NnzvCsmfbhTID2DgBN9u3Txk25K23rrwAZ4wHE4LjJs2+vG/23dwAG/Q2gVb/xY0eOOVx4eRU+8WVE2/GYYJ1i2YbhS4jU8VNmzDq8qSoAQNoBxG6YPHX0SPhy0RyAQA+Mb54yctY07fuGhzAf2wPPHSOnjH2NjCsCuB8Rb5w2fey0H3dUfgXQGvN1jcD7Tj/42z0Lz64bYS76VY6XgX82fZ2Wyenum2re/mNn4zilvXwLRrVYnqgF8C51DPWBLgr8sTOUq7SPpjd/xHSeIqbjrRhGgwgUFGgNnQGYAdsVgGqiVWhhM4L0E7iNVYENcbOUCHeJQ2AYWQTD6TaYwyEkQoA9D9Ox7DaMd0K6j9fF8oMRpxFFiCEIVzStN2IkYiCPY9m9vC4+Yxp/jkqrYLjshqnikHAjtrdGfAtuRzyF4U3sa9iiKYQpGN+M9V5lAAW8DNZZo9kGazH9CcwfjWlPIR2G8Y0YLsN62dGwVloBTk4RGkzPwOcsi443TXgN2rKq8Jc4llJ8Zk/EQmyjH9JuiF5YJgZpZ8Qi8hYsJm+FN2E+Urgf21/E0xFdo7QHPmcB5hdjvVSM349hF/ZDg9SM8CLS6fNQSGPhZaStcfxDI+NGvAXj+Zibx4T9j/bpnxHpY6+rgW2+gkihheFzSLVX9e163H8dbhZyoRrpJEQ8oj99D6awW4Agv9aJ50DgQMnkfDqFuImNgT4YJ9jPgWIdPMbjiN4qqsKN7AnYIFyCdph3t2YNjmMM8rsN4jK0pn+DlhofzEX56orPn4d4Cp/5nSoPY2AQtt8KaS47p8rQQsRybOtCE584bzA+D+d1ALZ1ha8YrD8Q0R3npRoxmfcH22/Nec7nnQwJFWLZs1imjAPT7Spw7FwmeR1eH5/li8rhpj8pbMIyK5CvZ5AyhI33oQmqnEWBeW/ic5wIDSIR0QpxDrEJMQnRHvEiIh3bBmxXUOUVZYbLpiofKBviW8hD7Jsqs5ExPKXOZ2TNbIw+i7fj1TwPk6Lw8mfy9cJlFvuyq+nZfE1xmWmiqnxP4nJPfubj5DLVTHHtsR+hO++DugZRtpooX3fYZ74e1tDBsBjpYyjH93OZ5f1ropwvXNZUnuCaiNKiq8aara4RpAJASlTW72+iTbxopuNhMz6zQjMKdcoG6MFmQA/hQRjFLkJXIQNaidmYhuPBskH6IwyQ90MuzmVfjK+7jq7lkI6RieJ+HOd25OcxeBJ5WsmO0WR2jIji9vD3IpBD4nZ6nxr+J3o9yP5IHqccV+f9r6b/T0CPi9tRZ24P/yAeC4dxPKv5mpB+JNkITxPF9BpENSJTziJr5UmkXhoMigZtG2IqC0B7MQAFbD/Ojw31PK4FTB8sfgmvCitgCTsW/oxUQzU9BgslG4yka1CnYVv0ONzPwZ+PdNpVcnSNzF0vS020SV6vp1znR2XKjVSD6+/9KM5GcRnxK8rR0yTSRgHXz6p9QB2NWBiR1/AfzfJ5CJ5BuqxJPq+T00nXyafherm8nqq2BfV70zrFfixpGj/Xj1zHcR3J9RzXM03lr6dX1V9Kt6Eccz38HgyPruvkKHpiH7+Krn3UwzjfQ8NhTbfwc5q68BbBGt6iycHwpwgx/ByOe1azTR0WDkXtaUaTLY2kg77Jjoq5MCWqzzar+ubv8LBqR4eo/dNqdsJcsQHnHXWg2t8N0TWI/MR+T2IVyPPHYDmOwykswvWI6YgyzhN1LgAc3C5wmyg8gnzmtmgF3C+cQH+B180Fi2ovimEo9v2QmoY2lVOeJg6FTZofIYcNRl27H8bwueLj4P3hcy/fCUbZhnriGLRhW7GMDXRYboPKgwA8p8oFrzsJ/SLkhTQaJJTZPliGP2+jWicA1ig/Nqu8UOujL8Lli/MCn6mxwQDVn/gR1ouDYSiuoY1SNWzUDMY1Z4Mt+IxnsN5g3hes51Lt9SNwK66vxaibFqPOAVX+h4cbhO04nlmo1xFCNfJoOzjEauThJHXsXVlExy7i60fYBn4uI5pHUA9zf+IRWMqyoEQzCVZg2goR9SS2uwzT5uP6zca1uwTru6N6G7DtJZjO6xZzX4b7CHy9SAGI0VSrfgCofeB+CrYvfA8bhZ6wGOW4k/wI8mEBtER7QVD2khBtIlDj90WxPAI1TYlQ4hUUuFdNz4UP6TZBj3LLbeheNg8msCGQI7QBJ7NAS/YBrtXf4XHBDCPYYXic1cNyHmcxkC4Ecfx16Fvy9CPQj6fTDzG+FoazIqy/GO5gI6BK2IWy9zHo2O0411hPfADlJBXr/x2fGwX5GoYLQ3BtLcTw7+HneTm1jbrwUA7WA1qq9a6C2tcmXNdn2gv51hPnFPvLw9f0F/va3M+mPv6L/qnj5M/FerwMexxwvxA+ifBFaKg/XQHbERvo59BF6A2zyRZUME9AN3IO8UQUO6CHSnch+qONzydzEK1YPryImIfhFkj/itgZiaPvlg8nEAvw2a8hreX7Ag7aGdpyimlPIdYi3mnKuxq8rX+VfjXEeLg2vhuqOcilcCPH9eWRz22xvbbsJuQnAmVxFYdmLgyXZuL8pWF6Ej7zuji2k8N2w8R/159/B3IEslUeRhC4eoxN84E07j/Ayauoh9Oobfjf6t//BDi/cxHlKn9/AltUhkzkOCQjHYJ0iHAnzOLAeEuMlzbxk+DuV8UWeEhNb56/SDrKCm4p4abr06+PXz+v/y5Oa+GZq9EkB83ysBrmc7BiLI+4Pi4fgvkcmjcw741/jrPn/g2GQ6bwmNonUGXsurimL9pMBE3FvrrUOss5muNHcC0jeFm1vhFWcKhrF0HrYAJHc34+6m/EVXxty/mKbar5TfPTNC/Xzw/2L8DeRwxHW/E+ZCMdiLRTE22W76i+uEbm+0fkvTnOdcm568r8uSb+XBtHuK3518/8/xNw7RxGvIV48/92W1zLcB2hcD1xEv2QYvQjj6F/civcD9CIuuRKa8SzqIcGIf0E09B6hzIQRgxbMG0c0icBGn7F8HRMPxZBmLJ42BD1K52YtidaV44+b2CkfsPbAH+gRP2xM1K/YRtiIoZ/RtyL4S+QvoZ0LZb/AevNR3ogkt84AuMzES9j/EeMT0YMw/AqpDakLRAxCCvWX8PB/ZF/2of+H6f/ev/xn1L0WUZjP938zAvpnOv3EP8xbZrPf0Ov32s0zf+/o1edGVxHI3zAPdNX6PcFr977/Hd7nCaK8xm6GmxwuBF9SgP3o7kvy/1n1X+MUnX/pvqx2C5AbBPlvjP3X7nvzP1XpBuRLtaIan8G830+7xeoJkVFgrogQDsAYxjSDQOmbcvPYPkxKLSD+8hcspKsJhtJkJwkYVpK36KH6BcCEQRBK6QI9wlLheXCRuF9ZmB9WRkbwR5ij7In2dOslr3EPmPfi3vF18UfxEsagyZe49a01wzQTNJM0VRq7tMs1KzVbNZs1ezUvKs5pvk9aUHS7x6zx+ZJ8iR7/J5WnmxPrqe9p8jT0dPVM9Uz17PZ85znea/ojfHGeZO9fm8r7yDvbd5HvFuSabIm2ZxsTbYlu5LdyRnJWck9kkcmj02hKUqK1wc+6jP4FF+sz+FL8KX6WvjyfEW+yb5q33zfYt9y30O+jb7nfTW+fb6XfQd97/iO+D7zfeMv8gf8nf0V/tH+2/2TvhO/c3zX/iK92KaBNnga2jYUNXRs6NTQtaFvQ2nDvQ3LGh5pCF8Z1Vjc+PfQlfCVcJifUMMGlXMbyE7yHvkDOfcmcu5TAZo5Nx8594DwNCPMxPqz29gqtoY9xjaxF1g9+5R9JwbFl8Sj4sUo57yagKbiX3LuYlJ10gaPwRPjsXs8yLlM5FyOpzDKuYnIuaeRc9uu4dxA763eVc2csyDnnMlJUc5VJI9ROef5LzjXr5lzq3wbfNuaOXcYOfcpcq59M+fG+id+R1TOkYusgSDnMhvaIecCDV0aujUMabi7YWnDAw1XrtzW2BE5V805F/4aBfORcCw9TF8RWodP0ndxRZhRIleTu8gkMv3KBoxP4DIbygplhjJC6RicA3fDTJgM4+EW6Hjliysnrxy98s6VM1c+vHKEl7yy7sraK89f2YjXQ1fmXpl/5S9XJlzJBfi6HOCrk5FT/TMLEI98eeuZ+Wd+/3LLmbsw9iIC9eqZpWfu/fLO0xNPzz6z7+sWZx44veX0mlNrTm06tQzg1LO87mn7qcpTqJlPZZ8KnMo9lXqy28mSk0UnC0+2PZl7Mvtkxsnkk/EnY0+SEz+d+PHEdyfOnfiK1zrx5olXT/z1BLZy4o0Tz5zYeaLkROcTnU6knkg+4T2R5Nrv+sP1pfJX9PT+Kj0rPSk9IT0uPSatk9ZKh6Qd0kZpPdqv7zUdRdydCqP52iVtr31PQb+J4Jr4RSGuKS6Mgf/mI/RBTfOvcx5APIUeUR82gFUgHXV1LrsNcXsE/9WH9eNgA6KxPv9dP66r6WfpzeHU/7ak7r/MueWaqABPw3xYINwGa+AbWAgPwDJ4ErbCZnQRliJb74eH4CL8DCvgUVgMB+AkXICnYBv8An+HS7AJnoe34U3YAaNgNKyCMXAYxsJbcAjeh3fgXXgPvoXb4QM4AkfhBRgHP8GD8DF8CB+hrH4PP8ISmAgTYBJMQem9AzbAVKiEaTAdquBOmIEyfRd8B7NQumfDPXAvyvmLsBHmwn1QDfPgB/gb7CVryKOEEoEwIkIDXCFryTryGHkcGiFENEQiMoTJE+RJ8hRZj7poI9ESHdETA9lEnobL8BvZTJ4hz5LnyBaylWwj28nzZAd5AXVWkOwiNaQW/gHHyFKyjNSR3WQPeZHUEyMxkb1kHzEThViIFc7AlySGxJKXyMvERuLIcvIK+St5lewnr5EDxE4csBOCxElc5HVykMSTBJJIksgb5E34Hf6Ar+Br4iYe4iXJ5C3yNjlEDpN3yLuoM98nKSSV+IifHCFHyQfkQ/IR+Rg9hDSSTjJIJpyFc+QYHIfT8Bl8DifgFHwCX5AL5CL5GW3V38kv5BK5TH4j/yC/kz9IFmkgV0gjCZEWaMeAEkqpQBkVqYZKVKZaqiMtqZ4aqJGaqJkq1EKtNIbGklbURuNIa5JN7dRBndRF42kCTaRJ1E09dDn10mTShuTQFJJLU6mP+mkaTacZNJNm0cV0iaiIFnpBmCfcLywQFglLhBXCSuEh4RFhnfAkWs5nhK3CdmGHsFPYJewW9gqvCK8JbwiHhPdwrX4gHBM+E74QvhTOCd8L54ULws/0Z/p3+gu9RH+ll+lv9B/0d/oHbaBXBJ2gFwxoXQgOajN7hj3LnmNb2Fa2jW1nz7MdaFV2siDbxWrQMtex3WwPexHtzF62D+30y+wV9lf2KtvPXmMH2OvsIHuDvcneYm+zQ+wwe4e9y95j77Mj7Cj7gH3IPmIfs2PsOPsErdRn7HN2gp1kX7BT7DQ7w75kX7Gv2Vl2jn3DvmXfse/ZD+xH9jd2nv3ELrCL7Gf2d/YLu8R+JV+Ts+wy+439g/3O/mANsAtq6FKSB7thD7yOu6NaqIOD8Bd4DRahLuorDBD6Cf2FwcIQYagwTBgoDIJfybd0P7sPXoZ1cB5X5jOwmhTDStKJzCQPor14iNwF9WQOOU9+YpVsOpvHqoRSYbhwq1AmlLP57E52F1vAZrKFbDZbxBazJWwpW8aWs1nsYbaCPcBWokV+ULXJj7Mn0Kd5Cj2btWwdu5etZxvYRrTUTwv5QlvhF4HvETUATS+KCcUbvU7tYKbARI0ka3V6g9FkVizWmFhbnN3hdMUnJCa5Pd7klFSfPy09IzOrRctWrbPb5OTm5bctaFfYvkPRTR2LA506d+la0q17j5t79rqld5++/foPGDho8JChw0qH31pWftuIipEwavSYsbePGz9h4qTJU+6YOq1yetWMO2feNWv23ffMufe+udXz/nL//AULFy1esnTZ8hUPrFz14OqHHn5kzaNr1z32+BNPPrV+w8ZNT29+5tnntmzdtl14fscLO4O7amrrdu95sX7vvpdefuWvr+5/7cDrB9948623Dx1+59333j9yFD748KOPjx3/5NPPPj9x8otTp2/4jjd8xxu+4w3f8YbveMN3vOE73vAdb/iO/5nvGOjUKVDc8aaiDu0L2xXk5+XmtMlu3apli6zMjPQ0vy81JdnrcSclJsS7nA57nC02xmpRzCajQa/TypJGZAIl0KIkpVuFJ+ivCDJ/So8eLXk8ZSQmjLwqoSLowaRu15YJeirUYp5rSwaw5O3XlQxESgaaSxLFUwRFLVt4SlI8wfe6pnjqyfD+wzC8omtKqSd4Xg33VsOr1LARw14vVvCUOMZ39QRJhack2G3m+KUlFV3xcbv0ui4pXcbqWraAXTo9BvUYCtpTpu0i9o5EDVB7SftdFGQjdiroSulaEnSmdOU9CAq+kpFjgv36DyvpGu/1lrZsESRdRqeMCkJK56A5Sy0CXdRmgpouQUltxjOBjwaWeXa12L90eb0CoyqyDGNSxowsGxYURpbyNixZ2G7XoP3us44/o/hwa5dhi67OjReWljgmeHh06dJFnuCG/sOuzvXye2kpPgPrUl+3iqXdsOnlyMReAz3YGl1QOixIFmCTHj4SPqrI+MamlPCUiomeoDalc8r4pRMrcGpcS4MwYLa3xuUK7A2fAVeJZ+mgYSneYHF8SunIrgm7YmHpgNm1zoDHeW1Oyxa7FEuEsbtM5mjAYLw6MLY5Tw2pxXmo14BmzhLeo5SbUSCCntEe7MmwFBxTO34b2w6Wjm6HxfBTSrBWcAzOyISgtkvFUqU9T+f1g6IPfcSlv6Jur0g5/7drU0ZGUzQ+5VfgQS4nzaKG+U3hYFZWMDOTi4jUBecU+9hRjee3bDGznqakTFM8SJB90A95O7K0fWtkv9fLJ3hZfQBGYSRY3X9YJO6BUfE1EGidVRqkFTxnf1OObTDPqW7Kaa5ekYKSXKfu+mxB2d/8Z1biYkrGtw+SuP8me2wkv9fAlF79hw/zlCytiPK216BrYpH8ds150VAwpsswIZ5GQzReUHNRKMuaC/PIMEOQ+fBPowr1mHpJRqlUU4inW1Cp6BG5l+q83v+wUn34Iq+lkj+rRbsZbJ91bbzDNfFrumdYKmCHmZ/2GjR86VLdNXkoapEGb44SlHgYNMzr6RKEwbgyffhXH97fjqM0PhhAlnXhBVD+IknR6DUF46PhUvxw6WzZohsquqVLu6V4ui2tWDqyPlw9KsWjpCzdSw/QA0unlVQ0CU59eN+y+GC35aXIq/GkfctOKWAW7HABEUYI4MZ7a0RfxAjESsR6hEYtx1OmIuYiXkVcVHMCgr1mdW6gHskyldROnJyjRkdGomXlarR2aGmE9u4foV1vjhRrHynWJi+S3KpzhKa1iFCrL6eaU50xZ3+nOHTdjyIoTMM7oQfBTAi4YYNggyCCCppoSkCw1qb6c9a/KjBAd0Ag6Ja6w/sFUmO05HTS0TC9AFZw05/o+UgOPV9rsuSs79STfgU7Ea8iBPoVXl/SL2EuPYMrwIz3YsR6xKuII4gLCA09g9dpvE7RU1jqC2iNKEaMQKxHvIq4gJDoF3hX6Em+ntQ7DxcjKD2Jd4WewGGdwLuZfo6hz+nn2LWPagoKc/aqgazW0YDbFw3Y46MBa1xOPf2w5vcMdz39utaT5d7QKZt+DEEExcY+xod/DB5EP0QFYhpCg6HjGDoO1YhViA2IIEKDdY5jneNY5zDiXcRxyEYEEP0QMj1ag83U0yM1/s7uTnH0ffoW2JGp79G3VfoufVOl79A3VHoIaRLSw/TNmiQ3dNJjPmAdBamCtDXmi/S12lSrO9zJQl9F9rjx3hpRjOiLGIFYidDQV2lyzRi3FR/yEhyWAUvWwPcqfRY2yRCY6A74u6CMefjN3/4mDOFtvWe9nwb8a9ZhlN/8D6zGEL/55y/HEL/5756HIX7zT56JIX7zj5mIIX7zDx+BIX7z9x2EIbzV06deTE1zF/SdRDydzPQu5NJdyKW7kEt3AaN38Qt+Z7xvj9dkZiLHHgtkZWS6q/eR6pdJ9QBSvYlUjyXV95HqeaS6iFTfRqqzSHUCqU4i1QFS/RJph6yoJoG6a6KFAQepPkyqd5DqKlLtJ9U+Up1Kqj2kIFBPvTU356qkRCW1nfi6QnpTxxwz9tGLHPWiWHtx2b+K9yOIsBoLYCFPcqSwM4nT5NrM4ki8VfucqZ160Nex4us4Da/DaQTDCXodxeh1fMjr+AAz3osRIxD7ERcQYYQGSydjx1eqdzPeWyOKESMQcxEXEBq1OxcQFKZGu7hT7VjraKf78hh9Ha9kvLzUG0hUEpQspYewMoGYk0jfpHASLYA4vsu3WmQL7tb2/Gb8x29G0HbS0gfoSkjEiVgVpStrfk9015O1Nf6X3J1s5FFIYih1pBD8xIe0HVSp8XxIkDnNgwS6HWlOTcIQrGau8bdw7yMmXmuP+/eEs+7vE+opBr9LeMn9iaeekRr3MUzZvsf9ccIS96HW9TKmvOyvJ0j2edSiexPauXccVovOw4zHatz3cbLHfW9Cd/ekBDVjbCTjtiqMBczuAf7h7h74vK4Jo9yBKnzmHndxwm3uokipfF5njzsbu5AVCWZiZzMS1EZTktQHDi6oJ+MDLaQ10jCpr9RWypFaSF7JLSVK8VKsbJUV2SQbZJ0syxqZyVQGObY+fCaQxQ+AYzUKJ/w7AwSYGlYov/OzYq7XiEyhJwRjhF6018DOpFdw/2joNcoTvDwwpZ7o0ICKKZ1J0NoLeg3qHGyX1ateCg8IFmT1Ckr9bh22i5AHSjE1SBfXE7R+9STMkxbEc1d1LxBiWbAintP0BStKS8ERN7PYUWztaCns1vVf3Cqi96w/P45rwonBNb0GDgtuSywN5vBAOLG0V/Ah7svuxf3zxZKue3ErjaR02F6hI/l7yQCeLnTsWlraq54MUcuBh/yM5VBiflbLyUng4eXAIydFyj0WKefD+lgulRMsp9WCTy3n02rVcozwcruqUku67kpNVcvYPVCllqmye64uc9iHZXw+tUxcNRxWyxyOq+Zlgh3VIgkJWCQpQS1CXJCgFkkgLrXIkD+LtI4WWdJcZInakkD+LJMQKWM801TGeAbLZP2nn7Gds7JIbYfS0WV8H1CRUjIWURFcNnO8I1g9yuPZNbo0ukHwV4waPZ7TkWODpSljuwZHp3T17OpQ9i+yy3h2h5Suu6CsZNCwXWWBsV1rOgQ6lKSM7Fpa271fXsE1bS1pbiuv3794WD/+sDzeVveCf5FdwLO787YKeFsFvK3uge5qW6DKeL9hu2ToXIpup0prqV6H8loR7y3tHKdM66gKbwev4774fYx/sU+PXrgBd3RGBM9q2allJ56Fa4pnmfhmL5rluK+DN34f2RLNUjDZktIZsmbcWXUnOEomdI38VeEHk2bcyRkeuWdV/VcfzCvBfVvXqhkAvYKZA3sFi9HP3SVJmFrBhxRs35Sm15eguxlJbIWJ7XmiIDQX5GlFPE2rjRb85/m/M0q78FVQTV+qJYEkMgOqSoVgUq9BFFXBoKhXvQ/dJW4eqkpxgFUki1Q1PUPtNkTCwMfbhBl3RkNRPsyI0kgtrFLVxI7mD9ZBVSXuAyfCJT4HTuYHB0D4W8R3nIYmhL/j+ZzSH7BwfRQAW2AHmQA74FU4QC4CP9nbC3XAPZ6u8ATMgYdhEVqx4ZiyBAbgJWL6w8QZroPWsBHt2EZ4D8sOhftgH8QRR/h7mAsLhI+w1gIwQjJ0gn4wFVaQW8J3QhmcZvdDAdwCd8A0Uh0eFn4gvDq8GZ6BvcLb4UbQgwtG4/Ve+Cfx0/BJaIk1HoF1cJqs1u6GALZSjSWfhOnwmFDOSHhc+A/sgRfuwj4w6A3vkf00C58+Fr4lDjJH6IJPeTocDB/EUglQDuPhMdhH8kl36hXLwr3D70EctjELn7oOamAPXvXwCnxODOLF8ObwRXBCC7gZx1MH75P9QqhxXqiYMxq5lAGFmDMV/gpvwVGSQl6jU0WDmCMGxLvDH0MstIHB2NvnsOY35Dd6H15zhTdZt3BnMCFfHuTchjfgS+IirUlfMoRm0Kn0KWE6yNhiG7zGwATk91p8+imUmj3UQI8IT7PtrEGTGDoTNuGM+OFxeBJeI0YcqYdUkb+Q4+Rr2oWOoI/Tr4SH2Vb2oTQSR30bTIEVsB1+I1bSjvQnt5LxZA5ZRB4k68h75Cj5jnaig+gkekEYL1QKr7DOeA1kVex+caG4TPNdaFjoYOiD0G/hnPBC6I/yMA97/wg8hSPbC0fgM7xOw1dEJHpiwouf+g4m9+B1H1lBNqln0HXYylHyFfkeLdCvpIGiYaUaGs9PWfFKodPRoXyYPkGP4HWU/o3+LtiFZCFLyBeKhFJhKvZqkbAKr93Cl8zFjrAw8jlHXCOuF7eI28UD/H2a9Bc06e9eeboxs/FUCEKLQ2tCNaG68JdgwzlEY4FbqCLs/Ui8JuJ8r0GJ2wkfEQPyzkUySUdyC3JmBJlIKsks5OR88hh5Ru37C+Rl5NIn5AL22UgT1D63ovm0M+2L1210LK1E32s1raPH6R+CJOgFs2ATMoXuQrkwVpghzBbWCEHhXeEL4SvhsnAFrzDTMTdLZn6WxbqzEexO9hT7ln0rlonviOc0Os0UzUJNveZndGI6Sv2k/lK5tFLaI30sV/BTVNgNL179qoOcEeYJJcJueIDmMifuWN5HeR4BY4TeFCWVbiGL6b2kjqaKszQdaAfSBy7i1v5h+iZdTy/TDkJv0osMhIn8l6r8o4ll/JffRex1OM9exrG9j0+epTGQ++gFjQFqiPq7afKGkM2yhHfgc+E0kdhGOMF0xE7O0+eEfigFr7CO4jDwCk/AC0IluRd20xIAXYO8HOW4D9mGemEQySH/EMLo9fZBKSoQvob7YRL9FM7jOl4Mj5IxbBw8ALlkDnwLz+KqyBDv0GRqbOQQncCW0hhSB5Rt5b9nJqlEEGNhPikXHtNcoJ/BnXCE6eCU8Dz2/gh9QejNLooDyHhcAffCQqgMz4PZ4jD2IRkHAhkCPnYGtdscIYd5kc5FrVKGOm0Pru59qAc6Cb0xxYGScwvKxWDUEI/htRb1BEMJmoBrfChqsfehTjOI1sM40URQ6wCwd0IDYHj4WVgXHgd3hFdDS9QHi8Jz8Ilb4ByshC1kQegemIY7x89wbd8idqNHxG7hlnQp/YwOpGuunV/kto844Ae8XoBu0FF8CZayT2AgFIeXh4+hdKejhl0Ho9A/PYuj/Alb6CHsh9xQH7or3E2YhuM9Df3Dz4XdRAfjw5OhL7wMz0gijJSyog1M/t9EDeKnCKhO/WXyNRD+8b8GlB0VmpkA0m2I7yLQdozihwj0WxD/iMBoQiA1bYpAwXqWi38ixgVg0/2JuNT/GvZvInAaAFwBgPil1yKhFUCiLooXAJJO4j4Y2/DKV+FtgNQcRAjAr4sgQ/4TmW//v4zQ/1lkVd+Air/+z9GiZxTIz5ab/z+Ej//vo5XC/+2RG7iBG7iBG7iBG7iBG7iBG7iBG7iBG7gBFZSoL1xE/q1+CTrXUXJWI9XTdYEYENlZAXQSO0vAKWvEs1R4mbYBLVlHWoEjS7lc1FjUR7lU1LuxCIoxrFzBW5tsr8Vr8eGNAIMrHmH/lQD/kr2H7efv+juRejqRTsG2WgSc0+g0gfYmvSklKUBd4jQs4GTTVjiy+ihny5VvoHXv822yoZKUx+R7bZ1oBqnfvZs/ZR/eFsF7+BRfwEGLQEeLRsBUmAs7gW3A/A1s41rewfLy81CMj8jNz7Xte++99/iv0waHv2UWcT8okAi76zQep5JQH75YQz36v4bPQBzCijCHzwRGMc0iuli/2HzIJGolvYOWxNxi6+nsEj8opsxW5hwQP0mapB8dM9k2yVkRP5vepZmpv9u8SLNWWqMccnxOj2uO60+YXa4kJsYmGY32Km3Am5KXrSWgVbRUu8ptqYL68P6ACVM9EMCurUp6a5na7azzeKvM4p3n3SfllVAO7fiHIGIUa9vcnLg4q02hmpTkNH+MEpeb09ai+FOSJc3gSR9tmFkzo/PEjzZ+PPvBvVvnzNm69b45PcvpR4SRm54fURsKfx4KhV7fsfZF8mTo0QsXyXgy8acJCzlfTyODGpA3OqgKeISA0ZI3ic2lK+k6mT3PiBY0IhW0IjFQclin9t3KRwTEg3VdBjFgNOeJTUPKFolHDIhUdOr3kSKyACKzWpmVpY4tIjXF9kJiKeQjhPIsb4pFo5Hy27YtyKUNdZ0+GvToV61nsHs6znG/0P3wCN4/Pnde8VlIgh8Cib1csxOXJq6JeS7mdcNxw4l4WRvjMGW6BG22mK3fh3Mo4BwqMTqbNSbmsMkca4qJNZmN9XRzIMakS7IFTBtM1GQyB2zEZkuw1tOXXjQz8lEAm6knjkAKS0owWkYoU5W5ykqFKdVSlUOdPwcBh+KgjlUe68skH8zkETCSdjWm3WQf/xd8cfT6PyfUXU9W72qa00t4Reb1UjmO/Xxj+dni89bC1uUWhHJeObtIbpUl3qscBIuV80Sd78rymII4Pr35eTi9GiktxmvzCjj/YIuVcPb9g1+xrZv8l7ody4cuT9/6AP2s8cW+8x/cT+QZKy693UiqlaXLDm56rKZvcRz9+fnQzLLQ5Q/eerDmDE50uBHXfam4D1e9iSQFRrdWspVx8nhthbJYWKUcEt/U7FcuKnpZLCVDaD9lvD6o/GL4xfiLScsMzMhMgl6nFRkzGE2yRpIMGJY1Bgm1iUcyxGICFQQPM8RiCW2SKMpJGkFTT6cFtCAbvg/wn5LsI3ogRB+wGjwwVhIG9GNH2GkmrGKE1RMS0Pcz7JdOG4RVBmLgccUsHZHoXKlaotJD5uOfIFcvlVc6EfjnOK+cdzmV8+fBUVzkOl98tkg5j3+LxFZZyNBFrRycEORroaWwcJFy8KDp4MFFYoQiq3sF9QN7BZP6Dx9Wx8yCLO0LX8Sp/AefgVIyvbI8heSSFMErxHgFf5pGEmjuB3TYF9sbH9/4Gfl5XbfkhFxx3x/dyMuhrnQ4WbP3rhXLUIxuD38rzhQ/Qj1TH6gYTScmUg/kGEfDNJiRWA3zE1fBY+J24RnjXqHO+JbxKJxN/CXRYrImWhIThUxNuiUzwePubhwSO9Q2xDlenJR4j3WZ9TFhnemxhC1kM91iOWaKgVhwKbGKi9H68Kma9ELCV19aeqFiBsLiY5IMQnwS0yp+c0/wewghLrfd75GJ7EwaXcbX46Xy3uf7oEz2RtFUlY3FziUvK6ucqxwyndg1LCU5lebnWVNzc5hd8nMhpLZYKxdJVnfgptDr586HPnl8J+ly4CRp0eHV3AMPbf26bMo3C5/+itI2FxpeI3d8eI4M3nXmnZYbVm8KXXjwpdD3S1/GtfEUSt9wlD4z8md+wO9xky5yQmISCoZFSTKDjB3VEq3Lnah4VP1SntShTF1I3NJc5oodA8WqxekyO9BWiJdkjSzKTGYap8PloBq9zqAz6gSNLS42LiZO0MQLdi+xmvDmkBO8JE5n8UJWFo41Ez/zSHmuxZtjj7OjWo2lJpri8+a0LWjLl12aP8X7FPl9+/D7SmdU9bn7wfcWhHaRwgefaVPS+9HJfXaE3hX32RJvGRU6cvC5UGjryJwdbduUfP/sN79lJnGdtQmA8W+G6OHWgE0jJsmyJIHA+EB12iQ9yBKfswTFmicNEnp6dB4j1bmMTBsdtaHDrZGJ4gpTnapLZ7Oax47ao0jhLMDO27xRbGKpV54Ssq4cE+aL+3aEip8PGXfwnkxBedyL8uiDTwMl8bHxNlqRRm6TY4hVSE0Fr9VOfYDdIhp7kknwJmm0hPjTfKkeXMfUk1ZBBTq9Oo2kJfo9OqJz+kff2iRBvZVynJbe2BFcYK1VOeIdU6P4x5cdV2g4TV1ZSnyCK8GZIGgMfsVn87v9so/5U3wOY6IX4swxXiwcG+ORMJYs+rwkQY/zFWvBW5LW64VUAW/qd3pw3pQipaj5Gzp8BlFi832WayQ2zi61oiiyGkmDQstQaAsswi10ysrQ0Q2fhtbX1ZJ+J9YTstq/0ztqz9QFB+7ytltE6IP3XexIi58njWemV+0lt316nFTVjat/OHtade/+8/suXn8w9I/qkQXEwrn6Kt7m4fwK8PBuQkGm3ALWtrtJtYS1uXkR2jI7QtMzIjTFF6GJSRHqcEUsZ2ujkucRV4k7ReQ66tKVsAGCwFqjOemH5vkiiFYPJq7C5jax46XqguhSNqymGjVpeWnl9KLG8iaOcPPKBSPX8uoBrp2wr2twzQ3gskgWBNoIyQWFsrZ9mi5f01bXXTdUWCh8IkgzdZ8Jn+mEdHE5WypuYz/Ioo6RfHacUS3/PqfW6s0TPPyGCqfWUGjlqbUYl6OUcZqo0v211jiefipwkxNb8vlukrVO5004F1qdVtaJAmMeURcrihiTPZIGjYZGpwORMkIlvQyyTqB69CLrafuAGd2JDWJQ3C+eEZnYU+Zp+myJeNAcBCUBHdaFAb3eE10yW1TbgAa2Ek1t5XkuKEXcRy0q4kDZ5HbBxA2DiJaBYUCSlSK5CO2AA+1APNqBvcDCn7YrVZVh01fRAhZtMo6khbOQcSTHF+KcndoTh8G4Qg0fqN5aKCfHFrJAbCEf+G4fBm2FV32RrJQLL6mcXg6VOFF8coiX4J9kWXOAfkqkxnX0L2FovHxR3NeYQT9pfOHKWvrNDyH1m7M4eywTZ0+E3ICBUCYkiSB7uLWkzwVMEhWiKkNzlaL8pjyiKyKC4LVhKx+iMPyyAws+gdLgxudp4YddVj2XvvwYW57M7bYk42TIVBIEWcso1UoyEzwajVju0ROPvp++Qj9NX60X9TKqKdURNGBNtXFdh4hQZvHW0TRz9YRdKEauc1evcBFrlbXoXm5zOUfr5EC3QhSl/Xu6FcqBnEgwp1BCNnOLtseJwZxIkKemqMGAPqVQMsUiYnj80p4YDCZGgokYtPHgP3Y18z06g6raKEU2EORECrE88ZZA9711JSTua5jH5v7RjVU3VKNdGo1a8gvxYzBBPMwNVLjMJFaJjY23x8czprBYvV0fz7ba95jeNAl2uyOeehIDlr4xfe0B1zBxmHaoMtgyIma4fYRjiGto/DL7Oqo4kwTBmqTX2vweiUiu6kSSaPZzXjkTrjbC5dwKX+3wowmOUcCbw7hBUnVagQLo91nyKBphGE0Wk7bvkG7b60J7Xj0S2rflbZL4yQkSP/v7B98PfUIPkynkyQOhZ06eDm3Y/TYZ/tfQb6EjJI/E1xL9Q6FzELHArBHn3wgOGBrIH2uZFEt7Kb1ib1VujWV6Q5LZZAK7gxsqkK1+2eVxEfxzOYxROXP+KWd9lMryy7z7TUaJ2ySukFWjmoQOA/V6LRhutqc0Y3XvyatLfwodCi0m97z8VPktbeaHloj7TNaxe6a8FGpsfF4gy+eW3W8zYk83oqTuwJ46IJncEjBb9SZibZsw3H27PMXNrPXhr2qtrjykF2uT0/IsPJ6YlqdEqTlKMf/T2kR/JB/LK1HK8wNVGPCZeib09AzUlyVMSZiunWWabV6gW2x+1LjVXG/+zvStWTEZDB6LOdZiMVvMBq01nnpdcTqN1aIYDaJDq42zu5xJdjt4k1WeORxms0lO8pue0JR7UqelVqcKqcmOKO9SuI5qMus4986zDu7RcE0VZSEmFxW2tvINkr1wkSmyM+BMbdYlwDV/QCcHzIVmpb3F2p7LN6lUvzNrwmXichZacCFZEaZAQqGCiklJdiOaV0Ypn564OFusRkLHxx6TIrSiODsp6kypOw7vRrr04Lt3H/6od/rgW8KXDgy+Y2hLb68vycYFa/o8+nQoW9zX9+3ZTxxP9KX2uTNUSdrMX95OLzXeKeQWzO4+Xt1TluGe7Uf0OrIhFHhitDCaVQkzGPOl5QuFCV2Em6VbEkvcXVO7pQ0USqWyxKHpS2JM6UZ/Kk0V0nxtzXkpXX0lrYd7hqQM9k3WTzROMt0eO9YxW3+38W7zvcqdqVW+hcJS/RLjUvMKZUHq/b7VxjXmNbYkX6rJqBe96E/Gy5KGCVRDfKnJmIYOWHzLlSjF5+OgpUI8pB+pINPIKqIh9SQY8LVMSooTxKSW2ni/q6fWDxkkw5Xj9VuJ3zpIXbNtmt2es7jluMZ3VnBDh7hkwUnDOeObDWLl/jSUV+KKjilIork5UY8yNc3vz89rq+7jo161LdYex+zqbOC+LtVf9qJxxNv3Tt02sF9Zh9Dk/hPG3ff3h5/+faG4z7xja3BjYTvy2bDquxc2PPlW6Jd15BPljhVDO1d1LRmXYh+ZVfD02KmvjZnw7jzTsgfm3do3N3dSeofdM+88UjXjexyDFldTN1xNOvg10KO1SDIhXfDpWhuyDRWGJfIS7SrDfsNFg95j6GegjOpl9FO1HlmMlWURvQwPFWMpFbWEit97dCBrx8pkLJW5EdCnF/aTSbW8SsY47tiMNJBeOIKSlXQ9pZSnWDxiP5FmixXo4uwXL4qiWE8X1+orcDE4+W6Onw9wOHA7zNeDy3ke93PX7eIim7VYNNI1YNbVh3+u0VoJJ3IsKv6f1HMS3Lb1CqZjsbaqLQf++0dVr6JO8uJmLs7etgAJ7dT49ofk3lbu5JZk+ZuN6Cc1fFI9bdYslqH6S04AaSbXO2R5oGsG+C0ZVr+jENpaCq1tHTdDd8vN1u6OYTDUMsw61KGsldeaqcBEkWok5JVObzBojSaz2RAbY7Xyf/HIYasPF9WK4PBwarBaOA0Mt6EVRa+HoimNJQQcoiwn2RyxNpvDatBqk2xWDFotBrPZo1hiFcVi1Rpkh000WxQDUNFmEAWHYjZr0YmiqHccVqvFArLLbncpnbSkP3jAgHcbIgAi6b/HwzeCTmc9WbYrqoNczt6NLkdjo8vZ6OhTMrbrN82aSIleXA3xvXMT0I73Vu04n5N/IqhZFplwb423ooNNoatvODdmnBsLn0KrzlEfvhyZMB8mZv45YRCZaROm1BoCYqCdOofT+QTGRCYwxookBvfmhG/LCXkqdM9bp1Nd7XTE/sOHfVMSWn7zeuiOl0LvpEn22NAhcd+V4kcf+TFVONXoCv3tl2V1wgto9suXe8Z2b3ia66oMtIhBnG8DfLPLauIi3d5oyetBuss9tIJO1mvpdkYE3CIxrY7JOp0fHd10Hfkd90MewnDymC5dn5BH+E3m5gYpOsKfBmJ4KlYRkyQN1euSDOjcvkR2859Dkd2BeJCy5QAump6GYj3Ru0wERE1/cBr3rFMnqDd6Uerer6j3pcoi5axypXkDWGQpVN0bvkRw3SDzkb8RHk9HXqGTyW2EliZ7C4nDW6jlTin6VmgSIvo/n7Qt8OZ7bUTy2jLohX49rrzPXFcOlQpb6oTtY3ru2HFFGrcDLXDP8HcsgXWEdCggiYEHtEZtptPoyswwZmYWGtvaCuLbZ96cWW4sz5xonJBZkb3UuDDjsbjHXVuNtmed29L3OF9KP+g8kv6h7Yt0uWsccdvdjqwWmXmFrLDFzaxHiyFyadbt8oSsmYZFhkOG342/Z1kK8kyEKa1T8+w53ljHiIypGTQjobWp2LTStN4UNonrTTtNF0yCyZQg2OvptkCc45HYhAQJStJ0OQmCPmOkMhJ83tR6emtASQuAX/F7/Nn+nX7R36aQT6w7KSUvu3B/Id1QSArtPkdy69RXNUc01K0p1lBNm3b8oI6f1yFTL5efv1TUeO4cV/Jni883nuW7XMytjGxv+f42ouWR3VDp49pb1e0F6pWflxY5uOtIVWUfZ7PFxtlT/IJGMqFfxM9RsJBQNGbvxJ0vd6/qkT/p83Ekt2Tx3NmJQccdR5cs3tZP0dqTX06wjzo4tSxnyoTxm/yJ9w/utn1Bn3l9Yk1GV6pPd0fLm0orHZXLegVG9mw162LDgpvakS/SE5T03q17VNza96a7UND6hb8TzuMMuuC9QHetgbgTusR0sQ+MGWiviKmwP04fFx4zblY2uwyy0ambSCcIE8U7DdOM1cZnDbu1e3S7DYY4w0LD11QwJY8wTzXPNQtmwhl/c7a6N62Aabgr3QBncI+qBbNZj0vJmqCXHAlMn2Am5lRTcjz2IlWf5UYdhxro5gRb6hGJuKViiUpt4vMOqr5kJT8hnR79Gf1eIFwNnJ9+6fz0plMFS2FrBc1s+dkms0rsnOHoFauH4s22lPNVKNqVeOGFz0O/Tf9+yY6T7p3OucMXb9s8f+IDZIH9xSMkkeieJ3Tezo3xkya//tHxA39BOe+GXDqNGsACiXA8sF1HmdFnzDN2NYr5sfkJQ+kg3YDYgQnj6BhxrHZ0bEXCfvfH4rGYL5znYs7FXrD/6DyXeMYddse53VmuorgiVy/XNPcqt9SKphpbxbWn+cZetMTYLfbmhKG6IcZxxnOab+P+IJdMCrEJJr1ihnjkmAV0NpRgRy4Bn8XsU5SjFqJYApYKS7WFWWZYU1+VjkinpbDEOO/64ubXmZTXL6LJK3vzTW8RWs/GorOcZUUclkL1QDkqoN58LqAooRGG8S1o7J8HzEK7sQfnHrtz4sf3V6xpXdvoef7Omc9suWfWxoVPLW94ej0RlvbvRE1/dKPWdw+/9ubn7x5EnvVC3ZCEkmVDnp0KjHFDgo0OFsrFcu1g/VhhkjhVO1YvK6AQhaZZPxP/iL3sktpY2zvbJHSy9nZ1SuhvLXMOSBhpneIamTBLM8t2mV52KBBHzEa7vV9cRdy0OCEuwbxK2aBQRWHxCToJuOBpySMxKFz2gJGvZ21aZl7QSIwuNz948PnzOA0k8lXuJu64XCVVCqRm5l3FsoIIy7J6N57F7UtW1uXKLHUD1hg9lS9qrCxST4yt0RcUuG1vErbIRixW8qpH88TrV9e4cNu+Fj/t/T50gcSePEZM5Mp3upoFo5c3fk77G9oNWTJnKxlif7qOuIlADCQ9dCr0u+LZuW88eWRhl/HP8h3ZYjRCRepZkgT3Bsr7aldpN2iD2v3a09qLWgm0bu00bbV2fTTpjDas1bm1uJokRgWtRriPgEbUMJ1G8omg/oM7QbafnWGa/ewio8A87CjGGOsjd+8XWW3Ti9S3MCglpMm287FOr4zJz7UJKBqL6+rq2I9HjjTYmL/hc/7WYFOoP2mv9tEK6wK9megTO7BccaEo2mVRlBijTIwBYtRTIdbALKJe4v3Sa6QEi3lVLIlFv8RgMPp0ulV64tYX6/vqBb0zJnaHl/cpqw/KsHqS2EfhjkglFPc+X1x8Xj1BbO6iJTd3kSJHDn5NsmL2y4ounmhNUjwA3+3P4y8Mc22kQBVrHIX6pmRhXWh8clt3Qdu63E6P3sy+/+CD3+9ZZ7p5NStr2HCw9xhu/+/HW4E6tuV7QUQ5KmgXOZ7Ly4/Q7DYRmhw5vgv4bPY8s+gW14unRdYXbxdFwS1OE6vFsMj4vytOBZ96QMKfpB6UuHLz89YD2Y9qkvJ/8PAoakwGfVjzjGRlReZE9Vanq9s0fnRzf130DG8RgMaPqy0F3twLWvQtOumNeT52lp3Vfmk/5xGPiZc91C57UrSOeI9WEFKSEjS2BD1OANGkuJyK7qiP8H+DkPpwJky+VRZiqSflux2+VfEkHkMBJ9DcFB85CoTrc+qGYuiLHHGm+urJrNo/p2k6bl3P8mO2S+WNqtdYOT1yxFaME4juu8V+9cmvCb1gf6zBEk+sRls8aZonKOejs6kbITu/2SwplryIDldD/OzWZlm0MefZiTMfdd93+KlttSllHac9XDdszC3z2jP/I31GjBq2b+eexjT65OQR7R/Z3PgorZk1q99jDzZ+FuGW8A1yKw7eDcSIgiaGblHqla+Fb2MuCpdjNOicXQy0QQbOVsha5ajjjCPsYB451hQbZ00QkWNxRp3RZDCl6gO5bfPCeoJ/+j4OdSLz2uYFHRcddJpjgyPo2O9gDoHm2uIi8x2wYvmL/MftzVNsj7z3bCy6VBTxKiqzzqNfxw/HUMC5f60yKk5j0epknaQTNIrfojHFE7POGmUYP+SuBHT0VJ5F3YmrGLZo051fVGzsp+jqMif1qHqO+R/dWTKtd869jVV04R1TOq1+t5G/eemK+joNeWLE/c1rgXKrpHMaumt6yEM0pfI4zQRZzlPaW9vH5TtKlF7WXnEljjKxTDtAKbeWxw1wTBGnaMcoU6xT4sY47iI2rUY03ioMEgfpbjVMFsaKY3WTDTp7ApMsKHKxqeq7jZhUX162REBSJA+q3januaBhupMrZwybUiGARbigUWjj4oo58g68Mqv8cnn5n6/BufVS3dmB4kDtKHGUlpHy0hilIPI2VNXMMVeZsq6bl7xxgsTd8+Oy06Hze2sWLaypXbCohsaQtAdmhr5sfO/Hv5AkYnz3nXc/eOOdwzhVC1C9vYl8scD9gQ6tY4jCSArLY13YQHY7m8E0WouslbXGGIvWCIJM9Al8SYFOm75KJnKyJ4bE0GRLdPpt/zT91u5RDwdFAP2XS9PRPqsTz3dTqmoD5dAiEz8ahfLp/HVUZFmgR8PdR5zdBZs6Tii+9baOnTt3uC02ifk3VvZo/1xa9+KK6Y0fc+1VjL7LLux/NvkscA9Ljk1ur+2p7Zo6JHls8hztA9r5qc/GbG9xQDBq7S6HPbtXi+N2MZ4OplTJITpHmVymLdOV6csMZcaJ8kTtRN1E/UTDRGOdvy7NzM8rUjPapg7XlerH+Mekz0iZkVqd+pDuCcPq9EdbPJK9WbfV8HTa5vRa/xv+uER+RGtNKhwup/kMOuby+G1M3yrRxW12gttZ7OzrHOHc6Tzi1JidbudU52kncztXOqnzJToYfQjAYopCAoQq5CihQBRC+WvN2ti4PPX1ZpLJkkdIq7LEyYk0McEmsYRWereLuFKdgRhHnhM9/hopNRNLvphQeDSTZLpyeC0/+gcVOftzaHFOdQ7NUdADTQVPqjn5NBCu4Cg42zS5BJW90fE/P72Puky5V3ApK+qAVqJjkIXrbzo/6MF78wsue2TxBtJaJqWIsS38FsWqxCiCJtnoiQdtuhRPxJZ4S4rFqNeUEg/JKUaDnIFmKz1Nq9NksXhwK4l8mUdea6k3dWeXmTVv3jy+5rlZ/vPLAGn+tFYUdxkF/3SGhBc/cFW3IcU15iX3zJmV73vozXV9O7XLfHDgva8MtwQNVRPmTIyLax0//9VHh0x4894jn5GbEiZNH9v1phSHL+fmeX26z053Z/W4Z5xjQNmAgpSExBhdam6nOWXD1w99nktaavjvNFNcB3ao3gs6/jrLn6flXO6EgWoneiQGo44IEKdos8w6TRy6s2YlGZKJ0eozkLAkl2hLKqRpUrW0SmKAWmGDFJT2S0cljbSPTgQHabvr9shiuXRWOc/fJJy9VKS6s43ozFq5A6AcipyF+uwRb9aSkp9rKcA1k2KJ5SyiiuuWolGTW8yfX7t7d0xWetLG9UrHsZvo6OVEmhxasbzxod4tXKrNx1VzRv01/yt7wcU9SbTp1BMTxw+GLwYyrLF5WTEkVY6JM5CYOD0ueAsOB3LjfA67ahTsZL+d2Pu41GXPjYLrootOc21wBV1hF3MZfNpmhcC/++PRHkXPjWn7OJtMftb5JnuAmoGPsrgoohFUkXIxxWQ0G/mZEn+/jVaBGeLBKFsizk5m5jz0TFFOom59ml91eOyqmKjOj1A859htT/dV9HV6yx39+z/Qoe6Juh5T+uZX0dWNtSvadO8/cOViWojOHf+PgkB8EXlhpbsCijmWZLIMHe1pudXygEWwcG5o3d48JSExjc/2xcAOd2oe0xi0MZp4rdMqMmAavVZvkq0KxAixUoIcr09E5e6TMuUsUx7kS+3lDqauQndNQOot99J3MXe39LTeah5gnSSNkcdZZ2vulmbIezX7zHusv2oatOl6SzqkG9NM6eY0a+vYdlBgvUteKK8VHjU8R7bQLXrcmcIezT7T2+y45jPtd+w787fWS5o/tAlWQT2Ek0StTifrDQadYrHgbPaqFcHqqQ/fHLhdZzZ5XrdIskeyWK1ZohSLrqtJZzD4jKZYo9EkW8zmLJ0ci9X5yZwvcjIHlEhWJpstBpNRZ9ExwWo0GPj7fEqJxmrm70t0sZcVI6kw8o2zYKwnzwV0nr46MlU3V0d19XRwQNvXQqZa5lqohcf0ikgqVH9RELHwbnI55vLtqhJy9r5UXu5AJYN/Lmcjhv/1qVxkg8JtiPU/OpSTTEoRx6LoOVGvoHvgsDqjx+ChL4fP4Ib7DJjCR+sg2+yx1ofPkHbRT2mvYN5A3JHL4aO7JP7dJEzwDuwVzFX36XL4zC7JE0m1Rr9Jw19CHN1j9vBny/XhozVSNn9iDbSj+yItNT+8uZ5drWcJn6nVeZiHf+WNn/hF32h8vMdaCC0Q9eGPd8XwA6zSiIcMleVEPRRUDwNj7OqJoJAmkF6hl/ZtLWa5W/euz79pz85Q3UtbMz5h/sbHz1oO0zsa177zHr294XM6Z/eVIyj96egrfozSbyKLA0ZrPT0kUyvJsdr5Ud77AS0GSMck9U33gUBPDGTQdG1rpZAU6m4m3Wg3+WZtX6WMDKKD5OHafspkMpqORkN6D5kh36NdRhbIS7S/k0s03in7SYacpS2Un5E/IZLCLZViy6M4MlxVHwfS0KDQ9lodlXU6H6EoeJTw71jRkWKWpNHoRhrBmGXS0XpirkPhEzX8jKsFSMnGDSYCpoCpwlRtumgSTTNAdx8hO4H0hakQ5i68WZnhnXPwz20Wf2VRWYROPBenRlQ9Rco51LHn1AMDfqSIcqOYDmZF3oKj+xV9Eb47g/hlbpUjbJE5kzB24EXOHs6jyFc0KktJuTp3cvhUjVk9fIyQ716ML9TKcfE3cTVSY+dJ/wjo4gppLMIV1/zCnJ9PalIi55Ntc722dLq5aliorzCm8bWpsyeSH1cLsmb1XY233aN9PByO+LXiAeqHAfw/yoUVcAkA4gMmeibxIvoKDHpkUZK4lyZDBled5flc/59i22hf8QCyZyw/b3q71u7Mo9zfQCqongMG5pJqepoIU4W5MFcQpsJUQvuSfhR3cIIiUGERYaSeVtTQhUI9HbgbnOyz5yInt42XkK2N5erbUPzgmGJicoVTC/52km0jjtB3gP1W3zhLLbDfOZF+k7fIREjdRdUjDCXOmTfNfdpN3YLaf/de6o30vzIrH/v/YmgInYLWSwFULp9RYvIYY/I0fADOBKR0PW69zKCTPdpsLdUOtqpjMmGe8LyZPA/tzOstG/eSheoXernbo77RRdf0PM46tlJekBs5TSuwRQ82z8xJG9Cx9615d7ccPctR2qvroFZ3xSfPGjsk8t9AsCz1jfLdARsQmRkFZqSShkqiTktkUSe9REwgox411RC9rp6YAl6DUSaEybKOUQ9urgaZJfkLUUeJbix69x4jMdbTmlqT+qq5sfybIvXLHCivZ8uh2Bk5D3SoXz7Grpvx0yY7Rsov4IdaNsK/D/Xggw8eILeE6uinV26n+fc8N4d+0fg2cT4X+rbxQ/VbUQLeXlW/nzUz0MetKu79xqPGsFHDB6AXmP6aAaBSOER0el20z4C+jNrbADuNLmUf0Hv0R/WCvo9Bte7Y4/KmLl/d3z97S8qj/S2wSZaUBX/5S+2iRbW04x1P3UFPNe6rerKqMQNlJNwYqtAYxH0oI6mqjJwS1oMO4gIGSswo12lI5Ix6MmYBFwyUKfWtqypTbaMyZUKZ8gZiyNy8lXm0b96IPJoXhwJFSF6zQJXjisC66j5CXUdDousInT7wBeLIkbzTebQ1r9s3j+TZmqr/P61dzW8cSRUvxx9JHNuJEkhAK5Y6rJINsmfsrLNOIoFkJU5kZe2s7AloEWJV010zXaQ/Jt3VnswKiSMSJy5c9obgwEoIsRIXkBAIcdob/wASRy4ILlyQEL/3qvpj7HywsDvqntdV7/v96lV1tPJU64nk8d/9Wfrr9LP8ty/+zXeiZ8TizNc8fUqszP9ZVL/G9u3533t6rsUzL74w/zdPL4iVhdc9fVr8cWHV02fE1dPf9fRZ8YPln3p6ce4PbJnoc6K/0vH0khis/NDTywu/Wvi7p1fEt1b+Wf+MyffOH3h6Rsyf/4enT4nTF7c9PSu6F294eq7FMy+WLu56egH8ytOnRf9i5Okz4tKlC54+K3Yuv+HpxVPq/J88fU5sXDaeXsJR80NPL89+8+Innl4Rncv0f8DPzM3Ct6XL/2J6HvSFK+eYXqDxK68xfZrHrzF9huktps/6Gjna1cjRrkaOdjVy9FyLx9XI0a5GjnY1crSrkaNdjRztauRoVyNHuxo52tXI0a5GRC+24j3HsTxgeqk1vsKxf4PpCxTLlSHTl0BfvFIy/bkW/+dJj6cvt8a/yLLfZ/o1tuV0fqnF8+UW/Qbz/4jprzD9E6bXmP6Y6DMt/8+0bC21xpeqWHpiIkZCi4FQIsC3FB/h6omI6T1s7Cku67mkuIunHDTdFcYNc0iMxJDvgLrH4+r/1NStPZPiEDMx//aC4ykwtotvZ29D3MZnXax56gaPbkMixvcBZIbwwbLUAfQVuHJxhHvIPqSY0yKpPclhV4JLeUuO3yBDEhIkTxpTscpWaEaxpcDrUhhxkglrpAgieJ+wRoMZy9wR26KsW2+h4AgDlrU8n7IW+iafMvbB+FhGrJs8Ctirgq3RDPGH/O38L9maZAttrwzrt5hP+XnMuiNvXXvejHU529V4zLqtz0iAJ5eZ43wWOjVnxeDb6Q78SMmZplo1KMm4LjlnNGZ58pTQkXipykLA8kfeqvGR0pzLZpOFAThJmxtt8mp8djMfiWH+kp+aqhaM2Ji9ez4mqpVT1LHQXML6Gh057Dzx3iqf/4AxLT3uq5yFbHvIo05+jBnja0g8MWrvMJLhPsTckc+209CsZcW1cuiQnMPAx2+4ajHzjHidOTSmLOkiaaPb1MiSmH/mK5OwN4RNV7fCr+S49iPhpwa99li/KY7FF3gbfdZQcqbDKWxq8RTjVWZL/muDVYQDxrZkDDzj3BaMO8vVGNZVJ9/deqe1tFqvpsKjrOlHbjbhiijxAcs7r0lvwLMN0pz1kLM14lUyqaOobJP8mOcVZyL3NmgNuSxalq88rrSPGEMJ99DKt86JvnpnqmrU74aMf6ruHZy3nL2q11KvvIW7xLvpHtcg5/Xg1tH1lq494Lp5+gXjPPfrPmHtT+oa/68939Vl6Duh9v2t6VNO62PsB1K8y/JSXGV7e7g/gu0BI7fKGGGz4GxHXltH7IOvh93jPq67iIjoRxgl+fu4v8PjOxg5xJ3WwANkcQefPR7t4d1jka8eo7Z4DqZlPe48dpUb+do2a+FkftyelyEHOaMjYu4qnqrzV3jq8+wE/GVtM6h7qMtdybJN79N+dVCHavq16xPG9+bC944ha9F176XcvuetURc58j27X+96zqZ9SWYqbI3rLqj9ytb12sm5T1nfNwYe98/LV7XaKWO6paXpFifthR5fhOU+d2Dndd9XJvWan1ehaxzVdKZc5z+JipOWqx5K3VLxiUbBauyzXfhe9SLbHcZ+2urnkxO10P400145bpdQ7NGIM0v7luH19uqaS4/FtNVDK7u0+kPOtGntVnnrxLVac+ct3DZnhJdnirxLWH+Fq2xK35jr/4Sr2e4mVR9uODPwuj5TcsZJf1TH4/xqozvxndvl362qkcdH0+GnMfSyiBp87HLsJytXnfFob9P+JOiicefKgKuaHqtBfizfjWaKL+POH/q+esRnsLFon+JeXf1Kn1uT2p81pnfkSt/JOrpsNSfjgHWeXMdVxdSxXA8+lbdNlk9amD5XTHuk/WnZYoesNNAus43RNUF74y2xKbawH0rcN/C0hveNTVzrgt45H4uHnnOd//LvJj6O3hJv4SKpt8VNvJvQRdojPpOMYK+Lz5g/Hd7bp1d8wJ3vRfsEUfd4dY5rXLhd0PhuSz4dcId2e+i+P2dl/gRP69PtpDnPGK7AIe7NvkGoojcrOid8Or+7zE+/otfF3XKHoFp1ee95n1HizhOdmvOztTDmM4Dj1Z+JlWquewyPte7eZKQHKtDyI9mLtNzL0sxiSN7N8lGWK2uyVI7ioCPvKatewdQlZfIwi0saKeRuCrmN27fX13C70ZHbcSwPzDCyhTzQhc6PdHg3S61OSEk+kYWCEMbNQIa6MMN0VW7nRsUyAJcymEyyXMuoTFRqCiuDSOUqsBAorAkKaSOVSsxNZDaQBlZGuQ51oIsiywup0lAq6C+DSBqvyqTSlqmWY2MjiGuMZiFJEx0r2IC8gjPVmB3r1BoN7gBEmU86klOSHelcITyba2UTTJFAUCLEgowV2QBusguDMo5Bsq8wn2QwYtKwLCyHWthJrNuZoOIUZEXniUmZI8+eQK2C/0EJQyl7Fho1zGh+HBlEGOl4hIxkcmiONDNwlZWMkQ6ZaOQuNQHY1WikkcY00DDi0m0oWVI/QzCJjicSsRUockw6EhNzeq3HTeHtBZDoa1kWOnTZ1E9LcrYMKP9ykCFkaERQ1pp0SKHnGnW3xSqVqUDKGEd4TNRQfWBSqNY2WHVJg3hoilGsJmSCpFM9LkZqBNfAEsJFawpSTOyjPEsy1tapsHrHhXagh2Ws8jtfhxyh9kbn1g355p4J8oxqdJ259nr89TPZy1H7ROVPKOKXIR+xDAFCDbwxpsD6+FC+q6y8Knt78tFg0GHHdFzocQS2zv6j3u793bvbvd1H+/LRffnO7t2d/cMduf3gYGdnb2e/t7y4vNiLUIoq01QWUozgELXlKtT+YOVlw1yNognbIfBTnvoTOclKkgwIofCuTENGHzABQDGugQkDNINdDXOtCb0d+R7EIgXoZH1aepC0U85QtsYEQY1ia6pOrgMLbAyQ+8YvKns21MzCsKjlUE4gvl9aqIabGVZhK6BrReUUwF+nohYmhMojFZeqD1SqAqhqS3fk45RxPqmiQEy+OFgSShYjHZiBCU5GLpHFlBFKsioMDdUYyMm5ca3ScM655Y5wzKnYJIYCghHmG2f5k8IBmzHMg9kYmCn7sSkisgNdLt0JwA3/UarRRDrA+wxNG+J87A6a4KjjPS11wWbQKwOdpz6C3PvNzEWUlXEIrB4ZPXYt7kT4xIdKanSNsGmLdYxwi5txYJsaU2DKez14vlp2uRbwvcIrgh1l7xDD48NtuSbfvLW5dV1ubdxaW99cXz979vFDDK5vbGxu4r711pbcevvm7Zu3lxcja0d3ut3xeNxJqsIHWdJeE1rey9WYcoElCKeg6SDrY4Xuo2dlaPCrtEhzExglDxWvjQI71q0bL9DdjWwSdxObqkR3k+J9RX2iQ4P/pcBYxxjVrxahp67PI3PjMJTxa7Din/md8DFpMrOMzfw7eP4rHwWq+UM+LNKRiA4t4eyHsx/P/nb2d7h+Pfub2Z+3dCk+GFTPf2HdesqWntLG+uZen9uYezj3YO6ruN8Gt+JXxNAfR6KZX878eFbwEY/+ESbn4xnpEOI/gs/EFGVuZHN0cmVhbQplbmRvYmoKOSAwIG9iago8PCAvRmlsdGVyIC9GbGF0ZURlY29kZSAvTGVuZ3RoIDM5OCA+PgpzdHJlYW0KeJxdkk1ugzAQhfecwst2EYGxgUZCSCkJEov+qGkPQGCSIjUGGbLg9rXnpYlUJEDfeN7YbzxhWW9r088ifLdDu6dZHHvTWZqGi21JHOjUm0DGouvb+Ur8bc/NGIROvF+mmc61OQ5BngsRfrjVabaLeNh0w4Eeg/DNdmR7cxIPX+Xe8f4yjj90JjOLKCgK0dHRVXppxtfmTCJk2aru3Ho/LyunuWd8LiOJmFniNO3Q0TQ2LdnGnCjII/cUIq/cUwRkun/rUkF2OLbfjeV05dKjKI4KT/KJSSVMsWbSErQBZaAStGZSW6Zkw6ShS6FLEqYsBqFKhioJdBl02ZqpUkxlxbRlkhHX9D9v63r+u52bfRmxSkrY2sIWTEqcS8FrnCIFljU2jXcIYm/9DHcoplBF7xCELYUqCdwpOFAwmaRoB1I0Nkqxkb72Dw1I0UZd/RXjYIkeZQhCnmkO7mBzxyeTuCEpuZiU7EH6y+BWoTd+GPzQ3iatvVjrhownm6fLz1Vv6Db84zB6lX9/AUZMz3ZlbmRzdHJlYW0KZW5kb2JqCjEwIDAgb2JqCjw8IC9UeXBlIC9PYmpTdG0gL0xlbmd0aCA1NzIgL0ZpbHRlciAvRmxhdGVEZWNvZGUgL04gNiAvRmlyc3QgMzkgPj4Kc3RyZWFtCnicfVJNb9pAEL33V8wRDuz3l6UoEoTSoIoEBdocIg4O3lKrxka2kcq/76yhZqmiCoHNzNs3b95bzoEBFyA5cAlc40MBTzS+guAJcANKsU93d0CXdZUdt76GwepXntLldAZ7K4dwf9+1J2njZ1XZAh3XeVos1kCnvtn6MkvLNjQaeAt0DF5gA/Rzua2yvNwBnWe+bPP2NHoEujq+t6eDB7rGX4aP6luZI9BD0h3s6kC7OZe5D9UR/3CgX/MsjOgnnKHLdOebXuMC6FNV79MC6DbFQ5f6OOhsIWGaCCuVQtb08Ojz3c8WLNfECYYGXfZpYSQ4JwlXzKCUIt01oM6aJpPqN0oYGaOI1sw6GEmhiGWWSRBMOCKRCTiTlnCWSBd0hoOzvPAC3HnHUHhK9z5yct6mRb4dl7vCI4auWr//DgqFJU4hS2RL0Fjnh7aq/xPMw3y6OjVIMi9/VBBAz3Xm6xDH4G8cQ6Avfpc3bX2CwTir3v0w5HM4FH4fTGDI3zGtqy/z6SI9XJNEp16DzH/04JXq9utDxsMBEsSLm2jpK7rI8Gs1g/AR1hLXebcJd+gNpJSEocH8tuVAatDaEK6FVCAD1BhDkiRcZgZWCMKNTWz0dtPvyeKZLMJcyW/YeuzH0+JqzJYoSZyyxpzrnCELRoqrGIfS42GaMSxbjp5EdWvAJiD6QS645npzrsCYikX2xXt+vBvrh3PBb0Rt8E7bkFIUgRDqBnOVhj2D05yO9hcuJHRm34QL+wfVSB9nZW5kc3RyZWFtCmVuZG9iagoxIDAgb2JqCjw8IC9UeXBlIC9YUmVmIC9MZW5ndGggMTYgL0ZpbHRlciAvRmxhdGVEZWNvZGUgL0RlY29kZVBhcm1zIDw8IC9Db2x1bW5zIDQgL1ByZWRpY3RvciAxMiA+PiAvVyBbIDEgMiAxIF0gL1NpemUgMiAvSUQgWzxkMmFlM2Q5YzdhYzI1YWE4NDY1ZjQ3ZDkwMTBkYjZiND48ZDJhZTNkOWM3YWMyNWFhODQ2NWY0N2Q5MDEwZGI2YjQ+XSA+PgpzdHJlYW0KeJxjYgACJsYYLQYAAZQAjAplbmRzdHJlYW0KZW5kb2JqCiAgICAgICAgICAgICAgIApzdGFydHhyZWYKMjE2CiUlRU9GCg==</ns5:Zawartosc>
      </ns5:Plik>
    </DodatkoweInformacjeIObjasnienia>
    <DodatkoweInformacjeIObjasnienia>
      <ns5:Opis>NOTA 2. Proponowany sposob pokrycia straty</ns5:Opis>
      <ns5:Plik>
        <ns5:Nazwa>NOTA2.pdf</ns5:Nazwa>
        <ns5:Zawartosc>JVBERi0xLjUKJb/3ov4KMiAwIG9iago8PCAvTGluZWFyaXplZCAxIC9MIDIyMzQ3IC9IIFsgNjg3IDEyNyBdIC9PIDYgL0UgMjIwNzIgL04gMSAvVCAyMjA3MSA+PgplbmRvYmoKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKMyAwIG9iago8PCAvVHlwZSAvWFJlZiAvTGVuZ3RoIDUxIC9GaWx0ZXIgL0ZsYXRlRGVjb2RlIC9EZWNvZGVQYXJtcyA8PCAvQ29sdW1ucyA0IC9QcmVkaWN0b3IgMTIgPj4gL1cgWyAxIDIgMSBdIC9JbmRleCBbIDIgMTUgXSAvSW5mbyAxMSAwIFIgL1Jvb3QgNCAwIFIgL1NpemUgMTcgL1ByZXYgMjIwNzIgICAgICAgICAgICAgICAgIC9JRCBbPDU2Y2Y4NWU1ZmNlMjZjNjExOTcxMmIzMTY0OTUyMmE3Pjw1NmNmODVlNWZjZTI2YzYxMTk3MTJiMzE2NDk1MjJhNz5dID4+CnN0cmVhbQp4nGNiZOBnYGJgOAkkmJaCWEZAgrEexFoPJJj9gYTPd5DYNAYmxrWBIAkGRmwEABb9BjAKZW5kc3RyZWFtCmVuZG9iagogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCjQgMCBvYmoKPDwgL1BhZ2VzIDEzIDAgUiAvVHlwZSAvQ2F0YWxvZyA+PgplbmRvYmoKNSAwIG9iago8PCAvRmlsdGVyIC9GbGF0ZURlY29kZSAvUyAzNiAvTGVuZ3RoIDUwID4+CnN0cmVhbQp4nGNgYGBlYGBazwAEwVwMcABlMwMxC0IUpBaMGRjWM/AzMLA1RTnwFVgxMAAAa2UEGgplbmRzdHJlYW0KZW5kb2JqCjYgMCBvYmoKPDwgL0NvbnRlbnRzIDcgMCBSIC9NZWRpYUJveCBbIDAgMCA1OTYgODQzIF0gL1BhcmVudCAxMyAwIFIgL1Jlc291cmNlcyA8PCAvRXh0R1N0YXRlIDw8IC9HMCAxNCAwIFIgPj4gL0ZvbnQgPDwgL0YyIDEyIDAgUiA+PiA+PiAvU3RydWN0UGFyZW50cyAwIC9UeXBlIC9QYWdlID4+CmVuZG9iago3IDAgb2JqCjw8IC9GaWx0ZXIgL0ZsYXRlRGVjb2RlIC9MZW5ndGggNTIwID4+CnN0cmVhbQp4nM1W3WrdMAy+z1P4elBXsmXLhjJY23N6vRHYA2xtodDBuveHKUnbOJCvJ2l3sWM4OJL16ceSJXZk64ztr0h0Px67391ASTUbIfialJN7uu2+f3K/jOc1UQ5Vwyi3/DJhdsP6duOmzdN9d35D7v7PiKlVHHOIA9zduylm2EiioPMhGnROG9N52Xfnx+BYfE7VfsX1dx3PnpLrHzvDOWO27U93QRSvP7v+oSvmcMjBXH5hiDwzONlpLa+MlEaG+FIoS2oYE5T6GAqHVySmBJBEAYPiswpSrpp0VhF3GxWQBISCEowYBTk4RST6kDJLjTMDSbzDc2SuHFav441QXa0LHAF9YWvlGScDxdqczySnLWpzkCqCmlUzfQEC0Nb1nMWaIVAAApuuQfN6biydBnHdYtLwjqyWXUNf4LT3w2ldgCOgC6BfAjqB+28VLEIBNfwzJBgMFD0U7f8jqpzCaSBkEATK+yyFoUhT2TL5ZL23WIf6gAh6xuCzhMoEFe76eWZwXoBeuWqaQ5YNPsOLQxkD6HRECt4GOvTbJoxc2SfJpbSDBu3DGKeUeOqZPjBosLiPAqS9WYOyA49NUxVx8IGKKpX1RPiQE1BAjmgQgeYqgMJGofEI69gyKDQqFu194zAHZzZw37Bam1d22UB4V14HVZ+FrN836b2zNswyrxQZYHy19RfTrbCfZW5kc3RyZWFtCmVuZG9iago4IDAgb2JqCjw8IC9GaWx0ZXIgL0ZsYXRlRGVjb2RlIC9MZW5ndGgxIDM4Mjk2IC9MZW5ndGggMTkzNTkgPj4Kc3RyZWFtCnic7L0JfFVF0jha3afPufuafSH3ZLk3IQECSSAEI7ksYRHZFwkSSUgCCSQkJGFTkCA7gqKOCm6AoiKoXELAgDpERR1RBBecURBQQXEchJkPmdGQ3Ffd5yYEdJbv/33v/d57P+6huqq7q7urq6urq89NAhAAMEEdSNC9qKKwanrD8PsBQv4EEHRf0dxa9ZGqj+cC9AkHULpPq5peMS/xhZ8AOpcCyL7p5QumhRaUHgcY9xcA92OlJYXFh7Iur8ceX0PoVYoFzgrHdqQvISSUVtTOP/R0jReA9AYI3lxeWVQIewf8HsA7BPNbKwrnVxk+ND+E9TgeqLMKK0reINOzAe5cBRBlqaouqfrhpdlfA6RivbEFuOz0o7/cteLMxim27J/0UXrgn6e/SUzmeM/N9X/4ZWfLdHsf/a2YNSA/EQyY6vq2joABdvhlZ2u6vU+gvP0jJ/ESOQmTHCgCGSjYIRX6AzAzjisBVQJNaFY7+Ogf4Q5WAyEIQ3WdYJ48ASaSlTCJboeFHKRO4GUvQjXybsd8P8T7eVvkH49wCiEbYQJCZKBsOEIhwlieR959vC32UcX7EbgGJuldUClP8LfgeI/I78I0hKeQfpp9A9uULKjA/FZsd4ABZHIebPOIsh02YPkTWF+EZU8hnoj5LUhPxnbdA7RBtw4iOEZQsLwz9nNvYL6J0hvQi9X4v8K55GGftyCswDFGIR6EMAx5ghD3R1hJ3oVV5F3/01iPGJbi+Ct5OcLAAB6C/SzH+hxsl4D5pUhHohwKYhtCLEISfRGyaDC8hjgV53+bNm+Ed6GUz7l9Tih/QKZfgybjsI6AY76OEE+z/GcRGzrIdj0svQ6GSulQh3gmQhTCaHoYKtitQFBfG+WzIHFAy+R6OolwMyuGEZgnKOdYuQEe43mE4QJq/C3sCdgsXYLeWHen8gjOoxj13QPhMqTSv0BXxQ2L0b4GYv9LEJ7CPs8JeyiGcTh+N8Tp7KywoRUIa3GsC2164rrB/BJc1zE41hW+Y7D9WITBuC51COVcHhw/leucrzuZ0JqFvGeQZzIHLA8TgHPnNsnb8PbYlztgh09fxfA08qxDvZ5GzBBCuAxtIOwsAFj3DvYTgaAgdELohnAW4WmEmQh9EF5BSMKxAceVhL2izXDbFPaBtiG/izpE2YTNanN4Sqyntme2BPri48QqL8LMAMTyPvl+4TaLsuxq65vvKW4zbVjY90xu9+SvfJ7cptox7j32AwzmMog9iLbVhvm+Q5n5fniEjodViB9DO17KbZbL14a5XritCZ3gngjg7A5z7S72CGIJID5g60vbcJsu2nEpbMU+C5Sp6FM2wxBWC0OkB2AquwgDpc7QTe6OZTgf5PXRH2CMvgnScS1HYn7jdXgDB90xMkNuwnnuQH0egydRp7PZMRrHjhFZ3uH/XgbynryD3i3oX+HrgTRpdRxz6Fj33y3/PwH6mbwDfeYO/5/lY34/zudBvid0P5DuCGobxvJ6hDqEZH0K2aCfSRp148Gu4NmGUMm80Ef2QiZrwvUJQT+PewHLx8tfwQFpHaxmx/yfkzqoo8dghS4ECukj6NNwLPoZLOXA+0dc1cGOrrG5622pDbfZ6/WY+/yATbkQK7j/PgzAmQBcRvgJ7egZoo2Ryf2zOB/QRyOs0OzV/0u7fb4HzyK+t80+r7PTmdfZp/l6u7wei7MF/XvbPkU5VrfNn/tH7uO4j+R+jvuZNv7rcYf2a+h2tGPuhw/DpMC+jgvALSjj14G9j34Y1/s2v18Z5H9eafBvk5z+bUoa0n9CkP3P47znt5+pE/2tgfO0c9tZqpWDqe0cldOhIuDPtgp/8zf4nThHJwj5DMpOWCw347qjDxTybg7sQdQnyj2TFaDOH4O1OI8IaSXuRyxHmMx1ItYCIJyfC/xMlB5GPfOzaB0slY5jvMDbpoNDnBc5cBvK/p4owzOVY14m3wZPKz9AGhuPvrYJivla8Xlwefja6+eARR+CfuIY9GAvIE8IGJFvs9CBF54XdsHbzsS4CHWhKwId2uwI5OH9bRFtvOAM6GOr0IVoj7EIty+uC+xTCYExIp74ATbJ4+E23ENbdHWwRRmPey4EtmEfz2K78VwWbBcpzuuH4XbcX6vQN61CnwPC/if5m6UdOJ/56NcRpDrU0Q4Il+tQhzPF3Acyzceu5PtH2g4ebiPKw+iHeTzxMKxhKZCrzIR1WLZORj+J496LZctw/3bHvbsa27sCfhtw7NVYztvm8FiGxwh8v+i8EKTUiTgAhAw8TsHxpe9hi3QLrEI77qd/GPWwHLrieUHQ9mIQemgg8ncHYK0GosyuYRIr2WGRKE+Hj+l2yYR2y8/QfWwJlLEJkCb1gAjmgK7sI9yrP8Pjkg2msEPwOGuEtTzPgiBJ8uH8GzC25OVHYBQvpx9jfgNMYtnYfhXMYlOgRtqFtvcpGNk0XGtsJ9+HdpKA7f+G/QaAfAOTpAm4t1Yg/bP/Rc4nxmjw38aBDYGuol0HELK2wXUy02Got1twTVFeTl8jL8raLmebjL8hn5gn7xfbcR72OOCdwX8Cwa3h1tF0HexA2Ey/gAHScFhAtqGDeQIGkbMITwTgJRgi8C6E0XjG9yQLEbqxnvAKwhKkuyD+PcJOLY+xW084jrAc+34D8W5+L+BA+0MvjrHsKYQNCO+31XUEPtZvlXcEOQquze+BOg7kkr+Fw/X8qOdeOF4vdjPqEwFtcT0HZTFM0s3F9UvE8hjs87o8jpPG9sCMfyfPvwNyBLoLHWrg7TjHtvVAHPofwIkOWOU4cDb8j+T7PwFc38UI+UK/P0JIwIas5DOIQzwB8QRpDszngPmumM9r0yfB26+AbfCQKG9fP60cbQWvlHDz9eXX569f13+Xp7vh2Y7QZgft9vAgLOPAcpAf4fq8/j1YxkF5G+ve/nWePf9vYBIkS48JmUDY2HV5ZSSemQg0AWWNFG3WcmjPH8G9jMB5RXsLrOMg9i4CbYAyDu31PdF/I3TQay+uVxxT1LetT9u6XL8+KJ+XfYgwCc+KD6E74rGI+7XhdvsO+ItrbH60Zu/tee5Lzl7Hc3VPXN0bR/hZ89t9/v8JcO8cQngX4Z3/u8fiXob7CDv3EycwDsnBOPIYxie3w1KAFvQlV1IRnkM/NA7xH7EMT+/WzggWpB1YNh3xkwDNPyFdjeXHNPBTFgWbA3FlBJbtDbTVB/obq7Vv/gPAL2hRv+zU2jdvR5iB9F8RFiH9JeI3EG9A/j9ju2WI39TqW6Zgfi7Ca5j/AfPlCBORXo84BHEXhCAEJ7Z/hAOPR351D/1fx799//hPMcYsRSini7/zQrzw+jvEf4zb1vPf4OvvGm3r/+9wh3cG12FND3hn+hrjPl/Hu8+/uuO0YVzP1o7AxvtbMKY08ziax7I8fhbxYwCL+5uIY3FcgOA2zGNnHr/y2JnHr4i3IF6lyEKe8fyez+UCcaQIiBYbAgxjMIeUcSIwQy/+Dpa/BoXecDdZTO4nD5ItxEdOED/No+/S9+iXEpEkySDFS3dLa6S10hbpQ2ZmI9lkNoU9xB5lT7Jn2G72KvucfS/vk9+S/yxfUsxKlOJS+ihjlJlKhTJbuVtZoWxQtiovKDuVD5Rjys8xy2N+Vm1qiBqjxqketZvaXU1X+6jZal91oFqpLla3qs+rL8bKsUGxobFxsZ7YbrHjYu+IfTh2WxyNU+Jscc64kLjIOFdc57iUuCFxhXEl8TTeHh/rBjd1m912d7A73B3tTnB3cWe4s93l7jr3Mvcq91r3Q+4t7hfd9e797tfcB93vu4+4P3d/68n2eD39PQWeIs80z8xz8rnwc30u0os9mmmz2tyrObu5b3O/5oHNI5vzmhc139v8cLP/ytSWnJa/tV7xX/H7+Rtq2Cw0t5nsJIfJL6i5d1Bzf5KgXXPLUHP3Sc8wwqxsNLuDrWePsMfY0+xl1sj+xM7JPvlV+ah8MaC5WMWrFPym5i7G1MVsVs1qkBqmqqi5ZNRcmpoV0NwM1NwzqLnt12hubOztsevbNedAzUXExQQ0VxBXLDSn/hPNjWrX3Hr3Zvf2ds0dQs39CTXXp11zJZ4Z54jQHLnImglqLrm5N2rO2zygeVDzhOY7m9c039d85codLX1Rc3Vcc/5v0DAf9gfTQ/R1KdV/gn6AO8KGFvkgmUdmkuormzFfxm22NaU1ubVzaxKSC+FOmAvlUAq3Qt8rX145ceXolfevnL7y8ZUjnPPKxisbrrx4ZQs+D11ZfGXZlXuulF1JB/gmH+DrE9pb/dPLER7+6vbTy07//NW20/Mw9woC+tXTa04v+mrOqRmnFpze/02X0/ed2nbqkZOPnHz65L0AJ5/jbU+FnZx9Ej3zye4nvSfTTyacGHQi90T2iawTvU6kn+h+ovOJuBNRJ4JPkOM/Hv/h+LnjZ49/zVsdf+f4geO/P46jHH/7+LPHdx7PPd7/eL/jCcfjjscej4lsivwl8iv77zHS+73uOd2Tuid0j+se023UbdC9p3tJt0W3Cc+v75W+Mt5OpSK+d0mva7+noN9qcE3+ohTalpeK4V98pBHoaX675j6EpzAiGsHGsALEUzvWsjsQpmnwzz5sFAc2JpAb8a/kuK6lhyW10wn/ktP4T2tuvSYrwTOwDJZLd8Aj8C2sgPvgXngSXoCtGCKsQbUuhYfgIvwV1sGjsArehBNwAZ6C7fBf8De4BE/Di/AHeAdegqlQBOuhGA5BCbwL78GH8D58AIfhO5gGH8EROAovw3T4ER6AT+Fj+ARt9Xv4AVbDDCiDmVCB1jsLNkMlzIYqqIYamAO1aNPz4BzMR+teAHfBIrTzV2ALLIa7oQ6WwJ/hL7CPPEIeJZRIhBEZmuEK2UA2ksfI49ACrUQhOqIHP3mCPEmeIpvQF20hBmIkJmImT5Nn4DL8nWwlz5LnyPNkG3mBbCc7yIvkJfIy+iwf2UXqyW74Bxwja8i9pIHsIXvJK6SRWIiV7CP7iY3YiYM44TR8RYJIMHmVvEZCSChZS14nvycHSBN5g7xJwkg47AQfiSCR5C1ykESRaNKJxJC3yTvwM/wCX8M3xEVUEkviyLvkD+Q9coi8Tz5An/khiScJxE085Ag5Sj4iH5NPyKcYISSSJNKZJMMZOEuOwWdwCj6HL+A4nIQ/wpfkArlI/opn1d/If5FL5DL5O/kH+Zn8QlJIM7lCWkgr6YLnGFBCKZUoozJVqI7qqYEaSVdqomZqoVZqo3bqoE4aRINJNxpCQ0kq6U7DaDiNoJE0ikbTTjSGuqhK19JYGkd6kDQaT9JpAnVTD02kSbQzTaYpdBVdLdtlB70gLZGWSsulldJqaZ10v/SQ9LC0UXoST85npRekHdJL0k5pl7RH2ie9Lr0hvS29Jx3GvfqRdEz6XPpS+ko6K30vnZcuSH+lf6V/o/9FL9Gf6GX6d/oP+jP9hTbTK5JRMklmPF0ITmore5Y9x55n29gLbDvbwV5kL+GpspP52C5WjydzA9vD9rJX8JzZx/bjOf0ae539nh1gTewN9iZ7ix1kb7N32LvsD+w9doi9zz5gh9mH7Ag7yj5iH7NP2KfsGPuM/RFPqc/ZF+w4O8G+ZCfZKXaafcW+Zt+wM+ws+5Z9x86x79mf2Q/sL+w8+5FdYBfZX9nf2H+xS+wn8g05wy6zv7N/sJ/ZL6wZdkE9XUMyYA/shbfwdrQbGuAg3ANvwEr0RSOlMdIoabQ0Xpog3SZNlMZK4+An8h1tYnfDa7ARzuPOfBYeJDlwP+lH5pIH8Lx4iMyDRrKQnCc/stmsmi1hNVKeNEm6XZos5bNlbA6bx5azuWwFW8BWslVsNVvD7mVr2Xz2O7aO3cfuxxP5AXEmP86ewJjmKYxsNrCNbBHbxDazLXhSPyP1lHpJ/yXxO6IC0PZFMaGY0OvcDlZKTFZ0eoPRZLZYbXaHMyg4JDQsPCIyKrpTjEuNjYtPcHsSkzonp3Tp2i21e4+09IyevTJ7Z/W5Kfvmvjnefv0HDMwdNHjI0FuG3Tp8xMhRo8eMHTd+wm0T8ybdPjn/jikFhTC1qLhk2vTSshkzyytmVVbNrq6pnTN33vwFd961cNHdi+uW3LN02fIVK1etXnPv2nX33b/+gQcf+t3Djzy6YeNjjz/x5FObNm95+pmtzz73/LYXtu+QXnzp5Z2+XfW7G/bsfaVx3/5XX3v99wea3njzrYNvv/PuH9479P4Hhz88chQ++viTT4999sc/ff7F8RNfnjx1I3a8ETveiB1vxI43YscbseON2PFG7HgjdvzPYkdvv37enL43Z9/UJ6t3Zs+M9LQe3VO7de2Sktw5KdHjToiPi1VdMZ2ioyIjwsNCQ4KDnA67zWoxm4wGvU6RmUQJdMmNH1Sg+jwFPuaJHzKkK8/HF2JBYYeCAp+KRYOu5fGpBYJNvZbTi5zTruP0apzedk5iV7Mhu2sXNTde9R0eGK82kkmjJyK9bmB8nuo7L+jhgl4vaAvSsbHYQM0NLx2o+kiBmusbNLd0TW7BQOxul8k4IH5AibFrF9hlNCFpQsoXFl+1i4T1JYKgYbl9dlHQW1AoX2T8wFxfRPxALoFPcucWFvtGjZ6YOzAqNjavaxcfGVAUP9UH8f19thTBAgPEMD5lgE8nhlHL+GzgXnVXl6Y1axvtMLUgxVwcX1w4eaJPKszjYzhScNyBvrA7z4RfzWLnzgETV3asjZLW5IaXqTy7Zs1K1bd59MSOtbE8zcvDPrAtdQ8qWDMIh16LShw2VsXR6PK8iT6yHIdU+Uz4rLT5lcTn8pKCGarPEN8/vnTNjAJcmsg1PhizILY+MtK7z38aInPVNeMmxsf6cqLi8woHRu8KhjVjFuyO8KoR19Z07bLL7tAUu8tqCxBmS0eipL1OUIKdU8PGtGuWcInih6JB+NQiFSWZGI9z6s2Tkt6wpqg3suEnj2ArXzGuSJnPMKBgjb0PL+ftfbIbY8Q1P6FvL4g//5drSwoDJYrb/hNwkttJu6lhfRvtS0nxJSdzE9ENwDVFGfuKfM+uXeY20vj4KruKCNUHo1C3hXl9UlH9sbF8ge9t9MJUzPjqRk/U8ipMjaoHb2pKno8W8JqmtpqQ8bymrq2mvXlBPFpyg7j1hfj0nvZ/NntoUG5pHx8J/RfVJVr9sLHxw0ZPmqjmrikI6HbYuGtyWn3v9roA5QsaMFGKogGKRkmiFo1ycjszz0w0+5gb/ynCqIsbdXq0SlFC1EE+e8EQLc0zxsb+h40a/Rd5K4GuNguI6euTcm3+pmvy14hnXiOhwMxDh42btGaN8Zo6NDVtwKEBhBYP4ybGqgN8MB53phv/NfqbenPIi/J5UWUDOAPan1YUyF7DGBWg8/DDrbNrl0Ho6NasGRSvDlpTsKaw0V83NV61x6/ZR9+kb66pyi1oM5xG//57o3yD1uahrkpJn6794sEmhcEFBD+CBC5MUxFGIkxBuB9hE4Ii+HhJJcJihAMIF0WNVwqrfzDd24joXoF2zyhPE9lCLTs5X2R335an4eGjNTxwqMbWR2PrkaEVd+uv4cQuGna60+o4NlrSmvqFYuh+FIFCFaaEHgQbIeCCzVII+BCopARKvJJzd4InbdMBiQGGAxLBsNTlb5JIvcWR1s9I/fQCOMFFf6TntRp6frfVkbap3y30a9iJcABBol/j8xX9ChbT07gDbJjmIGxCOIBwBOECgkJP43MKn5P0JHJ9CakIOQhTEDYhHEC4gKCjX2Jqpyf4fhIpp3MQKD2BqZ0ex2kdx9RGv0DqC/oFivZJfWZW2j5BpKQGCJc7QIRFBQhnaFoj/bj+586uRvrNbjXFtblfd/op+BAoDvYpdv4pqAijEAoQqhAUpD5D6jOoQ1iPsBnBh6Bgm8+wzWfY5hDCBwifQXcEL8IoBD09Wo/DNNIj9Z7+rn6h9EP6LoShUg/TPwj8AX1H4Pfp2wK/hzgG8SH6Tn2MC/qZsB6wjR2xHXEq1sv0jd0JTpe/n4MeQPW4ME1FyEEYiTAF4X4EhR6gcfXFLid28ioc0gNy1sP3Aj8HT+vBO8Pl9QxAG1N54ulzM1KYbFI3eajX88hGzPLEc9+DSPHEs2wtUjzx3LkEKZ54yucixRNP8QykeOKZNAUpnnhGjkMKk0b61CsJia7MkTOJ2s9G56GW5qGW5qGW5gGj8/gDPzMu2+P1ycmosce8KZ2TXXX7Sd1rpG4MqXua1JWQurtJ3RJSl03q7iB1KaQumtTFkDovqXuV9EZV1BFvwzXZLG84qTtE6l4idTWkzkPq3KQugdSpJNPbSGPrh6YLlCvQ7n58XyG+uW+aDWWMRY3GolnH4rY/gOkRBL/IeZFJjdOYI2I4jtudnKPlu/VJq+w3hL6FDd/CZXgLTiEwXKC30Izewk7ewg5smOYgTEFoQriA4EdQkDsOBb9fpDZMUxFyEKYgLEa4gKAIcS4gUKgMiLhTCJYaEHokz9G38InDJ5bGejvZo+0p9iHS/dHEFkNGxvhjaCaE8lu+06F34G1t798t//i7BQz9DPQ+ej90woVYH8D31//cydVINtR7XnX1CyGPQgxDqyNZ4CFuxL2hRuR7QrSe4wyIpjsQp9VHT8BmtnpPF9d+YuWt9rp+jj7j+j66kSJ5LvpV1x/VRkbqXcewZMde16fRq13vpTbqseQ1TyNBtF8VrPuie7teOiRYl2DFY/Wuuzna61oUPdg1M1pUlGgVd9RgzmtzjfFMcg3B/gZGT3V5a7DPva6c6Dtc2RpXT95mr6s7ipCikckobOdoMWh8jOhwfGYjKfV20T2im6gbqeulS9N10cXqXLpOuihdsN6pt+uterPeqNfrFT3TUz3ogxv9p70p/AVwsGLniP/MAAEmaDvlKX9XzP0a0VO4BXxB0jA6bGx/MszXVATDpqq+y2PjG4kRD1A5vj/xOYfBsHH9fb1ThjXq/GN8mSnDfLpRt0/cRch9eVjqo6saCZ5+jcTPi5ZH8VB1HxDiWL4uiuOk5evy8iA8dG5OeI6zryNr0MDfSAoCacrVT/g1dCffI8PGTvRt75TnS+OEv1PeMN9DPJbdh/fni7kD9+FVGlHexH1SX/K33DG8XOo7MC9vWCOZIPhAJX9FPrSYvwo+fQyonA9UfYzG95jG58b2yJfAEfIZDOAWfG6DQfAxwvl21STkDtyVkCB4wlSoETw1YWpHnkNu5HG7BU9oHRwSPIdC6ziPr69giY5GlphowUIiIVqwRJNIwTLhKktqgGV1O8tqMZJErvJEazyW0208ltPIk/Kffkr6p6SQ3TflFU3m94CC+NwShALfvXNLw311U1V1V1Fe4ILgKZhaVMpxYYkvL75koK8ofqC666bJv1E9mVffFD9wF0zOHTdx12RvycD6m7w35cYXDszbPXhURuY1Y61uHytj1G90Nop3lsHHGpz5G9WZvHowHyuTj5XJxxrsHSzGAmHjoybu0kP/PAw7Bd5NTUa014Ko2Lz+ofaqvsJ4b4oNvztqP+M/2GfCKNyMNzoLAq/q2q9rP16Fe4pXWfllL1AVfvdNsVH7ybZAlR2LHfH9IaV2Ts0cCM8tG6j9q8EPFtXO4QrX0pSaf/bBuly8tw2sqQUY5kseO8yXg3HuLp0OSwv4lHx92spMplwMN7XCbljYhxdKUjsjL8vmZQZDgPHX6z8ngAfwXVBHX91NvDGkFmryJF/MsHEUXcG4QFS9H8MlfjzU5OEEa0gKqWnrQ4gNGg18vm1QOydABfRQG8BaK2xS06aO9g+2QVcl74cIhEj5eYhgHggH8H+HcI7j1jL/OV7PMf0zMjcGAGAbvETK4CU4AG+Si8Df7O2DBuARz0B4AhbC72AlnmKTsGQ1jMFHxvLfkQh/A6TCFjzHtsBh5L0N7ob9EErC/d/DYlgufYKtloMF4qAfjIJKWEdu9c+ByXCKLYVMuBVmQRWp80/03+d/0L8VnoV90h/8LWCCSCjC57D/R/lP/hPQFVs8DBvhFHnQsAe8OEodcj4J1fCYlM+If7r/F5QgFuahDAyGw2HSRFOw9xL4joSThdIA7OUZv89/ELmiIR9K4THYT3qSwTRWnuwf7j8MoTjGfOx1I9TDXnwa4XX4gpjli/6t/osQAV1gKM6nAT4kTVJry5LWHK5o1FJnyMKaSvg9vAtHSTx5g1bKZjlN9sp3+j+FYOgB41Ha57Hlt+Tv9G58FkvvsEH+/mBFvTzAtQ1vw1ckkqSSkWQC7Uwr6VNSNehxxB74FEMZ6nsD9n4SrWYvNdMj0jNsB2tWOrWe9ltxRTzwODwJbxALzlQlNeQe8hn5hg6gU+jj9Gvpd+wF9rGuEGd9B1TAOtgBfydO0puMJreTUrKQrCQPkI3kMDlKztF+dBydSS9IpdJs6XXWH5+xrIYtlVfI9yrnWie2Hmz9qPXv/jT/ChiN9rAEpX8YnsKZ7YMj8Dk+p+BrIhMTseLD3/qOJ3fhczdZR54W76AbcJSj5GvyPZ5AP5FmigcrVWgUf8uKTzytxoDyd/QJegSfo/Qv9GcpTIqTUqSeUraUJ1WiVCul9fjskb5ikewI86Oe0+RH5E3yNnmH/Cb/Pk13Dx7pH1x5piW55WQrtK5qfaS1vrXB/xWE4BriYYFXqGyUvhCfGbjej6DF7YRPiBl1F0mSSV9yK2pmCplBZpP5qMll5DHyrJD9ZfIaaumP5ALKbKHRQuZutCftT0ficwctobMx9nqQNtDP6C+STjJJNilESpYGS/lSiVQrLZAekXzSB9KX0tfSZekKPn5mZC4WxzwshQ1mU9gc9hT7jn0nT5bfl88qRqVCWaE0Kn/FIKavbpRutC5fd79ur+5TfQF/iwp74JWOX3WQ09ISKVfaA/fRdBaBN5YP0Z6nQLE0nKKl0m1kFV1EGmiCPF+5id5ERsBFvNr/jr5DN9HL9CZpOBlGxsIM/puq/KMEM/6b39nsLTjPXsO5fYg9z1fM5G56QTFDPRG/N03elrqzFOl9+EI6RXRsCxxnRhJGztPnpVFoBa+zvvJEiJWegJel2WQR7KG5AMZm/Vq04xFkO/qFcSSN/EPyY9Q7Aq0oU/oGlsJM+ic4j/t4FTxKitl0uA/SyUL4Dp7DXdFZnqUkKyHkPVrG1tAg0gCUvcB/n5kkEEkOhmUkX3pMuUA/hzlwhBnhpPQiSn+EviwNZxflMaQUd8AiWAGz/UtggTyRfUymg0QmgJudRu+2UEpjsYgXo1eZjD5tL+7u/egH+knDsSQcLedWtIvx6CEew2cD+gmGFlSGe/w29GIfQoMyjjbCdNlK0OsAsPdbx8Ak/3Ow0T8dZvkfhK7oD1b6F2KP2+As3A/byPLWu6AKb46f496+VR5Ej8iD/F3pGvo5HUsfuXZ9UdtuEg5/xudlGAR95VdhDfsjjIUc/1r/MbTuJPSwG2EqxqdncJY/4ghDpCZIbx1Bd/kHSVU431Mw2v+830WMUOovh5HwGjyrk6FQlxIYoPx/AGdQvmSEaRrQewCkONQB1slNvwZd2v87gP9tgX8GRqsGpgwAcymApf4qWJ8DsN0CYPfgVSwJ4QOAoF8AQpA/FMvCJmgQjne1yDqAKOwveqIGrgkaxPYFiGv9/zbE192A/ykkmDtA/f+D8Jd/D+7w34BJ/8vw2A24ATfgBtyAG3ADbsANuAE34AbcgBtwA26AAMp/UE/GByTQQf8GSs4ouka60RsEMjsjgVHHzhCI0CvyGSq9RnuAgWwk3SA8xX45uyV7hP1S9vCWbMhB2n4Fkx7dYx2xDjcmBBhcUaWmK17+Q/Yqa+Lf9W9vPUmWwmEwwog9Rhxwh9JIRnk9RMqmlBhJNhiphBlQeuv6jIQpUAmLYTMKt9m0ZQMOeSn/0hn7+Ww7DshT+3l7y3nicGb16J7eMz0kWNEl9uqVuffwqNvSsnpJhw/PvtczPKLwdhy3H2mkM2gFzrGLN6KKVkl0OBmOQ8YDjZSrkCGCVa0LTxlhP5Nv/xZSh5/v0R1mk/ygnrEh/Whn0rhnD5d+PyYrUXoJ3N5wyoXN1kTcCWwz1m9mQsrL+fnnUUBNqP2HDx/mvxU33v8dc8hNYIdOsKdBUSPs0Y3+i/VUNf3efxpCEZwINv9p71SmrKSrTKts71llg84UTnODbg25JWJA1LigySGTI8ZEzdTNNBUFlYfMjCiIWkDnKXNNd9pWKht0j9jfC/+CfqZ8Zjpui4yMYXJwjMUSVmPwxsZndDcQMNgN1LDe5aiBRn+T14qlKnhRtPUx794rxE45j8nsFC48F5/kz4Z86M0/BCHI7uyVnhYa6gyxUyU+LtETZA9NT+vlsHvi43TK+JmfbJ5bX9t/xidbPl3wwL4XFi584YW7F96STz8hjNz84pTdrf4vWltb33ppwyvkydZHL1wkpWTGj2UruF5PoYKaUTdGqPGqktfiyJjJFtP76UY9e5ERAygylQwyMVNyyChkd/IZAVGxbaRZ9lpsGXLblLrLRJW9MpUjTPtJNlkO2qrOTkkRc9OsNScsiziy+AwhPyU23qEoup5oOem0uaHfJ+Me/Tq1lt3Vd6Hr5cGHpnD5sgGYDuWLge+8vW6Sb1JelQ8or+re1b8XrRtqzjOPs840F1vvdN4ZtNr5mvNs5Nmoi5HmA6ZXgmiUPdreyR5jV37vvwg6XGA9YoP/ojcyxmjXK8qh6Mjg6OhIfXSkRKg+MlqyxNgb6dbdIx3E0UjC91higmWIaaSvem2Emo01YZ+gPHw9yat0CahgJ729ZseeHDqFVtLFlNH9NAFc5P5d2oJewgVNybZfOi82aM75lvwzDiefOyYrrd1SrIvsB7X9A22r3BvySX61OyTWk4ka6dWrZwYur9hYuPa4xRQd/mO6K5k0zP3MYxe2bbzrnifIvqB/fPTJ5SHPv/n05JiXXuqXXdR098Gz02Y+9MSaoCOf//mlidtf27qqsAdqcoL/WxaKmkyBT7xJsiXUkmtZYWG5jtscc6OkMaHl9hnBxaFzLAuCV1jWBK+OetZilFWJ/yCRif/yKNOReIuZcAV5sbNXCf+a2EJ6NpjNISx8P90KEbTUmxASEy2zmM4WZ80UtVKlap2uxiP2gIeAx+6hnvVdwxtJ7/qIT8h+/leX0XJMVzdDl0by4K62/XApsCMu5WubogX1l5WKbocrUtMjWhFqDg2JzA7KDOU7QqhMl9lOtmmPq0/HU4iP80xocD08c/HOpxel3xrsNNU0rphRtja4IfbPL88/NHNa8T3rW8999oafLA3fuNJ3z8ItwU/R+YuK7lm2TN3z7vT64ilPdIt5/b6m1p++RV80Fj1LBOo0DOKhO5zyZvYMJZ1Dh4YO9Xxr/r67bOhOFsEispDV6mebqs1zLHeG3QtryFq2Qr/EtMy8wrIu7APHO0HOOFRzfbQayZGqpnLUVfVw3cd0Vs0QEw7mqJhum7uRbs7YGEVOinFaYmoOGIihkU732lNqbF4VVWwjYLPbqK2RPLA3LbzGh94c6+sTakLafU6IN4SGrO/R7nNQt+jYOyjZmZWfej6LazdL0672QbOcjU7Z4+mZEbDFNmUClgQFX9W3FHx1HRQyo6r82wNNf55ZsXJd6+XPP2+9/MDUFTNLl6+eNn1Vn6Hrxy7Z9tI9i5+XojpvmLH5i1Obpz3aucvBVa/5gZCm+98g40qXLZ1StHLZFf/w9SOfq7tn+zY0vM7oD3zyfjDDt7ucVj6xPuizhpDB+iEGyag3GegORiRKCDMYmd5o9HSKzUgykp+NxKgSFozlxiRTdAbhib7R/6fdiBlibxAvxSZyjE6hJmOMGfTGV8ke/mNzZI83CnTd9V491d9izjERU6SVgKyMhgjL3o3cyV0afgnPxBGo0+zhl2Zn28/Yr2jbHiHbkUX4D7KslLuloKpXLjpotR9ciXCQVKOKZ+fzH7XxGmhcbBYJj80yNPpP7onIonERWbxVXnpP0iszFg9DoosN6UwvjBpy5UMWeeW9PGlbg7Sj+JaXXrqim/4S7p5b/OdYNOsLSZBJOnnvM1gMyRGWyOTOluTkLEuvkMyoPslDk/Mt+ckzLGXJBd3XWFZ0fiz08cgXLCHPRWxP2hvxatLBiCNJH4d8maQfGEpcYa7wlC7JGVksq8tQNqTLBH1eyjR9Wcpc80rze+afLT+nODIzrITZUxMywtJig8OndK7sTDtHp1pzrPdbN1n9VnmTdaf1glWyWqOlsEa63Rsa/jA6XB3kJhrToiVT50J7IbhjExrp7V57ope7B9XT3bPTI3t6ZPGFdcWgUWc1ZdHNWSQrzB0el5pwQDmiUJeSo1ClR2/uHriXQKVezj9/Kbvl7Fluw2faXAXWzkbMTTngLbijyIfZbgXPUG7K6GT50zMjUXMVfSl3FWGhISHBoWHxHknRWWmIMG5kkrKL983Y+drgmiE9Z34xnaTnrlq8oJMvfNbR1au2j7IbwuJeiw6berByclpFWenTnk5Lxw/asXzEkhHBVktkgts4q+vNebPDZ987zFt4S7f5F5uX39ybfJkUbU8anjqk4PaRN89DQxvlPyedxxWMhMPewQYzcUUPCBoQNjZobFhBUEHY4/Rx6THLVvvWSLPeEmGcQcukGfIcc5WlzvKceY9hr3GP2RxqXmH+hkrWuCm2Sttim2QjXPFDu6N7HQUFUAXrMaQ7DXgOgs1mwq3kjDbpwqOZKdpGbAnWuCiUIsGU4iIE9yAZGh2ScERHXLocHdX1iMo4KLzGbO6XqwO/brEPCP/9hvPVl85XBxxIqiMr1Z5/Bv9xhWMkN5uEcYWDI0MEMWE6D9e2plcpe1enCy9/0fr36u9Xv3TCtTNi8aRV27cum3EfWR72yhHSiRhfJHTJzi1RM8vf+uSzN+9BOx+EWjqFHsCB0dxn3h1GyixuS4ZloEXuGdwz+jY6zjgmeGz0dFoslxiKgguim1yfyseCvow4G3Q2+ELYDxFnO512+V2hLldKZHZoduSwyCrXepeuG02wdAvtQ3tahtFcy6DgodG3GSdYplvOKt+F/kIuWe0kRLKa7DaIQo05wBiCFhyeTsDtsLnt9qMOYnd4HQWOOgdz1DoTDuiO6E7p/DrGdTdSJ+kiYjJGCUeRP3s4hgKzRfycfYarLJuDI0sLBTQDje3JDRQtVFOYI91BOrrY3iUHFx+bM+PTpQWPpO5uUV+cM/fZbXfN37LiqbXNz2wi0prR/aj1l0HU+cGhN9754oODqLNh6Bti0LJCUGcnvcUuiA6h46V8Od8w3lQizZQrDSUmvR1DGjtNdH4u/xJ8OVLXw9knokd0P+fwyH7Ro50Y/EYXOisiC6PnK/NDLtPL4XYIJTZLWNio0ILQqlApNNq23r7ZTu12FhVt1AE3PAN5OAiNK8xr4fvZkJic4bMQS6QLc7vdngyOvZ34LncRV2i6PUHnTUjO6KCyTE1lKcNbzoywYxh5eXbKcH5OtZwRhpaf3TI7WxxVzkBASWZXtxmbHdLTwBGsixXnEYn1iD0u3bG/y4/7vm+9QIJPHCNWcuWcsX550dqWL+hoc+8Jqxe+QCaEPdNAXEQiZpLUerL1Z7u6c38peXjFgNLneDy6Ei9r/KcNg2HXPrw6NHktIWEZbtZTypX2W5iIlxLCIjLC9A6zI1iS8UiOlnXBJqPZbfCm98rwG0gTntsjQvnUwzJ6ZfhCL4bSqtDNob5QfygLpcFuIsLsEGS+yH9CW4WjuGMZjAgZPCo8EEjjhQ/RJe2+wO0HzSeLcCUMWOC1Klad26qYo4hFb4sikIKnzxJIyScp6WhIWjDkiHcIU1JCHCsb7m6a+/KwhjkzR63Llve3/O3B/K1PtEyhW1beNfa+RS2vovWswoljlbilLvLmjzSsN2w2+AxNhlOGiwYdGFyGKkOdYVOg6LTBbzC68M5DdAzvDop0N8FbhMKMis4tg/hlVB9rYqeZ0sQuMgpMZUcxx9gIfdsMq7PFTQFnJlY3EIxgDBKEtzoJZ7GqoaGB/XDkSHMI8zR/gVcY/9Oto0kfIaMTNnqHM9kt38TS5RWyHKaXZR1jlMlBQCwmKgWb8TZo0nG5TIou2mFbH0yCw8IizWaL22hcbyIuU45ppEkyRQQFvxTLZUoZgbrOHm7nl+7ckoHfzoac4edzcs6jYM6sdhEd6ekr7fpsbRH0dptHbzdGEYNVFwXaIvBLbXoI0aJTnEWwDi11RUNraVwvV2avhvR+jw5l33/00c93bbQOfZBNbt58cHgxtznUv/QPnJuJfOCN1CkTlEkGyWb5L/myIo2X5hmpU1GDYnk0c3G3MzEDA4iLDYidsiiIFQXeZViiMCYzJdMwGLWjdDVONM6T5hi/kL5RdM8pJF7x6Nz6LKW3Iccy0pLH8pSJujzDIrZA3mh4R/mYfaacUb7X/V35WR/iNBplSWIUbyMGA8ZYskGvd+uUYJ1OkRhzy8ZgWTYaceWZnuD68r9AozeZwMj4L2HIcXpE3nhVnCqR69EhmNxA3YSsB5IDI9HeIsyWr2IHT7uq9/P287j/L+cPPy+uU3Z80DiyHXiTwqCK4U0Kcbi4UulwBfTZkkjRZnhoZTR06ZRl0HfqlK1gbFXfKQvRp/WqQLtiRZSVkodeF70HBH7uWfE31cdm4W5uqg/l6GS9PUvRkMiZBdpl0hqn5BHtB6a9zi8Z0QeH4mjBwdkiwVaX68N547/sitLYSX4ev+jN5tuRpBMST3Ro0GT7960zyIGTrVsWy/uvvEZ8rXNbiqnrzlb+DmUpmkGmsO61+0BG75nZW1y7d2f01HD3HhqOc2vXcTd6JZvskjfJp2Q2EpOLsuSSq+Q62S8z/lcXqaQ5Gt6TcDiR6T0zNgFpwuCAdvA6rH1PpqRou1K8nKgWM+EzWNog7/9lkOYZFQ+eMfHwzj68Z//J289kQc94hp0xfBV2VpWPyZdVGqZX4w3hUapBkuJjopWQaBNuQaLER0bYjUfdhP+FFurGvWh1rxfX8Pw94e71USQKKW8E0PR4NzkKhEcx1AXcWiSISHA3kvm7r27U6mw8L/DYwEtNywixXTE0yc7ORociTMkhbuFZbe7SHBzkCTY7oojTEtLmLsXLCZxdiLg8hok7j/CZInLp6D23pD03Y+6jrrsPPbV9d/zkvlW/a5hYfOuSPszz8IgpUyfu37m3JZE+WT6lz8NbWx6l9fPnj3rsgZbPA+fIt6itUPjAGyRLShDdZm+0fyN9F3RRuhykML5ne6ACF9jJBvvR8NPh/nCm6oOtwaFOPFCIEmoxWqxma4JJnComgv9MI8LFQvJTJfxiOK0K3xzuC28KZ+ESTQ8JDRwszl8dLGFth8qlbC2WxmNFvOfL5i6u/VwJVRwGo96oM0qK3eNQrFHEZnQGFJa8hL+wShE2HdIrEER3UNjKp+d8WbBllN3YkDxzSM3zzPPoztyq4WmLWmroilkV/R78oOU1tLqBGKUkok4sEAFvePOdOmOEebAyRD9BydNPV8r0+gx7H2ef0J7hufZhzmGhueGT5cmGMfZ8Z37omPAKucJQbK9wVoQWh88jIQZFttwujZPHGW83l0slcomx3GwMi2Y6B5pccIKOqyIowZ3RXUdAZ9epGHD0OMUNDcsjeEiCtDUBvMjCDY1Cj0gejmhv6man5F/Oz7/6so7HbOISN1Yea5gqTzUw3ONB9kzUBPD3NhiPdLwjD9y6+u3jJPSuH+491Xp+X/3KFfW7l6+sp0Ek8b65rV+1HP7hHhJDLB+8/8FHb79/CIde2VrGYlEvToiBI95nzfau9pvtw+wsR/Wp1KV2Nsd3SgtJ69S/U5W6XtX3CesTdUvYLVF5+tvNk8MmR83QzzSX2SvCZkY1qZ8Efxn+ZeQnMWeCz8ScVv1qaDxLsaeE9GR97IPYLfZJ9rOmHzq12k0OK8Zz0XxnhkZbTWCNSDhqJHaj11hgrDMyYy0JSqfpTjdAE7ptspn4yEXCXCSHjMTAKcI1ODOcaMFuNT81L52xY8iL2soRh2ZWINLFWpgd1LbJQkOC+atNT6JD6qCqlVv7PFi66uiMOafumnR/N8dzc+fveL62Zldrmfz6mtGj1/o3PNPafO+tfVqapa2HD75/7P1Df0TTXo4BwTuoLwcs9d6UGkTsjMSzDDaAjWXTWC1TDA69QW+wBDkMFpD0xCQmCkZD0no90cepQSSIxjn+aRzmHHywPQ7DW86laozixbQwBtOCAbC/t9K66CCfZDXJb3MjeO/hl0zcDcuf7luWc/sdffv3v+mO4Bjm2TJ7SJ/nEwfnFFS3fMrP+xy84exC+buTz713sbjguD6GWwwDEybElcQtNNxnWJbwXNCOLm9KFkNYZHhY92FdPguTo+h4Su1pxBg+WT/ZMNk42TTZPNkyQz/DMMM4wzTDPMPS4GlItCV6EhITOvdKmGTMMxV7ipNq42sT6hIeMj5hfjDp0S4Pd99qfMH8TOLWpN2etz2hnfCI8zpjsibpE91mI4tUPSHM1K1TJI/so10ROREjI6ZE7Iw4EqHYIlwRlRGnIpgr4v4IGvEqHY83DUA2u514CbWToxgHEDuhhJ84waEZHHtjrI4MQrpN7lTeiXaKDtGx6G4mVySJTIjwBoVnRDTS2+t1CcnI+Up01tFkkhyZxlt58BZRkNaURnPS6tJomh3vqQmgJtjiTrWHDz3aLg6zh19Cf189Qrg1fne4lBK4ps7G60MK+qtqYZrVZ7T3BIHXBOjsvIldY+Ll4C4eh91pD7JLSpxFjQJDki6KyF0xiQnGbKw1Pgri4i1mfWcM9JISDUYlhUWBy96Ju8UUHqZoiXj/k5yyZAmPw2fzQDa//X1loiexG+2Zwd/3Cr/ZdjsOQxcaFkM19+HJqbetvmvh/J7uh97ZOLJf7+QHxi56fZLDZ64pWzgjNDQ1atmBRyeUvbPoyOfk5uiZ1SUDb44Pd6cNXTJi8IIkV8qQu6aHj5k8JjM+ulOQMSG938LJkzbd9iK3tAT/32iyvBHCoG4fGHFt4j08XGzy9kOiLgJjeLPFSCQItRtSbEZ0BpLJZo+DOGJxus3Er9PnGnILdFW6Ot16HQP0opt1Pl2T7qhO0e2nMyCc9No1Tdss4ssjvLecuZQtLr0t2dwPYMhsf48HFCkp7jDtzuuI75nuyMQ9E+8QrxKpPfLW7KnlXZYt271nT1BKUsyWTfa+JU/TorVEV966bm3LQ8O7RPK5LMVdc1r8btjr+yCS3zcxBqJqUGiGjR+mnZ3BGSlBJEEfFGomQaEm3PAOnA6kh7rDw8QhGkaawkjYiEix7fkhGnkxklZFbo70RfojWSTe4NodAv9GRzUcxbsOM4yIaL+YnW87P9Ez8FnmZGseQZhUJLNbLTYLxWhY0ct6PEWZOQoseod2PUhOXoI+Ee0kcPlP9IgrQpgwE3FdkHIWHrvjmZF2U4PJMWv06PtuaniiYUjFyJ419MGW3et6DB499v5VNAuvQ/zPzoP8CurCSXd57bZgksw6G+ktjtsd9zkkB9eGwRWbYY/upN0WvC+5EjKYYjYEKVGGCKfMgCkmg8mqd9ohSArWReujTJ3wMHTrkvUp1gzoqeujv8k6UBqseHXD9cNMA2yDHbc4b7eNcc7UFeunOxcod+pq9fuU/ba9zp+UZkOSyZEESZZEa5It0Zka3BsynfP0K/QbpEfNz5NtdJvpOfMe2Kvst/4BbxmfG86xc7bvnJeUXwzRTkmWUWE62WA06k1ms9HucOBqDtstg1Nt9A/1TjParOpbDp1e1TmczhS8auNlz2o0m90Wa7DFYtU7bLYUoz4Ym4NM8ZYBwUT82rPOyfQ2h9lqMTqMTHJazGa9XqejlChOm81qBWPwZbuFFFj46zXJ0kie9xrVkUZSaVxspMZGOt5rGOkglY7FDurgOZNdJgUivpZkZN5DLgddniacUMTwS/n54ehk8F9kRAvS37ZfYeyBR/teiL/G4GcIpiuHd+MvilfipeZXiL9KtuJFx2rP5rAy8DZ5mM81dmKDRTWr9DX/aSAIVv/RBuhuU52N/tPtr/LzhvkyxuIVR+8/ukvH3/BjQezYYb508TZP7z+9S6dqpU4sjRGl2NFem8r7xkvm0Xpdd95jPfSm+7WR2jtvbxcm2jn8p3cbVabyb7jytNsY7+zTvc4s6ILAL2BBWeL6JW4U/A6WH0vSg8J6ZQZhigmJlxIlMqz11f0v5LD0F/Zt6nnz3p2tDa++0PmPzNPy+BnHITqrZcP7h+m05i/owj1XjqD1x7aOln5E648kK3fboomNn2Nbo7OSgifYdholr8VrozY1qXuGnSc6s8EZagl3JpoSzYmWXuZelp7WjQ5TkjMpaEhonjMvKC+kzFkWVBayQJlrWeC4M/jOkOWWNY61zrVBq4M3GLeZXrO/6tgf/Gfjd8E/WVrsPwf7o2PQhMx2tEcjSBHBQUFupzEYMzYzGpzbZAw2mYxBTqfZbFKk6AgbRNujaWr0gWga3Uhz9tiCvE5vcCMd5zXlOL1OOsV5wEmdjaT/XhuJg9woI69y2lST16uau5tHmqVRZr+ZmpFjd6oNJ0tzGqLUhWh8eKdqmY2HYGT4eSTPh9svnYngX8eejwy3nxcUhHNXzC2Q36P1He/RgPaIZpWdrUfLsuKKhuOKvgpm/zkw+c+RDusZ7D+5NzPLGJeZZW30n9sTkuWIC9G+uuDf/OKS4oU3KFF7iY4Prm6oWF7Cv86Mj1scfFOX7CFhDo9saq1488uUOFfKNw2t5f0Sui+ckNE6/QV7UkLUTFsnltSycc6ShXPpzOY/7OyfN5Z7/CS8Q32K62wlq7wWZyN9T0+dJM0Zxl+FfOg1IEH6xogXI296b0GiM00ypNqzSJZxKBlEB+mHGkbaJ5NxdJx+kmGUvZwU0SIMmO4itfq7DPeS5frVhp/JJRoVofeQzvoUQ5b+Wf0fic7OIxJ7SAZFC0bv+ak3EQMH2sdgpHqj0U0oOhhK0PMotFBOwSkaCy1gSbEaaSOxNaCTkRX+jUcX0MVZNlsJWL3WAmud9aJVttaC8W5CdgIZCZXg51dbm702duHBji9C+LtjvNzyRWsRP4lxFs/Ss+L1MV9D9A9268EUXBtcALyWBH6Te09n4tHz6EtTi54rCXNvvsLVw3UkGMnsPJIv1lTvP1lvE19FaejcK1FZBn1o1M38uKgP40X/8BpDs2gwQmRoVtsva/Nvq5R47duqXumxIUl0a83E1pFSccsblQtmkB8elPTKg/Na7rjL8Diu30m2nY6U38SJlvDvEf6wOywig/IIEbEkYj0kFpM6eopIldJiWCxJlVBJ6EgyilJce7tEpZWEkUZaUE9XSI107B6IYJ8/r30j13IJFdSSz98C5eMHpQsKSpdOLv/LCbadhLee4xb0SusEWoERgx3QoX9OiVW1BGUoXISIaMR0k9dqtoFRrxq6G6hhvFNIZcU66UUbeRF62zY5tuwjK8SP5PBQk4cB/B3JeVwBPNTzM9O17zkyQwJfOZ1emDim7/DbM+7sWjQ/PG/YwHHd5kXFzS+ZgLJIKNAB/pYP5npHuMTB02Q5avFbFCB6ZpKYieoUqpONBqKXjTo09veI0WQkhOn1Rgy+9LmykRKjl53CkHgEmFTTUZNkGmEW0UlL/rf52ef5TufxV4T2LU14Dv8a4rwNP1xJQbqeGHc5MkN0jvjl99yze+XK3bTvrKdm0ZMt+2uerGnpDH6/9h2V/Cb1wCD+XyLCOjgEAC6vg0DM5hi6OIbEmGBICiEx+2gcdOaRTf7snrzlLa2V7BLriy0HipYn6Uq85zu9Zkp6J8KQRERGwY+8w1on8O8rkHeIxktuxlES0R2ujzkScypGKoipi6GjcLCgRv/+vUNSKInRGuNKi9HEHUrIOSEgJwa84PaGkiMZpzJoasaUDDoyg2SECGEzrgrL24vPIIn/pVdJ0K0i5TQBI+kboClY5ZPQ9j+b3MF/C1TQrAOPDOHyjwFaAasSE6B1cFDpEqD14NEtDNAGWGPZGqCN7E0xMqdNMNXaLUCbYZp1fYC2KA3KxQBthcnWy+1/EnyxbQy0/f/Ssu2vAZqCztkvQEuQ6kwL0KwDjwxm59AArSB/YYDWwVRnaYDWQ1CQPUAbIDc0IUAbaaHtowBtgh6hZQHajIH2YwHaIk1yHgrQVugWyn+qizAJZTOHNgta/A/XYSZBK7w8LErQOlGeKGi9oDMFbQiskUZra6TR2hpptLZGGs068GhrpNHaGmm0tkYara2RRmtrpNHaGmm0tkYara2RRmtrpNHaGnHa2GG+JjGXwYI2dyi3irnfJmg7n0vYdEEHIe0MmyPo4A78IbyfAB3aoTxCtF0p6CgxltZnpw48rg50guB/WNDJgn5G0F0FvYvT+g7y6zuMZe5Qbm6byzhYAFVQAtOgEIoQq/ACwjgoFfRwPO5mIdQGuFQYgLlqpHlaiOVlgkPFknJs3w2pgaK88H/YU2q7ZCqMxZpy8XeMNZ4aLBuKWBuvB2Th0x26Bqg0UdoPW5QjHoNtpqMMtaLVGOyvBqEa5mJaLGSYhXUlUNEuSTWOqyJXYWAkjb8MNaRiC96e9zgLuohReE2hGKko0FchlmgtK0SPfAalKH2F6LEMa2oFd6kYi2u9NjBCjZhhkWhbK+pniV445jJVChnKAnOpEn1ziYqEVDViNF7D+YsF1uSfI0ZTxQgdpSoT/ddi/SyRnyf6Lg2MXhLgrRR9aWO3lZeLvmsDGinCnKaZ6/lqsc8SoZUyxFrfRYGSOULTfK2uWkmlWJdqodFy0Z5Lyq2jItCqbYQi0X5uYNSywEx5nabNq1qYhpy8N630ql7LAtqtDMykTPDPEbmrq1ojLLZcSPfbNtG2c2ra58LrKkR/V/uoxnFmBqQtDOi/SNi0GrD7Np0Vi7Gni1Kt/TysKQusIecpx7XXbKQS0+lYNzegba2Hq3u5UKyVZh2q0GFRYP5lYtXKBU+V2GeaNc4SLbWZdLTusnbLUrF+fmBlKoQ03Da1dasJ7OTydjkqRO6q9dZe529qrptfUWCMqaKHOULTxdfYZgnMxvI2zc4Rf7mnbYbThG2rwgbmC93WCLurFasxvX3Vuezafud7qUv7bqoJWNlVf6TVVogVKYQ7RXtNat5vkai9amna6MVCW1Vilyxon0Xb2Lz9PFFfKDRRHRiD7yFNi7WifZvEbb1XCRuqED60TbZuv/Krfa5ZNe7vpgv756vbByMubbw2X8t9ZW9MVbyxDRdrUC32g7aPOnfoazja9dXcy8LOqwP7vkL0PrN9jf9Pfb62LtMDnrAk4N+u+imt1/F4HqgwSrRXwSPGG47pSBx7mrDcNo1x26wR2i4N9NYNRiDfODw9BiEMwBlxeiSW8vaDML1VlOdiyVhM+R4YjFrMxWe4KB0HFjAKGCestuY3bFptL9ck1lauKrC2V/fCr/WjnXmVqINqYR2lgrttPm2ev82eporaBcg/p33MonYfqulujmh71feVBHYH91BX/bXmJ8oCvrkm4Dumi15K2n0v121eYDTuReYGfPbU9lNPG7P2X2imzbbmtXvBksDOLmnfO9XCT9UG/Ma0gN3/lr7adjvXWEmHXq56i1+PVxywL27LU4UH1qSeGliZWYGef2uFEsWsrtWU5vl/bRW/HrnNh3JvWSgimkIctTyg7ZqAr/pnY3cTtj+rgz9f8Ku1KAlEMx13jnZKFAqJqoRm+blVJvbbv19zNWCLszr40LZx+e4vFpou63BaVXeIuLq0c1d3sNurMcK/1hSXrkL032ZXldf0N0+s/0yxmh29SZsfvspZibyan5kjNM77L22fjyZXR+uuCHhuTf/arqoK2MdVD3+tDf2rGV21j6Fi7r9eubYYj59tJYFIUJuNFlcWiVWddd0aVF+n76s98/lVCs9fHPCrc0UMNg86RnH/fvXb+tP2ZEkg1rj2RG7r79frqGnramRcJPr89T5uW7HC63Q97b8l7VUt/3qEa+OKayUqCUTLtXhCtvXAT5l+WNoV+NnYGzIgE89DFdMemOuK940MhO7A75zjYViAs7v4K3oZ+Gh0JqQj8Fa9oCfeTTjw3ktFTFKF46XiM0883cTZfu2OLxKe75+dE5waKHbnvHa70E7BsoC35TKNER5aO0NHBOKsykAEz/endpJWi5oysQJjMb16bnCr4jcrHif89+ROFfz8f6RJxbRWeAi+Vqni7JkirESLJ7q1c/7vjjBPxAAab8n/yihtdanX2WN73+MWVJVMKywqUV9Qx5WWqMMrZ1XWYpE6oLK6qrK6sLascpZaVV7UTR1YWFv4b5hSeWfq2MryObykRh06C9v1yMrq3hWTtG5qv/JydUzZ9NLaGnVMSU1J9dyS4gGVs2pLKngn1QvUmkJshOVl09Tikpqy6bO6qP2qywrL1SLkKizDyorK6hK1dE5F4ayymlq1qLSwurCoFhvU1JYV1ai1pYWzVKxboFZOU8twlKrqkuKSopKamsrqGrVwVrFaiP3PKSpVywJdlc1Sa+fMKlHnldWWYvMSLK0s5q05XV6IY2D7QhSmrax2Xsms2rIS5C5CYk71gm6qUEnl3JLqQpxebXVJYW0FVvEGRXNwijV8sJrKaSimEGHanPJyJIWsOHxFJQ5SNqt4Tk2tmGpN7YLyko6a4ItTw0cpqa4omyU4qitnYreFKH/RHBxolpCsuKxweiWvn1dahjMsLSmvQo1UqtPL5pYIBrHKhWo5qkOtKEHdzSorQvbCqqoSVOOsohIcRFN3GVeWWjIfJ1NRUr5AxbnV4CKX8z4qysqFemsDdlMTGK8IW/xfjV1Nb9NAEG2VXFopfwHtCVopOElJSRqJQ1XSKipJUJMgcUIbexOb2l53bdcyP4UbNw4c4AgS4rdw5F/wZuJ8Ub4OTeXtzOx7M28mk1w6VSKNlbPIprpJCWxqU/7FTIMyIoJUknjhnKgbhboncZXKFCNlrCM8BnIu33ghQqvEri6SBnfHiyNf5nQFeYcqiyMZARpMHEBMvJgCk3lkdKA5mrXUamdB7UrNU1+azgv4kWqPrPaROOh7ttFUo0O26o/513sxNqh9IM01Mf6b8sFlDhEq6I01BdPJSDyXibgvxn0xnM0sBqb8WGUuzKzBcNw7752djnvDgRiei2e9s+5g1BWnF1fdbr87GFf2K/tjF6VYZprKQoFBDqwTrsIKDzpPz42M3JzvIfFTnqa5yHVKnjYpFOjS0GH1QRMQFOsamvCgZpjLuVGK1GuJl3BzJaSjp9R68Ey2wFC2MpKgQrEVVccoO4E2Zsj9GheVXc8Vm7AsVn4oJxQ/TROEBkyNLtwg9CBegoL4V6lYOZNCxa30UzmFKmUMVW16W2ISss7zJQtwKoqDlpAijpTtzTz7LnOBLIasUPKVjuNRjaEcw4OrSseGc8sT4RdQvhd4RAiXsF2mzXW8EDZrmA91Bs2kU9+LXboHsRbpDiBu4EepolwsBF9kaPsizkdvtiZHE+8mVTFfg1lpKxMWDEyBm41jV6e+A63eeipbjLg79MkOlVSYGs56LK44AhYPYztZ15iIyQL17PdhGfLKoZgVRSDcI5MOGUxGp+KhOGg3W4ei1Wg/rDfr9b29ySUO641Gs4nX1qOWaD0+Pjk+qey7SRJ1arUsy6xgWXhbB5s9ocRTIzPKBVoQoBDpSk/RoQPMLI0BX6UmNZ7tSTGS3Bsx3rHaR3+IXXOTwK8FSSgDVQviV5LmhEWH/+mQKR+n6t8u9FQr8sjWWIY0fwyW/C/zcl6T8t0K3sxf4/kHrwLLv494WaSViJYWp/S29Kn0tfQNP59LX0ofNmJJXgyWz985ttq6S21F43jle+VG+bJ8UX6C1xNYS/6I6BTriLv7cfddaYdXPPoSxvB6RjF2dn4C7v2KrGVuZHN0cmVhbQplbmRvYmoKOSAwIG9iago8PCAvRmlsdGVyIC9GbGF0ZURlY29kZSAvTGVuZ3RoIDMzNSA+PgpzdHJlYW0KeJxdks9ugzAMxu88RY7boSIBQlsJIbW0lTjsj8b2ADQxHdIIUaAH3n6J3bXSIhHpZ/sLn+PEVX2oTT+z+N2NqoGZdb3RDqbx6hSwM1x6E4mE6V7NN8JdDa2NYi9ulmmGoTbdGBUFY/GHz06zW9jTTo9neI7iN6fB9ebCnr6qxnNztfYHBjAz41FZMg2dP+mlta/tACxG2arWPt/Py8prHhWfiwWWIAtyo0YNk20VuNZcICq4XyUrTn6VERj9Ly84yc6d+m4dlqe+nPOEl4HEHindIKUHJLlDyjKkXCD5YKD1GukoAoksQTpVSBxzgksiSYQ6IbZI4ogk92j25ir/8/jo6YQ/E4K8VuSVnIucLFMDGVG2Ia8pBakrSYKcmpOSglS5JueyoiA1vt7ebJGRcJ1h7PdZqatzfkz4NnA+YTK9gfvzsaMNqvD9At0yqiJlbmRzdHJlYW0KZW5kb2JqCjEwIDAgb2JqCjw8IC9UeXBlIC9PYmpTdG0gL0xlbmd0aCA1MzggL0ZpbHRlciAvRmxhdGVEZWNvZGUgL04gNiAvRmlyc3QgMzkgPj4Kc3RyZWFtCnicfVJNb9pAEL33V8wxHPB+f1iKIkEoDapIUaDNoeKwwVvXqrEte5HKv++socbpoQfb65k3b968WcaAAuMgGDABTOFHAksVHoGzFJgGKemH+3sgm7bOTgffwt32V+HIZrGEoxETeHjo03PX+WVdBSCztnDlegdk4buDrzJXhZjo4Huko/ACeyAfq0OdFVUOZJX5KhThPH0Csj29hXPjgezwTfFTf60KBHpI+8I+DqTvc+37WJ/whwH5XGSxxdDhAt243HeDxjWQ57o9uhLIwWHRNT6LOgOkVCXcCCmR1TVPvsh/BjBMJZZTNOg6T4ApZyxJmaQapZQu70BeNM3n9W+UMNVaJkpRY2EquEwMNVQAp9wmApmAUWESRlNho85YuCxKz8FeZoyBZ3f0IydXwZXFYVblpUcM2QZ//AYShaVWIsvIlqixLZpQt/9ZzONqsT13SLKqftQQQV/azLdxHXd/1zEB8uLzogvtGe5mWf3mJ3E/TVP6YzSBIn/PtKs/rRZr19w2iU69Rpn/6MEr1c83LBmLIySK5+9WS17RRYqPURQtYv1tNCaxF/s0ZoQQCUWH2SiBSAuaglI6YYoLCZrFG8EoAtApBGiLgVsesTR2MAxbjeJGgzHAOU+YNqkB05ch9Bayg4A94AvZb+VWvVPYFw4qx93NQLeHFEfh8tolEvBRkit+VYBnnIprPSLiVg/Zfdz7H0r7+8llbmRzdHJlYW0KZW5kb2JqCjEgMCBvYmoKPDwgL1R5cGUgL1hSZWYgL0xlbmd0aCAxNiAvRmlsdGVyIC9GbGF0ZURlY29kZSAvRGVjb2RlUGFybXMgPDwgL0NvbHVtbnMgNCAvUHJlZGljdG9yIDEyID4+IC9XIFsgMSAyIDEgXSAvU2l6ZSAyIC9JRCBbPDU2Y2Y4NWU1ZmNlMjZjNjExOTcxMmIzMTY0OTUyMmE3Pjw1NmNmODVlNWZjZTI2YzYxMTk3MTJiMzE2NDk1MjJhNz5dID4+CnN0cmVhbQp4nGNiAAImxjALBgABngCUCmVuZHN0cmVhbQplbmRvYmoKICAgICAgICAgICAgICAgCnN0YXJ0eHJlZgoyMTYKJSVFT0YK</ns5:Zawartosc>
      </ns5:Plik>
    </DodatkoweInformacjeIObjasnienia>
    <DodatkoweInformacjeIObjasnienia>
      <ns5:Opis>NOTA 3. Srodki pieniezne i ekwiwalenty</ns5:Opis>
      <ns5:Plik>
        <ns5:Nazwa>NOTA3.pdf</ns5:Nazwa>
        <ns5:Zawartosc>JVBERi0xLjQKJcfsj6IKNCAwIG9iago8PC9MaW5lYXJpemVkIDEvTCAyNzExMS9IWyAyNDcwMiAxNDFdL08gNi9FIDI0NzAyL04gMS9UIDI2OTkwPj4KZW5kb2JqCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAp4cmVmCjQgMjAKMDAwMDAwMDAxNSAwMDAwMCBuIAowMDAwMDAwNzA4IDAwMDAwIG4gCjAwMDAwMDA4MjEgMDAwMDAgbiAKMDAwMDAwMDk4MSAwMDAwMCBuIAowMDAwMDAzMDQ0IDAwMDAwIG4gCjAwMDAwMDMwNjQgMDAwMDAgbiAKMDAwMDAwMzEwNSAwMDAwMCBuIAowMDAwMDAzMTg3IDAwMDAwIG4gCjAwMDAwMDMzNjMgMDAwMDAgbiAKMDAwMDAwMzU3NSAwMDAwMCBuIAowMDAwMDAzNjQyIDAwMDAwIG4gCjAwMDAwMDM4MTcgMDAwMDAgbiAKMDAwMDAwNDAyNCAwMDAwMCBuIAowMDAwMDA0MDU0IDAwMDAwIG4gCjAwMDAwMDQxMTcgMDAwMDAgbiAKMDAwMDAxMzI2MCAwMDAwMCBuIAowMDAwMDI0MTIzIDAwMDAwIG4gCjAwMDAwMjQxOTIgMDAwMDAgbiAKMDAwMDAyNDQ0MiAwMDAwMCBuIAowMDAwMDI0NzAyIDAwMDAwIG4gCgp0cmFpbGVyCjw8L1NpemUgMjQvSW5mbyAxIDAgUi9Sb290IDUgMCBSL0lEWzw2Mzc4QjkxRjVBNUU5RDgwOTMzQ0NBQkYxMzE3NDU2Qj48NjM3OEI5MUY1QTVFOUQ4MDkzM0NDQUJGMTMxNzQ1NkI+XS9QcmV2IDI2OTgyPj4Kc3RhcnR4cmVmDQowCiUlRU9GCiAgICAKNSAwIG9iago8PC9UeXBlIC9DYXRhbG9nIC9QYWdlcyAyIDAgUgovUGFnZUxheW91dC9TaW5nbGVQYWdlCi9QYWdlTW9kZS9Vc2VOb25lCi9QYWdlIDEKL01ldGFkYXRhIDMgMCBSCj4+CmVuZG9iago2IDAgb2JqCjw8L1R5cGUvUGFnZS9NZWRpYUJveCBbMCAwIDU5NSA4NDJdCi9Sb3RhdGUgMC9QYXJlbnQgMiAwIFIKL1Jlc291cmNlczw8L1Byb2NTZXRbL1BERiAvVGV4dF0KL0V4dEdTdGF0ZSAxNiAwIFIKL0ZvbnQgMTcgMCBSCj4+Ci9Db250ZW50cyA3IDAgUgo+PgplbmRvYmoKNyAwIG9iago8PC9MZW5ndGggOCAwIFIvRmlsdGVyIC9GbGF0ZURlY29kZT4+CnN0cmVhbQp4nO1YbY8WNRSNoiY+v2Lip5nEKb19r98QCYIGdN2ECPEDIsrbsrwZAn/TP+TtzLQ9fZ7ZhV1ZNBGJSTu39/T29pzb++zTTgpSnUz/8uDOweb8nu/+eL6R3R+bp5sgdPpvsuH4zkH39T4vDV3s9n/fSBFjNMpPNuq0FtF1PkQRuv2Dza3+h2FUwgUrbb83kLDBS+qvD2MURD6GvhtGLYwP1vVXBsUfyfTXysKfhtEIrUjrar08GKGUCv23ebA/jFJIE0kHg4DgncMwrv8ru33+3RxGoNBfmLZ0UbM/x+6U0f1NgLqencru82mcyq5qCZsHc6hKUX8xf7o5kBYqKqpI1wav1C/7VzmXcc4lecVrIidy/7dN/9Gw/6Dm2YlgdFSz7Va/Fto3eTM4bDHeGEgJ77xfopM29pe+2Q2vWsu5vi8Huz54H4SU1IQdmB5e+iXsj5uwLd+LCjGHfbGmSIfYoEQmjvW0evhRBSWsUt3IYcowY90enNBB8yEfpfOSk871YhiJU0XRw0VeKHyClXcHLbzhjPQPEn0sLwyRQUcnovXaYoq/GEabiKQscupwDiD4Aqtcf78Of+MAJEXe63ZZ+WzeVfn+cbEeDlbKJhfeCSd1JsK5JhccUyg0uFPgYF+OViu+Ja3wDLScjAiHvg7Pzwgkfa9W18Y6vDAY4hBj7L+sO9SEy7oShiPkGWANLqYovDKIVYm7lsXnRWqvy7e67iUfiRkYbUgXqyRTPOj1kLfEmwSwyIih+q+GxLzgGhdbA1erQ72aBrO6wK+mPGRqjN4LJY1OCtDCuvn6r0FFzbXwAodKwltDePuwk5gPoJO9YZ32Qhsyb6PjegFVBJXuDwelhGaVTawMXLmUw2CelJXA2rsryrg/KB+aGA0TW5lcJT75tAnSi6iMNTnICYeT5hl71E4oTgqGcb8Svl4qfKziureIXjXvVY14OTBZZBwgNTzcKhrJvnX2KeYXoJZXs5Mzy5WNfqoC1I18Zy5sk6HuUUVRQmDrwintonD1SdGrDBSrFUStLlCrfF+j8/k9oi5V/NJC6OCWFsLadCQbJWd07iGmnJPzLqYaYoS1LhJ+PWqobN5Nld3IMh+jb98ZiMZy0rUuTN/jDEZDPj2lI3EfxCzhl7YMv6vDK9Pb40gZjIKrCEn+qqcFOfhLaUHQli//GiJwl7UTseUBcUoWXZ7bCZkLJDG5c8gAmLepiXHMgCuDF4a7Nt1+nIOjJqI3YvHbKoWn2GSCG7KU5sin+7l+vTGM3ClE5dP7MX8lhFrJFAdzkkwRl3jnzeky5Whqa8gbQbmbuTVXa96rObQqH3UZyTL6si6M5aNJ1M+t02h4rJXrUqk20NFtcdiweCw/iFLp09KdMyKZ0G9L92eVoodz7Fan0l6GD0tA9we+eEXkGik+qUuXBalLuJtqneS2ynCFqwClukPARqd3OGxVeIiY35PU2tYqn/F4EzKpG2ju6mW9Y/haj3G7hvactc6thNmKfAUUcvOsUhWgXnMdj9xsNJs+xk0rajoQtxZHIN2pw3vF/88yqpirR9rxZvDEz8jRG/PmPL2omXg1kBfee90f4M8AzU8f3+P704tVRwtma5gy62OTzrfwKuLbTUlacAx15NSzAHeMMSn+HP5/OUe/VkqfwOsISpeVUE/OhtOjlsIYZzGVcJTba4WnooKOX6Yeznubec69Whvm3LlwKyCCLc/D6g1JuMDmhhbfE97Q8Tdwso4oe61Tu+QA0vao8rneEGSmrZU6sAxM86zDiwDpBthSdnfelq2n40F+MD5cwkkvATIP6gCEfyvNhgvfCVqc4gW99OOVwtE827s9BldKY3KP8dlOi6HTT1bt3klTtCWB998ULW8Q/3wVFP/5ZR7Trr7b1xc4f7iWaoBtupR5qWqesX/2ItR3/EPd+X8Uf8NN2FlXpeIP+a5J2G28uSHBK7KS2+7oT11mdrvUpuJExz75L9C79cZw06W8Pq7ecNDa2rO4ptOxvv4lp/mdtvYCHqS/9zsyVP9GisXYCZJB/z9ej1P++p2H0599889biP0IbShOrAtNK/+utcFUOy6H5I9IIUkGL3f+tikkX/VAwQmlzZn+Bry0v/lxE7qXG9Vd5f8fbGR3ecP7d5b4Gp3vDjZaS1umjzY/TWYTWe7VnKfJrDWhu2HmN+6TvfpPdvQ3RqI7z1p0GdvgYuvN1Gt252mL/qbg9fHBJXcpG/d5Cu7ZvrhX++QeJGY2T6t7sc/uYJ/cvQzovkyre7HP7mCf3B1pdF+m1b3YZ3ewT+6W1QPuy7S6F/vsDvbJ3XBPCe7LtLoX++wO9sldK4vuy7S6F/vsDvbJXamI7su0uhf77A72yZ10c3HLtLoX++wO9sld6ubilmk262gUmPO0ohf3GR3cJ3v1n+zojxrKikLOgoSKooCzKKGiKOS0RMqnWUNpgMvoSGmEy+gN5SUqIs0axgNcRkfGI1xGbxQhUTBp1ggC4DI6CgLhMnojGIl6SrNGLwCX0VEvCJfRGz1JlFuaNXICuIyOckK4jN7ITaIa06xRG8BldFQbwmX0Ro0SxZpmjRgBLqOjGBEuozdilajlNGu0CnAZHbWKcBm90bJEqadZo0WAy+godYTL6E0pkCh1nrWVAOAW9EbpCLego/3Hzd/JCJQHZW5kc3RyZWFtCmVuZG9iago4IDAgb2JqCjE5OTMKZW5kb2JqCjkgMCBvYmoKPDwvVHlwZS9FeHRHU3RhdGUKL09QTSAxPj5lbmRvYmoKMTAgMCBvYmoKPDwvQmFzZUZvbnQvVGltZXMtQm9sZC9UeXBlL0ZvbnQKL0VuY29kaW5nIDIwIDAgUi9TdWJ0eXBlL1R5cGUxPj4KZW5kb2JqCjExIDAgb2JqCjw8L0Jhc2VGb250L1ZUWktYUStUaW1lc05ld1JvbWFuLEJvbGQvRm9udERlc2NyaXB0b3IgMTIgMCBSL1RvVW5pY29kZSAyMSAwIFIvVHlwZS9Gb250Ci9GaXJzdENoYXIgMS9MYXN0Q2hhciA1L1dpZHRoc1sgNzIyIDU1NiAzODkgNDQ0IDQ0NF0KL1N1YnR5cGUvVHJ1ZVR5cGU+PgplbmRvYmoKMTIgMCBvYmoKPDwvVHlwZS9Gb250RGVzY3JpcHRvci9Gb250TmFtZS9WVFpLWFErVGltZXNOZXdSb21hbixCb2xkL0ZvbnRCQm94WzcgLTIxNSA3MTAgODg4XS9GbGFncyA0Ci9Bc2NlbnQgNjg0Ci9DYXBIZWlnaHQgNjg0Ci9EZXNjZW50IC0yMTUKL0l0YWxpY0FuZ2xlIDAKL1N0ZW1WIDEwNgovTWlzc2luZ1dpZHRoIDc3NwovRm9udEZpbGUyIDE4IDAgUj4+CmVuZG9iagoxMyAwIG9iago8PC9CYXNlRm9udC9UaW1lcy1Sb21hbi9UeXBlL0ZvbnQKL1N1YnR5cGUvVHlwZTE+PgplbmRvYmoKMTQgMCBvYmoKPDwvQmFzZUZvbnQvSkVERUZYK1RpbWVzTmV3Um9tYW4vRm9udERlc2NyaXB0b3IgMTUgMCBSL1RvVW5pY29kZSAyMiAwIFIvVHlwZS9Gb250Ci9GaXJzdENoYXIgMS9MYXN0Q2hhciA2L1dpZHRoc1sgNTU2IDYxMSA2MTEgNDQ0IDQ0NCAzODldCi9TdWJ0eXBlL1RydWVUeXBlPj4KZW5kb2JqCjE1IDAgb2JqCjw8L1R5cGUvRm9udERlc2NyaXB0b3IvRm9udE5hbWUvSkVERUZYK1RpbWVzTmV3Um9tYW4vRm9udEJCb3hbMTIgLTIwOSA2MzggODc3XS9GbGFncyA0Ci9Bc2NlbnQgNjc4Ci9DYXBIZWlnaHQgNjc4Ci9EZXNjZW50IC0yMDkKL0l0YWxpY0FuZ2xlIDAKL1N0ZW1WIDk1Ci9NaXNzaW5nV2lkdGggNzc3Ci9Gb250RmlsZTIgMTkgMCBSPj4KZW5kb2JqCjE2IDAgb2JqCjw8L1I3CjkgMCBSPj4KZW5kb2JqCjE3IDAgb2JqCjw8L1I4CjEwIDAgUi9SOQoxMSAwIFIvUjExCjEzIDAgUi9SMTIKMTQgMCBSPj4KZW5kb2JqCjE4IDAgb2JqCjw8L0ZpbHRlci9GbGF0ZURlY29kZQovTGVuZ3RoMSAyMzI1Mi9MZW5ndGggOTA1OT4+c3RyZWFtCnic7XwLeFTV1ejae58zj8xM5mSSTN6ZMznJkMwQEiYPkhCSk6doDASMmACRhEcMDyHhpVArAUU0vmhVKuIDX1Wxf5mcQRzAR1r9tVotqH8rVS9ETVtfAf4W9a9C5q59Jrx69b+9/Xq/2/t9OYu11t57rb322nvtZ4AAAQAb9AKDpumX5flB/y6/FsmsBVd3dEfyzaeQzFywdrW8+qO/PoDpDwBM4zu7r7p60oPXSgBmE4Ch8qpl6zoj+hkn0eiSrkUdC7/IbFkHMPt+LCzuwoJYIRnr26diPrPr6tXXjrZ3CED42bIVCzoi+fx2FN9/dce13SnbDDtRfxkWyss7rl40qt+ExNO9YtXqSH72dVzevXJRN53Xno36twHEdol3gEu8VMdUdjekAIQ/RBxC/GTkkvApcSkoI0vCgywWgGRGcPTLghshEz6BbfAitMGvKYM6MgFaQCCJkASUlEIDkSABRBIF2aBAAzRBPFwCfyA22A0T4TNSDxtJFkyH+yEDpoETquBHsJNcFP4UNsI7ZDE8jbWfJCqMg0vJ1PBRmAFN4WexDYDJ8BO4j0SDCyVRRAkfQQurYAvsh99BGGbDveJOtNIEM2F5+FmYC2+R2WROOBUuhuVwPdwLD8PzMERuJgOCGG6HIpgPK4mRxJJstin8JJSIh83PhF8OHwIJ9R9Gq19Qn1AfPgYqfCKQcBfOhFgoQFgOj8Be+IAkkiJWA9FQiG21wXWwm2Wjj1PhFuzbfvIDsptFhx/D3kyCBbABBsm1ZIC6xcPiifB6cGD/CtHTPngMfgEvwedorZ40s6tHKsPTgIAJfFCHLd0IN8HPceR+ifAysRM3uRgt/4IcIR+y5eyPaPkJGIav4L9INllMrqeVdJPoP70x/Ax4sIcq2rgYroBl8DPiISqZg3Xvp9fQ6+kGtpd9IGQLx8Ml4ZfAAHmouwl2Yb9+A+/AuxivetJIfkevZ0HxpvAP0N886MJe3AiPwz74kojETKwkjsikgEzCnv2ADJAPaRpVaAubz3aLt4XXhW8HN86VNliENZfADbAZnoWD8BF8DsMkGWvmYc1K0kRuJ3eSl+lBdgWby7YJqrBNeFr4pXBKjBF/OfLWyCCOOreTD40IbdAJ63GsQwgvwXuEkRSSjpamkEvQ0jzSSa4jW8k95FHyU7KXvEoOkU/JcfJXmkhvo3fTA/Tf6UF6iKUxL6tlD7E3BLfwnvCtseN02siLI8fDlrAvXBDeGr4//H54WI9CKs74SqjB2bUU94IbYSvcAw/gmO+BN+G3OO+O6jAEJzAG3xIDzqYk9CiDKGQcGY+9u4K0kGtIH7mLPEZeIR+SIXKKArXSDAQvLaaX0Ll0E/2CnmJRTGFV7Fr2E/Y2+0ZYJ/oRnhafEU8YhoxZpjdO7Th9ZARGFo9sG9kRLsK5aMCZF4trrhCqcc5dglFeCD0IK2EtXINjtB5H/H6cObtBgwPwK3gDx/4gvI87FPeXw6cYiZNwGkYIxXiKxIQQ8T0fI1ODs6WdLMLYRuAHZBO5hdyLsIM8SB7G8X2LvE3eIUfJx+RL7BPQXFpFL8IeNdE5tA1hHl1AN9Jb6R6E39Df0ffpR/QbJrEY5mLjWB27it3M+liA7WH/wX4reIQqYaqwVHhVeAt7PlW8WJwnLhBvFR8WHxV/Kb4uDolhw12GRwwhwyfGKGOxscnYbLzF+JTxgPEDY9g0DudTI3qfA+e+u8gcIY9uJWEawn6/QFezX9O7ydPnaYDYhx4shHk0xJ6nD1y3lX3EfkY34Z5bq4un4C72BjwHb4jvCPHiJ/AqTYZjuB/ezTroC3Q7TSTFbLKwWXgDd5116Oej9Cg10t2o8TlGYx5cTpLgz8IsOI7jf1DswzGtp0fI0/QVegnO5MPwGD0A22EnLCKT0LuF8Ax8Az8i+5hM9uK82wCH4AsYPOetkHe6mlYaEulaQxlGaB+ZEX6V5oQ/x1X/IdkM77NvcO7PItNIHvwUPsao/5YUEpcwIqTAW7jzpcMOnLV/giCuwdeFTFxBX8I+VgizhUGMed7p10ZqxdXsBvIVrcJwJug793S+G+MefC/uVXwfjYbdOBNwF9FX9OfwJsnAUXzH8B7cB3fCfhYPWexx2kvD7FeCDD+GQXYptvpD3J9SSSFauhoWYz/k8B9HHkMLS6AESsh8MhtqUTIV0sNXo+c/xb1IDc8NbxdbRR/8hlxK4uFF3L0ScRS3ieaRYdTcg+vwfZhKboXgyEIYwHMlkWQRP86mYXGtuFXcJe4RXxDfNEyEa3HV7sAofgQn8dSQyQIci8/ga5zr1bh6xuP6qUIvpuIZtoy2suehhiRDN+6B2bhvV+MYzMZIrkIrm+A2XE+P4xnyGzhBJDIXXoDDuHIScJ0vwPZNaKcBLseor4Kf4u54AwliyUJIBy+O0zckmpTQ1dge32e34T47gD59AH/EnSOs+zWeTCa1GL0F8DVfy9hCMTSRfjyT90IpnpS17A34A2Ti6VqNa/QxrNeOcyMa0qBU/JhQGD8yLVxCF7PniRNPw2icVc14sk8hPeiFHftxGuLJdCgauQitPY17WZP4OJ6+PjwZ4mm8cIV4Ofr9Hp5kv4GV4RZyn7GWvctOCN1q9eXNamXFlPLJZaUlk4oKC/wT8/Mm5I73eXOyx3myMpUMt+xKT0tNSU5KTHDGx8U6YiR7tM1qiTKbjAZRYJTA+Dqlvl0OeNoDgkeZOjWX55UOLOg4r6A9IGNR/YU6AbldV5Mv1FRRs/NvNNWIpnpWk0hyOZTnjpfrFDnwZq0ih8jsGS2Yvr1WaZUDw3q6UU9v1dM2TLvdWEGuS+yqlQOkXa4L1K/t6qtrr0Vz/ZaoGqVmUVTueOiPsmDSgqlAgtLdTxIqiJ6gCXVl/RRMNnQqkKzU1gWSlFruQYBl1XUsDDTNaKmrTXG7W3PHB0jNAmV+AJTqgN2nq0CN3kzAUBMw6s3Ii3lv4Fa5f/xA320hCea3+6wLlYUdc1sCrKOVtxHjw3ZrAwnrhxLPZdG4o6Zly/nSFNZXl7hY5tm+vi1yYOeMlvOlbk5bW9EG1qVZ9e199dj0bTiIDZfJ2Brd3NoSIJuxSZn3hPcq0r9FSh0vaV8iB8xKtdLVt6QdQ5PcF4CZ69xacrK6LzwIyXVyX3OL4g5UpiitHbWp/XHQN3NdMEmVky6U5I7vl2IiA9sfbR9NWG3nJxadlekpXZ2nGmaeHVnCPVIuxgkRkBfI6EmLgn0q4WRRCfQtKEE1/FoJ1gosxIgsDphr2vukMl7O6wfELEmR+74EnAHK8BcXlnSMlhiypC+BJ/k8OTvVUH4mHfD5Al4vnyLGGowp+lih54tyx68N0YeUbklGhsMHTTi2Ha1leTj8bjcP8K0hFeZjJtA7oyWSl2F+igZqnq81QNu5ZOCMJP5yLuk9IzlbvV3BmbwH+CsmPmDynP1jl5yxdV1lAeL8b8SLIvKGy5SGGbNb5Lq+9tGxbWi+IBeRl5yVjaYCsTUtLIWOpmgK06U4KeeeVeaZFmtAyMI/Bn1SLwwZTTgr9RIi1wek9qkR2hrldv+dlULhE7yWzs5VG3UzUOa7MD/5gvwF7ln7GDoseGhD8+y+vqgLZPW4A/X11StyfV97X0co3DtfkSWlbx/eacb1dde1n4loKLz/1pRA/W2t2IkuUoazlUJ1v0JuntGvkpsvm92yDx+I8s3NLRoltKa9urU/E2Ut+2QAVS+lvJQX8ozMM/i4womuUZOun7JPBejVpYJeoOcXhAjoZaYzZQQWhGikTIo05NEbUvGUXxASIhL1jLaAZaZIWW9EO3tU24QSiUv240MPQBdGPr5r1DS3nD8f9EXWmgt8ihndI3VwhQTf3vKNRdJLzv+Ij5c4GunbUI4vDwN6JeFrYxa+pHeEwyACrbJCsxBP78OzziXEI8QJsajrEmKDhjSXHBKsQWu0n3MtNsEfEizBbNllr5IEB/QiUrAjrUSch8h0SkAVHNq1BWoI2coIWx5hSyKsuUB9DhUvgYLwgOAIJiT6eXEwyurv5dxk5vkYbXaBWmUWYtBdrhcDl0W41lSgixu5lRi4KFIarK2L1KqOFFeMKpcVuKoyMS8jqojdiLsRTyAa0PsYyEPcihhGFPQc19uAeCfiTsRBrqtbMxXYq1IECSWS3ncJRwpHFJFBu2DGvgd0ahdMOCommI74kGAEQYjSYJlrHxphwTrdUxb0TdC5lp3j1wVacqr/eTzVt+MD1oUFRHOm6BLQqqtHE8UlkUTQm+s/WhUlABxHpAIIBK9Veq1g9gT/iRcxT9gI2AnhpexUUIrD1tjpoD3Wr1ZJ7K/QhEghwPphAJHCCvYlbECkqL5by53IG2K7g1HRfgn1j4OM2IvIYCdSoudVRK5/PBjr5Ob/pNlj9HpHtfzCSCIoJfqbquLYB+jPa+xtUMCFT4G38fLmYq8ix4nHXmG/Apvu52NBu+TvxfYeRfVH2TrIQfHjbD34kT/Jrsd7F1f7vRYdaef3WrbXXxXFnmDX6SqrWA9eO11sGVuq+V3yAfYYn4/si6DZwv37QpPi/c+zT9lSiEOtIdRKcNmfZ8shD5H3JBQ02/xbq6wshN0M4bC40EcCD+lUZW9raAjbe4r14hXPxQ6yjXiVd7FdbJMW7xo4wL7W1b7iVrC9R3DGcBa0RfsHqszsET5D2J9xxP+st3Yy6CnxQ5WH3Qb5iBQH9WNMfcwXKjuGqWMYpmMYmmMYmmPoxTGctMCGUTKMOnnsCHSz92Er4kOYFtDkOg1HcJ+eyMz272M/ZNfhSEgHcOwIll4fNEdzz67THLG62nV8gVc+z96F6YgUnT/MV+SKA+wOvStbg4kpvMJ/aGYrDt0PIrHAiut5DJ5nvWyTPhIb9REIvIBZnP/sBr1yOGiN8W/A6DdjdgXSOxEPIR5HFFCtGfvQDPMQGao3BaPtfvsBNluvfLEWXeB6nk3Frk/VR2uqFp+h+3zRaEKwaynp/hd4AnJxp/ML0YJBy3PNOMAacP5MZ9O0hS70fYaGdnnFacGSMn/+ATZNH4tpmkuJFGuxSXqiXjNH5lVNMCqGe1KrK/o0U7Re7BtdkswbjEvwu3Celum9LeD7LJuE4ZuEoZmE66RAD4Y/KDlw9i9kfr1HfmhH3IkYQBQwxn5U92OM/fic8+sjUozdLYYwIsPYFsMJRNxq2ESoRLwT8UXEQURRL21HpFiejy20I92KSNFiHuYlpCpiO2Iv4k7EAcQTiEY4yHKxnVzUzkfaixhAPIooYKzGox/jUeZgMpw2AbhgA92ulpENsIFsoBvYBmGDuEHaEGNSi7LG+9UlnEzgJBvJpHZzt7nXzPLNqrnJzCSzbKah8IBmLCtApjoMZQXvNX7W+E0jc0zaathqpAerrCQGjiIeR2RwEB9jRxGPE0ndwg5WHK04XsEONh5tPN7IDh45euT4EXYw92ju8VymNqaU+SfNIyvIBnInEVwkj1SS6USYx1awDexOJrhYHqvEuSC0W7otvRaWb1EtTRYmWWQL3WrZaQlYBiyHLGLAMGA4ZBg0nDCITYZ2Q7eh17DVsNNgcBnzjJVG1SCcqKqh7+Og7kQaQKTQi3SrnpJ0yQDSQ3p+q55vR9qt51WkTXpKQZrPU4gK2noP9XqRbkXkejyvIM3neUQFd/ffY1k30q2IlP5eTc3Iz1QzqZQpZ1J8tp7IJIcyBzNpIHMgkw5UldHDupeH0cvDupeHseZhve3DaBdTiAp6+66u9y7qvavrvYt6PPVdZe1Iu/WUirRJTylI83mKvqspk+xVCXQHWpyH9CHEo4gM8pBWIq7Qcy6uQXcgVel9wXHj8cCn92ke3CORZURYeoSl6iyYlOyfV2XHC8pDiEcRGfCcC7GS58IDdLtWy3W3a1MirKzgaFUpnqLcle2wG5HCdKQP6ak8pJV6areuYz+bDyAd1FPdSHeerTdPT3E9F+KZ+gK9D2E7pux0PZauVy0UnE68dTliTI4Q3a8tdrhCdI+WLSELRpjGWVUsZTj+NnJMpz/X6UM6vVunV+jUrloU218V278rticUW1UUvQQysfiETj/V6RI1OtP2SabtlUzbo5m2RzJtB8jHkIECt5qcYftDhu1/ZNiezbDtyrDdlWGbm2GbkWG7NIObygYZbDSNU3KlTlPVBNl2SrZ9KNt+Ldt+Jdselm2tsq1MRnXyZzxTbeR+nf5Ep0XPFtpchba0Qtt+imND5mh2MB+glMwBG4vSvBWuEDPrjLq1xixkqVpjFbIUrXEmsmStcSWyWK3xLleVmdpJP15YXDSa9Js4t2rejSi2RJhJ816JTNS8pa4QGdG8CrJvtc40ZN9onenIvtI6C5F9ydlz5C/QiddoF/lPrfNBNE8+g2xulvwJPPRp5CGtsRK1n420TvZABcnCYnwGci/IzzQvOkee1LzZyJ7QvJnIfhphj2peF7KHtc4JyB7UOu9C9oDWOYTsPi17Gbe3HbJ1O/eCR+ertMYUFPdojdxCt9aYh2yF1liEbKlW8SayxVrFEK96FeknOLtJJ3h1Tzu0Ti+K5412pA2ydfFcKNItX6Q18iGp50aqbKRutCO1pIbf+0g16detqJo3H9UqNK8H2ZTIyJVrnT5kJVo2jjGZpGU/iCNXPNpADo/PcyQT3eCGFM37NCq5tM4cZOlaZx2yFF4TnYodbdUBFbpTMZqXa0maV3a9QCzQqVuMAg+5b6/rNNr9tiJEZmmub9SQiWiur7OR7XV90Tjf9XljCG+9rs9wGT+913UUVY9UYFK1uD7wDrne78xwve5FDTXF9Zp3guslzzpXKPuAK9iY7upHxwKd8127O3ULP/dgNc31ZHaIEqy9s/NS171en+snnhD34ceovIW3gYY2e9e5Nnk2utbgVFjdeItrlTfN1Z19pWtJNm8owbXYO9PVhR25Cuss6rzK1eG9y9VepHt8pfdN12VFeh8aOvUeXVyhC6Z2znTVowcoqOQC9GAyzks/Vp1QdICPEd5WaoJvui6f9BzFk5j0Iq5UJxifN15vnG9sNlbjmTPOmGV0G9ONcSaHSTJFm6ymKJPJZDAJJmrC9yKNC4UHVR9/8sUZ9JefQeBU0NMS5ZSC/iKkxETxsRWIZQ204bLqwCRfQ8gYnhko8TUETE1zWvoJuaOVNAQGFkDDfDnw1WVKiEThq11UqknA0QANzdWJqBygN+P7t7klRMK8xuYU/qOwfUDI+M23p3Bev/n21lZwrq1MrHRUxJTW134HaR+ldbW+c1+iz3dBLi2wreGylsCutNaAnyfCaa0NgRz+47J9dBldUle7jy7lrLVlH+miy+pm8nLSVduKapN1NaigS1ENGjlDNToXKrgals89T430Y3Ftf0VFRGk66edKuGim60qzI0o15yuxW0mNrlTDbtWVHow06EU/sEGVM1QTl4FXb9ArLtPVErlav8eDljo9XKXf70GFfo9fF884J86OiP8tIv43Lg4Rck5e5Il4mw0evQUPzUYd3//Db1H1P1CJBKesXd7Cf8zZrtQtQmwP3Lq2KzHQO1+W+5evHf35p6d9/oIuzjsWBdYqi2oDy5VauX9Ky3eIW7h4ilLbDy11zS39LeqiWm2KOqVO6ahtDU7bWNJzQVu3nG2rZON3GNvIjZXwtqb1fIe4h4un8bZ6eFs9vK1p6jS9rYaZ1aShqaXfBNWtNXMjPEgtUbha2lPcrdVOqbtCXzqT3YnXp+wXgDwJFl9rwKpUB2yIXJRblVvFRbikuSia/yh7VJR4/WR3yn7y5KhIwuIYpRpWJ9YtrsU/q/BbvXoNfjjGq1ZFxjoxIljtq9PlqLAaU6v1DzUxzXGVXjoqXw1rzn0+X0QXVvlqWvobG+sSF9em4EU+yO/evtZV4PNFGvT5ANvEXuuXfad+2bcYnAW/bfxD45eNbEC/5R9CHNRv+QN4wz+EOIi3/HQ2UHGoYrCCDTQeahxE3SOHjgweYQO5h3IHc9mkUQ94U60EPTwHa3yr1vBiH9F7q/ebO4JOY4L3+swwrNIFq/WBwS9Srlf1oSHf2eq+c4lVEeEavUqkdNW5OYwCbn71Gt//+kVK0TiOPXJKUgHEVBGfifj0qt5DyUsGY4iZ1FgQhZcYRBmFlwgkmQziS5Q9R6rATLLILEj0SV+Vny6fJp0sbzxdDpWYlk4hmZjvjnHHZCEhqQKcktnAKVWEb0EWBkD/KR+IAbGL/+0+yVfvypAsjspOaa10jbJFuknZZXtWMm6zBW2UZCoUMhTFHRVtSYtKcCemJVjMxExNaWZnTHyak2RGQYZzlWKXZAXckpu6FerOjZHiYmIkhSpumh1tj4uOttO10SQ6an0MccdIdsGpuGOiqUASFHtGZjaeQYQMSapkZwlOZ1SU2WR3Eud+sgkUMkFV5KikfE+3p9ez03PIM+gxZEke2aN6mrBkqyfgMd55daJvWo/UdjIpufH0cBskVpZLCJXlydLpNhyJmNIYR0Ip4aTNUdpWuiV6gs/0Q+ll5Ik80fayL6a0FP8kgjRMpIEIbTs/Y5TKy43lOKbQRtqIj7iNhvi4BGdCvLuouHgSKSDOSKbAP6m4qHCcZ9w4xljziLs0dULKkpEpF19ZR/4QSz6tz82oON2dMl12GmjqktcPkU03VvtKUyRTVpZlwQ6h7NsnH8xxiVlZTindEWuu/gt5ZyQXz+cl4UHBLS6FUpKrlibmX5FzjZsZoonZbvQZ8hPtCb5cu0/KicnLkH2Z44u9xb6rcm7JucX7VGHIu78wtjQNZtM0QkLkYjUeZtuLXcW0+KmJaWnps+U0l+wieBO7Vq1Pnw3JUjJNfio+x2c3eewWuz3VkmoX1trX5uywP255xvKy3eDLsVsERSyayJSiePN0cuZRLeKLxCN5KN6eJDXakTxZtdgKJ9tNLryKYNEe18QJSWUhUtrfgqGSTjYODbdNk3xfNQ63DbVB5XDlcAzGpgdDVFpaCtIXbSeH24g0fHI4ktaT/Qb+U25VZhZmp1k5Ht8Sy2L7ess6+005m3332H9mOWB53fK63QZtPa08Tj0YqFjF41EyIqHSIT5OUDI843ihUYkpcDoL/Bgvj2fcBFpUWFzgT9BLJrFfWnLSPr6x85r4NDVv17HLZo58/Ya6cla+K7nMkZU1/tsfdW8u6Lpx3yNXHHumuiJvS0pyuk1cOlK+6+DVF+UqeRPczWu6um7a9WVyZlx2DoXDH6+fkT97RtWc3gfnPTIkWavkKXwFTg8PsTb2EsSDTIrUFoOjIa4tbkVcV/yixHVxxqyoJ+gr9LWYt+hb7LDtcPxf2H/ZojbEkww1Nr5wFutkKzKuYRsybmA3RX9m+yTe7DWFncRkNvsA74KyiZnaRNkJpN4ZItl7UjyxRhFv5UGrxezUd1x/UqVTTcoodC4GzO/FrB0nGt+wLdGFnKuJMUWQnJdRmTEv43iGkCHn2ImLUOKXuBLq6zzdEeGe/ELOVas1uvCQRKQkd8kdeqTb+EbXeLptaJrU9pXP1+NrHPb5yvWQnzzNl+nJtiEivdYzMb9mnZoek56WlZiQlEANqQ5XOiTHOdNJekxKOkmIR4I7JfH5vL6NpM0HPW2kh7gxpDxeCQaDkjHOU1ToKPA7jYV6gHGFxrO202Hz7LqO8vklGZeG1h1aOuv0rjveOqZkxSuF7snky/3LLqu5wrlj486NL35G4j995OFrXY6C1h0KDsWM8BD9D4yOn+5S51ry46VKQbLlxElpOYIhzhn3StYrnt9Ln0l/lYw5Upa3RCr2brHco9yT+ZTlUSVk2aNYRKtoM+XEWy+yNFgNqkW1UoffBTuoixA1zlqJjw5H5UOEL8s63OF3OPKwoDDvL75EV9KOFFdyMg8LqmxNJsn4BlTTk3Y4/+JwiB6f0ZHucVgckXCojvhCMsfhxqv+HnOc4XKeUKPMcfTyyGasB9tiL4zkMqJ5vgyj58LdONleSPIKpxfOK1xRuKFwd6Gh0GGSuRFO6eWR1ati5UgqIzkn+8xcySY8qZox3NlJBSU9eqx5dHFhI+Pn+zMmOUWqNHG1BKxiUuPclabyeAWJMwuz2LfIMdiKU6Pnq5U4M85Wdcs4QnpXzGjDfSXW5z0JogmdoxWdoyHOtbO2fK1Dvh60oCYRNTsRBzk1BomUgiQ6AYnNGVFsxRnIG0pPT7dXpofCHwWtcRGOGpxrqK4r6nr7QAw/ozpQV0xHRTEdtcS4MyrSF7hd+c7sV7wDqj1PjYqpzFPNdiTYF67GlSJavOWsXHTNEgofCkY4djXJXpmVmxDNc++oZkxk5TptlVmh8H8GE1ycDz2bKFdaU5PclXD2EtEKuHb4amhrA9/5G56Am11kWeBaUNjZvQ4PJ8/oXseXDr3bnjHlhqqcsjiZeNqm3TGrpjvd4na6pYzcB+rzp5R3bc+tvuf2Sy9KiXE4E9kvRn5xR9ekzJSknFdvnTVtW5PX4idNN9442Ztff9GSkpkLlu3OstsV/qaMx2vMMvYEOKgXQw3MyT5l3zLBFgp/opqVrEImJ6cV8lwwXeb8hNqQlFJYRi+hXWwDW2Pto7exbbZv+a7XwOqttbY5bJb1AHudGamE1ddY/0xpninPLMfIjlnWd61/sn5tNVmoYE2hcVZh9LDIttK42CiaTH9I++gzVKQ2IlrjrWusm637raKVRbH6KIOtnkRFziC8HHDUjyF8rzpw6pbGRFWazDGOGOyC1eZYaFttu9F2t+0x2x7bK7Yh21c2s+1KyuIoZZQwG5itcRYaTVi9xRxiHtVmiQKH5KAOB4kyOHhJtq0e6F4gUXEqTmqII3EqbrhxCQnJFtNeszlqDmFrLDkOn74FS3GloDriCishDHQF359plmZbQ7gsii9D3D6SYuNCpCHo/sXl+EaedhKX4OmTbbiMTp4cxhuhNDxNqltU+0dcWHgzbByWho9VOkrz2qTyYxLvL+8q762jdMsE30p9ptvCg3wRYEgG+aJAvj+I60vPS/ERbtfLB7Xo2DNreMsPX+a2pNek18Cnn7/oBFnZo7/j2nTDDCvierDyWJtjUitpIhI0c6w/oZRfg4mv1e0uIjh9+dnsjid8ehawmlNvU7qto7kwVWGxI1Qd2OVLdbJmpXEBkVJO7Vn+Yz7bhgQX3Syux5tzmWrFmzQGww4uPGRDpH7PBhwmTNTuYdfSJIH/cGJB5BZy8jTkteHhhOfQxHy8LhAllm4eeWbWanH9yONkDtp9SfCTT8UWsIC8x7AFp3SIaJo5ybqfPEbugIiV01DZOIz1/XzB4e0CFxeZNWXWrCmIYovOELmXLSP1ZBDK+d9QqjZbtJgCzh+L0baoGIC8vDf9fpI3fORN6Q3/xPysaBYfl07Pnm38elJBJ5F9mbXF7ni3Lz45LzauaGm+rzY/qax1qb/cUX7JjOxUb2p0rJRUPSXZN9ldvGRufSyEwyCN1PObPi0G/q/JE+AIawZ8Ge6l4PUmOFXiK+Ja00fq+W0EtaZGtEjiqNYkj9V6Rku/iRpTqAeKUcsIt8PvyMMwTk0kU/iNb4oqOZDEOwunTJSn+giZvI9uhxweW/x0C/q5Kq5HC3xIDHA7OuVWY8m84hV4LVXNjsJin8CrFu6j941W7dFr6juK2II12/S234Md6OFEVSZO3rbsJIecYSfNc25w0hXOh5zUyZZzQ0lnDbX19KxEL/Sv+J8DxPp3wNf/e8C73veC0Hk+iA/8X4bX/mXhRAQMvn8iVJwHM/4uWGm483vgmTEYgzEYgzEYgzEYgzEYgzEYgzEYgzEYgzEYgzEYgzEYgzEYgzEYgzEYgzEYgzEYgzEYA+D/NjzyW1rigOk8GwyY5v+CTCQCSSPpJIp4QQAv/7V0CKh9QgyHkRJO+f/xQ3JOU/9FL8B/Fwv//366rebFVy9aJU9bdI08c8XVHcvHV69YtjDyfwHJVhDBBH/f9zd6J+BE+MJfLxNhhlKS+q+E4qsg/TNQWAVL/hnIbofp/xCmwYzvxdsh/v9npLtgaAz/j/Glv8GWf0X8vvX0D6+D70B9Xf136+N71szonvv9H9/TjAD9gd3759nLvzQlRTbBRz4uquf8dzcenv/tLac7JDCtRF3zmT3wfwKay+iMCmVuZHN0cmVhbQplbmRvYmoKMTkgMCBvYmoKPDwvRmlsdGVyL0ZsYXRlRGVjb2RlCi9MZW5ndGgxIDI3NTAwL0xlbmd0aCAxMDc3OD4+c3RyZWFtCnic7X0LeFTVtfDae58zz8wzmbwmyczkZPKaSWbyAiZMyQl58AiPAAEzlJgJ4U0wIQRflRKvIhJUqFoVbQ3WolbwMpkADqglWm3rg4rV+rpW8IpWW6nUorf9lcy/9pmA0Gvv13u/e7/v3u/LWVl77b3W2s+z19prH0wEAgAG6AcGzXMX+MpBeVaYMVnUua6jJ1FefqdSvrLPmb2MvoH5dwDMN67oWbluXci+AMASBtCFVnZdsyKh70F927JVyzuWfdIlnwNYW4nMCauQkTw7cxmA/Uks561a13f1WH+rAIR9Xd2dHYmy/BcU/2Bdx9U9GTbNQdQ/iUznFR3rlo+N5xNMXD3dG/oS5bUpXN7Tu7xnuPz27wFkYdF0h3gbgDgLHIhZ7E6wA8TfQzyF+NHozPhX4lqQRtfET7JkVH9sDAHccBcMQh6cIWXwDIzATHgIaqEZ7oRp8DLsByNcQ14EASSoh0fATRxAoRHSiAi74C1YAr3wAZyEQmiCd4kV22mAHkiFQPxjTJvg5vhh1NJBHfwzHCFdZAH4MD+deokHe94RH4E0KIwfi7+JpR/CByQvPgTTMfchWKAANsP3wApr4IX4V3wFYSk8TK4jH4MLwrBdqBQG4mthMhyE35AmzM2Ga8Q3tQehC2s9SNLISPxE/HfwU4HAcmzpn+BmHHEURmgpqxN3gxPy4VswBzpQ+h14iySTMibHC+JT47uQ+zB8Rj3050yN4/DADGiHW+EBXI3X4RR8TvSkivyQ7EV4hfxRfBPH1gQb4VrcVz/E1XsY9sFhUkbKaBpNw9VKgyJYiLIdsAf7H4bjpImEyAh5mu0R/aM18ZS4Lf67eByKoRVHOAhPYx9niR91sAeWy/qEHKFPLD93Pc5wGfwAjsMrOI53cd0/h7+QYoT36Hfp5vhl8UfiH+BYNOCASTAPFkM3XAlXwY/wrT4Dz8KfyJdUi5ovC8+J14pn4rfj2ubDVBz7XNRegG1vx7cUhRjC6zhLC3HiLCaROWQ+WUl2kLtIjLxF3qIq6qLr6e9ZhL3I3hEmiGK8GltKhRzsV4LLYBW+ge/iat+O830EnoPniY3kkxKc0etY/ws6mdYjPEhfpu+yLWyH8JV40+jJ0T+MfhkfADXusmm4DhvhUVyFT0kqjqGIrCEbyPs48p30ADMyM5NYFatlLSzEbmZ3sl+yXwm9wl7hbXGG2CHuVXeMXjH6SrwpfiOuBQEVjqsAvFAJE3H/rMDdtBbH14PQC9fB9TAAt+F+uR12w16c91F4Hn4Dv4VP8A0AceGYV2Pv63DXbSG3Iewi+8jT5DnyPHmPfMGB5iIU0gm0htbRRrqSbkG4kx6nr9OPWBbrZJtZP8L97BB7SwBBEOJiOcJ0cbv4sOpFdaF6unqp5qWvTp8rPhc69+4ojGaOfnv0rtGnR38XXxS/BsfvhhIoxZFuxVHuwj24B+FR3ImH4OfwEryhjPUzQomIOz6dSLgbvPjWasg0MgNhNpmHsBDhMrIYoYMsJasQNpN+8k/kBnIjuZV8X4F7cG57yE/IIYTHyRGE35AT5EPye/IZxU1MGe5mNy2gPhrAmdbRaXQunY+wknYj9NBeeiW+oYfpMD1MX2fJzM1KWAdbz3axf2bPsNfYXwUqeAWfEBQWCSuFG4SXhVeEN4UvRYfYIK4S7xefUdlVlaqFqjWqe1T7VR+pvlKr1M3qperr1K+p4xo3eqtf4LwPwsWPT/Uy2SCmCFfTE2gX6axH3EoW4oqpaAvrYrexX4sryBnmJG+TAbaarY0/yBrpX1g3WUSPklzmEKvZCrgF4mQvfY+epb8TbKSFfkwKhe+Rx2k3q6Mq3on4qmATbhA/AsCzoJpuIiP0OXYDuyH+FFSL95MT4v30FXAKJ2kynECr3krvxkq/oqvpdmgVKsUvYTWu+0/Eq3G9p9CbSTF7TbgfPmAS/TM5Q+5Cr3GMzBTy6OU0QPaixz1HcuA0WQ895PsgkyfIb0kMCHmEPUxm0SR8WxFqIBPxGDvGXOQ1poMQHyPJpzbSTM/QhexJ1XFWRQh6iV/DtYQRP+6d888oXIEWcCctQJ/WgN7kVVIO6XA3+vuzo09yjy2+KW7HffYA88J88EMbfRGq0TY+QGiFm6AcjuAevBn89B64Lt5PlqHfn43+k0KMrAEf0aO3TMOxbcbzIpXmoi9sx17/gv7/BfT6TeSPcBVxomWNQKHAJbcIDeiZwuh/tyMsgzYs/QBuVx0UX4W5JA1PSefo/bjL34HL8cx5H/vPhCCObzE8IHhx1E70zOuxxg9Gp4OMcBO8SChswjFPQTtvFqaj570rvgZnuBrPqFl4Jj4Pq+N3Qx2+u/nxG+LboT3+QHwJrIQF8UfQ/14Zj8IE2CqG6CLRI1Sij32ePIvn0b+Q7ei3p8Pb6I/cJB1+j/DPOP4p4hMwILyBvrMmfkv8N2DD9cjFFVqKp+gpWAd/xHWbzkagYnQOHYo3sh48oU7AvPjDcQfRwap4F3reJ2GPWkTf0w854h7cu9uFFdSP4y2CVOJD7hJxkL3B/iT0wPgz/ow/48/4M/6MP//3nlSENIy30jGKseMdtggjjmK8mfD43oexTSXGHhPx5hbA+GUyxjnfwihmKsY9jRhNzMI4ay7CAoSFeMcK4c17CcZLbRgZteMddhlGYSvx5rUaYS1Ged0YF12p3P6uwnjouxiR9eNd558wQtqKMIC32dvw3n8XRkZ3Y/y0G++ID2K0tg+jnGG8WcTgMPwU70JPK/fG5/Cm8QuM4F6AFzEWewl+hffPX8OrePd4G/4FY7N34QRGVycxPvtQvmxL34be9T3dV6zrWrtm9aqVK5YvbVvYMneOXDPlW8HJ1YFJEydUVVaUl/l9pSVeT3FRYUG+O0/KdTkdOdlZ9syM9LTUlGSrxWwyGpL0Oq1GrRIFRgl4G6TGsDOSH44I+dL06SW8LHUgo+MiRjjiRFbjpToRZ1hRc16qKaPmir/RlBOa8gVNYnYGIVjidTZIzsixeskZI4vntWL+1nop5IycVvKzlfxOJW/AvMuFFZwN6avqnRESdjZEGq9cNdAQrsfmhvS6Oqluua7EC0M6PWb1mIukST1DJG0KUTI0raF6iILGgIOKZEr1DZEMqZ6PIMLcDR3LIs3zWhvq7S5XqMQbIXWd0tIISFMjJo+iAnVKNxFVXUStdONczWcD251D3pGBW2JmWBr2JC2TlnUsaY2wjhDvw+LBfusjadeeSv+6iI1b61q3Xiy1s4GG9NVOXhwY2OqM7J7XerHUxdNQCNuIUHdjeKARO74Fl7BpgRP7oltCrRGyBTt08nnwOSVmt1xq4JzwGmdEK02VVg2sCeOLyRyIwPxrXNHMTPlw/CRkNjgHWlolV6TGLoU66rOGUmBg/jXDGbIz41JJiXfIbEks65DRNJZJMlycWX5BpuQUdZ5rmn9hXQkfkTQDt0PE2enEkbRKOKdJPFk+CQY6J6EaPiGCtSLL8H2sjmjrwgPmauSbef2I6DZLzoHPAd+/dPqTSzkdYxyV2/w58CzfJRc2GsrP5yMeT6S4mG8QdR2+URzjFKVcVeK9MkYjUo/ZiQSXD5pxbTtC1T5cfJeLv97tMRmWYiHSP681UXbCUnsUZJ8nFKFhLhk5L7Et5JL+85IL1cMS7uMDwD+Y2iKa/As/JnNqcsOq6ghJ/Q/EyxPypgVS07zFrc6GgfDY2ja1XFJKyCddkI3lSEKACx4R3LhSMyTcevMXt3IG/ojuRqlhdXg6mhqOMZJc18rsNJTIUTtTmsL9u+RCy7zQmsTbEtwqZf8vi6k1uIEVDnE2Rszh6Yk0pHO5/sFKsfgZXkshX1cbm1Ok2nNpefIl5UuGlzTAcMBCPm1qWTwwoLtE1ojOamCgUXI2DoQHOmLx/qWS0ywNHGatrHWgpyF8/vXH4ke22yONt4RwEqtIdYlX4pKBgWVDwNwtrRHZPkSUzMS67aHIXE9Iiiz1SC6pdTl2MlQNSa6WcB3mKEwdksjN84ZkcvOCxa2HzQDOm1tao5TQuvDU0FAeyloPOwFkhUs5lzN5wckLeM1HW4pSjaJvPywD9CtSQWEo5c4YAYWnOc8j0BmjCZ450VG+0pEMFCVCQiKf1xaQp0nw+hPahWPaGpSYueQI4KkBijDxDGGhpVXWTZSr5cnyFFpDcUU4K4qcI6g7mcDwFFJD7EPY5nyFHSP9Q5Nl+2Glpfljmv2oyXn9F3g4cq52UUPYX2LiC7+ewcLFrcNTANtXUtSYyh/uL3EQF1uC4l64FSi+tBMPsBVIuQmHJbRqaeYQneNRKFHowEypYRlqcMQTogpH5XIuC3Etie8O/ob/rhK5SIn7PaXxAfPk8yUyVsIC/gxEVl5aXHWh2MgRD1R3acJAcD8re9MVWWOPdIU8F1Q6Iv1LnQO4iav5Tq5WKk/jGEbDnhbp7+zgNo5G3ykhYyYynK1L7a4QNsjPlQF+zHd2YDUh/0JPkSs8lzSJm5+0YNfUzacT6W92hkPOMBoLmdeKhuqMiEidK/Cslzq4gTQn5tOMvgpJx8ACrAv4IkL2iBo91oqO5RI37wh/sYnVT/immRFY0BoB+8CANBAhOER3Iypj8/kRVf4MTvCnxyN1LOdhyAoehSxPnJA4XGV1eGv2BskVQhXqVtYSFw531FKedA7wIKct7MGVsAxYB5yBAdzZbWiUQn7nojAasNPsbHQqr7rDjiVchBm8FMKGEopaN1fE+spPfmSdZ6hN7f6ao/x0exLKGqVV5cyLNJ9XUSs/mFnvidC0SSjkkyfcHye8M1880T0Dl1fGXWXntZ0R2jLmKRP1Z/Cq9vMvLFENOYppKscp+h43ubn5YpNfEklumv9tOy5sCY+f0bTUrtEGuMwMX/b99dfmy5XT56KHeDjHOpsexOj5FVBjQ2blqyCIH8fjIAKt1UEL+5Q+BtngYH9kp1HPwU5HVdmOGPtkmBU7ampt7BSE2ccwyD6AE4gCmJFjxlwNYg/m44hifIS9N9zQUC7HkHpKFRotLCo/zAXRzKzyp9h7dB8UgAMZJ6KpdkXybnTq1LHMhEmJzHBxSfmJWh17Fz5FpOxddgIKE7WGC0vLz9QakEHYd8FECDhgN/stRBApyOzt4bz88sGj7CWUv8Cex0sAr/Z81GApxwZ/wR4HK07vEDs4Jjk4bLSUQ+0Gdiuu0wimxxFPIp5BFKCbPQybEXcg7kcUwISpA9GHOJdz2F62F8e5B+ubMPUhdiPuQBRwZR9F/lqeskfYGsjFurewO8GGdDu7Q6E/RpqJ9EfIz0H6AJY5HRwr34eUy+8d4+/CcirSe8bo3ci3I71L+RdOB/v+WPlKtlGp1zdGd7MN0RyHuTYH5U5EPyLD3J2YuxOX7k4sAaaE3cC6lJ6GkJYjXZeguFyboi5JeUebhtMyynfjkm7Cpd+EK7cJV24TCCi67rzOdQmdEnYd6lyHOtehznW4Kn62AfvbgC8MMDUjOhEZrvsGXHfOj2A6gnhc4d+I6U7E3bzErsJ1LMJRbWNrooUO3GQrhwNyec0TbAUutcxWDGdkl+/4uqTV8Y2I1DhGTVx3uSJdPqxN4tzlw5nZCYpaa2uNrBO+g0ghBdM8xErEekSBdUbzfI4jbA6s04BsdGymm9lmYbMo+OuJ9Sgrh2YN4Ja0shIIauCQoz1IJm7ZXbuFLeV2iKkZsQdxJ6KAs21HvpNdjtiO69KOg7oc+YApYMmMeBzzJ5GKWDKhngn1TMg1IdeEXMCUS5oRw4g9Y1LVBcn5Olz/DJcgFqDUiFwjzvIkpmd4DnEmlgxYMmDJgFrH6Vc4QjOmTsRmRKbwTiLi+8P0vMw/Jg8jqhT5GUXnvEzmdelXsrdgpIhEisjuIrKziMjBmtpyORcTq9W6Zces/bOOznp5ltA+q3vW5llsYiw+Mhz1+MsVmuvm9GA0I7N8oql2Mt2PI2vHdBDxBCIDB6Y+xBrEbkSB7sfUgd7Nh1iDOBexHVHEGo9xm8XUMSbj/EFFxnNcTi+RM5zDvmh1xdza2ejH2hEHERm2vQ/l+xTtRG6/wo9gelLhzx3T363wHZier8OUOtx3LB5LHYg1iO2IPYgivMwuQ797GW8fUwdiD+J+RIEtRriMXUYfQ9hH9zGvbCizOSA1FZ2/1aIx15ppEr5UA3lESe9R0m1KWqOkebJxpuGLmYafzjTcNNNQgBlaCLUouFNJXbK+1nCg1jC31lBUa8DW0sAFBmpTUhVPyR+UdI6SeuUUl+GvLsOfXYY/uQw/dBnWuwzfcvF6WWgWBpqipHqekruUdKaS5st6h+HnDsNlDsNEh6HWQO4n2DtMVdIcJbXzlHx2wFRvAu0T5DOox5ZINFjkwKhAISQeDdYiGY0GpyE5Fw3ej+T/RYN3OJ4kfyXKaUG+iOadctTayFkyQ+DlP4/RP5EZsBfpGaQrkT4EQeJG+uNo8Hqu/yDWvxfLP4JcDdd/AJqVeoNkhsL/4Vi9H0S9S7HX+6Lea7DXe8Gr9Hp31HsKuXdEvduQ3B71diHZEXXzAa6JBosdtRayEvIo1+0EN+UjmTXW43RsuQvptETlhqiX16rnHcRIXVQqQ1LAR/kkkaBZ6c4RlZRJZoOkNJEFkjJoO7gVaiQmZfAGyFWoJipdj62oDrhPOf4t+ASfOHxOTNH7He8/ifNbhMV/JTOiex2vHObLFXW87I0R9yHHr6QnHM/lxciiqGPEG9Og4Kg3RslBxxAucgR1KTnk2O9d6XhMUqR7JJTiqx4MljjukxY7drmxHHVc732SDwPW4YwXoTjkneKYFdzraHTHCIrlIHYm6xzVUq8jgOxJMTJjeK+jLC/Gh+LHNvYechRjj/kSDuWAo2rhwolHaBWoyUbZq+5TL1UvUs9TT1ZXqEvUTnW2OkudorFqzBqjJkmj02g0Ko2goRrQpMTiJ2UPj6pSVGblP5QQeCooeTPlKU0EXZRoKFpPJJk10aYFU0nE2gRNLVMjEz1NMXV8fmSSpymiaf526xAht4WwFKE34/2upRW3KGdtsfNvSYeBEN+WW+2cXrfl1lCINEVGOqFpqTPyxQKciQ7vxKI0NR1Sr6xJr7FOsQQa678hCY+lnq+fdM/FT3r21MhdTQtao1WPPpo9NRQpV/LxOOabItP4x6jDdD3tbqg/THs4CbUeJtfS9Q3zOZ9cWx+6oAa5tAfVIMgJVxuGXK4GuWRYUZulqOF+zW2oH8rNTSg9Q2ZwJdxHzyhKKxNt5WEX2FYzJ6hGcyBPaSuP5nA13BiJxkwXN5YExKQ0ZkoCpbEsrjTkdqOK181Vhia6UWHIPVER7/1aLLkTwwmBW+nHTUJKP4R8rVOY0MHNMKZDNajj+e98lk/9TyiT4Y53lnXyT4JhqWE5Yjiy/cpV6fy66Bxa9s7Yt8L88NLOVZzihekdaXl9ZJlU7xzq6PwGcScXd0j1Q9DZ0NI61Ckvr492yB0NUkd9aPihzXVNl/S17UJfdZu/obHNvLE63tdDTd8gbuLih3hfTbyvJt7XQ/JDSl9N86eSpubWIQ1MDdUtSdBhqtehWYTxjjk11dwzRbGRya7079qPCIDnl94TiiRJUyMGRC4qqS2p5SI0Ui4y8o++Y6L070522Y+QR8ZEZmRbpKnggfSG1fUXfjZs2NC3gScbN3ow7duYrjD70HhdC5oijfwbVTASbIjI4foQ4e8DFVvlCe1Su7u9sH2P0C11u7sLu/cIc6W57rmFc/cINVKNu6awZo/gk3xuX6Fvj+CQHG5HoWOPsFF5QnWtsvlo8OUg7Q5uDu4IDgb3B8UE23o09+Vc2p7bnbs5d0fuYO7+XBUXLGk9JAcHcz/NZRtxJ5I+fBrqleFuRIo/vNi3kU9kA44uL6zt0fZrmVnr1Pq1srZZK3azzWwHYw7mYzVsLmtnIoZRUXV1BRK5UVVdsVO/Wx/Rj+iP68WIakR1XHVSdUYlOlV+laxqVoVVPap+1U7VbpV2p2qnmob1Pfp+PTPrnXq/XtY360WHmgDObQMiX6ONG+2yWa2qd+h19Q5G6x1aTb2DL1/Is9FT11qbC50YHxOM5UsgGVFCrEBcgCjCzzB9FfF9xD8jCnADpncgPog4zDmshJU0pK+u52sQ8nBPms7Kh/1V5ZNiSDtWJOiCxQnaMCdBg7Xl6UijNRW6WhOG6gSOYPoC4tuIv0f8f4giK2flSuMbEzYY2gAbPASnBVjo48kGTx/eq/Fd8L3Tt8HjAY7cXHE/oaqHXGrFQDZshA0bAHcXElRSuBt4tY2cnn9QAIo+ni9ZeEPP4sE93tm7Ils8rUOUPEF/ipG1mh6NgijE6E8PMNCpeeYggQyNSjyKcgqMFIGWrCWXQ7rH/EXwXHCO+Wxw9rkg1GDe/BUmZX77EAgx9RvDXUDUSIe6CKT7PD6PvyzksrgsbkxIlgBfOdnIV7IIX4JTGOEHXzGAcECcBRVkJx+TXCNXrcy6Kus+/0/S9/mf8J+s0izK6FH1qDdrNmv7Vf3qHZodWm2ew57tynU77B6XpJHNZrpQ4zIaHVq7Rs03oItz1C5KHSq7Ostsp0QymkzZFbDHUwol5hJaEqOvyi6v10NtKXuy7R9lZWVrtPvw2N5Xo96spqA2q+eqGbb1odystHVl6T6vx1Hiw6pdmfucdtl+ws7sC5qreqp2V7EqMKuSk+lCc5LJxFODAdNcd14Sr5unMPMyOTPv/sqTh8lWZRnxrQTP8cTc9kXb6bNtp8594WlrOx00n8ZlNX9iPhdEMtoWxIw14MM1JhZrwHz6EzB/7iFj1OMp80MbabPLptJSj0rl0Wo9mZnZHmKKqU8f7CIk2wOYi3Z5imPsXw90eWh2ilLOzsEyEg2k13g8+INdeLB9n+X8D743YnGVT5wwscIi5Rfk50suS0pqakVF+QTOY66qChROqLJUFuRLUpUrWcpHLYnsI8V9BZUqt9totM5fOPq6uXDShxtW+afUFm788g9+v8eZlpnX4hdspgJbRXnhcpGe+0gq7Rst7MySCkdrFxekOX1TNo3uc6eZ5U62/vqcQvfoG2ubbSa+V/rj7wmiuBYm0V3KXsmwft+L8Z2J6vHOIxRCkeiZS+ZSraU6Rhrl4xMmTchkdqE9vT2jPbPdrhINohGKR6qFPn2foc94paknp8fR4+vxb9PcpN9q2Gq80bTV84jwSIXZaqgwVBqqsiuyK7OrfMRHSwRnjtNRVFRSMYVMoTWCP8Of43f4Xd+q/FbVdMP04hb9IsNl5kVFizzZGCpTe4Wjyj6hJb0loyUzVL6kYknlkqolExZPNDK9vihZby+S9M7qyUX+6l5rb/K2vHvU9/h2+R/xjRQ+Xfxzz0j1meqUOZpJdrxv2veTlwklmwkhRyDGmmRD1b1lWfbsboc9J+dINudUZtybUozbKMmYkpRk9CQVG4V8rUJUEjkHoCosY1JhipbuI3JObiUhjnySHyOSbPZZjlroCQtxWvZbTliYJUa3Pu7Yl+Mxa4mWKzgGS8nR0k9L46WsVJ5WJZe+jAUGpc5Sf+lIqVD6JGmEAGkk6bif0SO0tXnW984+3Xv29Lm29b3nenFH4VY+XcO3tMWaFiA82Wos9Rg3mZ9NB/MnZ0/jPj57Wsm1EfN6zJf5666RJ+T51cmF+XqvtgKKTPkVJC8ZE7Ufi7qSpArQJ3k9BebiCmIyFhW7rVIFaHyqCoKOzhw0BxNJwl9efz1pg942PAm1nfoVhpXmTo/QFmoj63s9sB7a8CCUk/TppoDgNwUqELmvDNnl5KqqsoyMMkrLcnJSyiaxbG2ZKqb+04GuMpYixdSfRrtStNxwEGr+vdFIpVTKVdlS0lLTctC/qKRcbj25KrVkqcihaD9VlWgpefn5VZUTKsrTUrlFsb1ua9u+Jatu9kz5+Kfbmz59cnKl42eZGdlqtzuz9WDXpu9NrC4Y/fEds04+1nXNpLRMl05cO+rZuvvyzfOmVDRtWrHuznn3ntCKNTk+8srt3wvfuLh8hTfnZ323tNz+alWGw8dtJxP97KPoZy3Ex23ngFVOJsncWS5NzqycZJpkrhdnmm4Sthke1x42HTZr3Xj9biBzdMuEpepwcp/Qq+5Jvkm4Qd2f/BP4iW6P4SjEyFFdzJBiMosqtciYyiKq9HjvyNXqUrRandas0eF5rLLwzcRkuUKj00sWC1B0xmqNRqtJxAKDKkGV6UuuSZ6bzJIt5U4zMd+mybAmX+Na24Z3jjlnZ6NnnGNu+7DNzI+b2YpXxEPnQ2tiP+Hyby314IYCayDBMT9vfr7Mj++9jXh68Q0fAHNSAGLxM9GUgC4W/8tQSoAor/lxUbSYzRqLXnm1FqohMfVn0S6NJvFqrRfeKHHhG0pDj0dcuSp8jcnCvV9tod7+bVUu+csIWzE6p6ujwpafJc76UtWzVzW6yy287gtdSxbgqQuz4qfYAhaBFMhmBxKnrvJPZIWa1BQbJJnwhACjQozKuWG0+WUgTvDzi6GZ/0e/8ZEDySmoxV+WxWLBHOjtbouan1KUn3cHeG2eOcj18PiOv67UwMwLj1utmCnT69ESPZ5nPZ62mtPc1+NR03aaIP2tZ8R3bISf3yplXNm2ftgNEWB8CDL/gMsHkegxcbrmmc2qhWa89kbUGEqE1f3q3WpBfbvwIyEqMN6VGqcWi5+V8w0G1cKUFEcOzpNncbYmlTJbJMZUzsJDO8eijOe3Hs+Ikjt2/BiOte1ZdCflylhxpMdwfOjt29PbMsIQTnmdiRnOrEAaYqqcFXDwUenqZlZqHHWGtgm8OFxYWKmwFxSXVtpVGdrW5MtT29MWp387U02YVqXWapJE2wzVNnqLamvSgHlL9oN0b/rB5NfoW6a3zWfpn1myNawOa3pwdtu0T6t/aTqj1ghEbbiRMu2R+ElQ4b1+5gRtI52mnetooS3apbSXbkvelrEr+cfaH+timoPaiO4X9Hf0ZNJZXYrmOIaz6uNqup5TvnY7cdEiapV6k5AC/lQbH2qyNWBtt222DdpO2ASbzf6qQPANHsdti+SjaDInb8rTrQG+xkvshL8R9Uua1EJ7wJRKulM3p+5IZalnU1L6NcSv2amhfs0OzQkNM2tkDc5EE9Gc1Kg0jxptAmzj+4p5ZavfKBubjQyMZqPTyM4YiZGPRItraazLqWtS/Dp69d7Z59aj2bWtb0Nyum29hwcpp3v5lvL0opWgh41220hbyMMDQjwJ1vcGFBucNAnWt5G61gMqIJSuD6HjHQtguWkeBjX2ppcCSXJJwICo4beIwoA6QVSc2BMle0I2VtIlSrpESauUZKM2YDNnBDKcloABkUfTieD3/BMKhewH1Wq9xYZB0QcHu2w2vcUeU/8BjV+tF2IaR7QL7eS88SvhEJp/sirhotNU6MlpVaW1ojzV5nblJ5z622TZsq2Lt5Q4bC/cs+cPfzp078/PbSWPiOaMzgkLbqCTX+rr67w6Zdt7hLz1B6J+8dHq1rxJ8vXcHy+Ln6K/YfuhTFhzkVcoqJB5IFkh6/V0ISXpPIAk6TyAJCZ7pqYgifMLXKZY/OQBLjNxOyvnclOZWlNgcglWj0iuEUmXSES3D8OGYnXGVTmkM4fkuJ2ZJJzZk0kzregN0L5Ot7X5kCJpQ79Zw73BMbS/146ZX8Ncmd/jOe8Uyl2mAo1QnJpjLRVpcZk60UyGtUkka8XviFR0F6vrc8iynL4cmuO26gkf4WdyJjdxk6miPFNj5FlNgZWTgoKK8oSpe55N0Ge5xbdxND/7bFuN+Vkrd+w4KG73RVpvhpdaraWyPuAt1AfSU0JJi/PvM9+ZJ+rUukJdUbiip6K/QmWqiBGnvBU9wIuGF43P5j3rfkN6Pe8t74fCh9KHeR979dYab5v3ipJN3h1kB93B+m39mf32/qxtJTtKDTyK1DFtkipL5/1l7vOSJoulplizUrMziuzeXdpduvucd0h35OmtHkOhd6Z3bkV7xdVFV3tvMj4i7a/4iH2YlVSkKcuBp2gOcRAfhmwx4onCU6UxkilbitNzMp6y52Q6Mok504krx4UZT6VyYa7VmicZ9IKpQCFiDvkFlPqKy/Cyhoua+d2MDLxhNsopqT6+sPQlKyHWl10nXJ+6mCvGUmR9j4mETT2mnSZmipEJckZBZkapQ0M03sECEi7oKegvYM4CfwEtOIKnSjlxDiWsui0RqylWfY7bb9yF9hvwoVlG4wSz3MJPoVwJ4ILmUxcFcRhP6fIkKc+gTzEY9OdDulAipmvrvSSqw2xiEx0odWoNleAJKTFeVmGRw2m2qNQOiyuLqIo0WeA052SBulDMIorpYvDGwzceu32p/sL8heXLQozdSK8SuGHsP0gG6SAb1N9r2GnbmbnTvjNrV+7d0mBJEgZ4HrIe0NOgmt4n+fK2e+/Lu88rtvEvH7Kl0JkR0BZmBIisC1BEe8KVZHIfkqELlCLLq6A2kGTOsdYYnTzhAYQ9oJCMQF7CIUsJkoTkUHLAm56caMuaaMuEIYpsxS6sAa/TyuuckU0mVDMFmNmA/Rh4A2dkqwH7MaAOYrpFwUvc1r//hIeuDNrQmckGSkszM1NLy9QZumIxpv7wQFex2pqDmWiXVf83IWoiOFW8lhKcpqWOBadSLg9NU1MToWlewcWBKd3pyr9qSeMip6P99hef2tjS5bKlGVyurPuXNlzWMfpuScl935kwu8Jitiax/aO/vGPNzJJJhUWl0zp/tGlXji6TTLvltnmBhst3VgcuW39PmsmYjp6vCIB18X+xJq8rtzifVSOkC4PCoGHQ+BMhJqgH04ghbaOhbEIztJqabXiFSzMmmy4X5ptOCMdN6jGfVEhYWiozUaOYhG7oOyJpFsPoifxJqnoT6TORdlO3iZr8VAc153rRufDEovgVfpEO2GUtfGE219pyQB8jeXK5KB7Q5egFo8mUx4QUxgSmp4KJJBnTDLwXoRk9qt+QpDK3o6/wE6ozPUGngBEEOkX2MlI6iFMrbTYQv0E29BiYIdOXVpM2N42lJZXqq4ASmpGa9oDr2DYlql0/+yyPar9o6+XxLRrXKf4tpTeoJOfHyIeZiG+3bno2fezOP0bQitAUej14+VcOUmP8uKzFfcr8mAh8+xkwY5J5KS81gO74nUOpAaEwhWffPIQxRY+VZ3cewngi3cazHx2yYdakZIdMgUu+OIESMOv1STYbTcKN9smBriQVNcXUv4924Qr/TcA8FipjeCNNdNmU8BmvOEv0X71Jw6OvdQST7UKhisG5e8mc1U1pZj3JGP1dHivOkMpnjrq/ek3yOlfiYn4i2GiVeAWGoDOVfWLCRSQmFPyGimlpmXj/OHeQ3UgzBDFGtcOuD348dl84N6dhef2H4Jt9ui3IgxB7lMkU0n2Zx5RgXkqmVaNv5a4Wrxh9mISwuY+FdvIzMQB6CCb62cw/JK7TZiQZ/s31wW8TrZ4GX1ti40VUx+yHQcXmnA4pX71U2HQFb7k8YU/cesiiElkuKZFrxAAnHPmZnz26gFwBQciCiUpPxiS9mAUpXWKSXlNtAvD5TpeXE9/pd4+Zf1XOGzdpYuq/RrtMWdiH8inNbWRq1dfhSCmdOGHCxClsIpmWNdnvJDRQnuxPMRW2THAHi2yE5udYpeJJRcEcuX5GQWWjMSm7sDzDG3B65ufZpHyvk/8+9OgC/v2NToBCHGIavEtfAgDjYSDx1w5UVmZlycRThXqzRhfwewzqTU/okaWopz9EQZL0eq4EqKV8oVGLNB8moJYaboXnyN1QJGcSX7A9SGuC3UHKv0+fCLJgWfV0DyGTD9N96BLQqWEYorShREbiFdjGImxDBbfWAUiyjdRUza1qr+quEjZXkariZl658jB97Hxlpa5yy1U9RvlvnfP+3yZ+0gf5spH4bEdttN1GNtuIDa+Bkw7w+ukXOl+PDWB9xTeJAazflqgPO3GWvH57BjFlDGZQpBl40HqV+llf97++twr4b6j/twLp+PtA4xcDe+KbQPzF3wdN/cWgbf0fhlv+18LTCdDp/0fAoSv/h6BF1/uNcLsuOg7jMA7jMA7jMA7jMA7jMA7jMA7jMA7jMA7jMA7jMA7jMA7jMA7jMA7jMA7jMA7jMA7jAPx3URJ/ySmF/1IHPsWgglTljw+oSDbRkiKiI8UkBwTwIpf/EjTqn1HF45gSnvI/XIDJxbqJvxhl5f/rAsypwATQsnrd8g3OOcuvcs7vXtdxxdjflCI7QQTNP/g3XP9G7wyciV/6J6gSRBUgWf+bUXgfiv9LuAH6/y6+D5n/KLJbYdZ/CbNh2d/FW6Ho/zLSR+GTcfxP48d/g9n/V5Db03/ZDr4BuQ3+h/bxNzhmh0VjPvfvP9ynqQGGIvuPtJuCn2syEk7wR+9nP8Pp6ze+Gf6y79wt5ss187CoPe8D/z8br+vdCmVuZHN0cmVhbQplbmRvYmoKMjAgMCBvYmoKPDwvVHlwZS9FbmNvZGluZy9EaWZmZXJlbmNlc1sKOS9Mc2xhc2gKMjExL09hY3V0ZV0+PgplbmRvYmoKMjEgMCBvYmoKPDwvRmlsdGVyL0ZsYXRlRGVjb2RlL0xlbmd0aCAxODI+PnN0cmVhbQp4nF2PTQ6DIBCF95yCGyhW05gYNnbjok3T9gI4DIZFgSAuevvyo1108QgfzJt5U43TZTI60OruLTwxUKWN9LjazQPSGRdtCGuo1BB2yie8hSPVeBXu9XFIYwGqwjfxxurB+vzCigesxNUJQC/MgmSoaz4oxQka+ffVFsOsjkrGd9Utj9jE6ylhJxK2vIixPmHHi9gZcvOjTZqTAh/5KGzeowl5q5w6pdUGf4s765KLRpEvczVcbgplbmRzdHJlYW0KZW5kb2JqCjIyIDAgb2JqCjw8L0ZpbHRlci9GbGF0ZURlY29kZS9MZW5ndGggMTkyPj5zdHJlYW0KeJxdkLEOgyAQhneegjfQs1XbxNxiF4c2TdsXQDwaBoGgDn37CmiHDh/JB3e5+8na7tIZPfPs7q180syVNoOnyS5eEu/prQ2Dgg9azpvFU47Csay9Cvf6OOJrAankNzFS9ijyeAOpR9qBJickeWHexJo8x0YpZGSGv6cqNfRqrwTcKAWuWmAC4BT0gAmo+6BHTACcg5aYgFoGrTABZR9H70PCFiHOvj2Xi/dk5pg5ZgpZtKHftzjrQhdfYV9v82LgCmVuZHN0cmVhbQplbmRvYmoKMjMgMCBvYmoKPDwvTGVuZ3RoICAgICAgICAgNzAKL1MgICAgICAgICAzNj4+CnN0cmVhbQoAAAAAAAADNQABAAAAAAABAAAD1QABAAAIDwABAAAAAQABAAEAAAAAAAAAAAAAABEAAAARAAAAAQAAAAAAAAABAAAAAAAACmVuZHN0cmVhbQplbmRvYmoKMSAwIG9iago8PC9Qcm9kdWNlcihcMzc2XDM3N1wwMDBQXDAwMERcMDAwRlwwMDBDXDAwMHJcMDAwZVwwMDBhXDAwMHRcMDAwb1wwMDByXDAwMCBcMDAwMlwwMDAuXDAwMDNcMDAwLlwwMDAyXDAwMC5cMDAwNikKL0NyZWF0aW9uRGF0ZShEOjIwMTkwMTA4MTQxMDQ0KzAxJzAwJykKL01vZERhdGUoRDoyMDE5MDEwODE0MTA0NCswMScwMCcpCi9UaXRsZShcMzc2XDM3N1wwMDBHXDAwMGVcMDAwblwwMDBSXDAwMGFcMDAwcCkKL0F1dGhvcihcMzc2XDM3N1wwMDBQXDAwMHJcMDAwYVwwMDBjXDAwMG9cMDAwd1wwMDBuXDAwMGlcMDAwaykKL1N1YmplY3QoKQovS2V5d29yZHMoKQovQ3JlYXRvcihcMzc2XDM3N1wwMDBQXDAwMERcMDAwRlwwMDBDXDAwMHJcMDAwZVwwMDBhXDAwMHRcMDAwb1wwMDByXDAwMCBcMDAwMlwwMDAuXDAwMDNcMDAwLlwwMDAyXDAwMC5cMDAwNik+PmVuZG9iagoyIDAgb2JqCjw8IC9UeXBlIC9QYWdlcyAvS2lkcyBbCjYgMCBSCl0gL0NvdW50IDEKPj4KZW5kb2JqCjMgMCBvYmoKPDwvVHlwZS9NZXRhZGF0YQovU3VidHlwZS9YTUwvTGVuZ3RoIDE1NjE+PnN0cmVhbQo8P3hwYWNrZXQgYmVnaW49J++7vycgaWQ9J1c1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCc/Pgo8P2Fkb2JlLXhhcC1maWx0ZXJzIGVzYz0iQ1JMRiI/Pgo8eDp4bXBtZXRhIHhtbG5zOng9J2Fkb2JlOm5zOm1ldGEvJyB4OnhtcHRrPSdYTVAgdG9vbGtpdCAyLjkuMS0xMywgZnJhbWV3b3JrIDEuNic+CjxyZGY6UkRGIHhtbG5zOnJkZj0naHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIycgeG1sbnM6aVg9J2h0dHA6Ly9ucy5hZG9iZS5jb20vaVgvMS4wLyc+CjxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSd1dWlkOjRlMzI0ODVhLTE1YTItMTFlOS0wMDAwLTI0NTVkZTcxMWJjMCcgeG1sbnM6cGRmPSdodHRwOi8vbnMuYWRvYmUuY29tL3BkZi8xLjMvJz48cGRmOlByb2R1Y2VyPlBERkNyZWF0b3IgMi4zLjIuNjwvcGRmOlByb2R1Y2VyPgo8cGRmOktleXdvcmRzPjwvcGRmOktleXdvcmRzPgo8L3JkZjpEZXNjcmlwdGlvbj4KPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9J3V1aWQ6NGUzMjQ4NWEtMTVhMi0xMWU5LTAwMDAtMjQ1NWRlNzExYmMwJyB4bWxuczp4bXA9J2h0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8nPjx4bXA6TW9kaWZ5RGF0ZT4yMDE5LTAxLTA4VDE0OjEwOjQ0KzAxOjAwPC94bXA6TW9kaWZ5RGF0ZT4KPHhtcDpDcmVhdGVEYXRlPjIwMTktMDEtMDhUMTQ6MTA6NDQrMDE6MDA8L3htcDpDcmVhdGVEYXRlPgo8eG1wOkNyZWF0b3JUb29sPlBERkNyZWF0b3IgMi4zLjIuNjwveG1wOkNyZWF0b3JUb29sPjwvcmRmOkRlc2NyaXB0aW9uPgo8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0ndXVpZDo0ZTMyNDg1YS0xNWEyLTExZTktMDAwMC0yNDU1ZGU3MTFiYzAnIHhtbG5zOnhhcE1NPSdodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vJyB4YXBNTTpEb2N1bWVudElEPSd1dWlkOjRlMzI0ODVhLTE1YTItMTFlOS0wMDAwLTI0NTVkZTcxMWJjMCcvPgo8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0ndXVpZDo0ZTMyNDg1YS0xNWEyLTExZTktMDAwMC0yNDU1ZGU3MTFiYzAnIHhtbG5zOmRjPSdodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLycgZGM6Zm9ybWF0PSdhcHBsaWNhdGlvbi9wZGYnPjxkYzp0aXRsZT48cmRmOkFsdD48cmRmOmxpIHhtbDpsYW5nPSd4LWRlZmF1bHQnPkdlblJhcDwvcmRmOmxpPjwvcmRmOkFsdD48L2RjOnRpdGxlPjxkYzpjcmVhdG9yPjxyZGY6U2VxPjxyZGY6bGk+UHJhY293bmlrPC9yZGY6bGk+PC9yZGY6U2VxPjwvZGM6Y3JlYXRvcj48ZGM6ZGVzY3JpcHRpb24+PHJkZjpBbHQ+PHJkZjpsaSB4bWw6bGFuZz0neC1kZWZhdWx0Jz48L3JkZjpsaT48L3JkZjpBbHQ+PC9kYzpkZXNjcmlwdGlvbj48L3JkZjpEZXNjcmlwdGlvbj4KPC9yZGY6UkRGPgo8L3g6eG1wbWV0YT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAo8P3hwYWNrZXQgZW5kPSd3Jz8+CmVuZHN0cmVhbQplbmRvYmoKeHJlZgowIDQKMDAwMDAwMDAwMCA2NTUzNSBmIAowMDAwMDI0ODQzIDAwMDAwIG4gCjAwMDAwMjUyODYgMDAwMDAgbiAKMDAwMDAyNTM0NSAwMDAwMCBuIAp0cmFpbGVyCjw8L1NpemUgND4+CnN0YXJ0eHJlZgoxNDYKJSVFT0YK</ns5:Zawartosc>
      </ns5:Plik>
    </DodatkoweInformacjeIObjasnienia>
    <DodatkoweInformacjeIObjasnienia>
      <ns5:Opis>NOTA 4. Roznica kosztow bilansowych i podatkowych</ns5:Opis>
      <ns5:Plik>
        <ns5:Nazwa>NOTA4.pdf</ns5:Nazwa>
        <ns5:Zawartosc>JVBERi0xLjQKJeLjz9MKMSAwIG9iago8PC9Qcm9kdWNlciAoQ29tYXJjaCBTQSkKL0NyZWF0b3IgKFBERkdlbmVyYXRvciBmb3IgT0NFQU4gR2VuUmFwKQovU3ViamVjdCAoUmVwb3J0KQovVGl0bGUgKFJlcG9ydCkKL0NyZWF0aW9uRGF0ZSAoRDoyMDE5MDEwODE0MTQzMi0wMScwMCcpCi9Nb2REYXRlIChEOjIwMTkwMTA4MTQxNDMyLTAxJzAwJykKPj4KZW5kb2JqCjQgMCBvYmoKPDwvTGVuZ3RoIDI3NTQ2Ci9MZW5ndGgxIDY3MjAwCi9GaWx0ZXIgL0ZsYXRlRGVjb2RlCj4+CnN0cmVhbQp4nOy9CWCU1dU/fO6zzEwmy0z2IZNkZjKZhDAJCVmAQCATkrCvsphQImFHBQlr60os4hK14FK3WsDWrWDrEBYj2hqty6sWxbpbF6xYtYql1rVCnv/v3HkmhIBi3//7fd//+75k+D3n7uu555x7nzsDCSKKoVZSyblg3Rpv1kLlVYTcTuS8bHHLkuXLG93TiRK9RPbGpYvmLfxkWaiLyH0R0gxeioDkSRkL4d8Nf+7S5Wt+MmfQlsfgf41oXOnyeT9p6Zdq20O0KA7x3pZVi1p2lV5/HfyFRI4blq1YME/v99LXRGs6kX7okmXnL/7V+Y7ZRL9H2Ngr9Z8R6RPJA2SqN5KbyHgXOAR82DXeOKqfS/6uc4yDajJK/60JogDdRFspl46IQfQYddJ4uptqaCrdSGPoebqfEuh88Sxp5Kc6upcCwkMKjaZ0odOt9DrNoVX0Ph2k/jSB3hZJKKeeWiiNKo2P8JxAVxoPIpWdaul3tE8sE9OpGO6xSqEIouZNRielU39jv/EafL+k90WusZPGwvU3SqR8Wk/XURKdQ88YR3nEaD7dIy4SH5GPmulqrVxrM86l4bSHXhYT4JpE5+uvxeyhZcj1a5EuOo13jA/oD5qgRSjpp3QlWtxOncpAtVbfRl7KoxE0meYh9kJ6XSSLQWrIyDdGGbci9B76TAkqT6pWtCNI42guXUt3YDReoUP0hYgVFeKXYgc+L4hPdcweerqWLgBf/BKjdw/dRw+KQWKQkq6kY7TSqYBmIm4T3YX6d9EBMUE0ik7xqHqXXtJVbaQYqcYHhkEDqAEt3EqPoo7PRQnSoAY1R12jZWtr9NJjl6KHC8FrB+gFtONtjPsX9LUYgM+7yiXKeuNM417jfbTFRh4aStNoNq2gdfRj+hVm9TF6nP4pvlVikPJ57Qn9Av2IcT3GNo9Goe1TkHo6yr4as9ROHfi8gl4mCi96MVRMFmeIJWKTuEl0iNfF64pF8Skrlb+rYfVZ9U1tsK4bw1BSGmWjXj+dSUsxA5dgtK9Hf++lJ+hpkSryRBF69Aryf6kMV+rw+bXyvPK2ulHdpB3VL+862PVx17dGG1nBZWMwDmtpO0bhHyINbSgQ54jV4j20fLOyW01QnapfrVBr1Blqo3qleqP6X+pz2ipth/aGPk6fp++wzus6r+sFY4JxGcZCkAXtyqdCKqch4J/F4KZz0b4WfFbRRXQptdHPwC/X0zbagX4/Qk/Ty/QWfYIZIOFDm89G7cvBdRvFz/C5VdwnHhVPiKfFu+JL/ig5+PRXBivVSq0yWlmibMTnRuWA8oryoZqpLlDXq634bFH3qq9rpGmaoZfiM1a/Wr/H8qy1v3Wsdb7tT0cPHxtwrPHY213UldH1o66buh7t+sCYZZyP9geoiAaipVeglbeCB+/CZzs4cS89SX+iV2VbPxOK0MHxLuEHNxRi1qrFGDEOn0liGj4z8TlTzMZnnpgvluKzXrSKn4oN4jJxrfi5/NyCvt0lfiP24vOA2IfPy+Id8Tfxd/GZAiZWVHBzQMlXipVK9LRWGaNMUc7AZ4myAp8WZZWyDjN0j7JLeVB5RU1WA2qROk9dqd6q/k59TH1J/UZTtEKtWKvSZmlLtA3a89oL2mvat7pHr9eX6lv0xyxuS7llpuUcyy2W+y0fWo5aLdap1vnWi6wvWQ1bANLqKfR7D/X8K7Y8L1brKdpPlHewLlxqi36FmIkRsygz1GXqz9Q/64vFEdUr3hBt6tnqucav1dHK1+oKMUt5ROSoHn2YupiuIUPsUN5VPlc+0FLFDOUj0V+7TjygrFBrFQtXor+opWob9A+JIOuHKReLTuUJdYO6wfg9DdO3iHf0LcoL5NUOKsn0Dlb1FcrNyPSccrZyNTVo5fq3dDbG/Tf6TzDeI5UrxQD1JW0Lva/6lX+JI+ImSI39YryWq5ylVIodkLjHRDYdFiupRfycQuIh8ZboICHuVe8RE5U4zFZYiRdDoIb2qz7xkmqnRm6jyFNSxVTliDJTfdhyQK0QAlLiz3SBUEUJXdQ9Xl10HlbAjUo+ZFo9pMmLopRcdDPk/eddD7PE1l/Trwaf3aEW0hlUQk3KszQMa+N9fBrociqlfeDBK6lEuYUuMlrFQsj9SZCfCnWIc6hYxEJapqNt66Ev0pQcyMK5qPVryP9nIPUniE/px8KLldVJ/TWOuUarh2Rqhvy9Gp+F1ATf7XS9ZY/+Ik0R6USat2sLuPxNOgs65z3Un0FVaN9sukMrRKu9kMwrkeP2rrEUwudyelYodDHaPBLrfKo2FpL3JuMc9PBs6KiJ0IlP09nGzVSLuTvD2GBcTXONO4w5tISmG/dC/q4z2mkwXaE3KrP0oFYOGfu0eBz66C/iasjtsfQG5FFAuOjv+PwO7R+pP0Rt2quQndXGNcbLlIrxyMEIzYcWPUTL6VOM21i1k8q6Jis7jdFqCzTUOzTNuMfwCDstNZZB8j5Md1l1yJ5WytbvAu9erS1WStDeAkoTxQido29VX1X/qbVQ31/fX99f31/fX99f39//+/7S8EmHveWCFePGHrYAFscA7EzYvi+GbVMO22MIdm6VsF+Gw84ZAStmFOye0bAmJsLOmoLPdHxmYo/ViJ33HNhLTbCM5mIPuxBW2BLsvM7G51xYeStgF62Tu78fwx66BBZZK/Y6P4WFdAU+bdjN/gz7/ptgGd0M+2kb9oi/hrV2H6ycXdhZdNCD9AfshR6V+8YnsNN4ChbcM/QsbLE/0XPYf/6ZXsTe4w36C2yzt+kdWFcHYZ/9LXTmxjWrV61sWXHe8mXnnnP20iWLF81vmjljyuRQ9cgRVcOHVQ4dMriivKx0UEnxwKLC4ICC/vl5gVx/js/ryc7KdGf0c6WnpSQnJTodCfFxsfYYm9Wia6oiqLDeP7rZG85rDmt5/rFji9jvn4eAeT0CmsNeBI0+MU3Y2yyTeU9MGULKxb1ShiIpQ90phdNbRVVFhd56vze8v87v7RCzpzXAfW2dv9EbPizdk6R7s3THw+3zIYO33rW0zhsWzd768Oh1S9vqm+tQ3M5Ye62/dpG9qJB22mPhjIUrnO5v2SnSRwrpUNLrh+1UyBaPRoUz/HX14X7+Om5BWA3Uz1sYnjqtob7O7fM1FhWGRe0C//ww+UeFHUGZhGplNWFLbdgqq/Gezb2hq707Czvbrulw0vzmYNxC/8J5cxrC6rxGriMxiHrrwukXHHId96LwpNqGK3rGutW2etfZXva2tV3hDW+b1tAz1sfPxkaUEVYCo5vbRqPiazCEE6Z7UZeysbEhLDaiQi/3g/sU6d0ifz2HNJ/jDcf4R/mXtp3TjInJaAvTGef72jMyQg8aBymj3ts2o8HvC1e7/Y3z6jJ3plDbGefv6hfy9jsxpqhwpzMxMqw7ExymIy6+p2NRd5x0yeTsmnBG97gKbpF/HNgh7F3gRUsa/OjTUH4sGkptC4YiGf4aBXKFF2I+zg7H1Da3OYch3Mn5w3rA6fe2fUGYf//hT04MmWeGWALOL4idzCXdjIb4qDscDIYHDGAGsdZiRtHGkdJfUVS4rkMJ+1ucXhAMH03F2M5rHFaMwff5eHqv7gjRfHjCrdMaIn4vzXe3U6g42BhWmjmmMxqTOpNjWqMx3dmb/eDj3cQHnqlhW173P4czLbl+6bCwSPue6EWR+AnT/ROmzW7w1rc1m2M7YcYJvkj80O440yUiERjwsBbASI3zg/XOmN3AAfinB0b7689uHoulhjaGk2sbVLfSGHEpblUWBf6d010yexriuCwtYJH8v7DDagMDyxDhHR12No+NPBvtPt8PzNRhHOFckhzPZvYpPCx4on/4Cf4TmhfXpqLBWp4yYcbstjb7CXGjIaza2kb7vaPbmtvmdRit8/1ep7/tQbVBbWhrqW+OTn+Hse9qd3j0NY3oxFIxrKjQzzFtbQt3khqY0RAOuXcK6RhSe3VjeEqw0R+eH/T7/A2LUMnOYRTnm9FcC5dCo3b6xZXTdobEldNnNzzoJPJeOaOhXRFKbfOoxp25iGt40EsUkqEKh3Ige7zswTYfa6ldscn07gdDRK0yVpMB0r+gQ5AMs0XDBC3oUCJhzkhFebKiECmI0SIxoWhqDWG2SFhrJHV/M7UNMU6O2UfQGiQjI3874ZnRELIPCQ0LDQ+NVKoVjAgHtSNkH9IOF7RrpKgW7p0o8wwZ3CFadw4PuR+UJZ1hpmxFSg5r7Q5DyzlZj4JQX6TjM4/3YObshl0jCeXLJ1KM4j+Wl2hEz5UgxQuvAilLF0CBLQblJdzsx6r2j9+pTA5KKiRtG++vX4gUDGiICrTK513YyKn8zB08w9+ZSPRIxHJPFt7mHB71CdMHD/61hZec6F3a7R3NgEINDIwsEPCz5E1f+Bx3eFljsDvJvHDrfG8bmHgYc/IwmXkMoxkLe0y4dcE8XuNY9Av8CBiPAG/DfLevEQWyXmljNb9gHrJped01hc8LnlAkmF/MQNVKgLsTbp3qbW70NmOxiGkNWKjesA7qXQxd75/HC2RqpD9TIatA5rVNR17CRDS6w1ZIrMXzFvl5eYd5YiOjH5FN48M0vSFM7rY2f1tYoImB0UiM4vPClrxxTPCvJeift4jNkMVshSyKaEg0V44Ol+au9/sakUQJyLHEwIGj5vNjQRsbOU3NQYxEYltSm7eyDZzdhEWp5S2Y1YwF7HV6R3vlVM9zw4dBGMe+RhQUSRgT4ITIL//lhZcHdzZZA8dD5L8VwUhimyxV6rzw1GgSq/wHx8pgWEkfikjuvGB5HJHOPHh6YByGNwSucnNub1iZYUrKSP5xnNUdnbBINoTIpSnVKWRPQFw5teeSnxNOnnDGj9wY2CK2n1nXWImm3+sZNddR9YWtn02a1b96L+sxpq9c9lrzt2uOXeM8yzaN+C2dkDlkPquvq57OdNK3a775s/MsM7z7L2mipVJkskuJYjsdUutoo0YUAJZZttNYSyVE20qahrgZwECEX6dtoADSnwf/dNDrlEpSET4eOAIUAtMBLzAfaAAmAhcB05A2DPyMy4hCvZbmWM+iefpT5NRnUQ4wHm6/9h4N0FaTD+6x7Ed9ZWoWDYA7B3EF1iykfcp4n+ORLkemm4V8q6kV8SPhjwWSrNeSG9QBJCM8A+Xcy20GnaA+yn01/gH3OrRjHNzfgo5GW+tAJyJ8CtwjgHjkqVIqjQVwJ8I9AmOTCHccUI9833AepI9HGxciPgV+hdOi3nhQN6dFmQXqq8ItbqM71FdppzaDUmS/n6IE7jf3Odonbj+36TswmtvXE5H2SXBbleNtOwlKLyxSy+RcXWr29XZlP7Wo24zP4PZbUqieYX2VstG/T4BKbSH1s2YZH6KN4/TdVAG/DXBJcJm30+Xq5xRCXNByE/hmIY1UBiGiwvi3ciFlWQI0Bv3FeFM+2t7IvAdeyEW66TL/QsrW3qcMuEMMcP3f5BhFMBZzPwG0FuP+qY2MwyijloFyHgQeRf501F/MY8DzLmZ17UDajxD3Y2A1eKQfkI74qyUPb6dnOT/qqeE6IvNATsmDAPMeUBqFOT9RxEYhx3+7RBqQDgwBuN6bgIeAyUAWp0G5aUifjXZcwjzDvMn8wbwh+R/8JHmW53E1xoZ5LLJm7lIW05VAClBoIbrcxACkleuF55HbzGuBy2beYp6JUsTnmXz/CfeTeaoH9euFsm65Bpm3etAC5n2makj2oUDppBHMs5GxjlLZhnpej7wmojTaHl6fco2AqudSMo8dz3uURseim26jAOIm6q/TGG0Qnak+Af6fA/dU0CEYny1yDf5D+zkdUjaSYu2kQswlr91be9FbGNaXxTkorxNjmaftp1slfVnJ0V4Wur7D+EjfoVwSQdTdk/aG6IzEMWX0jPtPw/87UF7Rd9BiuP+uv2wY2st0PWsJ68eiBPBGKcLbgVZggC0obrGdKzqsM8kJvvkcWKGFaJgeoiFaJ1VrqXLdBRA+E2WXaedirl8mVXTSVepM+pVlB5WrL2MeUZfyCm1gcPmgLd181JvnTuYlSaP8egrKayA+SuWaqjTeluuq0nhHrslKoytCqYp1A8tnqR9IyubEKL928+UvKU/9ogd/9uLTHvw5HPmcvfmyNzV1S3x0nSJPGusa7r+Uj7PkepJyDnHt0fS9aXf+7dShbDf+IuXwfpodXdfAICCA+D+acgRyGPPNuuNaY47lx8YcdbwxB/3ca7kC9DNjl5Jv7OzWqQEqNWVZRlSX8jjp+ymzW48GaIopzwKsT7V7ocMjejRZ6s8PyKV/JmVbqWwvr0Neg8WQe/nQ418a/9aS6Dz1KiIV65LDwSPTOE6zUar6LmTueFqjbjFeVK+TMqhe7aJGNYg1jLwYM5euUKZeRxOQh2R5nAaUw7j9Fg38ybJgLPyYq6hc5rm3/JvigXz9UxqMPgf07bKvASnHb6FcHgeZdy30CsqyBilJUyhopgnIPMthL8jxgAzsMRambh7JZVrOkDzrkHnKjH/bkqiSod9Ng1F/QNY1lobbKilPn2V8Ku2KJJqsPkUl6ljywJ0h+f4K6KgC6Mux0I+A+h7QBd50RvxSV0tqfCP1/Xqpz+P0YjpT2hMcZ6FsSwENZGh+xDVTkXo3ylkBvvo33L8zDGkfvEWJXDfCR5v2CdsJilwvLyDf01TEa4zbIPUNt+c28Nvz5GGdaP0VxtBO8T/s0NeI3LIiAdVqPKe8TrNAhysz6CCWzP1wn8t2oPoGzVXvxPzdTz51NvT3E9CNw6HDx2OsDlCD+hzcOQjfAqyD7beGHJqDFqp/RbpSxLUg336U8SvEMy5HnjdBf0cj1GfobLUT9sFf2UYgn7YWtAmoo1pxH52rfEPnWgZDJw83y2esMRolfgV591czrwnZ1ihO1ebzYdudor2yrT3byW08RftkO1CuzIc0mkYOjNObQCBCu6Yp19IOYJvyBtJOovPFvcY+DPLoXhjb069ViIuAgVoFPQBcCnch6B+A+yN+ug34C7ARZXeC7rLIC0SClFHgZ1CEbQFuAZ6NxvUE13Oq8J7Q3ca+E/x7oGsA8bmxj9E7vXYpDUZ9g7URxj6G+hF0CGBZTynWdZSi5iM8G/l6+XU35NweylXJ+Op0bfo+4K+kxziGfkgffyh47bJ+/p8q74dCudbYjzkulm34ipIjPATb+BXjVdBZ4hXo7bWQpQD8RfAnR8czOk8Iv0GG95o/8ArxmPcO7+3vPa+n8yu7aG5PRPmgmx+up5EMrRrpgd5+29M0kmF5AnFPnOzX7jkNZsNGuY3bBB7MP9lvmUL5DCUXbc3gPFhzQLf/echVgNPK/PHQlwCvXYayG7oY6I6vgMwHeozrYB5X9bZIfHR+ovPSe37QvkHac1QDmgdaAjrd9Evac8325uneYVFZcqo0vdZGyXeV+f8lYO08AzwFPPl/dV2CwKuAE5A26nDswStgc87io5pjfyI6mgKaDL2AlXcUevXYS3DPB4JwP4CwW0CvBIWoOdqFcAN6RAXdomXAfie6EkAZXS2RvMe+BH4cKePYQ0TfvmZiTST/0WsAzO8xWGZHdwP3Ar8D6pAnWs518K8E/SP8YyJlHYX72LvAFcAE4OYIPdoGcHwM6niV7ZFT7EP/R+l37T9+KI3uM6L0pD3Ef0KH/yB6wl4jOv+no9G9xCmoHAez/ZYe7fnePU6Ugn9iegK2tJ9tSraj2ZZl+5ntx27K+7axkiab5USpg3Ug285sv+pl8ryR90DBHvvB+qje6Clbxee0BXACbpOeizTfYK/zHGSPAzL1C/TvTgb8yazXQNn+fB5uB3TdI5wGdD/8WaBfRHVaVLaeJGNPo9P+p/3/qY78b+jUUhNze+G7wqMYamIco7cu/k9xOt3939bl36Gje+rp/11/VM9HETOSShnWkLGP0dsuPckOOI3/dHbuf+rvbXf8x/5edknU3xsnxffmvag9k4E9cBS91t1/Ct5baHuO2/7RNvRex93rzfRjjOp7AnKgP3RWAfAr4F+QGVlAEnA9/JfYjlK57bdUCj/0qsH72GpgIceBDhbXQrh9aRyD/6fwO7X9Mm2DiYWn4+fefMv2ubQPMWZSDm7m9lMxMBxIAnYCy6NzzXtP1P1X5WEi3udqs40vtOeAXjbgaWkFrQR+C78Dfge/E7FAw/O5BuTyTSYl+Z4mesYHWW/ZKNPUyXcnT8jzvmJNoZnaamO5eZaSZCmgeMUqz+z80XM66KIiPhuyXMlhxgXR82PrYpR/JfRAGcrls28+A19N56lZ0A/3klfpJM08Q6boWTKfT7G+slTLdsR1nx9PpSBQbb43mWq+pxqgXkPlajO/qzG+4XN3pZruEF1k5/cRdrQxZgb5rddSHYyoAmsmyllFI2yHjPdhm71vqZTvcyZG9SrrxKi7x9nfWDlWx880c8x3QifYBNw+5PPxeUzPeqP5rLdCl14YOafrqcu/y7ZRthsvoqw7omek33Xe2U17n7/3Pqe/lCaoBTSj+0yWdfZLZtvNMe7dlmhd4MlPvscWSjbfi2laMeas2PiKecx8nyPfw2mRd3HDdI0m8nkjENJ2U0i9murRz5LuNNvku61EpOU9dvSdm5v5i88gQQcD/fk8WL7D2Crf4cWYSAQPlMi2/Dvy3ky3A3zuuFzW86/jkOe08ZH3O8a/1A8g0xh8rob2MNSLjRtB3+Ox634XuJAWq4+d8E4wTv0nafKd4FcA5h+oAppMPm0y19ZYeXaI/so+wqbCnK5Hez9XWyAvIuMj01rOpTrLY8CLGJONkP+/oxR9EKVYJtEU7Qr0+QIgC+Gvw469nrKBPDHS+LP4PWUDOkNppGx1OdZWM2l83q98Apjv1SLn0/QVQxxFHqDHu9zpDGW78JnvCdeZ7qyIG2GVtFfCLAO4uweQzviHmoD5akDdM1D+LrRxKtyoR3WCL3oBeeabYLs8nflGOxMy6kTU9gbyMi3uDYQzDfSGGZ7RGwhnOqo3ED7qFO34rnTf1Y7vCs/rDYTn/Q+047vK9fcGwv3f074JvYHwCf9BO75rnHN7A+G539OOyb2B8Mm92wH59DfgEexLPwXFXtq4MRJm8N4W2qXrfT7HBhab/oNmuiuPQ557/whoiuQz5iIN9rzGJwD2Isa04+h6FGiL5InWY1wOLDJtheciebseitQt22fWKfNG2/poL38asCdSn6yb278P1A/cZqZ5wKz3sUi7u24B/Wkk/bFDkT7KfI8dB58bGGcg3gOK/MazwHTACqQCfG7wDfA83P1A3wb4PGIA/BWRcel6HXjzuFygN7QkmqZ+KXVjstUTodpgKXMJus7eQ1edB5mfxXdB1BsoXfsF5NftkGtvkF07j8iCfaiU34ehL4JIPx6y4lqknwU/oIcgM+9G+ltQHr+H2Y/4NMhk1CH9kJvme8Ox6gjI3RHU33z/H5A6FfI2Zh7sl0TYJ2chXwNlW/9A+fq5VIQ0pHUQ2WrRhvuoKLoXjtkB/X0hbHqFYqA3Sf8A4fxuy+yT5ac0TNtLlVFq+yPsHegbSwYNgJyuj9lNYy2z+Dyta0h33aatpdxH2Qi/G3jY5BvgaBCYKN9X8Xsh2GjqH0H5PR5sGz0e4R7y8Psufudk7tE9lnHQHzdRnOU5rOejNNA2igKWqXIPP6HXu9uR/P7J0oz0L8L+MPfuVivGcC7Zo5TtjZ7nAahzIL9Tk++1jp8HRGi0DH7fFnnn9WZvuyZqR/WwKeQZQbSOaH8kvVbefQiYdUTpifbGWBou341tl3dL4k6iZpv4PR6/S4vas5bzgAHAubRYv4tmaDdDl2+lGdYa2LQaxbF9Bh0r62MdrV8HO/8AxWFuYJMbJcDyyHsxA3Nq8HnEaszfq8AcLMYFx8NpqrmWpsA/0kx7EXB2xM1xxk/M8JFm+WdH0nDeY2/Bvdasyzyr6fprBMYmwNfTTpXvRGHfn4J22/UamXdBvof+4LM0rGG+U9XDHj7hHf93UeSBHWf83Ty7UnrY0SfQnvdTkP4jk35ghjuY11hW9KYn31/5jvss30Gj66x7vfW2r7/rHsypafOp7skcp9jTmf4fenbHey9znCQ9xf2DyJnccZp8wv6pN5X34Qyj247lc8VKSuM7Ad8HPXLXzWmZAbl9Cpj2/UnQj0KGAtaqE8F3DL4PFmhMhs17ash9gYRxvwnDxIsMyFBi6OqpoZ36fl19tD/Wr02URCDvOXwPZFsHHAfvP74P2PsTw/qtiRUnIjru0XGMjku039H2RuuPlvu/O4//u/PyP9Xv72t7T2BN/gV4zaR8dy/tVO1mHrQkA+8CX0mbhddzvok08Mw/gReAf5k4IBG5p5KmPg4eeEPete3OcxIf8J0YRnROIvdvkq2Q5NYy1HkN52dZKOXhulOOz360rxiARWcpRZ7IvR22vf6i/SOi1xlR2Wc7IM8b3Hrkbu1IPu8AX5Roj9Ji09571rT9HpD76cg9WEdE3lGdlLl8f+kqyCgDe8LD8q7o9SZeMHGzaftNNpEq7wNvp9/0hDoQ9tlAmX8Y6rsQ2Gba237TD3S1R8K72/Zs9x1JnXS9AIDdoHZQkfoieLwUuhxQfwbAXpDvceZSP20a/JtgW/W4c4P0RdpM5JkEzJQ2xUj1kuNrW96v4Xs1DL6TMxHp7bAFR4DGyHs0cn8v9/WoC32p16aQQ979YR3Fd2tQhjYMYbCL1Gbw62TwhQv9ZrSg719GoK4DLsc++FbgG7i3I/xrjO8ZcGNvrF4BwB5VwsA9cI8B/RvoNqSBbayUwM/YgDAP6AXAxUB8BOIfEShLQaeAoi71I9AaYDIQZ9LJkXziStCtwCoz3WzSlWuBUXB7QIOgvwVGkc7liVfM9LN7pDnreBrrxTTavhg29wbQYvBljbFPfERV2mxKxJzGR/YPXc9F9i1dT/N7HIBtoy3wP3PSvYDoe3KT6haaq12Ovf4/se/jez3byKEPh179hGr1QvLxuUXPm0t8n1iePc0ka/TsOwpLAyXHPAEZSvxzQ1L2S6rsiNyfFzMjYdKN3RbtiJTJ6yxq41rSSLEMgh05UNpOTnmvjO9dvyd160jW+VK/1lOTeX9qFPrJ+0deC8+CX+zIM8Zcv2P4PhvzlWkH/o6htFAD18vvKZSpvFeQeX8U2ZMal0T2t8Z4lHt7j3dPNzH+n3631fsd1He9KzrdvYzT3dM4yf8fvk/pfW/jdPc4Tuvv9b7ldO/KwKf3apHvG6Qc/x4C5n678TADfJQNGf1701YbodyE9foE1Vh88jyyMKL7IStZdi2EHITNb5Y3RXtSyvJe33Ewvol+t0FdJM9JOdwtZRyf+RZ3n9N2n9FGv5Mg19Nl2B+CJeUZ9wbIgDhTtkwyZdAOhnG34pNnkiyLUsVa0GkSGeJqrIYppowagL783JQ/W407pXy5wZRR10XODMWDxvWmrPJAJ2UrtwHTTTk0CJRxHuAF+sv99A0RyH3Zw1IvlZhykss9A/ngNt9jOKFrqngN8ndgTmcrQf/vN22CKPabdoKkp7MJe+T756nSm+9txkPPJMu7vvxdhP2UGd1zdd+L3iJlTV2PM//o2Xuted5cK/W4+V6/956A3+VAn42K7ucxTk+a5zxROjcCeeYjdbQ1AWYydKk8l43swYpA7eadWr+5b0josd+L7uPkPkN9kmr00YiLgb7cwu8SjOHmOZrN3CfzWded8j7GLylP3mMGNe2FRtCPQR3mORvvhQ8DX8CdEHEf+5O5h6vr3gvtIdgZXdfrTyP8SeyVjpLbcrP8Tk1Y+Rfl8fehGMhzK4PfFfVAlXkPCu2kQZG7B1RnUshceZYxROPv3LxLo5S/0HnqYzRKvZRK1T9TrtpG5YhrUD/AnLxK58A9TaukJcqjkXdWsGcmg3qRV363Cn4er+h3q/iOfoHlKeBerPvF5LfcDZpDdvUZ2LNjUPedGNcbqUKdC/8u6XcpqZCDm8AvayhffYHy9QyUeSd4oxX2072YqxGUr2XDtv2cJqFNxdplZNMSyG6ZThmIy9SSZJoh+iqEpSLPC5AtnHc7wu+H+0pycRtOCbRJtqcHZHsisCipxqfRtpwEbkdPJJ1c9glA37k9vetjyLHoCYxLZGyMZ4E/AZ9E24XwE8arJ2Rbo7gAZfRsL8YwCh7L3uCx7YmT+meCx70nZL+jwDx0A2PAcyLrNnmA513dT8myz5yG+znC7Bf6A3vYFZ1/9R80XOZFGuYD5HHJdnE965CW534H8l8Od46Mr4jyk8zH4Ugr5zDJjN+BvjHvDcC6537LMTU+5fHUH0V7/4zy9qCORuSFnSjbx2XvRt1m+7U6yCyUpT+C8EI55nKuZJ4Qyoi039Wz7ZLPuO1cZrTtnAb7KUsruRhIX6xvRnrUhfZVWZpBoRm4TmUWpQPnA0nAECAOGA74FJankTDfD02HusfYrNDnX4FPDtMvGZZYzOXLtFF/jzYqeZAreXQVUAhkAwuAEsANZJkoMOPyTL8N6B93E41OiGMZZOxL+LOkbPtdZ+7ffnE6G6y3rRG1QXqng53yR/GKMRf0I9CN33XP4rv8ve9x9L6Pcbp2nWQT9b5Ts9s4qJNxULvJ+FA7ZHxonUOl+htUCp1UqidTZezb32In3lWIMeFfWj0XWM20dzt/6J3zH9pvvn+mnQPdN4VioQsmYw+ZK8+rz5Tfa8mB3puDPRHbU6yvY20jKUl/gOIs10JnXmZ8Y201PtV/YxjyrBb7V+sdlGTJpjjr15D3i3qcfZv36qCTxrBdx3cyLK9iDzQEOn4GjVW/oXq9gr9zaXxifuclHTovqF0v7w90yb0u9sv8fljq5Mj3ajNYz8Q00Nn2EUZH7F4iewmVgs/qTtgvvUKKuFfeIx8dCUO+adgHQC/1CBtr0gKTRsPPk/Sku36wQQfStfLO3600WnkE+wQAbetnsWMPfx3wDU3AHsuu3wj3PXSz7oL98DD5rQUYm3Saqc+jGsiJGutahL9MsyFz/Nqtxov68/RT/Wvke5Q2YH79+uXgg0fJol9Ba/RO+G+gn1ieNr7UdyF+LfwIw5jb9WLkmyfT/0i7GPZ1Nc2GLCH9QzoP+4slaGeR6KLVYobxMX3F9+yNA+IIxnMt5VqGgocMStFmQm9fBARhZ6XBXl4LezsZ+9Rc+FdA72eRQ7xFDksV/INoNPu1/siXhrhq5KuiDZB7KYpqnAF5NVv10QD4q5TRpFuKyaFeTzMg/6o0F/K4KNvC58goH/uNKtUNOTcE/nzIyAKMx0aapJeSQ9o6GHPrBVTP4DG1nUUptnOBJmAwZdsvQp1/p2SGlm98xWBe/z5AxuYy1Pexdnq4T7tP7HUv/7T37k9zz/60sukByDxGVD6dC/v4XClLeT89NkK7PoicRch3Mnsi70iNMb3cIvKujqk8v8oFzjRxfS8UR94JGeXAoKhdL/f0Pb5vBH73Rd7vdHVFvxMEnVgFnVZ1Eq3v4ebxQXo1n3Kwt0kRF9BAfleE8nL4bMPWfQpiTOo9H+IsuiG6J+99b+2kvfEo5LuBhkX1Br/Plt/bf8hYoC01FsQWU4YN0Nl24fu/0feD62i8vg8y71bIoaF8l4bvkgD7qQX0b/oM+ht/V9tmnrvYb6Ua0Ql5P5OmA6qynspsJM603oP2AMrtRNYN8rvdXM69wATLAZqjLwJ+RIekjb3NOKZu4++60yQt8lsGP+O87AaW8PfzTf98tPE6yEBV+4Su07PoNxjD31idx93iqcjo2U4DSxJtPem8opfu4vfrymJKFjMpFzQRGA4UmKiRdA9sjQjSIGdKgUzO0/O74f9hXhtg518TVh+G3baYrMpi46h4m6oQFsff+bVcROXQOxNtg7D/eI9mWHXsVxSahbAR1skISzH2W6up2PoBnQV9ko9xk9AvxZ55OWy27TTfEvk+7gf6+9A/nyDPasQ/RROt59B1ljnINwt+E9YaGhtzo9x387uvdPmOl+/43USVSr7Um6y3nMj7qh6k5WoKVWN+bkN9/JsQM2yfgAeraaL2e1oLe7xanYT9kIfOUH5JA6zTUfYy6NCFNJK/s2p+xzwgf7OgEjJ1NXTwarlfjkV7/6hFfqsgBB5pUCq7tump9BukGwn5WWU7B/aL+T18W1b39/FTAP5+fqLpHw6Zcrn87m0hlVl/C349W34PN8k2j5Lsf6Wk+AVwL5NhDtt8ctjfg5zGvjn6mxisp+U+rxRyO4hx+DvcXH46JdqclGiPgdsnw9zgM7ctVt5/GxG9S8fnOdgET+ayJOWzxDdR9k/ld3plGNqZbK6Xk6Ceh/FkzOmBS47DsofvvLI5xfjWEAdpIZ+Jah9Cvn4IGvm9jzMQNk/h35HA+oL9M57BbkZ0rfam+uc0mqGlQp+l0kBeh9G1yOByke6f/B10+a6DaKn8bQf+rZJK+dsWizQSP41Qup1/N4KBeL/5uyQbomc6nJfvHCLdTGA77+kVKy0UD6Esk3afWXwAG/R5tJ3vuvIZyYXYnwPQXxJy/4C9TswEcsKeGBcTR06LQuNs18L/GxpnXYi18dnJtMc7+FO+69Z/jXX8TvRdN42w7IIOx57bMgi8/xOqtyfTWMu+Hu+mv0D8m4gvBJzd92vqLK+Dvg8+b4Ce+wh84KJC7JNG9X5XrE2HvWEDou9ZovQs8NdCYCBNZcC+SpEgeUYyjfc7+jKqU38CGXEn2n0O9m8DMG4NlKb7MP97qD/K7yfPD6+KnE1Bz9VF0d2/NMrTViKvmUZ3YHxW0VSeB/VOfhduvqufZTypXmu8YvJ8vP4l4vKwJlYby+Xdg5U0G/u+qfptoMmg+xE3nb/PbTyH9PxOnc8qE46Xx/UZX7IsgB6balkMPubvjT9NY/UN8m5ATuTs3vhMj/yOSw5/D95aibL5fdwnGBd+r/K0tL3Yjpb3crCW+bdEymKmYJwHGYf1NdijjqC5aPthbZNcB1PkmezlsMe2gIeye571YhyqMV/V0v5dqD9HtebZpJxzhvoe+sdnDZswnh8CDszJJIz3JOyBP+H3a/IcV/4GDN9llmWPQvy12AMzlhsXwu/WqOtNbRRAXRepa+Td1yHyjLYdNkQ79qRbSYAX3fqV8j7UPMtClFkMOXu5lOFybH7oewI1Ffov9fie86S92Onsvl7n6rCRdkbOCvmuTFdm5N6dwffysfM3nBEcuwzyiu9QbYB7EdIMinyHTp4vLlHfpndtayiZ+P8QgRWXNAlas4peICsp5JS/wE76R9rHpJNSY6cZ6j+U31IWedRP1cNI51EPt1uyPB3qJ7vUAZ7qmlT1EDWjb1vV9+kdQCMnQpxwVQMtcBuAbnSq7+6qry8NdYAGB0ra3r+g9EGOaM/ILP29+q5yH+WTBwHvtKe5Zczb7aNGmY7BQyOOXQOKSt+psaMX/wAU9W31HeofybWr/8DSIzXxCBAQ3g4hyEPb1LcoDCgUUt/YlZtXuvUR9U+Ifwbsu1Bme7o9PrEUBT4FZZaE7u3FAo7E7NmVkFhKNavBPYI68TwAHASO8P1uWqHeQ+uBTcD9gIa9yT0o4B4qBqZwiLpD3YF23oX8DjyLgRXAJkDDyG5H+Ln8VO9Vz6Ec5L1GvZFSQa9Wb5D0TtAM0F8hPBv0DviZbjX9vwDl+NvM8FvhTwO9xaQ3I9wNepP832Q86s9N/zrsAjjfGpNuU1e3Z3ucNdmI9wIlgArXjXDdiKG7ET7CU6gb1GWypp2gpaDLIxTDdXG7zy/n6OJd6f1Kt2FIL8bQX4yRuxgjdzFpiLoomuaiSJoi9SKkuQhpLkKaizAqJepq1LeaXzvi6QS8gIpxX41x5/Awnp3AARl+GZ6bgW3sU3+McSxAq65Sz2nv7wGTLdlVGSqtfkhdjKEOqYt39csq3XTcF2NnRgRNMKmD0y6SsYt2xcRx6KJdGVkRilTn1iSoC+hCQIFkWYDd3gIqB+oATV3Qnlvs2adOpuU2CiV41ivr1fXael0rqRNJj6ilNBUL14PdQREsLNrrmVslhmzcVrNRnc/rEE8n0AJsBjT0di7CvepZUueWACG4Id7w5FM8J3AA7oOgOnwOpHMgnQOhDoQ6EEp4csxUoBloMWMt3THRPJz+CMcA/GslCQhNQC8P4nmEXcB4+OLhi4cvHqkOKEfRQieeXmAqoMqwgwDmD89oXIkZ3wxYZPwRmSYaF+K8ytFQYX5ngQgXiG0FYnOBCFVV15SGcvBISkrauGni/RMfmfj8RG3uxBUT109Uh3QYnbvagyWlkuYEmO5p75dROsRRMxw7TIH93f20FXgHUMmDZzFQDawANOV+PD2QbsVANTAFmAvoyPFbXrN4esw4Dt8q49jF8coJ8Sr6cF/7sLIpNZMgx+YCWwEVZd+H+Ptk6ojrfhkexvOgDJ9ipt8mwz14RvOoMg/Ljtnm0wNUA3OBFkCn59UzIXfP5PLx9AAtwP2Aps7G50z1TOW3+Nyn3KcWhuIHpXooLQ2SPwkGdo1TicOkxot75fMW+bxKPqvlMzeUMD7+y/Hxfxgff/n4+Hw4lP5Ug4gb5dMXiq2J310TP6UmvqAmHqWlYysfr6TKp4Wf4mP5nCyfhaEUX/w3vvh/+eL/6Yv/pS9+pS9+hI/zZWJZxCsp8hnLT3GTfI6Xz7xQrCf+SU/8mZ74IZ74mnixRaB2GiWf2fLp5qf4bLejzkExD4nPqA4lifaqAk+HQpIIo72qBqSrvWoMyLH2qi0g/26vusHzsPhGSG0hvmzPPeSpSRWfi3Ea+/9l0n+KcbQD9AjoEtC7sYUMgN7ZXnUpp/818t8G/68ox8bp74DZyHSrGCfDf2nmu729cD5q/UV74fmo9TYqlLXe3F54CKE3tBdeBXJ9e+EykE3tAW7gOe1VAzw1iWIJNs6cdgEFFG7JRLPGsSh5GeiYSOb69kLOVccVdIjadv8gkHxu5cPCT1NldZ52v+xkFvllEZnkl412U0DSBOGQjY+HrcPU1u6/FKVYdgcOeb6qeog7Tl8IR/sWz3sPo3+z4P2rGNe+w/PCgzxc7Z7nCztEYK/nOf9DnidyO8Ssdk9nYYcNEY8Udihij2cnBjmMtIrY67m/cInnt34Ze5cfsZjqrVVFnl/4Z3tuDcDf7rm08GFuBi1Hj2churFwpGdi1Q7P6ECHQHSoCpWF7J5h/lWeSgQP7RDjdu3wDMrt4KaUoIwdez0DUGOeH03Z7amYOXPIPqWCrGJtqNC6xjrfOss6zTrcWmYtsnqtWdZMa4otyea0JdjibHabzWaxaTbFRraUDuNgKMgWUwp2G/y7Bho/Nel2KvxUIgaVImwKVk84WZ2gTJg+SoSTJtCEGaPCQ4ITOqzGGeGhwQlh29QfNewU4meN8IWVKzsEzWgAi3LQRjf/bveDJETxxmvdTC/aeG1jo5gQ7lxAE+Z7w19OR0/s02aHdf8oF6Wtq3ZVJ41MrBxdd4pHs/kMHv9zBXv+ubJGhW+aML2hvWL79qxRjeFS6TYMuCeEx/APfz+orFRW1Nc9qLQwaWx4UFygrKw/g8PFBXWN3cmwk29BMqpiwsl2UQ4noxyxSyabKJOBX3Pq63bm5EQSPSbGcSLw0WMy0ZJIWbmoAmVNZYJk2CvkyrJylWxOBsaIFOboWVgcCYcszBFHsrBMTrQzEECSwgAn2TkkgAQ7A0Nk9I7j0f5ApDmNFJD1BESjrEeI42n6R9KAGcw0ig1pgv+Tf4tG/QeJxa55by5cwD+/3uyvXwQ0h69et9TFP83r3bnwTfN32fOa5y9YynTeovCb/kV14YX+Ou/OeQtOEb2Ao+f563bSgvoZDTsXhBbVtc8Lzav3z6tr3HX3+toJJ9R1VXddtetPUdh6LqyW67p7wimiJ3D03VzXBK5rAtd1d+huWdeEM0aJCVMbdtpoVGPtnAjdpcTasSya3b7GUWnOlpFyjQz3uS5x79MI+is22BiO848KxwMcVVRTVMNR/MYJUQn8A/tmlOuS4T73PnGvGeVEcKJ/FAXJVX92Xfe/1atXr1nNj7Vrg3iuWeuSgWuweH3TJ4RH8++BV4Wr6sOh5rpGwfOBhA2hwXP9cwNz+8+9S1vhXxFY0X/FXdoU/5TAlP5T7tKq/dWB6v7Vd2nF/uJAcf/iuzSP3xPw9Pfcpa2Vf421DSHnI1XPVykrqtZXbaraWnV/lR4JTnok5/kcZW7Oipz1OZtytubcn2PhiDkNe0NVW3P+kaOuBSeKNfirr5PNXQuKf+xds5Y7shqty22OaYlpjVGdMd6YkphQzNQYfYW6Xt2kqh61WK1Wp6hzVR1mVLt1WBlIaLRlWNnm2G2x4djO2AOxetjSaTlgOWg5YtG9lhJLyDLV0mxpsbRaNlu2WWI2WzZblebYltjWWNUZ640tiQ3FTo3VPVZB6NtqgMdo7Vp3yGm11Hli7XUeVanzxNjqPDx8jcG1wdqGmhxaAPtYwJYvomTAD5QB0wGd/ojni8B7wL8AjTbgeQPwa2AXh6hFalG96+w6HoPGIEtSl1q6q6SidGgH6LzFETp9doTWT47QqppSF2h7dZm9xgFTXdA+PJ8B3gD+Dvwb0NVStVQWvjayBhtX0+qgQLcInjX8WB1cI4JwCOadNauDQWLwcgU/IWlQnLiKSaxeS6tXE7gLBIlk6GrOtpZp9A8RJNObfzf99wFzjMTDx6Hyb1En/N8D62PfDft9/30kfBhB4hqilPOIXJcfh/sQUXZnBP6FEfQHgl1EA9uIBl0eQdkbRENfIho+g2ikN4JR7RHU30k05k99+P87xjb/H4zL+9CHPvShD33oQx/60Ic+9KEPfehDH/rQhz70oQ996EMf+tCHPvShD33oQx/60Ic+9KEPfehDH/rQhz70oQ996EMf+tCHPvShD33oQx/60Ic+9KEPfehDH/rQhz704f9IKCKTSM/kH0UlKy0Lbww27FTEQ8ofyEJW5ZF20rUO5Q+7VbJb2bFHUD+bRX8E8QqpooBixLniLHIFnV9WHaua7Py8atKxKqqG23kUj0El7p2kdVhf3bWMhBV05zJBruJgcbBkUKMv0ZcYwENkanTUq3YeDen0LXm1Tv7BwPO6dohb6L8onZZzm0L5jUpj+uNpakx6c78D/dQYQVZNc9iSaG9SKC5WG+ZI9aS2pqqpHWJAKNbjmOtQHP1ct9/pCqJNTZOONR2m6sOHkipFYlJ65aAS0SRWuh8QRHFJSXGx5KouK04sKy5Do5IrBg+uKM/P8+dYLf6cvIrywWWlaakplvOWrIyxWmMDSSmDhk0YPGrJpq4dhTmbpibHx6TEDCsbNHr13CU7ud3TRavSoKRjNGfIdnsVvTVr4eD1uhDyV3pVUpxiqmgWm8U2cUBYRIco30Ot2ozZPIbHmqqcVVR8GE9uZBCjp3bYMnYuIx41bp4v1Tdd0Y99q6TfjPLGGx+qD+hLyUm59Beur32ezdshLO26nsokPj6jQzhCSTEZlBfKU0J5zXnb8g7maXmJHJwwl1bQetpE20infoF9IhuNNEfs8GRn08ovJ/GwVR8eVFJ7fmiiyPXn5uQqFkWoQrFYA5nuLHe2W7Uk5zkCsXmufun9FItPS5xPHkvGfJGSAFdaHFy5wjtfuG14JDlT51M/Ox7yp/r4MUBiwIBL3Tt1b4f177uX6Xp8aof1o/Zl8fGYmGC17HZ50hBMRHpaYoqCacnPG+JMTysrHTxkcCLmKjJbyvhr1sxuvv2iX1z54vzHLl3+eH3lysFrsgeW5FYWDKurGFuubPlQTDmjZusTXfd/0rX35+8/+lXXhzt/Pm/VfaLyw1+sLvGNmN51O8/gESwFC8Y0jdrlDKaEXM2uba6DLo1cIZeyjrBoEmqSxdmiBty/jXKwDthtg9uPAr4mhzib0hBC4rNQgnA4lBhF6DG2OEWlfeIrJB8XSkpIcIQSK0oc6x2bHdscmqNf+j4lVxwyhz9YNcl5+JCTV1J1VSKzbSV9cfio+CIYHFRCYN4m926hW2PsHepf25fFxPFAlVbzQAXKElPS0tJTfRUjlQoeHB6bI2K8L7lqTpfSPDTNbg1kBEZpT93x7RWrhmYrgYCSNegC5c0bB3izPeh9DqTBa/pEqlU+lb3vuDDhDwkKVu96WqtcmLCu5PyKCwY/Yt8Xb1tOIkmrH5i4cPBgZaaySGlVrgptVm4N7YrfnbCvbF/ty/GvlsYnxQo1ASyjl15NV5RuofvEtoQXSm2xxMtBj/PEZMcPoIAojqmOmRJzDT1Z/jp9Vu6Iie0XWyIqlLLQqNDU+rvFr5W7QnuVvfbwqP30Jh0QLymvqB/Tx+KI+MJ+JO6zeFdaWVp5eWlJ+QxxK90Yf1Ppz8tjdlqU2hkNIV+xIye7Kru+LpVSS5SEElLzXWn93C6Ly1aQ584fnq9gjJ3HnpAPFg4rISIqK4uPVblDlZZ4q9uS7XG7fDnFHnd/X05VzQh3la5pbt3hcaf7/B6PO9/nH14+zD0cwiQnIT4lISEeE9+hPB2aUVKeUlJSTiK+vEavL6Gacm1YvFDiYu0QJAktCY8kKAl5Vs1qTUvrd5+ravjw/v3zRwwbVlCQd1++Kz3dYtHzFd1WdYOWUFJSrLXqokUXeocyNBQXip8ar7TGi3C8iO9QvgkVFjssycnKTEcceA3P+Hg8czzZcU6nMjNbBmZncGD2lrr6h0UV+Wie6AdWC07+HMsPIpufXzatXHWsaSVEDz6HqLraGXEeq3Ie/0Q8GKGmYowV/gngioSBwYudj4O4mLrIeVg4O4NBSU7wNEKqrWpaSStX1TaEYorLimqKRxXVak2NTcHaOQ0h+2BXWny13ZtSWdphHNzrrAw5EypFh/Fhe0IlIaRd+jrbnezr3Ali/uhnMNiINUFYFqGEkgRSXfkW24BhI9T8PCyQkH0ZpjnVqqmufnK9uGy8XtJLq8v4wZOOlSNEZNmUQvzn5YvElPQ0iJgyhJSVDhlcYcoZf4UvlYWNJTVx8BAawmllWIJitViVheLzC3935rELh5UlV3QVxtu9wZKBx/5QMiPPFW/3FJaUKKMGFhd6XClrRcFI94BSj/iscOzSiWl7lCNdjgsbLYFAvsuVVy6e75qwxNcvyRUIDPJeoC5bkuNid7oztLBrXvIy4fxR/+x0fyAg0qpHpzwILQCpoy/Fmu1P5cokuWrbc1086wE591fkiKSNeU/4nyhSx+XeU6S4POkDF+dChcYE8gJjqEGsUFbkXiguVFZ7VnvX5fwk0Cau8N5StEPsCDyQ93CRkZtq8V4mrsm9LP+23LvEncrdufcXPVL0Wsk/ioyi+CRIugwlqX9GScagYQOHlSzOPbvYPsCmZGaKVI/b4cuhQH832TzuBJ8/zePO9PlDSmEgNzdHESmKInLvU7yKdUDBXVb+Zc10bq7VaZ1qbbaqm63brIqV3PdllneI60KO0v5ZWZmKIyEBosOW5GM+aKhgEqqfUkG++33KFN82n+Lb4xwsQoNbBh8YrA4ut8mFYZPjYJMLw5aTlioXRqoMTJULI3VLxbwH5ZKA9D2+JJxNqz5vWhmUv5dZrBZ95JbkcCOLDHzwPHzYeRiLYVVxkFdGvwzn4egqEEmVGS6T7YNXOPWLHx9U4oIazSwalO33BIr8xWViUDYeA3MKy8ifW+ItLRMUdFZJvXjppWIVGBqo5V/+DYD345j3j7SnVPbntZFSqTj7sfPIHmdlidOBpSD1KWElNAWD7vbcQthdH4VilhUWOjKFDYNoPdq+LLO0w/opiBsLYe+yTKV/nMMp14RDronS0mqUgSVRWRwsZRPNJ9gKyufVcOJaYK4vTWPLyMoLR5SyOubVkGPVl3bd1FVR5o3PdmbmTaw49lDZrEBqZv6kMvHpa/s3/XqHcDW3rTg6Ijkz5rEntm4YtkC5QBGia12rpzAQGOpZoy5jV/Vv1l7ckdd14eUNccqN4t6frt+aDE4fZxxWr1Lvp1IaoV4dsVWllPdWh3gaq0M826lu68CALTaW+Z9DAxRXhnEKxSYlKTPL0jgJ/G/vZiaA4/NQKjNCmUxbVmmV1Fo0kDnLG4MsA8soWysoLCmPC8Wg0LhQVhY/ExEV12G8FMrmRHFx2nqXcMlQl0zhcgayrVWFGsy56sOPB4NNPKj42198jDXMS8H9ohgeKcA6O98KBh93vrR/UAnmLrQiNrOtTEmaPlgkeT2VrdX3xuy1q0nBpIvp4rLL6erYqyssWUlpw5zVrdVaTOZEfaKl3lufM3FYqPqqLJs9weqlnHFign1c7LiKCUNqh40bcWbsktiNMZfZL4t1zEjbkKZ4qudWK822MiqvGlhQVP6QcFMcxRmde2Mq4/rHVsZx3zOGVTjjpsYpITya41SvJOvitLgqV4fxWqggtnKKa65rhUstdq13Ka5LPE7BPS6pClUp6HZLUWuRUlSBcetQR4cStdiBnUWiqDlAZfFxceXlGPijmAHLzLKH+GfcweGoEVI+4Am0BjYHtFDgSEBpDYiAkxMFHlJqsT1JxZr3VMKyXRLKdhdXDrKGEiq9kBatVtVpFUesYqpVWGtH1p4XsaJWrloVnHT488NB57EgPMGqY8GoHvuyCbbV58cONTkPr6w+zHovmFjJaYLB4ojl0K7GCWpqPIy54umSJvCYiuGZfj15yNDBQxVLjM1ug8Gb481RLBWxlV5KzErOpKRkhyc+U+T4h+uVmTTUVu4VFeWxSZnOTJGQg8cwS1UmSdnCCx0P/IP5O+BSXvECmlGsXEVNtQ3t1UmiiZcyrYIE2D0IPR3IGtApyd6EyiFe9J1VYxyTg6HY2EqXN7YyHchkbs+IrbRjKof0Z2oHtYPGgMZEtWb3XyP66d7jcmnZcZASH+5ZFhdXku2Ea/cyzGdBhy1+97KSWK2qwxbTvkyLjVjl1UHmZ5YUkBMBi7lbGjJ48JCIFLCkpqd076BYl6ayYcryY0hqRKZYWI2mIEgZc23u4BFzL8wuePaTM6dXB/KU4rxAcXjrBZOHZybZ0x3OuNSqlsWDhombC6fUzRo68bLlif1+ek7toLqfzMq9anFOTuGwgaXlRbM2F3hGBTd2Pb1heIo1vmroTXU3iKaqfoXNlWPnsl0/2jikjofs8Ck5cqdk00RUeigZFqkSLFIlWKQEsKQFHDHWZl8LVAr/HxosNnxZWO+7k1OUmXA8s5flSdYgFQscizfYVP34YcHLef/jvO1N8vMiWj2gqJz8tfFNg9Pjz9SVzOQZ2nR9umWGtcHdkGldoq/TW6nVt9v9hPeA9yC9r8cMEWPELNfMzLn+Zldz5jrXqsy2pJ8lb07c7Lobqvd+/y7xqHjK+lS/j2yHMv/u/Vy4sO9JOjPpas/V3lb/Eb810SseNg6SF/CAXSiLePmVOH2i2dfqU8jn9Hl9U33cr81Ql2Ffp++A76DviC/etzjrHYdwPJUWiLGie69B0zAJDU2qRCdjfX/yxIkpcZvilLhiJ5VQiJqphTZTmDrpIMVwgELbV2dsyFCmZoitGQI7z7hQ0hGLIIvTEvllbN1Sm1P7oHJdRM+uWjnpcNOqlcdWNh1auYo3mtA8hw+vlAv3UJJpv9unZy3IWp2l3pCF1biyEetw6NChYihbe01iFWHB8vIgp6vSDa7fm1ypO51RI9Hbw0gUwcZG3vM7HDE+nxqTJreaMVY1q8OW2b5MNZka3JwoGVpYwKNKRTlFdv+85TS3mZJjwcTq+MBrG27/UIjdV/xuUOHw7MRYv3/kwhHT7rhq/uQh5WLOnj8KyzuviYRNk/KK81LXebLHz7/jzm9rB55PGKc645Cm6z8jDxUpq3rosbziEPNhgcUlWdAWYUfJmuTNSrNzaFqsl5VWInOfN47Z0itTI/TrkGRgr4tzeDP3qX+lLBbq8GV5ktgUdiaHYhKUmckpFMA0FxaqUjtVvxU8XAwIUxu9BV3UKVkZ+shtzsQZSchF3lhV5ayZLVkilNWcpWR5YlFMbBoWjWVmmsbSGi1MYerVHA48FY7xeosHFsg0snOWmRZL8UBpZ+yXQoSr3Q/zhRvT1LS/+jCs8uq3UD/bXEbnrjFjyot5QY0KDixvLr5Iu0hv01qL7y/uLLaGiluLFSpOG5AanKnPtM0I3mS1jrUKb/EQ+xj7LPst2j0DthVbO4uPBBWvl7y+fVgbsZCY9VXeKd6zvIvty7wXeLfSVu9264PWJwfE5tmS8+NqkrKT61Kz8tNqMrOz6jzIFqsVpspR8xSKwkKPGuuhWF+cl5VRUmpzWmva/WmqJ21zmpL2ccFUC//3O/0HljN9YEyFpXZg7XrTuJx0+NiqpqpjVfwHQ/LwKnQ5Mb3SKff3FCFS22TkBTVbfiDPVuCloIZHf2vAKwbohV6ioGklUtNQXg+892GDEbIckjyFPLExAVh/ksMLO2z6cQ6vrpbiOiKZkyCZIa2Zr/MjBwdg7XTdX5E4UOnmdeWp2tbxNx38+o/nT3F4XRnBeJFY5PCluYtiu44MtFQtKG6o/1F42Y+WjB7x7RNPiDGTfvPLsRlOf8u3b90xJjPRv/Jp8VpdS+WUpf/1zKvM+RMhhaerYUqhLHV3D87vb0tLSSVYo8pMSpAkQYrhhNSSEAkvBI5C5MQDAyolMDtCiYmJcFGsO5BoJWwcFN5G7ObcVimzkc6qdRivyBxwPPMArxptUGysFDdslYHTmPuampok+78V7Cze38kCPML1WamttA1CTvVKmadGGhGp0SY3LbnM6k6r1xq2qoSNSyv2LZr1eu1XWrumclVWdI1XbB6zfUqKJxv9ZCd6i+XBvQVJSOOghARPdmQ1wB7sjCyPA/vR1qbHm5qCpbKtaCkvi1C/pLmupn7N1Jzyiqr382ZC9WdWpoUyKz3cKnvt+HKbhxWPR7Ji/3IZPH3AwHK3pV9MQ/JZaXPTZ7t+lGEVaozFGmOL01PHWa5SrrFcEdfm3Jj1a2WHa0/yS8rrjjecnyv/UpOTmq3Nthb07qqYR63/5Thihf60xl+mqDG8nixYT+MHx4xWxsRM8cxQZsTMV1YpVyVf1e/W5Dtj7rR32PbEhO1PKR8oB+M+t6fYDlgFWQ9YlZVMeex4sxfGFvpiLYVK0lK5qclJlUlzU9enbk19J1VLTXW/qAnM4AGoJY3NnmQmr4XGJlXyGM9xC54R659saf3dlY40sSJtfdqmNDXt85SUVpsosW22KSW2TbZ3bKrTFrKhJ7aw7aDNYtuekKrRVcxXamEoqSQhlDA1QaUEZ4I3QT2SIBK4JTEYy4Ta7NoJkRUMs3LSsZVVThiNfFxyGLajk9XXKmap4CooELbfVqTCfgvyQfjnTVBo8tCZhg6llU2itmG3hYSirGyUBqfctq2S+zwraov1V8aFiirjARvrsf6V1ghhWdLujvjckTjTZ4/47BFfjPSFEmIqU7FF7OdNrIwHpMg4wfJrZFlhtcYmplKH9f09y1JTYxPdHdaPdy9LtMZqHTZP+7LY43oxauolWyInr+mmhkxiDRnw5cmTRssbYuHCK2ZvLPKkPnPLXR//c+9tTx67QtyrO/stGDx9gzL8T2vWLPhJylXvCvH6x8L67PZhDblDQ5eydRZPpP4LGnGw8vseUiFraIgXrNOeaLexNLC7ctnvkpt2bEO+3s36zsX2r9SSrgTWh668DuPvIQ8H5PnKK/KLhE+Li4P5JsvwFbm4jKIO49+7ORSOL3dzBByfy/Lg+CTk4OxFsrwiAU1WY4c2TQICQH8gn8qhUB0VoRjkrRhM+YlZhZoVfFBczPrUefjwJ59g42DqVLmgnY8/Wep8PBgJ2Q8l+3gP/dpQnsSSpEI+UWN+OQrlIhPz7QqH2S0sKOxSdthdMsglg1wyyOUaOkT4ZLBPBvtksA+9OSJ3WnB8tpsj4Dj6AMcVFQ0dYirft4JRgcMtA0OiF1DF+yN7H8G70uKhoQEV9qHNkCmOgCOvdejmoVp4aOfQA0PVoEVMHdo8tIWDQkOF1+YqyE7sUB2hxJyiguz88Tn2gmzneL+vIDuvQ00IDfRX5A+sKc+uqBPe/MEkewn+SUx02vu5cmM220XYLhz2FvtW+/N2zd6h/D4UKCJf7kBP0dSi5qKWIq21aHOREi4SVOQs6iw6UKQVNQ+5ez2/QuHFxqvuWIRGj2oOV1clVlaaZ+h8ig4Fm5KRqdssAXdept4v83819i3QbVtnmrgASIAASDz4AsAXKIIEIVIkRYmiaasmlPgtKVZiy7YyUawkzjQPtZbdpnknmu40jqedRNOm0yY+p8rsnrY77ZmNX7HlZrJ1s55selolPruZzKS7aXpmPWncRE2mx832YdF77wUpKzOzu8MYF5cXlxcg+D++//t/KIBhdSYRBwWAIjJEwKBADCD6B+kJ/H05GoJFzvFN04yYzVOygokTmWuTia5rBfIAyhjhcCcK/SnmTAYwaYIVhMFeFvGMMFRqD0LnC0b3f37ouplYMMBVnNb6sFPlqNSGSu9d28KNTa21n8iEVDGlh8sBoHieWL71gY27bnK+0/rr3YYaN00rJ10HNvz5zeX+7a34zaWUaQa5NbuoT7iOGHndQdgwULt4oovcf1W/zhAmNBQJJPyKHyuHP42xYxrTh+mgSvkWrvzyJFIC2PkZVhMf8qfoMOy8egrN9vlVpDpY9Rau/MPJtnL+rKOcbzyPddNAADS6Pb0//WiaSnfth1o/5QVeB4NchJPQAt4ub5AoN9+AHnlxUnprso1JXZ5kESpQoSAVziGJ7OiN38Aak8YtWufk8HC7MzTkdhytXveOOygUedZLopNCIJvuYoLo633kxNEnfT4z48fa4yeRkvix9qBv5mqPiswE1jY4ctpVODOzSmNcbw2v/a3F5qJLJbQVR5szwZQ5Y86Zz5ofmh7DHDNJBzUmcs3Vaj/er1nr7nsq7j6TxXunpOn9UJ2C27r8dlKBSmRpQ0YyvUHQhOAc/CoNgugSmKDCzfmAr0EhD3BtDe0csVmj7hYEv+Y3VafQUDGrM7C2f04FYyqYUmfUOfVZ9UPVox7PHP8PWHnQZaPsIwKqSwdx+gk6NvjVpJX0k/u/6oGKAeMvqBlnCC/be9qBF8HIwRBWiSDn5qOa+hLSiHYEhZiA4Ir0DwysZKeg9Nvd69Z1dw+ue0TrHWpde20p5mOSejwfACHPE+jAYHf3ulZ62djVgOKuD46DW75aNDTRnIEyJBMELUDZrlO/XeU5Cjp2CBpuDQwlZdwC3MIROJ6KoBZ6i3exzKKOU3DdxoBVSoG2w6CxMnixCylhj1CKIBdS6niOUsdzlJC2oAVgp+VIeEgCcorOcVE9m8cngmP5F6D/yBE1qA/KAPYfA3Uipwn40gREzfoEP9Yw6h+OcV6EVZcKbbeyXDh79izKWaxyLIWzL0PNgJ4FZQc7GZGYc0ZspBqk4pUA/PcV31e5OX5OOCI+Ix9RnknNN05wXENr6HulvfLe1LS0X96fOkL63ksupchZ3x8FXqZeFi+SF8Ul+QOFbcpNtZlaYzQbm8SD3D0iWya7JSNr5MoNGI1LTFgaBzdIOw06I+0Gu8V3pF9Lnq3yltRLvpe4/8V5or6IlEqkUhvJa0QvL4tBvy4kxGQg5d1BjdM7PBPSTnln0KuJiUQytYOk26pdHlBxHAkkirNq8B49JADhQSjmnFezBAGeuu3vMIROw5v+DtbVElJaNAQ7v8O6Wio11lz1dNjRIQ+3CI0MdnK4KADesnFJBKSsBIOSltKTWgk6L6uLI31JDvkuKzNglYdqyYENRJngg5JkGqmQAUgjBdFCBZAhAEgYqxipIKAtUuQkSeXqBBFdAO87I6rwY57nvFDmNU3l+IowK5AfCuC88DOBnBHOIl4lGp1XgaqnGqABnR1hlstESSodLZ0tnS95xkpgtjRXIktTaxoL4L4T6W99GmfwDsBoclSCeOM66eBHqAvxJsSlq3IUEnR+GvrK0JcjwZEGB3GWIoCSdmy7Q8AJalvL3aQFbg+hY+cYBmfuDh5AtMtBMIlfxAHiAEatElSbkMI3U3mlAeCWcKDg5cUGiWwR3+DRTm6I7s7n7hDFfExutHMWbYYGIuaYwwPJoCx43wQOyT/0t5w3q0coKw/fnZjWLGRdcOaucDV5J9cHcDLOqqXDXi/DBCM4fQctDS7nAJ2keLV+1Rcjw7P94jaBTefAEzd8aui9927tqpja+ta1uVi+9XOtNNoqbcqEeTFg6OFuGUieJy4feH2DIgihBGkYZGndm62/ezBdDnCmCcLBaB/4ZOv8xBoVmKbMR9PXU9fMb47JmbaN8lao54jdNLvKRkUnMLqdwLmLqIyNjTw+UulYE9j5BbYmaMQRkeGpFPCsQm99U2fWps4sNOKk0axNQ5uH8LwhbLqGsOkaGgmhs410PjfSsV4jnQVg53eOhuaOcGiZkQL+eAF/vFDHORQ0UJfQx+oo98Gjz9XjaOE6Bt1oap3Ex0m0Rl3Ga8h4DRkR1u4aRqXNWb3krmF0Yz5r4cpPHB5NNcj28csOjzmuiFaubtyCBMLYvHPcQXPK42D7+P7xR8ep8V3ezb1qtsgzg0UPg2tWyshewqB5UVo+i14dc4l89b/sIgd+DhnQgnROKuD9yxhnFFZwxiBcHq7OMx5m5/guRu3dLGPjIxuY9DIKGHQX8FihPoTfDeF3QyPwe/zitEuD7amjsAUN1934BXd+hY/W63tGkAdBgyMd4AE7v8FHR0Ym9rRtmLzSSvDK8Qa/AoG/82KziVQe6utR//DOPd8nNl15l9gItzLcKlfefV5XNRUGC+4LKly8nzk/8UGEmoVqNIHQfcEP5iYgiDfspLpAXj7ZVbeTvbDj8F0jdnLzti7ZTkYhjj+ZKdjJygLlP5kZspObYMdZnxm3Rod2Jsc3sHZ91GnYeZZgspt37UY/TLYocDzjpT3M5k29FTXKTUSjuiSb6YoBZoyjBmksgJoj1u1SwVxTqYOZ+tE6WUdjkdHdQ+bISGp0bJScHZ0bJYlRaZQchTbkVCjSPzq1Z2KBvBFaxEfVBbDvC7iuAcH/UVSvcAnFARfc3eB1G2/f8A40jejVxP9GsXnEDhXeXGIlQujECF2mIPqzmZwppOMgIHYFsqtjBBgiFMCBSYiDHG4Xs7Fa1iKezUhKT0/3qpu5rGcQhgwnpnkolMhoVZt92HIhkwUNEY4U/pV4oW2kLBwwMNGroGllmFkVSHwMSvWBsX1Kzx19ux4Kf/KJ4a0H0hE/N/CJ1mBwXTrK0TFrV+3uEZIMr93U6h1p8J50cftAbUeP1jvcWtes6hh2WSIIFcj394m57n177xseHl/7UOtzu4wIjCuiUkYeA38yU3JqW/hCaxgHG9Dc3QDHep1Esd4K3zgQM83YunFw89eKaQzRoP0TYHT/v6H966Po1favhu1fBWO0XtwGWDGSQYakhN5lEqbNYkPGYivCYivCRjAJEMEkQAQz4JGOUYt0Eruw80snh6ZHiAT+cAIvlMBLJGzMAdgYzNkd0Ga7sAF3XNNoI4vIoU/YRJw0K9gf9TqIUO+t+v8zRHES3LpcVsDxmaJZZfQiiS1QuYwpAAmCNvnjPEDh7CqrIyGzI7lUwFVjc3M5gnQf3RrveC/u4wvoddcXTRaHJyy2Lyy2NWwEE+0RPBRh0VAkUusnEnhmAg8k8MEE/qKYi+8YGRuZIDTDtmv9/1ZKAOKltTWnu8bWkNWo1MZqU7WZ2lzN00MDB/dn4bujNe/R2vkaebQGpuDA2RqVYCN2UnTpAdtOmtu6WDsZ2JZJ2MmMSw/0Wt1DlWTvhjiRqfbhb2xmMqIY4KIRk5ljwVEWiOwMO8++xtIsogdidl/C7E7ZY/aUPWPTs/acfdSmCFuySRtTeNBM2FP9LkVQ+LdTBIqqUV46q1HROPB4VY/eUX6o+5MH4D+UhXIZgiojcqS+wAoQsZBMl+SnzCyUl5PTImciyLKaJPi/UgRQh1cPXkUqfWD4L748PG1EAnzvNa11QaePo4dG7/0cH0CqG9rUK6Y6mrv00vCuwYda9+9OaZgcELeDex8+8PlWYjKSgLq5eR/Y+c0tOtZMEjqGC9QZqJkikSB7VulmPCR4kU4IOKPUjkwkVFQh6DTSNnQQdZwgGqTxNDqaZXkpS7ge2E2GuoH7VULdh46jeTr6cAxJoU6HsIyGBAmJoCChNwKN8Qbq0nRSEFxiHLs8JI7Q57VLU2PORmU2DL4dORX5G/BD37nEmz6v8nMObPFtjOwOfwF8yXdYfDPGpJxqjcaE+HwKvBz+oU46KbCV7VyNQiMxKUAUux0KLw3Oo3aMnqJn6Dn6KO2l3xcceNAR5iFQX+GCUX0BIpwKw0fzO4aPjl1/4zEhufVYit56w417XkQVFQQNt9SVs8jVXrvnrwmdqhI0EaKqF6WLsVVvoReauFprOwASSjaQI7PxHJf15mQxZBAJoBsg4oM9lYG9oF8yQIyCTZiPGoTmgU078uu8cEUBlE4op+DaPY58D3mP9wHugcADyn2Re9R74uzkBITzqPDOF5fkRgxuYXjTj/E4ZYro4WOEhBOlBMNGF5j3jk+z/EpNLqhGUWCPc6MwtB+IdqF0voLLgKwcSZx/5O7Pvfboaw988uEf76jdfc3852955M7N1HPfOPTcg5dnv/nFv3rkt/cONb/x0Cutnz77Xy59aYogr/y2tY36HpRFi2iQO1bJor0O1/VUuW60QzQoYoKDGmFQdhBb9aCBy3oMxOl2cCO25DhV6scAk8oXFDrg1VFWNAqtNQ9hUCkbGJjwMha26wS26wSA0gttNkSQS9iEX42134Kht/QyNNXlj2VGzxDVK5efR4Ja5ZDM4vQmx61bC68Oy3UQW92g4XoVL7qoXzoxDBoNOCvvDVgE0ALwYnh0NegCkCQ0JdfWgpWM0Pl2SqiApP4Rbh2S5oa0VfoD6bBMP1YE64rNdcPFPyjeJd9V/Ax7v3x/8Y/ZbzIX2d/6/JV1e/om+qf7aWcdKLNU3laCEN5pj3UFIcizMoSV3m4liQ2kUshTdEkaAOhKSAZdk6YGqr0pbo4jp7hZ7jmO4t4zyCDKe8YMYwwVSMymASoscIsJPOmptS8Nt9kkXDG6PInS+8jAIi4pusIlUQEJ4TAs8Ua5xvjZbH9OyFWyNaZqgLIfNn2+AQP08qWVlGeHmkXFnsjsngCMd2CBjZ6c9jKB0gLz8+enA3xU0zERpQVcIgrlPaHEUtm+8ACuRgmHGCy3VgdE9UVW0bEe1wSjUrc22CKBntv85PY/uenA4zPf2TaQr0Ybwy1Dq1vBsJRJqlnQ7wt8ase+9dff5OyplE2qcfCN+2+Z/uPXl448GhZ7Whdv7kuiek2+dx9160RFDTza+s7+zNo91/3hmf924DpVcfMf5Gko+3nwk9VZ0W4s+d5UVLYwiLHUFGgHiqvjqlQH/6Q6yCWFZExGgp/CYV8KQ50UjqfwRCBRakRDRJRK5KD4B7Zb+61HLRhmM6pAQRFcRPHTEoye/gV6QSys9PHkRQYtl4Of3e971Ef64AKqF14pFn8Zx0foGn+HxT+F4k1k6FHnNDqWSnXbq9gZ6RxRbi62mRmENWLOfhgciFWyKjqkI36eZpxusLcbpJDs4mjksYxlGUO5pLWB4PhuOWRIgFZnESEqCUCYoCiCgfHGXi9wvMBbSnWDbkI2U6mUAWaNOYMkDAnGH2eN84bHmLK/tVI/5kYQBy8ccKlQaeng0qTsRgoNYhUlehDiACiMxyO4wvjktMrlhQU2eOJuKu8C/yYupwThgU49SQfDR1eyAR/jJUY+c399S7+Z2R1Wwj2VoP+a9a3Cpi6N8/gzesriQJh67tVXry1aAxtD9s2trSMWdOxmBKPz2579RNxlRvdduUD+LZSpXvquVTJl9WGZ6nOQ3yYB5vsB5vuBGNNZS0DjVlpEBD46JiITWsWV5b0Ma4lpWil4wP0eMO0BnmwZANDNaPcmwW1JkMwaOpjSZ3RSV3iieW5yEnrHMtzD3SQq6EBiBBHB4uuL0uuuDV2RoGpatFi6O5JUSh6yu5dxl9GUYQ+42/Ogh/Rku5kNSbAv+dkkmcwqPEBX+CtHRxIlin1VnQ1gRGwpaGdZfdW2rTzn7s+hLPok2qRz5yab0jlc14eJv5hj+4pakVSUksM3inm+oYYmhBtzR6SnTA/HcHnOnuqb6Zvt84p9C8BwDkGz+yP/jwLnzHPZv8u8Yb5ZfId+J/OOebHIK83iZPHTPQ8XnwRPkk9Ss+FZfTY2Gz/c82TJLwKR5Cif4I1zxVe6fphh41QkpMQjCc2OFZ/2Pc0dMb6S+YrJKwV/vrituL1vb9999n3FxwL/MfNc37vUO3HBZnuTxItkEqRAGZBgARSOEy+WFoDuyN1qUnsxltRTOpB0A945dFB7MYIOdimKmfHztGjhnScJ/itRKnf3EgS6qfojmqaiIrFQpIxuLPljBQDltfTb6Q/SVHqBCjn8jAimxBlxTqTEBTDgaJaulVIsYIvzFpiyZqxZizKsikVa3wMGUQXGseGOAo0uHbyEgfYyyolfSYPJiUYZIo7jVwDs4oey4HHo7PBDB9JSOw6PNiBe4SDmN/18yO/n0RMGiIicUAnp/UtLkweBtHRpye3jritEJ0uGz99PFCawS4nn7ZQhyV4mJcPQ3WuzcajmyTjB5D1x4LqTP0Jll5PoWYTfMx9JH8m/z9OTEzCoP4BKMB1tHsyT89Q8/4x/Ljynz8Xm4k93fS0z3yOgBxbAAVx9tsfhy5my+cXiEfNI0TM5geCUnDe0hi+vNYDDNUi4xdz0vI5zUVyjBIeKePM1BCmpNAMGalB5d6yBd1rDdIscMu5OgLtTwUZRDbprKe5aogJPocBTKI2ioaDPfOiIIpwmNijJD8/jRwt86Ch+eB4/nAM3VcbbPy8C/fgLuFWhEzHHT5IlXY+UehmN6/YsMO+cnO5mlCTsHJ9WVoqK5KuVc3IGJzrD6AGKaKd4Dlq1jNwXaReJm1anLBQFPuRcOnfvTZt2Gam9X/7Ri/fsnE6Ho/50Ov6NWzfuvqX1056eIw8OjPbJkiJQz7Ve+cpd23rW5O3S5tv+/cNPJzkdbP7SE9c3Nt48t7ax+8DXo2JAhZYvdOWfyEH6B0SMzK+yfNmEo0DLl8A1dryAKQAhHASeIO4GsYsMdqoKgsinYnCJ7iBmJ4I8WxQjIXoBxI4TwAt95PL5xfLSubZ3fAuiw/LHrZoWxRF8BLfhVf0YygWhjt7paIgbxXVzMzzgxRgI3xkCW0MAn86BAgzPzceAB4NJDw7nPdi/eoIugeHFV4o9a7CT9wgGE/FV4TyuJWoun5+cPCstSucmO9k8KAyxM4QfXsCQ0NgL9pJkM/G0/LT2/fD3IwvauxoznwCHdbBd2O7fK+z1/1qFsXBYtVQqElY1nQKoCcWeBVS40r5aqkKSwCvU0EVHXgu/Hf4gTIVvD8V+TPAoG1I0oFsulRNHE2SCAICmPWZoLAhmg4AISsGjwbPB88GfBb3Bqfh3D3egZPtBpMlLEJUsQesCA/flC25+Ax66AKBjJuCmQIuOqmwQRjyIIOKpGIC2DdALzPsnp4FXZHEds9iW3UZDlxax3PaFM3IIS2Qfro/L1eRMbQD65TrY9sYbffn0etnKzG4o7en+s/pneqI2/YPWf9+0/J8m1tv5W2/r23sbeUc6cueW3O1Q/kgYVS9TTxFZcs8q+YtYmO+ChhMnInkj3+a825jMSLZjlwtOEIcsOp6oK5hfVzqCqXSiHNi5hFPritkJagJq1ssbAdWbKAZ4BtUBPY+CGpYjym8VFuFvD4FLU1p6v51Vd+lvVO+5CsvtZtwCKYrleINXA2Y2Cld1l+QBi8tTOCRlAIsfMHRc56ljmKdzOKJXWDZnYBk1vC7vnVNQ+QmaonQy6aiDpVRRrNxqZhs2EubGUHMWiWwTiisGgxCT4uq3GrAQ6WRYyP8cteh+vp5aa2xJbTE8OhvcjmKa9PZk1sqwFhhikuwGg88m2AWw0QlyRDYLXR76PgGO53g+jYs4A8RRAEQwA+bBa4AGuOhE0XRTUcaCc0FyFjZHgxQST6MtoFA8cy89+nGsiJ6gWx4cxI9uImIZiywmAFfQInRNUiwuynFRjxOSHJMScQKTSqicE+Co5ripRjGJnFW9HB9ILLDCcRjXYIlFNDKW16vlmh1xhfCRqaXbQiyjbBh1m5iOpKxA65c9n3to4+iBYry+BQxNNAufGm7cSD21/LfzuEjzpdlrJr40C54eqsZAdvnI7NjACMlcVyezKDK5csFzEspykVq+KsuncumkHCCLKHAOEL6cytL5bMoregloFZvNchlGecvn4essKK9wP2eIHLQwG3DtRByDNdyq+BEP1m3VnI8m8njx+4ugSNyTBVn+njzI8+7qxWJPOl3qafsbdK7mZBMViuCTuYlcbNJixxT8CE+8WYtYEN7LWcso7S3d6ZspXcxezP8m+5u8gCYcD9bwvFdiqf50qWTvG0hoWiqWkUo0l0vkirlGbjz67ei31W/nWD5bN+vWdmIEjDJb2c3mJms0P2o/zsxKs/KfZh/PP27Plp6RnkKTsy9IZ7Jn8t8vvZJ9Jf9m9s38+VKK8NCMN0xHfVnG8uW9di16rXStPOa5gdml3mAf5p+UHlcPa4czj2cfz82Wood8j0UP5Si/bwLcK90r0z4fm8tZ2SwHGAgBpaiclIxMOmkQdjFJiFwgKaa0ZBIGVY+dYPMWNCMPO46aNQ2WYX2MaedDtp3PWbmsVWF9IZb1QQuuhU0uG+K4bMY0K6oWUlXNzmU0GCr5WIaDv8ML4H3CIJLg/RMpIMronUQEoP2G+i9JMHwyCBINAqIIpwDCq74A7iKyBAu+5Yh5B16saeZ547J4OwfR6rGTZ4nb7cwCYJ2wEyuPaeBZDbyovaa9rVHal82yCl3HaUPMAgn+6KgEhhf6sy8AicgRYaiggsOV9+aAk5vNkTnoRE76HrbK7PdADJ4u5nAGjJ1n8x/myTyyevCj+WcZTFKM2WDWBogLNmzHPmqftc/bjD3Vs+JZllC2SNOXli9AOHlAvaQvaRIa0uEAPKxe0KG7QdtSmyrW3Zw6ckOdB4rc/pKLYA95XLiKs+ws6ng6nc5IodB+TPb/3TISO8jivwWA2GaXbC4A/LTs8zkpJDQR5DsB90H0cFCiEV21C6Hdh8ejjSzahfG7Y+HGCkE4geEd1DgqTwbkZNpDFLHNSYmEN5vHfxXBTVe5OfYgyqxDVBcMoidhcaKdcd+vJN4ZkKFABkAp9YNZaIjO/U2/akUGwcktyRB7/gchqwHSu+3Wq/Y/tn6dbf0ksWaQeipLJ+Op4vI/gb86NBgNUNksFZUyofDyr8DvB4wgekDdf+fl98ity6cpcmufH2eTWpuoS9AiVcnxVd415PMVuiniPgtYCcUbwnEtqkc6JeOujLok7pKoW8XdKqqwIdoVNu/D/5rlxUlM+F1lrJO+ApEIyeQDVVAlFGiDMg+gc4ihUB9B9PetmKK3Js9BP4Us0VmXtjgqDe/c8yIRu/IbQrvyIaHD289J7aTrd32SwjcDha/aZLC/FNk38O88X/CSPp9HYTVW9xVCes5nKqaeK6wBA0ottlm5w3cHd6f2h/ptsTuK97H3c/dr9+qfjd1XPMwd1r5OfN33Nf3PCy8Q5/v/0ZuBlqJQKHZ3c4CFliKohZJBolhNEgonJ5Uca2i6XunmQnBCsVAwfWwI3jn4kW7dR3NsEe41qP9sJqjASJDwWriaGF6tVc40EmJ/NKprSIdjT3Lgbe5DRAvOcB9wFPcweoh/r4/yPQwdbcBJFN4QDSAa8wZpPLm3CMrFZpEsan39f4kStShJO3lw9MLkgQvLlyZRjfRyOzk7unyh4OpZJxg8xK7SJ/RsLapV+f+rDDhQgBqD/KkjFHwJ+NtV5RBFUJiiIbwJa4E1jk8nRBcI4kfBG21JdwMYLNoos+pWlUD59q56pA052TrIYW0QwHfDPT3ptxdlhu0qgO5sXvVprS8OPHf9upF6Jd3Ic8nN5lDrtJjWpGgflHorYW1sVcHv7Lzi4/3ZLK2mA83Ln/7C4xuK3X0Rcf3EPHkiVcoIkoAk3iYIahpKfBi8gZ/vLissrdLz9Lx/PvCX9ALNzEeBP3qPv3dgjNgjjoWpGB0NBMWb6RvEt+nzItOW5DygohFKJAMeYdgDHvSAMc+Uh/RUBO8GEXxWBHvF/SIpVkgOIuuDk5O4cZ9/xIxXzPERH0nSUDiJQLzpVD2ek1ySpwOiaFJ0iKJoiidpEQiBqB+dhR7zAE/FL3ilvSIQK4DkxBfI9USAoMn1TpECpXn41UpjflDxO/4ZP+XXy9FmdHuUigolvkaQgNQi0b9ILx5265xGL124Tpr8CArNpckL0gX0NwoGlw8O4qZzjegy4XaoVDj08DkVQKRLSL9u73AJEwwJIMbCZUuBK+cdH4y7qQpscPrHDzuig96ZEVSv9D9PRRp0PoS6f38q1KBnFNSdO6U0aDWMuu+eCsOuiLvHxMaq3AuOqSZip3leCIdJwYODD8FLigvML45Pk9w/f96MStdAugvJWKaeDoM0SgjWqZv4y39PTrVev2UwGKPzXopYfgZcd+dwVOKB1vq5SXVrmeq2Vvby65mi8Ul4MxOtHeDTxCARJ+pYTgIC74kToWmPwLNrRYIol5eqVVBe+umi9GoVWTgYDDG/hcFQnFDL+A/MZAMU471a4F8i0ROd66k62BxfVzEA2agGKyExv3MgO2iHAZlLKpnuNfZg0tmw1erfFBAS+apWbBiFG8xwJlc0rlwhtrZ2oGe2yQFiAxTlKPFT8D8IguBPkcSNN2azDijU4Nv/Axrqf38KZW5kc3RyZWFtCmVuZG9iago1IDAgb2JqCjw8L1R5cGUgL0ZvbnREZXNjcmlwdG9yCi9DYXBIZWlnaHQgMjQ5Ci9Bc2NlbnQgNjkzCi9EZXNjZW50IC0yMTUKL0l0YWxpY0FuZ2xlIDAKL1N0ZW1WIDgwCi9Gb250RmlsZTIgNCAwIFIKL0ZvbnRCQm94IFstNTY4IC0zMDYgMjA0NSAxMDM5XQovRm9udE5hbWUgL0JRSFpTUytUaW1lc05ld1JvbWFuUFNNVAovRmxhZ3MgMzIKPj4KZW5kb2JqCjYgMCBvYmoKPDwvVHlwZSAvRm9udAovRFcgMTAwMAovQ0lEU3lzdGVtSW5mbyA8PC9TdXBwbGVtZW50IDAKL09yZGVyaW5nIChJZGVudGl0eSkKL1JlZ2lzdHJ5IChBZG9iZSkKPj4KL0ZvbnREZXNjcmlwdG9yIDUgMCBSCi9DSURUb0dJRE1hcCAvSWRlbnRpdHkKL0Jhc2VGb250IC9CUUhaU1MrVGltZXNOZXdSb21hblBTTVQKL1N1YnR5cGUgL0NJREZvbnRUeXBlMgovVyBbMyBbMjUwXSAxNSBbMjUwIDMzM10gMTkgWzUwMCA1MDBdIDQ2IFs3MjJdIDUzIFs2NjZdIDY4IFs0NDNdIDcwIFs0NDMgNTAwIDQ0M10gNzUgWzUwMCAyNzddIDc4IFs1MDBdIDgwIFs3NzcgNTAwIDUwMCA1MDBdIDg1IFszMzMgMzg5IDI3NyA1MDBdIDkwIFs3MjJdIDkyIFs1MDAgNDQzXSAyNjEgWzQ0M11dCj4+CmVuZG9iago3IDAgb2JqCjw8L0xlbmd0aCAzNzEKL0ZpbHRlciAvRmxhdGVEZWNvZGUKPj4Kc3RyZWFtCnicXZPNboMwEITvPIWP6SECDLZTCa1UpZcc+qOmfQCwlwipGOSQQ96+Zjd1fw58kscetGOGfH94PPhhEflrmOwRF9EP3gU8T5dgUXR4GnxWSuEGu9xWRDu2c5ZH8/F6XnA8+H7Kmkbkb3HzvISr2Dy4qcO7LH8JDsPgT2LzsT/G9fEyz584ol9EkQEIh3180VM7P7cjipxs24OL+8Ny3UbPz4n364xC0rrkYezk8Dy3FkPrT5g1RVFU0BRloSBD7/5tS82urv97/EZZAEk9JEpLUllAonQskYVZsbGsIbEqSZIIiXVHUqUgUUmSarIwNRtrDYm6YslAoq5Z2kGiVix1kKh3LFlI1Pcs8UREzXMpSsfUnFGVkKiRJQmJumeJ7oBp+CYUpyMazqgoCtNwIEVRmIYDKYrCNBxItZBoDEsUhWk4kHKQaNpVWjvwm2sfvj/8Wo21wKl29hJCbBy1nKq2lmzwmH6EeZpXl4hP9gVnztThCmVuZHN0cmVhbQplbmRvYmoKMyAwIG9iago8PC9UeXBlIC9Gb250Ci9TdWJ0eXBlIC9UeXBlMAovRGVzY2VuZGFudEZvbnRzIFs2IDAgUl0KL1RvVW5pY29kZSA3IDAgUgovQmFzZUZvbnQgL0JRSFpTUytUaW1lc05ld1JvbWFuUFNNVAovRW5jb2RpbmcgL0lkZW50aXR5LUgKPj4KZW5kb2JqCjggMCBvYmoKPDwvTGVuZ3RoIDMwNDMyCi9MZW5ndGgxIDY4MjgwCi9GaWx0ZXIgL0ZsYXRlRGVjb2RlCj4+CnN0cmVhbQp4nOy9CXxURdY+fOpuvSSd7uw73elOOksnJGQBwpJ0VoEQCIuYIEhYBVHZUdwANxR15FVHATfcEWaG0FEI4JJRx2UUUWdcRh1Bxd2oM4M6o6T7e6ru7RACir7v+/9/3+/7Jc1zT+3LqVOnTtWtbogRkYXWkEyO2SuXu5Z/8J87EXIHkTl/3uKzzxty14UOuJ8n0irnz50558vM5ouI7GakGTwfAbFKCtLb/fBnzj9v+YVjL277PfxziEYXnzfzwsWpt2pbiJo3It61eOncxdKM1hz4O4hi55+7aPZMTbl3A9G5LqQfeva5q+bt6a6dSPRoM9HprepvyKmOFUiTb6FUotD7wGHg0+CY0FF1IXmC54QOybFELFOH8ZdFV1ImfUq30pM0nV6UZKpjA6mZFJZEySSxcmpgDkoklVkphzzUQE0UT2PoI2ajHTSIPmf1tJZl0Xhwwk3jKIGq6L9oCzst9Bmtpb+wBbQdubcyP2XTWDYqdJAmUFNoN+ogGk630WYWRU7EWJkn9B5KWEbraC+9QSGaShvVLSiliSbS+aHdNI1eZVPZmaE0Gk3n02W0ke6hx+kwu4Z1KmqolcpoFi1lJhbLcuTLQ1tpqPqW5dHQM6FXyIH096DULyWfUh/6ivz0qcJC8zGSsVSCz/l0L+2id1kSK5NrKIpKUdd0uoR2yDlo4yi6Fn3byy5mO+So0P3ozRCaTavpELuQdUoZ6lvqN6GLKAb9K0VL19P99Ed6mr5AafVssnxesDI0jhiZyUd1qOlKupr+AM49hc8zzM4y2GiU/Ef2HntfPl/+GCU/RF30Hf2b5bAF7DKpUrpcLe5eG3qUvOihH2WMpjPoXPod8zI/OxN575AukC6TVsu75HeVHOXr0NDQ06RRIdJeTtvQr5fpL/QmxqueNbI3pMvkdvXq0MVobyHNRy+upAdoD33LVGZhkSyOuVgJG4KeXcw62ftSuuSRmuVZ8g71+tCq0A2UAVmZTnOR8xy6gq6i3XSAPqAvqIulIGchclayJnYDu5E9Ix2Qz5CnybcqfuVWZbvylHJUjVafCr4aPASu83KKqBGf6TSPLgKvO/B5mt5mMktlA1DSSDYGJc1g89glbAP7LbuPPch2sefYK+wz9jX7j5QkXS/dIu2T/iQdkF6R0+U8uVa+W35JyVDeVn40zexODz4Z/DoUEfKFSkIbQneE3gl1iVFIg8RXUg2kayHm8pW0gX5Ld4Lnj9B+eh1yd1B8DtM3GIMfmQZpSkaL3MzDslk+encGa2YXsPXsZnY/e5a9zw6zoxJJkZIbnzxpsDRGmiZdLn0pHZWtskeuki+Ub5Nfk39QVqnF+GxXH1W/0Q6bsswvHb29+70gBRcEbw3eHiqDLGqQvFjMuVKqhsyNwSjPoSX4LKWVdAF4dBE4fgckZwcFaB89Ty+B9wfoHXpXtJd/PsNIHKFuCjIJ46kyMz5624swMjWQllY2F2Orfy5ml7Nr2UZ8bmd3sXvA31fZa+wv7CD7kH2LPpFUIFVJp6FHTdKZ0nR8ZkizpbXSddIj+LwsvSG9I30g/SA75GjZKWfLdfLZ8jXyerlNfkT+q/y64lWqlFHKQuU55VX0fJQ6Wp2hzlavU+9R71OfUv+sHlZD2s3avVqH9qnJahpsajJNNl1reti0z/SuKWTOhjw1ovW5dOzvZnamUihtYCGpA/1+Qlouvyjdwrb3SkHqerRgDs2QOuTHpTsv2SB/IP9OupxIqRXRI6HFXqLH6CX1L0q8+ik9J6XQV9CHt8gzpSekTVISGywPV65SXoLWWYV23icdlEzSDqT4AqMxg05nyfRPZQp9Df4fUNeDp/XSe2y79Kw0BpL8Ft0v7aNNtIXmsiFo3Rx6lH6g/2J7ZBfbBblbTa/Ql3ToWGuVwu5qqVJLklZqwzBCe9iE0HNSbugLzPr32VX0jvwDZH8KG8cK6UH6EKP+OitlTiWopNKr0HwD6HZI7SfUjjn4ZyUTM+hb2iOX0lTlEMa8sPuFYK26XL6CfSdVYTgTheYez7UxdPBG6CquR6NoByQBWkTM6C9oP3ODi3/R3qbNdCPtleMpS35AWiOF5OcVF91Eh+SxqPVS6Kc0VoqSzqMF6Icr9HHwfpRwDg2loWwWm0q1iBlFA0LnoeUPQhf5Q9NCm9QW1Ucvs7Esnp6E9koCF29VLcEupHwE8/AdGsWuo/bgHOrEupLEslgxpKlLXaluULepj6hPqPu1QXQhZu3tGMUP6AhWDRebDV58Tt9D1qsxe/Ixf6rQilFYw86VWuTHqYal0GLowBzo7WrwYCpGchlKuZyux3x6AGvIy/QNc7Bp9AS9hZmTiHk+G/WbUU4DnY5RX0YPQjtewdoRMocGUB749AOLYkOl5aiP69lboWc70aZ36WNojpBoVz4bzmoxerPpez6XUcNgamI7sSbvonKslLXyS/QRZWJ1rcYcvR/5WiEbUZRO5eqHTKL84LjQUGmB/DhLwGoYBamajJV9JFuCVtjRj26KZ+OpLHgaStsOXdakPoDV14eVIV6KV85QT0e738ZK9jItDTWzzaZa+U35G2Wxv/r0yf7KipEjhg8rHzqkrLSkeFBR4cCCfF9ebk62NyvT485wOQekp6WmJCclJsTHxcZEO+xRtsgIq8Vs0lRFlhjl13nqW11t3tY2xesZNaqA+z0zETCzV0BrmwtB9cenaXO1imSu41P6kXJen5R+PaW/JyVzuEbQiIJ8V53H1ba/1uPqYFMnNMN9Q62nxdXWJdyNwr1BuG1wZ2Qgg6suaX6tq421uura6lfOX1/XWovidkZYazw1c60F+bTTGgFnBFxtiZ7FO1liBRMOKbFu2E6JzDY0qi3FU1vXluyp5S1ok7PqZs5pa5rQXFebmpHRUpDfxmpme2a1kae6ze4TSahGVNOm1bSZRDWuBbw3dJ1rZ37n+us7HDSr1Rc5xzNn5rTmNnlmC68j2od6a9sSLzqcdMyLwmNqmtf1jk2V19clLXBx7/r161xtWyY0947N4M+WFpSBvFJWfev6elR9PZjYMMmF2qSrWprb2FWo0sV7wnul92+up46HtJ7jarN4qj3z15/TiqFJWd9GE1dlBFJS/HtChyilzrV+crMno60y1dMyszZtZxytn7iqPdnvSj4+piB/pyNaZ+zOKLvhiLT1dsztiRMukZy7Gib2cJbxFnlGQyDaXLNdaEmzB30ayh9zh9L62UORDH8tDLna5mBEFrRZalrXO4bxcJ6/Tc1yeFzrvyVIgKfry+NDZhohWpbjW+JOLic9oob4sLvN52vLy+MiYqrBmKKNFcJfVpC/skO627PY4QIB+6gJvJ3ZMqwQ7M/I4AN8XYefZsHTtmZCs+530azUAPkLfS1tUiuP6QzHxJ/OY9aEY3qyt3ogyY8Q34XEt5m9Pf/sjoTYuvnD2ljCz0TP1eMbJnkaJkxtdtWtbzV42zD5OJ8eP7QnznC1xdY0y6mS4ZJSZRELoZzWk5h7miPblCz804RQz+kwmSGVIoS56tscraP0Z4s1I+MXZuoIfcNzCXIsm9HMtmG+4/3Dj/Mf17zI9TIarHilhslT16+3HhdXDw20fn29x1W/vnX9zI7Qmlkel8Ozfg9smuz1i+tawyPaEdp7XWpb/fUt6MR8NgzSKlH1Tg+7ZsJOP7tm0tTmPdgAuq6Z3ByQmFTTWt2yMxNxzXuwZfOLUImH8kDucXEPNlcQ9IBkFulT92B3uEbEKiJA+Gd3MBJh5nAYo9kdkh7m0Cvyior8WOVndyh6jD+cWkGYWQ9bo6fOMVKbEePgMXux0SMSkfof1xo1k5t7y4OYZC0FJETMRPRy19KKGfYR35qTzcKWuffDsnpO37jyrVk/Xts900HmpUhrAZhIgKcpI1hHZzjox2t/iHCQEd7zFzNWK2dp3CWFsY12KEuoTSHKAcajpt9o22iiVE7XS5xuo2SEL1VupBykr4a/GHQq4iWEjwHWAcVABlAC1AFjDToKqOR1ABtRRi4vR1CiS01LaJr6HDnUKeQDnQCkwp2rfEgDtXKaBPjkdJE2Ae6BiPOabqBcpEuHvwnpSjmF36sso3MQPwbuIl4m+hEDGgXEIDwD9b/B2wxaozxINysU6oLbi7KnIa9PvoHGgY4HHY/waoQ3wl+PPHnSttBzcNfC7QNvxvJw0fdllA2MQ54GtHOCKG8ZVSIuFvVGgxYC0YiPl7PpAfY03Q16ppJLkaLfSCP6PeVYn0BPE206CXgbeft6g7dJKg/9A/g78KHRttEngLerN4hmyyU0HHQN4OHlS/vR54nEED9M/YGGc5gp1I1+HQYSlDn8sCX0Gdo5QX2EyrgfiBLgtvYdaNMRGoc4n3YrDUR4qTQIMjaPBkr301Atiyzo31SkrQWWCdnjsjCHJmM8QqA25SNKQVwm4MUY7jD45OC8gZ+PL/oX+hrt+BJpJgCTuGwJ+ZpDDtTPec7HPppNCUI2Q58hbjowA/0aDgxG/PmQ4RaRB/lR7nBDDnN7KMBlrxdyeBvC4OMUhi4jFA/EGcgGngauBP4LWAw08zQoNw/puZwsRJl18Lu5fHDZQFl8HMYYshMN+c4VMqbPmc3g4xggCbBrmFsGbEgbz+cLl1kxXzAXuDxy2eIyE6ZcvoXcb6dXeT/5mPeiqep7NIm3QfQdstWLermccSp3Up6geWLOpnN5C1MxJ/X2e/mcCNOe9mB+8jnCqeKjLD5XuSz2UMxTzosemki5KLNRuxdtv4DOULJpjLyQqpSpNFpug/4J8vpCXcrr9AfpefKZOoXMoI+0qQ/l47zR9Do7R+2kXeBllrKfNoF6lNclt/I6U9Xtoc/U7dJlOsLu3rQvWKcexylH77hfG/7fgfSGup3mwf25+jrmzut0E18lTF+wIsAVpggPAGuAPLOPbTQvZB2m0zGfiI4AixQ/5rqfhiid0Anx5AefshB+uvZbyNxCykbZ3ZKfnoX7Wei+ITJhfqIu6Q3oC4CXD9rYS46Ok7mTyJKgYXk9CfUZsiQol2fotbcN+o5Bv9Ip5fG1getnvj5wHQ2M6pHXsFxmUz5oQ1g++8qpIZ/jDPk8US77UGNt4bo7hs9T1GUy5uw0rh+5juM6kus5ruPC6fvSnvzb6Db04S2hh/cjrz6vnYAPyEf8KkOPQA+HrhT6cE5ohak+tEIpCK3QykPXaF+Azg+tlC4Ondezpio0yNBlGeG1VKyjj5ElvI6qC2mZodP4uluqDsfapK+jYv3URqId88X6xtMn8Hko5uB1FCNdDL5mk1UZQvPkJ0iWx2HdRLhSAJ3M45ZQpvwVpSnXQtfdHPpS/i8aKdbNUTRXbqVynlcOkF1dSxnq37CWXRz6RpTH1ytQHsbbr82jKq4L1PPE2nuOoY/z+dibNYo0K5Qt0uyHbjpIMbwvggdjyC34wPOuJeJlmT4jp1Iu+ODiEHm+o0jOD86j43ihr81jRJkHhT6LEmUfRJ0v0BQOzUljTO9AZ/K6zqNWi0Q56nOhT401ezTW09HyvbCDIomE/O+nSHkIpWKtrDdwmnIpeL4Mae8w7ApOoffFev8VdBVkRL2WJgp7gsddAbvnj3Qah7KNMrVK6Mfh0P0rKE1LB48mk0fI9Vi9boSPFvYJX6e4ncDny0iK1FqRH/NCtIGvN7zsXMHb0ZDRKrMVa8ssstMv+gv9XqcMS2voZelTagEdDpn9ChbADrgXivV+COXLv8P6+DV0/G7IQzKNlGbTUGk9DVUssM1GwH0JDZUfBm4CDy4OHVQSocNrEX47sA75/gJ+2hH3D6TZCjm4EnkHwP13qpF30VD1cvizIKvPgh4E/o18EXS9/Ae6XnPQVbw+UT7HxaFkIImXx/P1Bm9rGCdt80PQLydrb+2xdva08STtE+1AuSIfTzMkdBB8ehfI0mlwgnQDbQe2SG8jbyddxm4N7QWT6/tgVG+/chm7BmgCFOUyugu0APRz4HXgDuAx4CulDLy4gTpB2zVsFTikJ6iZU8Q/ADwOvBeO6w1ez8nCe0P5OLS3t18tpnIOKT+0l+OE9HdRqXIhdGxRaC+HvBL6AdCiMG/N0PcfIHwK8vXxqzl0m7KIBsgTST5Vm34O+CvqxUf/L+njLwW30fj6/L9V3i+FdENoP8a4UPB/Cw0UMvQpaZIpdIA9RmexQ6Ef5DtI49D9lCL4eRfWJWOcEH6NCO8zfpCVwZznfcPhHsER9vcd11P5Ue6C3gjLQRimYvJzKO8hPdDXb15Dfg6Ny1j+if6een8Kk6kUfKpXJqMtH5zohw4p5JAWw78R8R/DRgd6/JOxfkzW5ZMDvPVwgNd7OaQPsB8F5ImImyjSV3D04msz56vcyfOK/GJ8wnLed3yQ16E8g73PYXLDndKX9p6zfWW6b1hYl5wsTZ+5UfRTZf7/CZg7fwaeA579P1oP5JwRZBVwAMJGPR+26lmYF/upkqh7NdGPTxId7YT7KOgroFuwRqSAPgoUIuxW0BrQJOBVxH2PdQQme3CekkK3GXYl4oLjke43QIdeTjAB7gKU/wVwH3Atwj8C5gEugKcbY2Ap4t/V8wYvAL0G/h9AVwIvImwi0lwK9++AaXBj/T/6b+AuoFAv70ek+3E3t0dOsg/936U/sf/4pTS8zwjTvnuIX0XPPzXtu9cIj/+paHgvcRIq+GDsmz7ttff52T1OmEJ+LL0BW9oDm9LN7Whuy3L7mduPYSr2bXeIM4EY4xwsTPkZUSG3nbn9Cir2d+pn1AQ+D+1pV3gd6aVbpXyaCyQYgN6jGqR5DbL2DXSPnW0NfQvbcgMHX2L5OgYKhF6E2w6d+yR7LPQt6H7407GWWcJrWli3nqBjT1zT/o/6f+0a+d9YU8cbWNAH4fB5BvrGFxpwc/Rdi38tTrV2/7fX8p9Yo3uv0/9Tf3idD8NSQcUcJj/a7T/RLu1rB5zKfyo799f6+9odvfw7OX4mXvj72iVhf1+cEH+i7On2TArmWxh95t2vBeZptbIo9LfwfA23oc88tvbMN8OvraZaoC5M2VbKgR7JBa4HnofO8IBiPQtdBNpiPkql5t9TMfxY50J8H8t1YguPA41nj2B3+z03g4JXwW+CLuZpmw20nEqe+8ott8+FfQieibZvwFgcoUJgOBAD7ATO6xlr7D1R9wfSY0R8nysfDn2Lsr79KVvwpyj2eUv5fg9+O/x2/k5EsxLxcw24NxvUCj2+uNcZn0e7RqSpFmfL2TQZ6+Y0pZxOUz4UZ3KTxFlVFdVLy4+dPxnvUhL52ZDpapqpPheaET4/Ni+jCVoe1gFYJ+LsWz8Dn8jPJPjZFOwYMs6Qw3QMP59Sh8AenSTeHUSHz4+Vh5D3ITpN3g6+bKFU8Z7KgnU9jsrEu5o/UT0/d5c2Yn89Qpzb74hIp1TLZErl58aWKeI82Ys2+CzPUbp5CaWb0qmer1fhdVWsiYa719kfP8tM7XWmKfrc1ybg7UPbUvl5TO96w/lMT5NPzRPnUOFz9pPZNiOO2SqhL7HGX2Os9Z//xHlnfa/z+ePW+r7n9LBHFktB0Q99rcaarS7A2MoYi/AZfR87K1wXZPLLn7KFwrYJ4FY2QV426TKG/AOBUUKW9HdxRaoCupCqgEIF65C8lbctNF9ZgHwc+8Q7yFglE9Df79Qb7+0S+Rmk1CjOvN4DgvxMWHlHvMMrMzBWWRa6SuQpEmfJY7RYtI+fOz5Ho1G/sxcaBMogx2XkRJvzOMS52sLQVbxN0oHQZuk33C1kSX8X+A0tRtn5xjupeNHWWooV8plK8Xz8gVb4xxjv98bIP9I4wSt+driVRos+wqZCHSvQ3i4lSvRxXDiP9iH4/gONNy1BuwtRbwPmxcPk1V7EvPwP+vwMkExx8t+wJ98MXQmwpaEPJAvcFujIDOAFGiCvAJ0D+OlZ+d/0bPi9moEvBWTkAYx3uRyzOKRtLMN4T+g13HG6G2HldK9AuIxttKkXkC70pRxFBdJcsrKFaNvjqGMM2oF6ZAfd2hfIM8uAeOfG3+EpZ9CtfVDTF8jLaWFfIJzTrL4wwlP6AuGcVvcFwqtP0o6fSvdT7fipcG9fINz7v9COnyrX0xcI9/xM+xr6AuENv6IdP8XnzL5AeObPtGNcXyB8XN92QD89DjyFPerXoNhLhBaBYn8dGgTaqceHXgbmGf7XjHRVQI0Oce6NPW2oCKjVEVxnhN9yLEyEo+zg23pcuB5uY4QmAyP0unje4D6jbY8fq/O4tj7Zx4+9e/CQXh/PL+raq9sxoauNNDOMevfo7Q4OBJ2jp+8+qPdR5NtzDPzcgId1O0F3870UMEmagrkZgzmKeSo16LpEigXu0N3y10KnDGABUMxnMY979AI9rgyhOcqLNJKvAabZsB34WhCkJuhcUu6jAb3WiBVyK9bsD7F2zoC+exr6qw3xMlmFHfcdFfP3kdh3NiltSP8FDYYujFUJMrcTa9ty6Ki3sI97CfrTJd7DDJDRPl4Hfy/D9abx3rBJPgv+s8SaJt4L8XWM61srg175F0VradC/+ynLbKdcdbw4s4hAGRbzC0iPdSO8F7a+D5usG+tCOVnkIdiIjqQI/m6rZ/27mgqUN2l4mFoGUJNpGsJvowxlB2VYDtNobSWNBv/jw3WHbS3pEYI1FnwQeMyQG+x1j/qAsbzNYs/NbbTh0PUfwv6G7aQ+C/18J9bTcqzNQygVbfQgXZ12D/gVQZmmGeKdfr35C/QDNqj6Bo1U6sT9mfC72zH8/ZP2Dfp/kDLDe3dTBMbqCqIw5fZGz3nAx+j/k3Qaf6fG32uFzwN6zgXCZfD3j8toNMbgP33tmrAd1XMWY5wRhOsI90fQ7cI+cRl1hOnx9sZ5NJO/G+Pv8YR9eCLV2/QO7NlK9NuwZ7VXqVrzg7bSQvHu8SlaKO+gsaZL0K8RFMntM61F1DeJr9HqXvDpH5Rl6AeuE87T34uF6gFYrKFlGL83AdiNNPtYODUh7D/6uVyowkh7CbBAd/O40IVGeIVR/gI9Dc/b/Xe4Vxh1GWc1XCcIvcDzZfS2U/k7UW7fn0CP2fWpvP+npL/wDI3PYX6n6qfe8f8UNezYD8K0lx19HO19PwXpPjHoxwb9Urzbha3Xl/a9v/JT91l+iobnWc9863Pv5afuwfwEHXqyezInpb/w7M4Y79gwPcn9A/1M7hiN6b1/6kuFXb2NEnruJ/E7Y+VkEndzfgZq+A7X2dCJgPofA0a4Yd+fAA0SzmH6/HiIOwY/A9NYpAPM+09APIfYFwiEXtOBvaRA6CAH7HXiUHeegHgORb/3eAJMjFQO86WoCzB9okPcafgZaF8h/Zk9iBf7j5+BKRHlAuaNBhJ6EM8R5nuYj2G+oG//Efcywm026g+X+z8dx//puPxv9fvn2t4bkOX3gScMyvcS8SdrN9ZG0v6Kuu8D/gg3v3O5TZw/kMiny843QJdBvwS+CN8dVGBlKQ/0pBd5TpCDUTRXIDwm+v2bGBNWENMF4PF3PF/oSh3QASfjDx+XtUgP61a7A5hPDYbt9b6aBPtGv2M7Iaz7LI+K840Y427tGPM34q5tkfJHmmfYey8att9usZ/m92D5vUtd3/G7pxlCJ3yI9pyOtRlAXZcZeNXAlYbtFwnIQJRxH/jh3pAHwj4D4B6G+sDR0G2Gve0x/EAwoIf3tO3FnjuSXlLVLQC/XzoR+7wiyPddsBsB+XUA9oJ4j1NIycqn8B+Enu9956ZI7KnHhKHye6ayuDurz5Xh1Mjv1Yi7NfxODrdJx6OMR3Vq6Fn9ru907EUeRZ6pZBf3bz4iVdTDy3iRFG4Xyecg7FlSZR/6zbEYcBrYAzwHG/oj8OM3cL8KFMBu/S3o1cBfAdjbcibiPwHlaeKBy5HmKVKFjc7RjrCpoBsN+zzGgGrgLMOO32nY7csNmz5W7L913G7Y8tzOv1Skcwq7/hajHj/omaDvAPWkivIsRnqeLpym4Vga06WwrTdCbh4CvRA8rArtZZ+h7ZPJgTG1AWUYa74fguwFX4D/OYDbRnfxdz4n3AsIvyc3qDobOu5D7DHqYM8+Adl4KHRQrYF++4CGqhMogd+n7X1zid8nVvhd4oWUZpx97wyfgWuv0EDLleJeLplFWp1K2/X78+x0PUy4N+NhfFeSz7OwjatJVKn9Fnbkb4VtxXUPvxu4BnPOI2zsdHGOE69U02Dj/lQ1+nkl6DAh46MoEXnWGfN3Hb/PxuXKsAP/wCGto8m8Xv6eQh5JA4y8PA32E6HLDP7xc7m7er172sXR977G/+13W33fTf3Uu6JT3cs41T2NE/y/8n1K33sbp7rHcSr/Ce9fTvGuTFkW+ljo3W1k1/h9/GWwvZaF3pNvCB2Vy0KfQo4ioKP/KWSJKAvpBvA709rT0L+N4jw0EXkSobvKlG+E22eUV439XeMJ33HQ7yCK7zbID4euEmeXsBON8qeJs1vjOxPhM1q+TxN69gbY/B+LM/xKccbdbuiVWOiPBl3/sA0coQelauNMciHFsNt1nSTcb2A2LDR01HxdRwn9szm0DenTpVt1fSWfqZ8ZsoXgY1hXvS1okrTK0EOYEwLXAxXAVD5HggbEvuwx8OHJ8PmGaCM/u4gRZxLiPQb64uJzkH8H5lS2Etb/NuAt4Jle9IswPZVNaOTheJujb3zP+5u94n4v37sV8Xu9PXuu8L3oTWJ/Ma7XXkTY+MZZ+zgxTuXijqx4r993T8Df5WDtHBrez4NPzwJ/7UVn6AjFhddoUzPWtdfFexJuV3i47cD3YsadWi//rge3g3rt98L7OLHPwBhXqAXkk9+DDAb5/jY0HJgAmI198v0c4j5GNuVzW4ZThHGbIB30H8C/+DsIA58CkMbg17q7+xGxhwNfwnsh6AUf4pZptyL8I+yVOmAX7RR3s3dIIdGH6znAk00chg0TRpxxDwr6mwYBfE9fa1DoXHG2dDrylcrvU7U8n8aC+uU/UrocxLi8K76zMlb+hIbLR2C7EFWC72dLfwx9wt9ZyR2UBlqCPOK7VfDzd1n6d6v4Hfg2StNmwy6/EDK6ioZrG0DLIScaxSm1NE65n5LlpykKvI2Ce5zyCcVJkylRroYt9CDF8Th1FfLw+9f/hL1yFPlAlWsRdzP8r2I/fAXkZiR44oF7EGRhEPxZlK7eL2zTUShD6smL+pQ7xT1r60mBNon29ALaI9okTQ4dBr4It+UE8Hb0Bm9Hn7KPA29LuP99IHjRG+ALoKD+T4HPgW69TZDfm47n13Hl8LaG8ZjBuzDAwzAEL/sCvO2NE/pngPO9N0S/w8A49AA84GMi6jZkgI+7/CB0Bm8rT/OJ3kbeL94fkccYf/kzGiLyIg0fS/gjRbtQjzoKcoqxVwl5/gS7qtwo05AnkY+HI60YQ1AhWxr6xmWPj/vNer08nvNTq8T+t5LiUGY6ZDdOmY92vK63SYkjc7j9yllEvCzMW5Ino47XIV8cPM+tRvuNfoXbLurjbUeZ4bYrbyL84lBQA9X+Bvdj4Gckyk8HRtEokyLGit/Vj5K2Yg3YSouBWqAeqDQwHCgHqg23E1gC1AGnAX4DI4BhQA13K2up0PwM9hZboO+66E4O7XbM39fpKvVDukoqpvuAW4FxgAs4GxgkcVuqGG3RUQ2kAVnAQCARiLDNpfqoYq6DQnvtswSdA8AiDT0C3HEqG6yvrRG2QfqmY4+FXoKtMh/0Y9DL+96/OJW/7z2OvvcxTtWuk9iEx9+paYS9/EDoJaUi9Lpyb2i/aQ8Vq19TsckDmkhlESt+vA56HmtF6Cj08kJgGad92/lL75z/0n7zO2rK37GmvYF5dBP2hgXkEefV91O+WP+IPNZ5VKssEe+NVfNuzLOjFIk9fanWStnm+NBX6l9EnoF8/2o+gyK1+yjSfCfstSfFGpprnK/7jHO6amHXwf4zvYG4BQi/iJrk/2DvX0aZSlPoLeM7QIlY87zKNcKOcyo52Iu8jn3c0tAHwt7Tv1ebwdcZq0yLrBGhhyIOEFl+oGLIWS3/PkIPDpHEtlIxUC/8WylFmkDDQHN7hY0yaK5Bw+HnC3qE5gIJBqLZkdBr0kC6Ae4atgn9epIaAb6WWrRCcRbq1VpolnYRDVOfhP9RullNghvhpqsQN55mqdfRMMzpYaa/I/xjalKfx3raHfqLeoAu13IwHn+kS7UXkPceulBLIg30avVV+G+mG7T9WN/+TA9qv4MfYSL9XLpQvQD+q2FbPY5y65Bvceht5J1jGk7Xo52RLEhj2OTQq/R96DWM/QPs7dBrygrK1G6E7X025Px2mqXcCzt7EXQ13DKsErjLMH5lysN0Pg/HfrweY0PKXDpPuRr0aujYCpoi8p1Py+Evk2aH7gIdLbcQAx0rnUcZyFOmRNES5QWkQxrlOirD3CpT1vDvhtBMOYPGCv89kIPh1KQRTYVMEMZ4h3SEHKaLqI5DSyCrJZLKzN8C71GZJRrzJw11/hvjCiizSeZQZ4FnPwPlXOgRjmGYO73cp9wn9sGp7t2f6p5933vzff1yOewEjrB+4vsZALpigLGX5vuFT/S9tHgn8ygwETitj5vp7+o4FedXmcAZBm7qg0L9nVColL8TDtv1Yk/f6/tG/PtC+vudYDD8nSCsfcmnBOcP0svZ5JaKKI5dRPzd7wq+P+BnG+aeU5BQY9/xYFPomvCevO+9tRP2xr+ncvYZlYfXDf4+W9RRHnpGuSN0bcQeIvNvMZf4u7Yl2AcY+wn1LpQ5EfrwVRquLtX3VgL7Mc+30w71dMgkUYvZOHexPoT11k9l8r8hvwspXlpNWWZiDaaHKI4D+ch0OS01ynmDf9/ftBzzOwJgtIPb2MrEULf8L2oERqCNPO1tPK+i/85Bk9ICvav7Zym3oQ0fwc7F3OAQ3+ecSq+G3ew5nXvmU0A9QitPcl5x3Nol3q/Po3R2OiUb3/PO4H7ABXgMd2IY0DODALsR3vPd8F+Vd7N+/sf32OIMcDvliTO/x/VzP20b9ADA971m8F7dTaNNybAR59Fp6rk01vQa+BkXajNVUrTpE+gZfU+XzKGuDf1TOY8S1G1Y2z/Uv48rflfgS6xzxm83mM6hpdo0/R1ZGKZNVGy5Czah/j3WDHm7/h1U6PY46WLYDPy8GnHI26H6aIocBz32Ja1R10J3fEgjzV9CPippkFqI/dqVVIS5nKvcRA3SnZRnGkJNls8wtrPIzr+zanzHPFd9Q3z38xLkr+BQp4SC6nOhXZABvp8ugG7MlcqDu9Q7yQV+tJhiKMf6HeWEv4dvTu/5Pn4SYDco92fIv4Odn451fgDlW5yUbz4s9qwO80FyRBwiR9T7cH8swqJAoyI+gc08RfBC/CYGX6fFPu987HfvA2LEXbVYtDfWfD7FWu+DW7+/VqjlwcacAR1Zfuw3Lfh5DubeFF6WQRejXRPUy2ETGWHwp0Oe1/TCmDCVVxiY1gszjkF7lN955eYUx48hzNdW/tsQ6EMDUGH83gc/Q50p3n3Mwb7AyMvdHMZcbQjP2TDFvKnnUGbSRCAZ864tPBeBBkX/HYm/8++g8981QN014vdJ9N8quQ3xsxViF4HOMe4WdHEgnu/p24Arwmc6PK+i/6YET7uS7+mlLJrDnqE5YdpzZnEZ7PhI2E38ris/I/kI+3MAa62A2HNhr2NpgAx/S9OwdgpqXoZ9eiJNg/w41A0nUrRlkbjTVy7ea/R+16377yCvPCH8rht7mO+xll6PPdQ26K0raIx1Mdbz0T3vphPVK7E3Gh/6QHsLaS4OvRM+9zCdBjvIR+NgyxRjX18Mm6lYWUn+vu+KlemUo8hA+D2LThOhF3Pk5ZSHuVXGoWzG+G4WY1wpziUuFe/Dx2Pu+syyeN9ULz8KeXgY8/oQ+vA3KkJbssS7lTIhqw3KBLTNgNG/RPHd/T2IM9IgfIK8VPDHJ38u3pMZ7+xDL8k3hA4aMl8PebaCn8X8HrK4e1CBcs+CrGaB5oPej/myUNjTXM9HC7pM6Jx0g/J7QAnGPYQ6jb935t8bvxVt+wK8LIdtM0W8G7pAvUWMRw7/HrypXdxlGq8uxx70OejUF4QOHSvuMX2o33vh7wQt42EXXRDqUjfSNPlOyM8Noe/BUxI2O7/b6SardBfZpFVibMdi71ivTAEy0T8OzDHoUfGeioOPuRj3KvSZnzXkgGf7qUE+hL644P835agPUA6/q2rc9a3k5YqyHyYL/x6+jtB14tyPgkcNbFf03yjIlm7CmvAPsgIF0t2hw9oC9HMt5WE/M07jZfKzT+z/xXnkFP27PL/kPYGcDNsr+dies69d9gu+b3ncuTpsJOxQgt/pd3C6MXLB84x3JdGASUf3dOirEn5/Cu4RSINudt9unC+eLR+lryyXUzU+4leUYhql12gEvUkaSYS28JM98+3SM6SSVBVJk5V4fiKN/VM8PnFKLNI6ldh2Ld3p6lAi2yOjijkNxCYWdygR7Tkup73KocTQGkAiO56VwAxAFk+GHW5M4MISfwfIUp2cr5NzdDK5xP8YEo6hklCnEtOemFTMg9utkcVrODVbuD86MLXEX2VRotFcni6aJuk00FQioht5KdF0mh7aXlun56rWgyuMxMNKnFWZ8LsAP7AY2AF8A2hofTTkJZo2ACFAET6ebjVwI7AFOMTTitLMJfaqVMWBGIfouwOc4jc3Heh7q8J/zapNPO2KGVwx03jgbsVEimIN0LnOPShEbq8TLZXbfQMFDeTkFouIQEpa8eOKLG2ibHIigAUSUkUMBaqrDcfgobqjPa+g+GAV/7kJ/vpPUkhhlKPnas8ZWPzNk/AzOUh2xniofLTdEYfa5O52e2yxv8qBvTTfT0vUJu+kTkCC7v6WVgMSku8IFAziFck72q1RxQ6k/xo65WtaA8i0BU8m/H6Ap/+6PTaBF/9JwB4t8h0MFJXqjnZHUnFTVZz8LtrzgvwaecgpfwA6APQ5UAie/Kz8PNlEO+9vtzuK16C++5D8PnkV5SL6AfkirNJOeat8GaWKZH8LROn1/C2Qk1dcZZUfki8RSZbJMNBBz5UXBoqdrn3y/Vwe5S/bLRG8fV8GHPHFj8ufYWsUh1SHkSrRaX9cPh+7pfNFTzraLbbiDVWRcge62QG2ONFGRneLp19+LYCCUN/D8hpsapzyAXktxYNuky8PxDs798nfi2Tf8VJQ372QGE7abVHFnVUW+V4uIfI/wfF/itqOtHuHFlOVV74ept71qM0lfwjXh3yiyl/B9RWG6SsMzVcYmq/Qiq8gtCR3IaYLaQrl92ix/A5tAO6GW0GRqwLg4B7hyMwp3iNfKl8CTjj2gXcMoZe1W6J4yy4JxMSKZJfwCV75uPwmFrw3UaZffovPyEX75N+IrmxoT0rlGf4asESCdRfrY4GMF/ExeFxeI18uOLFWcKDtCXgh//IVInOoPTK6eDVGfzK8i/C8EXgF+BpQkGwy+jCZZgAykje1R9mL7fvkqSLz6EBUifNxeRS6Pkpwa1Qg3i3afJrhUOyB1AHFT3AHFTD+FZkoRQsUOifskxsgP+PlcYE5TrR9QgDl8ozj2ocOKy7aJ48TvBgXcHr04EBssnDUByy6XNW0W6N5S2pFQl/AHCWCfcaUlPPa4xKLnZDTYaK3JVzPykMwfEMwNEMwT0rEYBS3O2Ig/XPkYtGjYmoFtgBtgIIxLkbyYoxxMR0SIXZ5MLo7mEKAjLEdTN8AUDXyIKoEbgSeBA4BqghtBSSEF6GGVjw3ABJKLITfgacfaAXWAFuATuAbwEQH5ALUU4DURXiuAdqAg4CCscpHO/IRFyO7qBsmlZNWS5v8w9hqWs1WS6vl1cpqdbVjdbTZX5aVX+w/hz8G8kcOHkNaLYstayxykcVvabLIDovLInWEOgOmYSUg/hhtWMnbjZ83/tAoxwzZoG0wSQeqIlk0HQS+BmQ6wBzwOeBz+NfJByoOVnxdIR9oPNj4daN84L2D7339nnyg4GDB1wWyvzF1WPGQGWwRW81uZIqTFbJKNp4pM+RF8mr5RllxyoVyJWRBaY1YHLEmQi6K8Ec0RciOCFeEtCFiS0RbRGfEKxFqm9apvaId0r7R1CatVVusrdE2aFs0zWkqNFWa/JryTVWN9A6YugXPNkDCDgNTT7gcIqYTz1eEf4Pwt+K5WPj9eDYJlwfPIu4CPOK3URjKeRs53hbpuN+DZxH3Ax5o978hbDGeGwBJ+ps/zV2U6c+UHJmuTIky2TeZ7JXMQ5lSW2ZnptRZNUx6S7TyLbTyLdHKt5DzLVH3WygXLsCD1r4p0r2JdG+KdG8iHXedLKwVz8XC5cezSbg8eBZxl/RmwDPEXpWI3TejGXjeDRwEZFg+t8Oivp0WCZ+Tp5Bux9MvbW7PzseCL20OeKEjQdw6GaCTNEHak1OKZ1Txrf7dwEFAFht/J1DJfaFOaVOglqfdFBipk2ElB6vKsYrypmyiHYBE4/G8W7gK8awUrh0ijb3H34bnIeFajOeWnnwzhIuncwLh/Iq0GZ9NEj+IuAihF/kjJEpIgMkVE22O6ZD2BhbEODukRwI5DpB2nQQ4qYqVZPDfxr4Szz+I593ieYt4niGedn+Ex/Yfj+1PHttDHlsV/zpCJoK/Ec/PxPMcf1Sm7dNM27OZtvsybfdm2vaxD8mNiAx/itv2kdv2d7dtt9u2zW272W2b5rZNcNvGunlROeSCfZ7On+ws8UzzJ7psR1229122F1225122e1y2FpdtmAvJ2T+xptrYHeJ5m3iW7S61OUtt6aW2vRJ4w84M2MmyT5LYmWSTrYG8CmeHbBFEygg0ZoGkBRqrQFIDjRNBUgKNS0FiA403O6sskp3thMHilKLYTjOnkYG8tYiO0Ik5kHcWiBrIK3d2sGAgzwPyY2BeOsgPgXkDQL4LzCsF+ZaTx9i/aJ6EYtg/AvPuQvHsc8rhxbJPsDHbDtoRaKxE6t167ewRqmBZCA7A8uPJfhfIQ+PY1kBeDshDgbxMkAd1cl8gzwlyT2DeQJC7AvNuBrkzMO8wyOZAzrm8vE3YT/FyNmKPzemyQGMqopcEGnkJiwONhSCLAo1lIAsDFftBFgQqDvOsZ7OdDNLN5lGeaOnMwLw8RM8wOoIdrYiehl0rL/m0QCNnST0vpMrG6oyO1LIabvexarZTlOIP5BUhWUUgzwsyUufciMA8H8jQQA54zIYEcu4C5wYbFeTy8XmMZaIZvCBPIG87EjkD83JBBgTm1YGk8pxoVKxRawy2MjxxdCCPp3IE8lzOJ1gEzRMlWsnLNu9ydqPcHys62JSA8wd/h5kFnN/ngOxyftk4y/lFYwesXufnmMbbdzkPIul7FXD6I5zv5h12vjPP7fxzHlL4U50v5A10Pu1d5ezI2edsbxzg3ImGtc2b5dwxT5TwBy+yBZxbczokhtxb5o11bszzOW/zdvA23ITE63gdKOiqvFXOy71rnSsgCssbr3Uuy0t3Ls45y3lODq8o0bkgb6JzPjpyNvLMnXe2c2bezc7WMtHis/L2OyeViT40zBM9Gl0hIkbNm+isRwsQUckj0ILhkMtiZB1Yto/zCNZKTft+5+lDHpOwErM1wFL/QNPjpstMs0yTTdVYc7JNWaYM0wBTnDnG7DBHmSPNVrPZrJkVs2QmM0lxHaFDfh/f4MVp4tdyNYU/FeF2SPwp6fs/iZklbLbaYuUGqWFSddsQX0OHKTSxbaivoc3cdGbzTsZ+08Ia2jpnU8MsV9t3kzwdzDphapvqqWZtMQ3UMLk6CYnbpGs6GE1u7mAhnuOqVP6z43uIsfyrbkjltP6qG1paKGFlZVJlTEV0eX3tSR6txrOu1nfsL8nnO86X3nZrw6Tmtm3pLW3F3BFKb2loy+U/Tb5HOlc6p652j7SQk5bmPWy+dG7dRB7O5te2INlwkQwb6oVIRo2cIJk0jSp4MoRP65WM7URw7c6KCj3ReLaTJ8KkGS8STdUT1fROJF/HakSiGvk6keguvcI8tAMV+jlBMvVcyhMV5qnnimRJPNlOrxclzfPyJDuLvUiw01ssoicci87Ro3+vR/+eR3cwdiy+zKu3Noe8ogavlIM0vv8X/+ZW/zcysfaRK89v5j8p3+qpmwu0tl23cn5S25pZLtfO81cavzXvbZ01ez6nM+e2rfTMrW0731Pr2jmy+STRzTx6pKd2JzXXTW7e2eyfWxsY6R9Z55lZ29I+bu3QJcfVdW1PXUPXnqSwtbywobyucUtOEr2ER4/jdS3hdS3hdY3zjxN1NUysZg1NzTvNVN1SM02n7VKEFbOlNTWjpTrBsbhCTJ3hGUmXpe5ViG2lCF9LW6Snus0G8KiCqoIqHoUpzaOi+H8bYEQlXTY8I3Uv22pEORAc7amm5Ul1C2rxbxn+li9fgT/weNkynddJesRyX52IR4LlcC0Xf0gJN8cyEWrEL6cVx/58Pj0tLfPVNO9sbKxLWlCbCkO+ndvevpZl5PPpFfp8hDrRa2HsJwhjP0JLKHm98aPGbxvlTmHlvwIcElZ+Jyz8V4BDsPIHyJ0Vr1QcqpA7G19pPIS0773y3qH35M6CVwoOFchDjBbwqloYWnjss8K3bAUP9jHRW9Fv3hA0Gg7e6zAblomI5YIx+NPDRVYfCvL1ZPcdcyzTI1eILHrosmMyjAhe/PIVvhP/9FAUDt6D6n/nngj+P6r8ItwPvA1tngesIVKmEKk5RNpiIlMnkfkmIutEHRFpBPOOKKoaeOqn4ThfRwyaEruBKH6xjsRLiJIvJEqZpSNtJVH6hTqcqDcDfneHjkzU7b2VKHvrMeRNPAZfx09jINpQhHoHfUVUcoSo7KZjGPJvovIiomF3EY1MIapI0uHfQlSNNtXO0VGPvenoUURjSo+hcYCO8Ug/4er/y3jmfxcT0/ohMOVX4P7jMSlFx+Sl/x/Dxv8LgM44Pe5XorUf/ehHP/rRj370ox/96Ec/+tGPfvSjH/3oRz/60Y9+9KMf/ehHP/rRj370ox/96Ec/+tGPfvSjH/3oRz/60Y9+9KMf/ehHP/rRj370ox/96Ec/+tGPfvSjH/3oRz9IYmlEappKJJOJqh+R2NOaqUM2+2NJVZ6WyWpSnmaUbNbUpyX5MVZFFpbFplCSz/HdiO4R4xxHRjR2j6BKuB1H8RhUlBGdEZ2FB0tT6KhL7jzqV+lHcimdJH4C9FV5r7qQIimZZvhTJXNMXKlkTk0vJWZVbFGJ0cRMWlRClBTVwS7yJ8fFmVj0ukWJdydKiSmp1nUuhSnJKW8dSPKh4umNXeMc301v7Ebtjq4l0eXlLDqmvJxjUBHz+ZhH9paVDi4pTohHMcd5pg+aH3fGyPpxSWxN8dyklorTGlKkV9nahvKKM84sKzgruJataS4a1nzWIM98/gt/1cFN7HFWQok0xB/9H4mZLAp7il6KGR1pVRriO1i9P4KVOO3MXpX0+xvAmSPTj3R3UWXXkS4WLRozPbZs8OCy0myvx23SPO6elmjzli8wmUxaZLpv+BlzTpty0e+Dm/KL754UbTGboqdVVM+5avmN7/EWFLNF0iqpAmOU4o+U3iFKUVmywisb5zjs+JgKG7tQTUZZhrSqe490Glt0gOeaGvqEPcRKKYLcj9BoLULuYLH+CJelyCJZkiMXXctzHwUb0VTkFg3SG8eofuasurqZM1mpIHV1s8TvHYbelyoxejIN9qcTY5WSHCdJMsmMSRHyDt6oHVK+8lgd5wEfHF70iMoR69SBvksdz6AO5mFSZbBmDXtSXfjDSnU9l4kxocPyo+p8SiAfG+NPtqRqTi3LkptoSkqNd8VnJeVaTGZ2gTm9g1kDMWo2SLtmi0nskK3+LPJnekvJ7xuIR8lgPIaPLPVTE23hnCqIsbudbsnNU0bdaGM2f2x8qS05/9t/8I5/51va2DW9ptmf6PZnZpe6eSFuXoibF7LIzZbwXxlrQULhaOziP8aYGOpsR+JE/ttsSC8osnD6KHK1Jhq5wFHO05pV/lksz5XhzJA0e5QjStIyPVkeSYuItEZaIs2RihafEJcgaclJKUmpSbImMZkpTNbyfLk+SRsQ7Z5FXhMeabGJs1iOikdGVPos5onMnkVJCXD5GFzi1/74I8/4W0tL2BIWZ4qSMJzZ+JSVDhnMJS4xQXVwPxdDLdqRmJBQUjxk8BD50XL3spumzLprZH6Gr6LkleUr9xfVBF9SrN7kob7krJQ4+9CBxcl5mvTgi23nrp8wZ3rtkk33/X3PpvvuuWbfu2zO8OsGuZI8O7u/Dh6adVqRa+gKLivroFBmY1QT6YrHKIr9npWRmT2wyz3DtMgksSqbCDGx/5CHEtgDZGffUzxCEiTJH2U3k2o2RSLQySTWIVv8jqioJvsi+w677MA0S06KekIiMkvPUpKUyA4KbXSYq4TpIxod3dO5PqqMKf+26yj71sem+yB40XHoa0l8RllJMWZidKmX8yA7S7o9ob7R2T0484wxKTGDXCWjY9i/1Pk/br+0Lj8rK6d+jfTkWYUZrszDYg6iR3egR2n0qT/zGukP0u9kOTvyt7JkjbBGMFJTY7YkPJIgJaRJaJM1wpzWwVp3xRQmtkF5dTB3gMWYubhE2ErNHXLmI1Eqi8SEPOJPJdWhSuq7MX+xp7En01haygA7Y08yxpLT97JmtoHELJ++BMpuSeOR7umHqbKyi/8IoD/W7E+wVZr9iVF4JNvxsJUL+QMTaqYZ8ooUQk6RSNBUh6CBtOhKkfZwtFCaXHlOjy6PKYfX8QJXWjQ9I6OMYspKBa+EAHHtqbEM8HBIidx09AO26M7Lz9p8etbgdzecva11zNzg71jWuVV57swE9igbuGHBdZttnR2tD42+6to9wUdjfHWcjxmhD+X14KOPDvidJnuifb5vle+q+KsSbo/9bcLDMQ8m7I2NKEirTJPizKyD/dZvIeL/GQRlRFRZWCuZKUN6ibzSy5RCZnTHFl0q+BoTDyq9vMsfpabYKK5Din3ExZhq3ct+SxEsZdcAnc1QBruj/0K5jlwplyuGaHsiS0wpsA9gA7h6GJCc34vnPvB8CbTEka7pjiPd0eWFySldIyipsjKly+dzdB92HI4pL5zeFVOus4uVVUi9uQV9auIsowx3tq7zxYzDSuBlhUub/aumXj8ra9T762/YffqZKy4O7g8Gfze+vNqXke54+vQx53RKWz0Z5StGTLrgFttDW3+3rOG6svKHLvtr8M3ynMqBVVHmu1dMvfYTMKYEcvl78NNKNtrkT6q0sRLGZFIkk8Wqmm2RpJhttoiIDjbN7yAWhyGIwCprjrAxhfaxo6SSVXL4I81MNUfayOwwS+Z9sgUFm1irP6lQqVQku+JUJCXFTpxFlByla9DDmHXTpzceGSFmXCVMge9GGKtwTPm6gT4FOt9ut+u8iWUl0SXxHhgHGUMyokukKy+65JJgVzB+JlvPQvKCo7cdCL7Cig5IiZCQOqwI7epYcrMm/8AojVmsydYcypGVOGt8anyaPFQbre1W5QiVwShIU9IdeKYrLEWRZb2XbvTSDe3PyO0QC4DlkRiC7dDBvt4V45KflCUkdLczUlI62Ga/1R7rjJVi3420SR3S8+3sVTPtkzRyUzr71p/iNzeZt5hlc0qm49Ub3czNeeBO9ug8OIJV5DCEpAvL8BFMzK7pXbCF+OTzx8l+TDHZj/km8xkq87kqZlxwiZicCqQWKRRjUirGJBUUSTkNxEWKLL6Wruk8k3+Amxfq5oW6eaFuXqjbj2Ruf0yEntbXYiy4FB2TyIcjEfJJS6azpdOXsAw5w6QkctFUPGGpxLqQqMtlZobbxIZKF8/t/ryEtezd9JtgcPODLRVVvuymmSPzndkTlwW3BI+kDlbHBoPrbHdf8fSlX6+tyB/qq3bV5jkiL5zc9i4zxu8Axs9Kt/ojyJKsSpoZwmjtYHf57boIWhnJFhMzm7gGjIxxSU9KEkkOSZIwMLssFrNCkVqH9KLfakmJ3GBipu8ivt3D/otr+o+nc8uCK8ERUHM6qyXOFYlzReJckXpYfThGZ8A6wZCwOaYy9FMzeWIzGFvIlgQ/eWDSMK93lpwTLE9TZvgGTGIP/LCRW8Sj0JMOdTZWsUzYYOP9OXKkEhsdGRdbFznfu8prymJDEqcUX6BcIV2ZvNl2e+Y227bMDvOuuMidmlQzGbaFRY635w1yp0ZmJVFkaQknipP/sjm3ZQaC7NRswljoGapUf5mPKqk0Up4U2ZQ1O3JZ5BWkZkXabMVJmZkUaU/KGuSm+NRIv90hnZ6VhOmtxRSzzEy+TMbbiuOQkGXK7mLbILstkxUrWq/KApot1ViDUjtk8qfHFw161y83yVtkWU4p1c2ld+1FeX6LrTSPp4i60cIsXOItySVc4rnZ1OXj+hAyf6TbJ9quN103vddFDfSti7pUlz0fD+ahMeWmKMeIdVGOZ56BmpzeAnkEYQnHTM4h3mNWsSlsLGvxcQmyBlslPs5QnNLeGxd9/9cX3l19yz1nfvLCU68ueTorc2jemJqzFhQ4bXGuopbC0XOk4IJHV9z/4bM3nnd/7cV3nH3Ngd1rWm82F18y5vK6spmjRt8ZfD4t0XP16LNWD104/SlugVZilHcLCzSHOv2pVjlFzpPljZatlg7L85FKrVlN9KjmRGc224fFBIYH29yenU2csf5Iu0q2xFcp2ZEsJfMFJSY2Jc/zbsSrjHONJeeG9USjbhEbyzfWjS5Hl9CUnEfCVCxO8VpiMrJs3uis1JS0lPQUWcvyuqI8s2iAI3kW81rgckc6Z7GUGDwyrTD9qMf0A9au5UxNxCI0RDO4yhkaEx8nKUxwVLf84h18tlc++Mm61IozijbvX/Tyogv+etn+4Dks15qXVJicU5yWXe0bnZ2W5r3l7d+4kt/749UHL74mGHzgjeCFXdI1i0/fdecZuQm+4Q8Gv1hQy/m3A/uMo/JT2GckUvEeSg51+pNjYku10WSKHB0TYZdHW/KfjGfxyUk9W7cjfNMobGTYsL12HrG9dyFTxNZj5sxaYzciPzVT343M7F56bF/C/88tUtswfhmUyYr8N7sdETGV8xwrHRd41jmu9myz7XaYbrW12ySW6ZHI7fFkWKMi0q2JGUnpiREQb8mcbkmIjk9PAE/JnbDMY3e4PJThyJAyPFJGQbQjLjra4ZE8GVJOlD0uKsourYxiUdaLollGtMOuJHgyoqPA4USP3Z2ZA93G2GGH32GXsdhbrRazPYEl7GWXk4cN9Htc1uQi72LvGu8W7yveQ14ty+F1ef3eJoRs8LZ5TTeeBwYtcUw/kpzS2N01HbbGCAc+lSNSuF3bjfW1R1FMh70mppsZmg00iTumP+Pj5lx5eRI5upijU39O7+0xOUaMMI0YYUxBH8uARMRhBYBxDLMOxkOC7uFbAyE02VAOk4MZ5WkDU88Jjhx9Vh37KJZ9Vl/gruhenDrelaBJaef8+RV2+ZXVvvJUhzkrK2L27cqwH7feletUs7ISHANiYi3V/2J/CRZgbZgQel89A3vITJa+hxJCa9ot1tK0Dp1qBrWB+lvgiEyxpA6ObUy5OuG6lBtTr00zL4xeGLMqelXMtdEPaVttDyQ+l/hiqlVLIG9NQlXamoSrEq9OvTJtt7JvgLXQO995gbbStjL16ti9dtOQqOiYzHSaKqUzGJVx2L5OzXg4OiZKPSddjjon3sJmFEaz6JTFXuaNyTp/DysWBiB2hxa71WmVrI3JyUcaP5ue2q67urAvnP7ddL4Kcd1dXv4l9voObPiJm84Nk1btLDZjRmcmpGm2SG9iltliskhaqteWYM0iLQ2PiKSoLLKkqFlMn8F5fP6y6UsIa7TY0EV7+B5F44MTw3XfkHg+qTNhYMZkckOSB6lnZOd/s3H1XwdVTnvmjjWvr1z6/QN/C+7Y/SJreerGu6cluwpN6sJgXsczN628bc+u4OubFl+74oKFf2D1HU+xaZ0VmYUlfPakYvYswWqdiv13hH9ayhow3sMfDv7w8cfZsfOTzs7anNuRo54dvQCe26I3Jtwfq82OMrnSye02u9Kj3J60gfYoyV2WmkrmmII0e7ozXUqvMBeZWBPW70vzRz6qz/0lI6AEsU8Dcx3kdXglbyPFOeKK4uS4wWApmLzL21gUx4Svq0Wc7lSOgMGtM/YsztgxHp8jJSY2OlbScrJzs/OyZe2YT9IS4hPjk+KT4xUtM8vn8GaxPP7wpOCRHZvGHz6E+bLi3Vnkc4zovX3WlSj3lvC94hBjs+gpywDPoUqjJJPmkbGdTBR752ixn04tGF5ptyTUlBdIM/51y6P7pt305PqRV0x1xKaWPNR84cSqeaOyslzxC+RL5pdmZ1VPCHYcuPEfd85IiVRCP7432Wu1L93Mapl6x0X5TsyQXCLlB4zHIDbO35WgJFskV0lRyeKSDSVbE9+MezPx48TvEy2rrMvjLxl4rXxTnHqtdaO80Xpz/FZ5q1VzxdXF+0uaSlbJqlW2WqUSbhreotxhuV/5g+XBODWSkWlCZOSL5nSTy5We5Hb7Jgwa9H5+uk+bwNiLarqW4UrPdXuYRpEmG8U74qX4BF9cfIKcaEpMaI8ZmDQoJ5cNjIxMypWSzJrJbhpvkirxuNG0w3TAdNCk2fn+3lRcssP3pE8q9FX6xvtm+Bb5Vvtu9N3tM/uucCQsTtiQICek+EtYCdltTptkq8hwJRcb4iGEw5hc05fwPemSpYV8e6GvmI6urhGGBsRuVRgXPky8L8nRbZCwV3aohpLzLZmOP1rCovmAlkR7Bkoe/TyAe2Vd04mBFiskhprPPbikgalrlzu83sjGeTNjS4dNeOKj4qyRP55bMDwzJSpCtaZ6qwuURd70Ba1DNyvB7rfuvat72PJbSoKXLy52tT0SnJAVH+VOmidfMi3eA6ELLrp5zQD+vzdOCh3SMtVzqYSd60+wOtRMOSsq90LnNc4rM6/MuiH3mjyrx9CDkX30Yh7XizVwzDfNj7gg4oLMPfITSoe2O3O3d3eetdZTn+vPW5d7dZ66yXtb3kPafaatEX/KejHXNCYqiZvGi5PYgOfTk6a5+fbfH4eQ1Yks+vn0RLenpJdqdNPUood9A5zM4bQlJiW51TKfbCtzWyjaES1FV7ABKWU8vyXSUVoWk5NcWvYYm4Sl93x2CDrTN+5IY03zLrvFaZEsfC7vtAhl6ftuBPbRurVYXjgCJiEDyBHWm/yYRj+qIT7J6/gkL3blafYIjERWdiYmuCkr0mPJoqgMRzVzOe0OLQ8+a7Yti+wuWzWZc4UuxVTmC6bQqGIuLxGTmY++x5sJhSqF9Wl4XkOvQslG8/0QH/UyB2UY+/Qh6lVZNcEjd2/88+Rp+28YdPbghLpBHunmhuEOy+XBT277Y+jpIfUM6nTuhPw/xaQVxUHZup95aXvw5XueDr69Pj6OpTQVerOyVGdm7Jjgx8OGL9i+cP12VswedJgbcsv5WYiPSIvDXK9hlf6YGjesDpfDlW52u5P4bi6J8zlqSFolJTmStiTJfMZ2SH/b7S52pee53cN4dCzSDfMjjX2Yc9iOYXK1K30Y0uxym3gJpp4STA7TFpPMXOkmXoIn2sWHPTdcQq4oIdeZuyNX9kADII3/LE+JK73c7XFn5NSQnZzYlMhkysvNTUpKlIaVl5vNJrOHqh3VUnVFsb2E4d8MzOlLqa61TvLXNdVtqWurU+pc+ileRTQ5GP41OZjj0tqRK421YKmxGExf8l3YQ2GThz9jyjH7u0cI2fAZz15OMckdfJ73PuEb0lttx3OTlw9wxgkhfXNIRYlQ4d3D9Qku1PmfuNsewQ4m1JbnS8/kj/DAx93dI3S3dH1wGtfq4cl+zB1cw9Yc8x298phb/FcTXM9LX2HsnbTeX5DBB8DqSpfc7hRXeozbnepKZ25PhCs92u2JiZYkZk6xpzpTpdSKCCsftaR6T+UhKyuy+q2LrZ1WZQYekjXZlcEjU1PTSw9lsMUZnf9PX98CJ0dR519VPY+e6enpnp6e90z3zPS8e2dmH7OPyb46JNkQ8lqUhCSwJLxCSIDsRggSxI2SGEUlQSUBo0dEHoKeCUkIG8AjnoDin/uz56ngO3/NKaJBPCPqJTv7/1X37BLQu+QzVdW91TXdVb/H9/eonhRpTRmptantqUNw4DDnHSZaN+den5nvQcov1E0KKvF/nkA6XeTNfzQ9MG3ZfzAD5jPDk1aAyofhSXvxlcdRP9if6UKtn97nF0RfjcVunqtyvYvwIv4mfivahT6PP88f6J/A3/BM8E/1Huo/h6QD/dhZCVV68QD//uolvRvxdRUWeXt7BUHorVSqZQEQBg86TUkmgul0OZnIXZ7u7u1JdDsw6DRgqcDlmppMZNOa0IW7qp2Jrm9XcbXyQi+uFIReGUbByHIzlr08QHwe9fLUUIZJ7qU32kMbVcA3PEZsvzjbDHZ35bIkGHA6WEfU6Mf9ZUFURSIOqAcUrET6+p8ll4Bs7McRSzaOzai3X8Os9/XRzwyc13V219KKPuIFNG/bBWjebI00cTwQ+3kQvnkwIrJ9bJ9T9DYVHRV1mML3DhPLA3jHTnPV/sGiNte0aRdaUpBZgX927aLO3qmBefnLGy+1h+cvnrrkvNX+8gJYbA/+y/V68FLiW3jxZ5gFU1/9cDmZzTqUYOlmvKvUuHtj7T2UIHtTkesaa/C+FR25IMeAOCxuBZrIAerkgSayaLfRdRW+Fd+ujeZte7Q9mUcyzDuMsCRtsQBIbiamZRDKitnR7Pbsgaw9O4GPG2IyVSDAH5glbPZ76It4ghw0gu+wSiTXmjfyB/JM/ypK+U18cebMFCAK6j/qOzPSR12WdP71pknE/G8sEDJhA6gGvuPskvPm5j96TU4Ia5F1Yzfsub6Kf9rI/AOOOLCh7nUteegAzEACLKF9MAMpPHZYkoBv/3qYr9PKuNVTF+NxQYwnEgI/h6qDGBXXaTIn4UxT0R1c0rRPE8lESoyHsJBIDFiOtUQsjXyCF+NEKAUS2olIKMgKLkxtVx6v5TF/x7CGNdFXiKMYHo5hFNsMU3VHuimSx0aoFKYS+W2rRf1sM7K46duFwnTz2O54HsHJ8AyF6vouse+O53eJz2M6mTRghqYPGbq/Ewmi0I22JEdT25PbU/egPcKe5J7UUXQ0xduStlTJlufS/lLUIU5MX3bY3wnVI6CX6G/wiTIWxT34QPyQeCjOIkrhQOb0h5+eFFk5NijSn2lzSeFBxHr9g2hi+q3mkSAPChPTvzkCfaD+8WFvaBBbv6BEf+EIU7DnhJX1koBPo8qjo8k0vjyggE7cIF/UWsfwiZW9qfS5TZsWJBvq6KqEfsGAfcm5p8jCbfocAoautnzd2X226889eMv7slm85gbmG5muNMnSH4iF1X0L7FweKfirRscGcYP/Pver0quR16KvxV9N/EZyOcNOJUTCnlA0FM+LeX9eLkTdCjW3QrQINEGfcJ5RTGuWgsBrKCqkvTAtpH14L7nfcT+717OPf4Q84vm2/duuFxOv4ld5nticrMPlcIdwiIQ8IT6YcK2PrI9/0H6rZ2tka2KfcCx8LPFq7C2WW+n1diIm2Ol0SVxEvWmVSQ4A5owIiolAIksNBjPRanIwSZKCpEpEAnxHkfoYxXmG8K4OEg3O0z+dnomFUlh3MYV1fVgRs4mcnHNl7blINBwlDoGXsjBPsSwOsNAKOaDl83izmI8TKLHfHcyiqA0KXe+D/7MuLzCaMSB76gQ+yjqkun1i+ozBSXUSluoe+JCJ6dcP++oApH8PlZ0e8XUXHD3B19HML0mtxjMtIC2cAZzrJKlkPucTkR343idapp7UKZIcEwJL7d5932l8tvGZ7/wT3o97nr5y+bYV91+3YNVV1+y3r/U0bmp8r9F4vnHur89jHlfwZ5f8yxcaP208/MjN7QaO/BLOcTdRq7sGVt7DwP1RlEGvHEdJ4H5PPUm5/3KuvjyH94XfDr2d/FvaVmLjCHsAt6XTgN4caY2n+ECLVSRUiccdfgkMUlZM4dTP1wW3Bx8A0+quag7nYhbsKvPII3rIsGedh3juyOaexcTUR05LH43M4i9qa4+YUMAytGbAlemZVFRNjoZDkRBxaHKqitUoFOlApoqTIaVKvZAwhU3TmR7MKJ3ZWGtnKmk6dh2MrxkA00gxtuDyqeVXzIvF5o+Q5TjTeGjPlb9J+bbt2HEnWd/4+E31dDar9dzEjNLW5Bd2PJsOk/umjpF77tv3KTqDVIP8CGZQQ2X8QWNwRXRL9L4Aw2phbXF0YXxh+sr41WmnhOzIIdpFh621el3s1tit6Y9rL8f+jzZZZe8Pfj/6t/DZyNmovcp6JsgPjppzbDboNEPDqNOpjscsBihraVnT0uPaJzWioVI8FduePpU+k2bE9HB6Ms1MpnE6VIqntVy2EpvAvzRCGgCfTLnih0VKfi+VSqdBybIA0bAdzCdUEkuk9PPQBEOMoCeTzaJmwLvs8QxTOV3pP27iBvHtERrpMGWwOHV6RJyawcjUMQJH1KI6PdXXDOKObRmpUzdhfYQKaRNFhE3lBguZzLfI0UA2kitkW+RSFeejUOjBchUXw7kqisZmnSFNb7IVRi4AWXKeus566vGwPzCALSFKQ57/YKmbYWJn05GIGZ8ZdKdrnoS1nrqoueZb3z6154YFH8JDRqzY1VjRWLy6/sm7lt/zJbKxsePdqz//qdv3XjWgNjpXB1UmSzaS+6e+3rFz0/7PURy9cfqkLQWSto7LRj3cemnx1hTj8GKX4NQdrWEhpJcFXSz6qumknmnpKnXp1xU/UfxE6bHaROnpmr8+a/kuMgJojdCldpGux9oSCWVNMqEmVaxOAHUNKWtQVIyS6GOBoi6wOYEThDgXF2xbha3F/cLD3JPc84JDLwqcTbN3tjFaZ8C1HM/8aLYdX2o5uiawaHilaK/B8bVegVUBtMCpo2pbJTJnAtefaMrcU6dHqMUMDHnKMpkBnoyMmSEWajLTcHTT2UjbZrMZb0oyHCOQbDGnb+SuF7ZxtwkfK+7U7xW+xj3DfZf7rsCjkTEz+DIGOMdveRhN56/5PyDbzIQR6nbUfB0zsepcvkLOi153M//KFRO/2rH+1kDCqD7+5vvf1/jLy8aWla1qdI6UzbacvWd0Z8eGHccfvPTNJy8YqO6KRRUeLOO+x1+5cWFZq1ZSl9yyYcPHHv9zNCMXigS99qttF7euuXjuZdv/ae2Dp0TP3GQ/XdWLgLs9wN1J9LXjKA1QPBytpSkU7xWlWjJtAMudSNtaoUHwz5zOc7CG4WRCTKddyYSQ1tSfRaPnlITqjBZQkogCi0YxXeSSkWYt58RARAzjZHg4vCfMhJOiipPqsDqu7lFt6tO4hMLk60dSVAmKb1OXJRioNMx9pmkuTfXNeJtm3E1gPJnwu+k60v7OuDQhpOazezLJZfNza68NzZtTnppj2U9XfWLg0lDOvqRxz/jmlHT2jXcAoy045+K9eDOdEWn6/zmpvdhGHMaR3a6/Fsmi8PWRx8IT4e9Efhv5bdFZD2NnSwgQdRda3r62fbhjE6CgdrGD+gNHO7Z37Ok40HGow/Wv+JX2X6E/oel2+wdcH4jcXNjpujNyAD0aOIS+hVzhSBHlC9WOOlqUHGrbgrZgFxJj4uB2hF2RiNPlckci4WiU5VAMZPB/2nACWY6hkJTwJQuASMHIFz1CQlSjMP9tpdZEm2Er2hA3Mb3jSJhzg4673bi+yDqTUSvZgC0XC3KxWPAgTuQIx5XDITkcDrncLtZdCEegHXE4nYViCTqVQh7ObRML0YiLdTrCjhUlXCqWinAcDnkA5XBtSRXUNuHcrNPVEQpF0Vw3/gYQUZH0IQMWdRDa4vSJY2B4ipH2jgly3ZHU7hvDE7hlJ9WHejSydCoanopGpsLLFlw7/x0zzTLUpPqWOjAjDSiDrUZFq32XFXixGl4agXmnBWDTFMr16LvCMf8gKGMd/HlkFzXpKITuwyO6xdRPlZIuvpYsYAC/q2n0emRkbAxtAahieq6cWKNpdbmmEQfcjP3AsKZjy4ychvx+U/rmO51v5mqyo964NN841Lg727hgfpdBliystmH3D3oq7XMHyT0LlEC4/JefaWLPcvuSLJPJenaf/RKz8dxe2/sfHXJksySfyN0+dRMhe7YuBwmN3c5UILR16sNkwZoL4sUqQF+a4QO82w6UKuOQwWFRUgZRDjB5kLJvq6TUAAPZEZfAPs5+0P6Y41HxjMu2zrHVscu+03Gv/V7Ho/bHxGP2Q45nxWd8M5F0geZYsX5HMMCJLrGGKdDnoMEVEMEFN2vKScp/Zgi9w8lKfs7vAbOQQxgTX3wQl92c7IZRCOfZHATdFDIzO9axo+x21s5eiibIT42ozB5iMetGnoIsI0JgYHmt/053JBB8mno5ce2JGy0np5nsQUMTVNFWsdg4M2ISzBiiinmXHchD32WGwc3kKvEl8aVm1Nt0YeEUiAXT+4xBMHiJU/PbfnnuV5g8tejWr6Xy3WdzjPLSLdmh2y8OgcW85CwKZb75YCNoJ05+6far8KeB9y6f/hPzc+ZbqA31kcuNgEMU67akWG83+ubXPtn5Wef+TmaAzviVizuP1fGHnY+Uv9b3VPnF8mupV8uvdf667Op0LnBe5L8otKhzVWg9ey/a3/kwPoaPsZ4OJ94+cL/t8+UvtNnQwPDA1cF1A1tCewMH8cNznsMnB9xscHjg5l7mQpYEpACxPCSh+h96cXsHC4yptxT0lqzeUuzr+GrHMx2MraO/Y2nHHR2f7nig4587vtHxfzt+1nG6gxvtwB29YM68bOxzucgKmU2x17K3sDbC9rJL2G3sJ9gH2EfY77A/Yl0cG4N1YmSJZcJ8TtVh7OL6au+FpH0fGqlWSdgo6jUhrIbXhjeHHwgfDD8Xdv4i/PvwOZDtYcMr1sJEdRJOaFFbqi2DLbaW+cV5QlbNkuwbCGQe/fqqa9A17nrOZUtCRZBLBB0xgZ8xRGNg+wAxBtYNkIGvBHCAJlAYheHC4HQMx3TULXaT7na7oWVrm8HgI612wz5sX2e32SP9PStAsLTttDyd+tLTY2fG9G+OgCo5MzKyRaeY7m2q4Aelul6FDkA4Z2iIY+rMKdFS+Vt0M/4xk9osvsSKfd6+PhAEeIvFF0c94USYIJpRQT3mPXPimltkbAIYWKksl6vnvIpPQZ6kS8FpbQ7TrSAxzivYnYaix9arNDMITIe5hfo+8hEMssWUL2M6oums2WY8PttppU2aiGE2Sm/lD1newfbukJl5kPc5rF4d7WTRVz8+vHECd4aMwtxSNJ5b1Du4YsvLN+3cH/K6ZT4aU9o3zR9e476tN5+KlNvv2nf98k1fvfuKjd3FhBQOqHqhbcGSjgvvHBq7oLSvca+RErPhi+YtvhfXF17c1V3RYtQS0KdP2WIgb0Iojy82BGmIRSExRHA44suooQn8phHTcjsYp5LjOO8WQRC5EEIiTeVyRiXqAz+8uNN0hff09teGi5NF0lo0isPF0eKB4qHiiaKz6PUiIaJGSKTkkwwRt4qGOCyeECfBsogUlo2ZGN1K6gLlciSSom4JgCxJsz4cUmky0moKIOpVEZZeN7sWra7FZtfieV3fbtpi4ilTrDFesQ8UQjNLJGvj7dlMjuaIEIcrl8xmbek8TngiCuK9qhvamiOXx1FeUVCKVfLvWmMzzgxGvXaHfdQ1mhzP7GUftT/CPmVjP8rudJFx27h7XB3P7rXvyzhM03o19jUzP62lBQxTs1wkPtkChM1kHXxw66fWPb5u28t3Ltla3592uvUOvMPhXtLbsaitK3/BSvuSqaltY5Mfv/+/72ztutb28MX+eIxkpx5qrBvXehfN+drJV4fnUO2xbPoUsxbkmob+aNz4ZwfOuPBq1yPKC+QF7TX8Bv4lcbpZ3EJK8qXqetd16lbXVvcWZZ//a/6vyRPkafmY8rT2gvJK1odwwI8Yb3wSnQQamcQnMbFhGeyrlD8QjoTf8mHf78I5zpm60MYJXuzVMV2I9sggNl3oLl9NwPgAPgRXRA9m/wCyQoircRJvdzb70fpYQa9NOrHTioh5a85IpuduK+FnhCaNmw4S4G196aktpoo6PSb2mRkaI2P1MVNbNZNtaX7eWNbkH0LztTtmMmFm03asmHMXY6gXvLD5mZPrb3/tnq8u6Old6nKEQmprunbJou7Fbav+GP7QbTj64nP3HPzMmvr8ZdcMRiIdSx/Y8cdevUJ5ZTnwygLgFQWs5m2Gdh//Ff44/1TQJkndLFJEhYTUsosNP6gqL2iCE4SmE/jnKH7QoULjsqdYfYfHw3JgFK01IqHbUjnZCUMhC8mVaJyKhEvmBHphhgS8HJNDALyjVYvLaHUEmKxqRiBhvoark1UyWj1QJVU1h3MG5RcjQC+d4bJJ0SZGKj0fCc+KUTqnwEP629bRaQuTgxFE1bFoZn2PWCwzyzSFdIn3Z7JaljiknJUj4M2m/bk8KvFQZH2pPM4Ler7pygAuKZlcUh3lR/2j6dHSoeqJqmPUOy5tDY1ro8Xbyx8L3VW+j98X3N/ySPCrLU+3eLcLn/ARuoojq03urlrcXW1yd7XJ3djcHjBiMU8oYO+kC56fFaQmb2mdfitFv7nk3cz3HGy5p3HLws1DRzZcsuHJDfM29Lo8rRfsumhTNpyt1sqhwqplgBJevlFOJW2ppZ9bOXDgo9/Y94dttbk4uimYiJemPna3rH7hS088nvPfZVEBMwI8FkBJ3GmsckiL5RF5s7whcG34NtmZdT9KXiQv+f6d/DvzGv9a4E/MX3n3eMBKfV3JrGc2p29lxtN3Mh/zvsG/HnCV2OkgZl0unZJBkmXYEXsyiPBQcAIXjsZyfqd9AitHPJzLRIAcrG7QiKRrwesR5SC62MD2Zl6it4bM4JavE0Wr6cH02vQf0rZ0smg5RtrFJueZtSJZda61ZlKNB8hpEmyPSKrJgaZHmibcUR7UdUosum6lnZ2Zot6TMyOnAJiNWS4un5LIWi6uuKQqKCoHFaz4YgoOBaBouriou1GnizyGUxY3WhqPLqAE6+eszTBrgBmZmnatWXBl31U96SUTt01uWjn1+N3//qaWDWi1VC/+89M3vH/epcH9HznwkefewIHfPvilD6pSx+r9GkzFEEK2HuDRFvyL48gx/dZhrl6hT1hd3FmzDxEyXJmsEKfd7gg6cg4bGPVp1KLyYlpscUgHvc95SQwjf0b1TpCfGL50PqOmtbQro/KaFs+oqQnyY+NqrZBRWzQNx+BSBGamM51Keb28m1Vd2FWS/UZq7qDfWLCw5jf6O/3GPPjU58BBaxsU+QIUehmKdAYKRYUCDKpX/Fjw46T/FT8R/dhPwZN0ooLVyqEKqVZGK6RiDHTSBzkCQ5k1jGbWMKBZw0hm3VIxa8ML9FZBltgtFfLmKbixt/K4mj+Rn8wz9NSR7jk1s662WTXclNnVlUjV8pHyMkt0UGIAgWH6O8Um2gFsBVBsbIv+zr++8yLQIFBAVJunGRpkw5Y3LEV3G3CDZqTVJXsGvTTHxjzyB3k4CnmhiAhQgN3spXyfkgffcTaPUHG/healIDBXmtsqaGSJxh9mgk5ph9M3k5RnnQOF+62l2xesuqNY6G/k2iOSpMcKS1oEf28j1xvx5QdAv/7q4nnX7DrQ+OymTmcm40xFr8Vfurk31b2gwV0TSbOZjCMZ3MQc21hjaYSiBOpAs9+AOBRHPzGCynZfaFDwIQnFwYSWxLgjlFElKvzTfEb10YYWzqjxZ/CboJod8LS+WlftoAM7DIQ9cYfkc7voHMThrIWeDabo8VgZPqVwyIDhzX0nczrNbShJzdo+5Q+ZtVEtt9YOhfDuEDbBW+h2QxlWiKqsUw4ohxRbVRlUdkPjhHJScSSWnQDIBQv39sjI2OyyAXoG5qSwafB0M0Xg79JL3z3PMKe5uWsuM4w1a16uzGs4BxS5coH9BvOEYVzW6J2KXd1ty2RIOnQ1SUMT5m36240hPGXfBPPWhn5GAd9bhhIM11AHXt+6vu3m1pvb7pI/2vrRtkOth9pOdJzs4DpM6eT11VCb2EZaMmobhaReLfxGep8ktHkcdBKhz1Mwc3QyAxMMY8jU55yMtcaM2HBsXWw0tj3mik0wziM5XTfnOfSP5vmNFp3+zQcWDxDXc/pJHdCxqBP9WfJD1E7+EzssfzJlhJkZE081xugWQH2ETtzp5qyNjKH3zpzTcf7exRmA38wR9XWb03mNsXrN4OCa1S/7Ivduu/2W/mKuBRNRjIRSDjdmsL7Avmn1IJ3dwdWNvnM9H597xZZrFrbMLZc9YoDVfL5CVu7fHDpNLqgNVp1ZkIXzQRZ+DGRhBf/emEeSgfpx8g3va+S35L95e8IV5XLxdDqtdcdX8NfwH+Bv9W3nPx27h98r7BUfix7mjwqvia+LMhEY0RWNSgXJ3nQnpLBSKsrF1ipWEoIty5bVCuJA+DnkUDqrZoIZNxUCUy+88MLg1Auz2fn1enWqL2Z8AGVQRcyIlVbNDtZEIhFXFC/GBErV7eVUdzAeUoPFjJoBuUtg1IAoqwE1o2qaVsqoFU1j7N8koNBOwFVDileGC0VBuCIRl2EsgVcScVHwEsy2qqiC3C4HGC2gQQ8nLlNAoxrhTEYLBty/aP1DKxlvxa2hUDQw341/DObq6JGiG7sn8MHD3i3i0xisFqwYwfiwlYx5q6KoNJEHeL9UMg0eEURkFSycyeLJoq0YqbY+ixmUQsvwqWZ8SLc21PUBzZyaOnXmzMjUr8UzlqNjxPSNRZaKZ86Ep05RtjPhLLvLcoydl8OAfPXwu/MXzmubOQym26vP3BiAYWCwb33dTaeVmZng9DeTkk1yY5yB97LxN380L9Vi4C/3rblz/U93ArhoJJKJ0vG+wkAj0ZSO53b8YO6cWCzDZrNMx/ZrGv/yfDgNvB32hgaw0PsVU2aeJyCt7CySAdoTEYg1icrHdRI+JGHBjhxIVO2iQxQdXEZ1mFISZ1S7KSU9GVWkHB7U4EqH3d0M65Q8HJWBnCUDaXWkXKtxTVlIa0MDYXiIw7s5bPlCb1elA9IhialKg9Ju6YR0UrJLtH9brUbrY+VKzWeKQqrK3iULZ5J6LG0zZobk3j1jR94ReEvObp0Vc8x3rqJirolCugEj6sRGUcjrT3J1lwOXTTt5cedwGdsBgWQdzI/ID5kfRJmAoxOwCfND/IsYkQQvUJGuesWUqB8UnhNYHIvLGVWwEEkOUIiWdgNCMRFJkiKSgAY4Rde0VDIpCF53ZL2dsTljYGYcmaSO++knjZXhTnwbAHSH28QogYBMQYoM+ESQcVJ+RSYyBSwygBWZghXZ6OyCAjCGTMGKTGGLTBGLTBGLTBGLKGOZwhRBLR8qk2p5tEzKgFHKTYxi1jBIuYlVyk1sUm5ilnITs5hzIgBWKceb7JXP52bBSg5Xcydykzkm1wQruSZYyVkgJVPLRVreASkmRhHPAyk69Re9A1DoIvc1N7nCBWMAUvpOW4Dl75BK0kIqyRmkIlCkkpxBKgJFKgJFKgJFKsJ7kUpbK9pCkS4YWwgoKHCeq+fdWMX5d8T1rQt3LLnsg7KYH2jkO0OipEdXXpTvbORNVmQevW3ZwmsX1x9sfO4GE6hkI1fjAx/oS93e4K7vsbjwBiJaXGjtD38K6JBHKXyJEf52FOc9WLqU9eZ4jJyhnNPFcgnDNmNz2oycXhNs2BbVLJvTrBZa1aBZHan312htZMB6P6FNagRphrZOo027oT2gEc1KcDAmOcw1bQ6zhqFpfQxMDS6Spnm6R/OdPWNUVlqLZ1mnTURJd46bW+D7zEUy7Yz5OCVmSVZVkgpxyP6AnzgcuVg8Go/EGZoHkYenTCg46JIUFHYm8jQPIo8VxqtgvzukoLg9lD9vx7deog4dsFTbCriOF+FF4m0e+6hj3DMujka2O3Z7dovbI98hL6rucSfYssJ4eLdzO79d2B1mafh2bDVNeWgGbE3HgxRKm0GD5tbwLrqeOdzY9r0br9326n+c+u0rHYtCXu7CSlnJ83IuG2W+9eHX7/r2xx7EhW+9hPWFS3/13U0jCy+KpPvX4tTj44kAXcF84yIbdAQzpYpvNiJSlRUcyIl8qkN0ij6HvwpGDwhN6mkwOCpHHS9oTcxpxLTyjpDTJwG+dGRzKudwesUiLhqxqNRmrW9b06fQZoYXgAuH2ybbSGub0TbcNtpma5Oa4peXDA9u9RieYc8Jz6TH7om0vstz57EMdk/TYPe8x3PXdDqYXdusrm3Nrm3ne+6aaRSnLRxKGfJdnohkriWsRLJ6LpHLZ1vCxTzOKVCUouU8LsSzsx4Ivemn680YgwtrGi3Gw+PKeG68xXazPB4ZTXxIG82P6zvlT2l75X3h+5X70/szj8iPpR/PHJOfzUjzA9j0RtBIfXYmSj/LoalAV/d5bj1AcMGZ/T7Az/hgqHVo6nemdsAfb+tYtPK6x1Zd9s8bl85r7155VZdWq+eMa+eubTx0YS2czZJUaB3zE4qOb78wWf3of+64+3e3p6MPbatf8vv/Wt37GapLFiPE3AQUUMR5w83luDone0SLpUAgczRPK6bW9KZug3r7YbXTPEwo1mlBNGsjLwdroo73cnt0wkV4X01IIAUV1YSoiEUHDgRDIZR+UFVMlRx6UU2YKlnLqEVKTQnN3S4YSh9IvHj3oHAdVTKo6FASbmEEuZ/Ga5ENr31qj3PSedLJAD0+bXCoKIRUsEdKWtqit7SpDWpmNPpILGlFpWUpWDuRxqMzW45/XFq2YgZpWyoZsNPp0+Ipyz7po/tZKHE4TeKgtAFwu6m/zRS1pridcQE2ExADIcttZGWqNf2wL418am7PvLmVzmVON5+IFgNJ7PRUexrOfp1151qZR79/z9oFg/Mumm9zBNODV97yak9djEWYTMZe30bsw8F41J41d2adIt+HNWonjxuXc60BcdAm8kVZTBRtDjkov5h9Mfcj8Q3xb6KzKGZLPWJXaRd3r3Zv5jHuy9oEd1Tj7B47zxYDnoXcYo/D4AwPkdpVtJ+oGFO9gw1OGnzAjMIvMPxov1SFE7Xqn/SwGtkfU6NRKlihy54ojk7gTYYS2R/8kyTZc7pTUnIS1+RjQwrU8GU0X/LkUZfsWEEbhtslkxVWSqTpYuKEmnWUpva3MQfkt+rF3qhQw9Xa8tra2ubaeO1gzVGT2CQdhJZkhZWRYcDFVisdLRZmPFQFXJjx8RYiHVTkU4k/ptNkDaioXHiSTYIaZU3jDC5hDTk1yPYFNCiCWTiEZ2tqVqoi3t5CnZczl6aSlg/hpOGCMVJXwPX0SY7AEGYNo5g1DETrw7Nj6atP6ebO8Ag2CmGY5LgPCjEGBc14NPhgcwM5WHj0ixRFEQaVielfHvHIVg09aE0TJM2OZr/jyA6QS4K+dgU62hXoZZdnutBtGyP6TA6K+SoKoWq4fYNVwyVAYe1aX007Wb3oN2fLcGvA6pNHrBoeFaBHtgwgBI7+w3BBI1sGXJKdmP7jERCnUJ96ikriOMja2Ww9eJIxazv7CMi285NYbLPCDLhFY2bzV6w3oHTNbJojnxPS/XfOLc6Rkzg3suzulfNGFS4VTInp8heHWvv7NtxfvuDeTy9ZGPNJwTDzzcY3797QnYlFit/+5Mple4dLXDse3rGjt9Q6tHBjz/uuvuFgVhA0KuNy038ie21TKILuM7y7ud0eYhacB0Um8DFYH5ssM4E7CXYkuVbO4Bhui+taL0dfReA1EnbumCcawzYbEuyqndhL/mDgNln2GzD7fstUS9eq/hP+ST/jj0SpdLFcnAAWz/RZNlqf+codOESDU6dGaJaK6eXsw2aIYczcARXQZiN1pmDp9Gnmts+Jn/9cyIlz5ygXH1t9u8+97cNPXGCbajx+9dRzF1cTVwdPXN2f3ov/pq1+/jb6rL7pU/bDzKOohVx6VEI+3EITFx+V5BpikI0LciERiYxoc1blarAaGpQHg4Oh5fLy4PLQKvsqaaVyo329+xpug7QpuCl0jbJe3Spuk+4Ifij0AeW25Afzuyv36a85Xke/9r7R8lf0Z/efube9Z1tyDreDc3htot1nU4zKcGVdxQVGtyT5/H7kFsHsBsWqhm15nNcLat7yjdjA+g75k3Bn/qAayiWzas6YmN56xMcQwMEfMK5XUUtSb2kZUpOyqib9yIUcKkFXqAocKjbGxWDmCmsHMMgeRIZ8ErQl0cYQm6tF8UsYOXxcEv8ueTZJknpe1ZMqnPWJNuxuyefCIbDeWxiCuArVaS2W+7O7x3JzJlOWmzMcidYqBnV+wzORgxVcAZM+f0tSncDlY8Y636iP+J7BZZRErpnoiGvcNe1iWl2Ga9jFuCLlygRZeST1zRWzmSygS2guy8j5ySwjFix9T04LdWuMjEHLZ2W1zCazjHn/vkUbuv6udBb93fks70prsex606infnNTsCjTJ49wpu9+pv4riI56kJXrIfhY4Ge1HTMak7O2JqStLJb8eTkufv953gHMnJpGZ2t/68lHOvBrrZnkJ3a4lXIV/6JHSez4YDTXjQOVLr3x33Hy9an3kUf2V5PebDYu+VY0PotvDC8uUo9AJBRcDIfDF0bzGVs26+j80FTEpPTGEHMaKL2Kr37S9LF5qOT6uj8wQIOJF6GL+Aujq6NrYqsqG6MbYxsqn4hNxL4T8xb8BbkH9USH0BB/neM653We+6pfQV+JvhrhYVS+ynuqXofHqToCkaAaEO3Yjm0qCFi/KpcC+UJG91arQ9GIHI1GPDwfBunLX0ET/HkvwjhVjUa8vAc5A/kqytAm2OLRzBv6HkXIvKEEZBCDdkcUcevaTra91caYyJiXC7U2oCshUA2QwARmjJC9WEzma/n5eSb/UkpH9kmQO5HWNvibSUomGZkvHALxYhKQvgXNbAZYKtJgJnX4+Jqv2JDqu9iKblKIuRvda72BxCSw/2U3OitaFEIJZMSO/8c1Jue9OS3U3H3bjf+r8b35cyv4j22F9gM39rYN4HplzvzGn69tW7Dh/dctrLX3Y8yyQjhW6MqRJ794Iaw7SYdzo43P4Ni+3mwLyWbt/U9MLW6c67tk7bw5S4x5OY5LlPbSlQfThbkBVl4iJVDdiAkyv2XOMjZ+Yvp1w6Vla0wymqBZ+a8fUZK0fstYHInV5pCLyAZmnLnFcxf5FLOXP0tjZ4uZIc98/jJmpecZ5ruMk4hw+S2e/yKkylZdSV9SWul51fMbz188LEdsnhiRPbamn7LgIbLfTaLkDnIXeZLYCY/tnoDnFs9Oz9NgwjBuZsjt4Iewu/kSEXNT7IjJ07HjSIJ1r/vcg6zLJ/ngETy8dA1/M7+D/xz/EH+Uf5E/xb/Nu/grrPe4EczwyOWROeLFzBDnmmByBs+5kSSCKSxht0OiZwr8ECLHEHbLBggiJGOZSi4ZKItjj7lc7sswcwtXlHQzkCfKdWSA8B1E04hsplE+kj3M32IGiN1mgBjjiF+ewItnKQ4g1RSVXvqZM1SViaeXiSbpjVG5Bdrt9Jt0K/CI2PemmXv1zhtGdlX0LaaA4UGwUIahAga0J9RPU0FjHosBqxbM8ycPe/2z78y543k6Fk3UQqbvg0J3vGXMCiSYAzNwIeAbj7nJxBcfJGEoYJg3n6DbhwCM6KtTqU6s0TRvzZcKYGuD8bxz3yNk75WX1OIa428Q48TjejzIXKItvRqLsXNHb/oMmp62vBz2bSSH3gfE50SfRo8jhDRDJlmaLZzM4t1ZnEXoQp1g7Tj5PCrS74Nb7KS0esqmkp32baB95xgeRF3LjOWJmsBDR8dhkqEx/yjzQRKxTeDpJ6628ozPTKHqiBkPMxkQY81PdjaeXHmzfVvjYXwZ3JW5i8UZg7tqt+4Kv4S/hJKGmwjptendaSYNy3zuKL2p5HFyf/OmOuGOvmVrx7+1r0IcSh510JfBTeDDh10Rz9P4IXw3sr5/Cg0ufe+bB1f2r1zZDx/7KrOCD32+VY0hfBL1oRhSDZ732mMo+Bm7l3f7EKpW/629HVdP//zfxJfb21qzXiYgK2Q2MktTlwdINz6emd+VCqT0QLTqlzs3terzWyNzVm9q75P6Lrq4EC/FvX4xckF/VO9NdW28fMhPv7WEkJ2zL0E6+oGh7inhx0sHymS0/MkSURQ1lU4lvYqq8lwqmfS4zW0DJJ1O/Av5AUqgIPk+SiOB/MDIJAJpLaV4OVtAV3m+4AGk70l6+IKaBAmji/oBndHpVjYJTIwK0XUSDBDC6jOb2aS04E5Yb+YZcEdaEN3IttOcPXNHAHAJ3aEFLCGephFtK7mk+b4j8yU8NPmfvvNjF5x4nnlH3KLzd7p1Oju7z9vtZm526w44Zze8ddu5jrPvK/dpEd797LOh/t4ymZy7oN/FvP56NOPdeNO9363jnzSyWVlIhrdM7bvifuLY3J1taWnLCNc1/vbk5U815Bvm8I4jP6O0LjaG6PtaSBfKwDSH0M+ZS4DWuWM0LhEKGhhoGnqZef0m7XU1OeKHQHt5I4z7KUf0G6IERSBY629LXqhj3DtLgPDPHMG0aE2eokTkQJ+Gr0sZfry2a3MX6TJcUq1Lt9FLa7MMpY+ZV5qy374Krhwxv/vHaD/cYZuRxEGTG4N4MjgdJNXgeJBsDj4QJEHmJjpQ5DzOHNsCd/H/AQbO4FEKZW5kc3RyZWFtCmVuZG9iago5IDAgb2JqCjw8L1R5cGUgL0ZvbnREZXNjcmlwdG9yCi9DYXBIZWlnaHQgMjQ5Ci9Bc2NlbnQgNjc3Ci9EZXNjZW50IC0yMTUKL0l0YWxpY0FuZ2xlIDAKL1N0ZW1WIDgwCi9Gb250RmlsZTIgOCAwIFIKL0ZvbnRCQm94IFstNTU4IC0zMjcgMjAwMCAxMDU1XQovRm9udE5hbWUgL05XTVZITCtUaW1lc05ld1JvbWFuUFMtQm9sZE1UCi9GbGFncyAyNjIxNzYKPj4KZW5kb2JqCjEwIDAgb2JqCjw8L1R5cGUgL0ZvbnQKL0RXIDEwMDAKL0NJRFN5c3RlbUluZm8gPDwvU3VwcGxlbWVudCAwCi9PcmRlcmluZyAoSWRlbnRpdHkpCi9SZWdpc3RyeSAoQWRvYmUpCj4+Ci9Gb250RGVzY3JpcHRvciA5IDAgUgovQ0lEVG9HSURNYXAgL0lkZW50aXR5Ci9CYXNlRm9udCAvTldNVkhMK1RpbWVzTmV3Um9tYW5QUy1Cb2xkTVQKL1N1YnR5cGUgL0NJREZvbnRUeXBlMgovVyBbMyBbMjUwXSA1IFs1NTVdIDE1IFsyNTAgMzMzIDI1MCAyNzcgNTAwIDUwMCA1MDAgNTAwIDUwMCA1MDBdIDI2IFs1MDAgNTAwIDUwMCAzMzNdIDM2IFs3MjJdIDM4IFs3MjIgNzIyIDY2Nl0gNDIgWzc3NyA3NzcgMzg5XSA0NiBbNzc3IDY2Nl0gNDkgWzcyMiA3NzcgNjEwXSA1MyBbNzIyIDU1NiA2NjZdIDU4IFsxMDAwXSA2MSBbNjY2XSA2OCBbNTAwIDU1NiA0NDMgNTU2IDQ0M10gNzUgWzU1NiAyNzcgMzMzIDU1NiAyNzddIDgxIFs1NTYgNTAwIDU1Nl0gODUgWzQ0MyAzODkgMzMzXSA5MCBbNzIyXSA5MiBbNTAwIDQ0M10gMTIxIFs1MDBdIDIwNyBbNzc3XSAyMjQgWzY2Nl0gMjYwIFs3MjJdIDI4NSBbNTU2IDM4OV0gMjk4IFs0NDNdXQo+PgplbmRvYmoKMTEgMCBvYmoKPDwvTGVuZ3RoIDU0OAovRmlsdGVyIC9GbGF0ZURlY29kZQo+PgpzdHJlYW0KeJxdlM2Om0AQhO88xRw3hxXM/65ktRRtLj7kR3HyADAzWEhrQBgf/PaBLqdXyYFPooahq2ug67fjl+M4rKr+sUzpVFbVD2NeynW6LamorpyHsdJG5SGtjztmurRzVW+bT/frWi7HsZ+qw0HVP7fF67rc1dPnPHXlU1V/X3JZhvGsnn6/nbb7022e38uljKtqKiKVS7+96Gs7f2svRdW87fmYt/VhvT9vez6e+HWfizJ8r2EmTblc5zaVpR3PpTo0TWPp0GjTUlXG/N+yf8Wurv/38QdNQyx5EhoDqSehSSzphoQmQ9IkNAWSIaHpIXEt0KKidiS0GhKXBy1M6EBCayFFEloH6YWE1kNqSWgjpI6E9gVSIqF9hZRJaFuWDJsEHawadgQ6+DLsCHTwZdgR6ODLsCPQwZdhR6CDL5NI6OBrC1foOkg9CR1OyGoSOhyHNSR0OA5rSehxHNaT0CN7i9SZHj3aSEKPHm1LQo+GLMJjekToHAkDInSehAEVHfJkBlR0kYQBFd0LCQNSdR0JAyJ0iYQBETp2BIaHr0LCgFRdT8KAVD3nCQak6g0JA1L1loQRqXrkyYzo0SNPZkSPPpIwokffkjAiVc+tgBENeUTMjGhoWxH2eH3iVsAMqfAvzNT4onWzn80Hd4l/ggdxjloXEnrOS/O3/GBMPIL+zpp9Gu0zUyZdui3LNuR4sPJ02+faMBaZvfM077vUdlV/AJwsW7AKZW5kc3RyZWFtCmVuZG9iagoyIDAgb2JqCjw8L1R5cGUgL0ZvbnQKL1N1YnR5cGUgL1R5cGUwCi9EZXNjZW5kYW50Rm9udHMgWzEwIDAgUl0KL1RvVW5pY29kZSAxMSAwIFIKL0Jhc2VGb250IC9OV01WSEwrVGltZXNOZXdSb21hblBTLUJvbGRNVAovRW5jb2RpbmcgL0lkZW50aXR5LUgKPj4KZW5kb2JqCjE0IDAgb2JqClsvQ2FsUkdCCjw8Ci9XaGl0ZVBvaW50IFswLjk1MDUgMSAxLjA4OTFdCi9HYW1tYSBbMi4xOTkyIDIuMTk5MiAyLjE5OTJdCi9NYXRyaXgKWzAuNTc2NyAwLjI5NzMgMC4wMjcKMC4xODU2IDAuNjI3NCAwLjA3MDcKMC4xODgyIDAuMDc1MyAwLjk5MTNdCj4+IF0KZW5kb2JqCjEzIDAgb2JqCjw8L1R5cGUgL0V4dEdTdGF0ZQovY2EgMC4wMDQgCj4+CmVuZG9iagoxNSAwIG9iago8PC9MZW5ndGggMTA3OQovRmlsdGVyIC9GbGF0ZURlY29kZQo+PgpzdHJlYW0KeJyVV11vE0cUHYvmZV+AhpCKlmofovLRaDJfdz6Q8lKFEEGAkhiCqPOGqISal75U/Mr+BX4KZzfr9c61Ga9lxbFHZ8+ce8+5O+s/ptXBsalTPf1Uqbp5/ft3pdsPurZOKu3rEJMMztTTq+qhsIKEeVRPP/coskDZHHUjR5A0niH2hRYe74/F7yJk6GRlCKHEl7xUhiE8lP0vvgop9jKstpBni/K0hb7IIIc5xGlAfJHFWUmWQQzqI7GHWvfFb+IQ3/Xkh+wyo6HPqRKz0dCXFGd+gIqNeALm2UN8OcSHPXGArczkPjbbZxvNzQxeEqVrkiPxWtzOUCjCeIbK1TiSxHn2wHMiXoqjDOlhk4olLh+lIYbYgpVn4DsVz6DvrXgjzia/imN838r7bUgGr0r02kTUzCB38LonfhQ7+P8LD4t3yxfcZBCPDpW3RVnEWbbxuoUtd8V2Dk5uXZc08r7Upidtc96JS/z/wBpvrEdQUqkMY6NMKZW2NU3mOUsTZCvu50BqcklFLq+xHYP8BA92xF00Zhfv99Ccn/PEehlJ1d41E2zm+xsRmGveAGdzHMsZSa0YYpclP+h1LMEts5D4MnmMjJ4iobkJWpFUKZQItQrSOgZ5hfQ3tl6IL+IDu09FwH2R0SlJiUGeQt1rxOQNeM8QltkMYl/k18UotVZF6qSk9QxyyiBwwusyC6zQDHIOWc2wX7TFrxRonJfeFO3BKeMj1R59tzgdurgcoZV8PpxWUhuVQ7eRv9vdO0+iwyh4fgG7LUJYDAxyi0EQaB/KLAntYZC7naadgbKDY4v4NKc2K59gk0mrGwSPOSJXiCklzTmuX3xWSJscR22rT8Sf+VGdpEFXCqq00jjvGUT2QzCbsRlQ7VQVCJ3HGWaYujvtDfhm874qNBSiDMmVujZErO5axlHo2hD33ULRuGhTSZhWRloOae5EJ2C8aOe9GabTyRam6SS/FEWlpucFdlTlPGN/D6UYzneY06N2K3buwEpHRVqjrEyO0Z7jJLvspr69FbzPDQ2docNrSmb6AON9ycwhYrWZGUfBzCFurZnfF9abOYSstRE3lEDFgjVuKIZDlmxktHjONLZMmzC01uWQNTYaPIknG0u0/egOIXx0n04rUz/H3+fuF8vZMxD8V/11ic8fK/wcsdT42DyOx/qqsnh88nGx8k91PoaAYJpDEhYE85V1BDaFbMOryuE3jaIwWsKcYaFhzjBWAzXPREMJ3cImNeiYt/F6ZSxDo5jIsy40K6NrWG2E2tAItdRG2lhCDDLhWB5I6FZGS+gZegkjGeYS8OTsdBpK6FbGEuC3TcIzzICgWxldQy+hr2GkhJ6h19AzjNTA4rvI8wY+6CwK/cpIhuv0EgsTbeJkLnlRwwZd1FmW+pWRDFzxoIaxYcolL2rYIEs6y1K/MpKBKx7UMGD4BoJ1at4KZW5kc3RyZWFtCmVuZG9iagoxNiAwIG9iago8PC9UeXBlIC9QYWdlCi9Db250ZW50cyAxNSAwIFIKL1BhcmVudCAxMiAwIFIKL1Jlc291cmNlcyA8PAovUHJvY1NldCBbL1BERiAvVGV4dCAvSW1hZ2VCIC9JbWFnZUMgL0ltYWdlSV0KL0ZvbnQgPDwKL0YzIDMgMCBSCi9GMiAyIDAgUgo+PgovU2hhZGluZyA8PAo+PgovQ29sb3JTcGFjZSAKPDwgCi9Hb29kUkdCIDE0IDAgUgo+PgovWE9iamVjdCA8PAo+Pgo+PgovTWVkaWFCb3ggWzAgMCA1OTUuIDg0Mi5dCj4+CmVuZG9iagoxMiAwIG9iago8PC9UeXBlIC9QYWdlcwovQ291bnQgMQovS2lkcyBbIDE2IDAgUl0KPj4KZW5kb2JqCjE3IDAgb2JqCjw8L1R5cGUgL0NhdGFsb2cKL1BhZ2VzIDEyIDAgUgo+PgplbmRvYmoKeHJlZgowIDE4CjAwMDAwMDAwMDAgNjU1MzUgZiAKMDAwMDAwMDAxNSAwMDAwMCBuIAowMDAwMDYwOTc0IDAwMDAwIG4gCjAwMDAwMjg4ODQgMDAwMDAgbiAKMDAwMDAwMDIwOCAwMDAwMCBuIAowMDAwMDI3ODQyIDAwMDAwIG4gCjAwMDAwMjgwNDUgMDAwMDAgbiAKMDAwMDAyODQ0MiAwMDAwMCBuIAowMDAwMDI5MDMyIDAwMDAwIG4gCjAwMDAwNTk1NTIgMDAwMDAgbiAKMDAwMDA1OTc2NCAwMDAwMCBuIAowMDAwMDYwMzU0IDAwMDAwIG4gCjAwMDAwNjI3NDcgMDAwMDAgbiAKMDAwMDA2MTI5NCAwMDAwMCBuIAowMDAwMDYxMTI5IDAwMDAwIG4gCjAwMDAwNjEzNDMgMDAwMDAgbiAKMDAwMDA2MjQ5NSAwMDAwMCBuIAowMDAwMDYyODA2IDAwMDAwIG4gCnRyYWlsZXIKPDwvUm9vdCAxNyAwIFIKL1NpemUgMTgKL0luZm8gMSAwIFIKPj4Kc3RhcnR4cmVmCjYyODU2CiUlRU9GCg==</ns5:Zawartosc>
      </ns5:Plik>
    </DodatkoweInformacjeIObjasnienia>
    <InformacjaDodatkowaDotyczacaPodatkuDochodowego>
      <ns5:P_ID_1>
        <ns5:RB>0</ns5:RB>
        <ns5:RP>0</ns5:RP>
      </ns5:P_ID_1>
      <ns5:P_ID_2>
        <ns5:Kwota>
          <ns5:RB>
            <ns5:KwotaA>0</ns5:KwotaA>
          </ns5:RB>
          <ns5:RP>
            <ns5:KwotaA>0</ns5:KwotaA>
          </ns5:RP>
        </ns5:Kwota>
        <ns5:Pozostale>
          <ns5:RB>
            <ns5:KwotaA>0</ns5:KwotaA>
          </ns5:RB>
          <ns5:RP>
            <ns5:KwotaA>0</ns5:KwotaA>
          </ns5:RP>
        </ns5:Pozostale>
      </ns5:P_ID_2>
      <ns5:P_ID_3>
        <ns5:Kwota>
          <ns5:RB>
            <ns5:KwotaA>0</ns5:KwotaA>
          </ns5:RB>
          <ns5:RP>
            <ns5:KwotaA>0</ns5:KwotaA>
          </ns5:RP>
        </ns5:Kwota>
        <ns5:Pozostale>
          <ns5:RB>
            <ns5:KwotaA>0</ns5:KwotaA>
          </ns5:RB>
          <ns5:RP>
            <ns5:KwotaA>0</ns5:KwotaA>
          </ns5:RP>
        </ns5:Pozostale>
      </ns5:P_ID_3>
      <ns5:P_ID_4>
        <ns5:Kwota>
          <ns5:RB>
            <ns5:KwotaA>0</ns5:KwotaA>
          </ns5:RB>
          <ns5:RP>
            <ns5:KwotaA>0</ns5:KwotaA>
          </ns5:RP>
        </ns5:Kwota>
        <ns5:Pozostale>
          <ns5:RB>
            <ns5:KwotaA>0</ns5:KwotaA>
          </ns5:RB>
          <ns5:RP>
            <ns5:KwotaA>0</ns5:KwotaA>
          </ns5:RP>
        </ns5:Pozostale>
      </ns5:P_ID_4>
      <ns5:P_ID_5>
        <ns5:Kwota>
          <ns5:RB>
            <ns5:KwotaA>0</ns5:KwotaA>
          </ns5:RB>
          <ns5:RP>
            <ns5:KwotaA>0</ns5:KwotaA>
          </ns5:RP>
        </ns5:Kwota>
        <ns5:Pozostale>
          <ns5:RB>
            <ns5:KwotaA>0</ns5:KwotaA>
          </ns5:RB>
          <ns5:RP>
            <ns5:KwotaA>0</ns5:KwotaA>
          </ns5:RP>
        </ns5:Pozostale>
      </ns5:P_ID_5>
      <ns5:P_ID_6>
        <ns5:Kwota>
          <ns5:RB>
            <ns5:KwotaA>0</ns5:KwotaA>
          </ns5:RB>
          <ns5:RP>
            <ns5:KwotaA>0</ns5:KwotaA>
          </ns5:RP>
        </ns5:Kwota>
        <ns5:Pozostale>
          <ns5:RB>
            <ns5:KwotaA>0</ns5:KwotaA>
          </ns5:RB>
          <ns5:RP>
            <ns5:KwotaA>0</ns5:KwotaA>
          </ns5:RP>
        </ns5:Pozostale>
      </ns5:P_ID_6>
      <ns5:P_ID_7>
        <ns5:Kwota>
          <ns5:RB>
            <ns5:KwotaA>0</ns5:KwotaA>
          </ns5:RB>
          <ns5:RP>
            <ns5:KwotaA>0</ns5:KwotaA>
          </ns5:RP>
        </ns5:Kwota>
        <ns5:Pozostale>
          <ns5:RB>
            <ns5:KwotaA>0</ns5:KwotaA>
          </ns5:RB>
          <ns5:RP>
            <ns5:KwotaA>0</ns5:KwotaA>
          </ns5:RP>
        </ns5:Pozostale>
      </ns5:P_ID_7>
      <ns5:P_ID_8>
        <ns5:Kwota>
          <ns5:RB>
            <ns5:KwotaA>0</ns5:KwotaA>
          </ns5:RB>
          <ns5:RP>
            <ns5:KwotaA>0</ns5:KwotaA>
          </ns5:RP>
        </ns5:Kwota>
      </ns5:P_ID_8>
      <ns5:P_ID_9>
        <ns5:Kwota>
          <ns5:RB>
            <ns5:KwotaA>0</ns5:KwotaA>
          </ns5:RB>
          <ns5:RP>
            <ns5:KwotaA>0</ns5:KwotaA>
          </ns5:RP>
        </ns5:Kwota>
        <ns5:Pozostale>
          <ns5:RB>
            <ns5:KwotaA>0</ns5:KwotaA>
          </ns5:RB>
          <ns5:RP>
            <ns5:KwotaA>0</ns5:KwotaA>
          </ns5:RP>
        </ns5:Pozostale>
      </ns5:P_ID_9>
      <ns5:P_ID_10>
        <ns5:RB>0</ns5:RB>
        <ns5:RP>0</ns5:RP>
      </ns5:P_ID_10>
      <ns5:P_ID_11>
        <ns5:RB>0</ns5:RB>
        <ns5:RP>0</ns5:RP>
      </ns5:P_ID_11>
    </InformacjaDodatkowaDotyczacaPodatkuDochodowego>
  </DodatkoweInformacjeIObjasnieniaJednostkaMala>
</JednostkaMala>";
        }

        #endregion


        #region Help funcs

        private XSDElementDocumentationReader CreateDefaultReader()
        {
            xsdSource = XSDElementDocumentationReader_Tests.getXSDSource_JednostkaMalaWZlotych();
            stream = XSDElementDocumentationReader_Tests.String2MemoryStream(xsdSource);
            reader = new XSDElementDocumentationReader(stream, new XSDElementDocumentationCompositeFactory());

            return reader;
        }

        #endregion
    }
}
