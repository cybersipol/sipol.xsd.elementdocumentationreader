﻿using Sipol.Test.Automation;
using System;
using System.Reflection;

namespace Sipol.XSD.ElementDocumentationReader.Unit.Tests
{
    public class AutomTestsRunner
    {
        static void Main(string[] args)
        {
            try
            {
                AutomationTests.RunTestsFromNamespace(Assembly.GetExecutingAssembly(), "Sipol.XSD.ElementDocumentationReader.Unit.Tests.Automation_Tests");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: {0}", ex.Message);
            }

            Console.WriteLine(new String('-', 75));
            Console.WriteLine("Press eny key to exit.");
            Console.ReadKey();
            
        }
    }
}
