﻿using NUnit.Framework;
using Sipol.XSD.ElementDocumentationReader;
using Sipol.XSD.ElementDocumentationReader.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using System.Xml;

namespace Sipol.XSD.ElementDocumentationReader.Tests
{
    [TestFixture()]
    public class XsdXmlDocumentator_Tests
    {
        private XsdXmlDocumentator sut;
        private IXSDElementDocumentationComposite XsdElementDocumentation = null;
        private IList<IXSDElementDocumentationComposite> childs = null;


        [SetUp]
        public void Init()
        {
            sut = null;
            XsdElementDocumentation = null;
        }


        #region ctor Tests

        [Test()]
        public void ctor_NullXsdDocRootElement_ThrowException()
        {
            Assert.That(() =>
                            {
                                object result = new XsdXmlDocumentator(null);
                            },
                            Throws.ArgumentNullException
            );

        }

        [Test()]
        public void ctor()
        {
            XsdElementDocumentation = CreateXsdDocMock();
            object result = new XsdXmlDocumentator(XsdElementDocumentation);

            Assert.That(result,
                            Is.Not.Null &
                            Is.InstanceOf<XsdXmlDocumentator>()
                            );
        }


        //[Test()]
        public void ctor_Scenario_Expect()
        {
            Assert.Fail("Finish test");
        }

        #endregion


        #region CreateXmlDocument Tests

        [Test()]
        public void CreateXmlDocument_RootElementToXmlStringCallCheck_ToXmlStringIsCalled()
        {
            XsdElementDocumentation = CreateXsdDocMock();
            XsdElementDocumentation.ToXmlString().Returns("<root/>");

            sut = CreateSUT(XsdElementDocumentation);

            XmlDocument result = sut.CreateXmlDocument();

            XsdElementDocumentation.Received().ToXmlString();
        }


        [Test()]
        public void CreateXmlDocument_RootElementToXmlStringWrongXml_ThrowExeption()
        {
            XsdElementDocumentation = CreateXsdDocMock();
            XsdElementDocumentation.ToXmlString().Returns("<root><a><b></root>");

            sut = CreateSUT(XsdElementDocumentation);

            Assert.That(() =>
                        {
                            XmlDocument result = sut.CreateXmlDocument();
                        },
                        Throws.InstanceOf<XmlException>()
            );

            
        }

        [Test()]
        public void CreateXmlDocument_RootElementToXmlStringEmptyXmlReturn_EmptyXmlDocument()
        {
            XsdElementDocumentation = CreateXsdDocMock();
            XsdElementDocumentation.ToXmlString().Returns(String.Empty);

            sut = CreateSUT(XsdElementDocumentation);

            XmlDocument result = sut.CreateXmlDocument();

            Assert.That(result,
                        Is.Not.Null &
                        Has.Property("DocumentElement").Null
                        );
        }

        [Test()]
        public void CreateXmlDocument_OlnyRootElement_XmlDocumentWithOlnyRootElement()
        {
            XsdElementDocumentation = CreateXsdDocMock();
            XsdElementDocumentation.ToXmlString().Returns("<root/>");

            sut = CreateSUT(XsdElementDocumentation);

            XmlDocument result = sut.CreateXmlDocument();

            Assert.That(result.DocumentElement,
                        Is.Not.Null &
                        Is.InstanceOf<XmlElement>() &
                        Has.Property("LocalName").EqualTo("root")
                        );
        }


        [Test()]
        public void CreateXmlDocument_TwoXsdElement_XmlDocumentWithTwoElement()
        {
            childs = CreateChildElementMock();
            XsdElementDocumentation = CreateXsdDocMock();
            XsdElementDocumentation.ToXmlString().Returns("<root><child/></root>");            


            sut = CreateSUT(XsdElementDocumentation);

            XmlDocument result = sut.CreateXmlDocument();

            Assert.That(result.DocumentElement,
                        Is.Not.Null &
                        Is.InstanceOf<XmlElement>() &
                        Has.Property("LocalName").EqualTo("root")
                        );

            Assert.That(result.DocumentElement.ChildNodes[0],
                    Is.Not.Null &
                    Is.InstanceOf<XmlElement>() &
                    Has.Property("LocalName").EqualTo("child")
            );

        }



        [Test()]
        public void CreateXmlDocument_byDefault_NotNullXmlDocumentReturn()
        {
            XsdElementDocumentation = CreateXsdDocMock();
            XsdElementDocumentation.ToXmlString().Returns("<root><child/></root>");
            sut = CreateSUT(XsdElementDocumentation);

            object result = sut.CreateXmlDocument();

            Assert.That(result,
                        Is.Not.Null &
                        Is.InstanceOf<XmlDocument>()
                        );
        }


        //[Test()]
        public void CreateXmlDocument_Scenario_Expect()
        {
            XmlDocument result = sut.CreateXmlDocument();
            Assert.Fail("Finish test");
        }

        #endregion


        #region Help funcs

        private IXSDElementDocumentationComposite CreateXsdDocMock()
        {
            XsdElementDocumentation = Substitute.For<IXSDElementDocumentationComposite>();
            return XsdElementDocumentation;
        }

        private XsdXmlDocumentator CreateSUT(IXSDElementDocumentationComposite xsdDocRootElement)
        {
            sut = new XsdXmlDocumentator(xsdDocRootElement);
            return sut;
        }

        private XsdXmlDocumentator CreateSUT() => CreateSUT(XsdElementDocumentation);


        private IList<IXSDElementDocumentationComposite> CreateChildElementMock()
        {
            childs = Substitute.For<IList<IXSDElementDocumentationComposite>>();
            return childs;
        }
        #endregion
    }
}