﻿using NUnit.Framework;
using Sipol.XSD.ElementDocumentationReader.Interfaces;
using System;
using System.IO;
using System.Xml;

namespace Sipol.XSD.ElementDocumentationReader.Tests
{
    [TestFixture()]
    public class XsdElementToXmlElementConnectorFactory_Tests
    {
        private XsdDocElementToXmlElementConnectorFactory sut = null;

        private IXSDElementDocumentationReader reader = null;
        private Stream stream = null;
        private IXSDElementDocumentationCompositeFactory xsdDocElemFactory = null;

        [Test()]
        public void ctor()
        {
            stream = XSDElementDocumentationReader_Tests.String2MemoryStream(XSDElementDocumentationReader_Tests.getXSDSource_SimpleXSD());
            xsdDocElemFactory = new XSDElementDocumentationCompositeFactory();
            reader = new XSDElementDocumentationReader(stream, xsdDocElemFactory);

            object result = new XsdDocElementToXmlElementConnectorFactory(reader);

            Assert.That(result, Is.Not.Null & Is.InstanceOf<XsdDocElementToXmlElementConnectorFactory>());
        }

        [Test()]
        public void ctor_NullXsdDocReader_ThrowException()
        {
            Assert.That(() => { sut = CreateSUT(null); }, Throws.InstanceOf<ArgumentNullException>());
        }

        [Test()]
        public void Create_NullXmlElement_ThrowExcpetion()
        {
            sut = CreateSUT();

            Assert.That(() => { sut.Create(null); }, Throws.InstanceOf<ArgumentNullException>());

        }

        [Test()]
        public void Create_byDefault_ReturnConnector()
        {
            sut = CreateSUT();
            XmlElement xmlElem = null;
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(getSourceXml_SimpleXml());

            xmlElem = (XmlElement) doc.DocumentElement.ChildNodes[2].ChildNodes[4];

            object result = sut.Create(xmlElem);

            Assert.That(result, Is.Not.Null & Is.InstanceOf<XsdDocElementToXmlElementConnector>());

            XsdDocElementToXmlElementConnector resultElem = (XsdDocElementToXmlElementConnector)result;

            Assert.That(resultElem.xsdDocElement, Is.Not.Null & Is.InstanceOf<IXSDElementDocumentationComposite>());
            Assert.That(resultElem.xmlElement, Is.Not.Null & Is.InstanceOf<XmlElement>());
        }

        [Test()]
        public void Create_NoXsdDocElement_ReturnEmptyConnector()
        {
            sut = CreateSUT();
            XmlElement xmlElem = null;
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(getSourceXml_SimpleXml());

            xmlElem = (XmlElement)doc.DocumentElement.ChildNodes[0].ChildNodes[0];

            object result = sut.Create(xmlElem);
            Assert.That(result, Is.Not.Null & Is.InstanceOf<XsdDocElementToXmlElementEmptyConnector>());
        }
        



        #region Help function

        private XsdDocElementToXmlElementConnectorFactory CreateSUT(IXSDElementDocumentationReader reader)
        {
            sut = new XsdDocElementToXmlElementConnectorFactory(reader);
            return sut;
        }

        private XsdDocElementToXmlElementConnectorFactory CreateSUT()
        {
            stream = XSDElementDocumentationReader_Tests.String2MemoryStream(XSDElementDocumentationReader_Tests.getXSDSource_SimpleXSD());
            xsdDocElemFactory = new XSDElementDocumentationCompositeFactory();
            reader = new XSDElementDocumentationReader(stream, xsdDocElemFactory);

            return CreateSUT(reader);
        }

        internal static string getSourceXml_SimpleXml()
        {
            return @"<?xml version=""1.0"" encoding=""UTF-8""?>
<shiporder orderid=""889923""
xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""
xsi:noNamespaceSchemaLocation=""shiporder.xsd"">
  <firstElement>
    <name>Ola Nordmann</name>
    <address>Langgt 23</address>
    <city>4000 Stavanger</city>
    <country>Norway</country>
  </firstElement>
  <orderperson>John Smith</orderperson>
  <item>
    <title>Empire Burlesque</title>
    <note>Special Edition</note>
    <quantity>1</quantity>
    <price>10.90</price>
    <contenerS>80.0</contenerS>
  </item>
  <item>
    <title>Hide your heart</title>
    <quantity>1</quantity>
    <price>9.90</price>
    <contenerM>80.0</contenerM>
  </item>
</shiporder>".Replace("firstElement",XSDElementDocumentationReader_Tests.xsdSimpleFirstElementName);
        }

        #endregion
    }
}